package com.noto.gappss.baseClasses;

public interface Constants {
	// Request Code
	public static final int requestCode = 101;
	public static final int requestCodeYourChannelDetail = 102;
	public static final int login = 3001;
	public static final int forgetPass = 3002;
	public static final int signup = 3003;
	public static final int getCat = 3004;
	public static final int setCat = 3005;
	public static final int getAppVisibility = 3006;
	public static final int getFriendAppVisibility = 3007;
	public static final int setAppVisibility = 3008;
	public static final int getChannelDetail = 3009;
	public static final int joinChannel = 3010;
	public static final int leaveChannel = 3011;
	public static final int getContactList = 3012;
	public static final int sendInvitation = 3013;
	public static final int acceptInvitation = 3040;
	public static final int denyInvitation = 3041;
	public static final int channelStatistics = 3014;
	public static final int getUserChannelDetail = 3015;
	public static final int createChannel = 3016;
	public static final int setChannelSettings = 3017;
	public static final int getChannelSettings = 3018;
	public static final int upcomingEvent = 3019;
	public static final int getSelCatChannels = 3020;
	public static final int createPost = 3021;
	public static final int deleteChannel = 3022;
	public static final int getFollowedChannels = 3023;
	public static final int getYourChannels = 3024;
	public static final int getExploreChannels = 3025;
	public static final int getPostsByChannel = 3026;
	public static final int postDetail = 3027;
	public static final int createComment = 3028;
	
	public static final int getFavorate = 3029;
	public static final int getChannelFeeds = 3030;
	public static final int getFriendsFeeds = 3031;
	public static final int getAllFeeds = 3032;
	public static final int getYourCommunityApps = 3033;
	public static final int getGappssCommunityApps = 3034;
	public static final int getFriendsList = 3035;
	public static final int getDevicesDetails = 3036;
	public static final int getFriendAllChannel = 3037;
	public static final int getFriendAllApps = 3038;
	
	public static final int sendChannelInvitation = 3039;
	public static final int acceptChannelInvitation = 3042;
	
	public static final int getShareApps = 3043;
	public static final int getFriendsAllPosts = 3044;
	public static final int getRequests = 3045;
	public static final int getAllInbox = 3046;
	public static final int denyInvitationInbox = 3047;
	public static final int acceptInvitationInbox = 3048;
	public static final int sendInvitationAlertDialog = 3049;
	
	public final static int login_type_normal = 1;
	public final static int login_type_fb = 2;
	
	public static final String bundleArg = "bundle";
	public static final String title = "title";	
	public static final String password = "password";
	public static final String name = "name";
	public static final String userImageURL = "imgURL";
	public static final String response = "response";
	public static final String list = "list";
	public static final String className = "className";
	public final static String statusOk = "accepted";	
	public final static String statusNotOk = "rejected";
	public final static String success = "Success";
	public final static String dataToFollow = "dataflow";
	public static final String msg = "msg";
	public static final String message = "message";
	public static final String status = "status";	
	public static String deviceId = "deviceId";
	public static final String map = "map";
	public static final String set = "set";
	public static final String jObject = "jObject";
	public static final String guest = "guest";
	public static final String position = "position";	
	public static final String url = "url";
	public static final String images = "Images";
	public static final String FRAGMENT_TAG = "frag";
	public static final String object = "object";
	public static final String countryCode = "countryCode";
	public static final String flag = "flag";
	public static final String drawerTab = "drawerTab";


	//Shardeprefs
	public static final String VERIFY = "verify";
	public static final String VERIFY_NO = "verify_no";
	public static final String LOGIN = "login";
	public static final String USER_ID = "userId";
	public static final String friendId = "friendId";
	public static final String EMAIL = "email";
	public static final String USER_NAME = "userName";
	public static final String PH_NUMBER = "phone_no";
	public static final String userPhoneNo = "userPhoneNo";
	public static final String userImage = "userImage";
	public static final String COUNTRY_CODE = "country_code";
	public static final String GENDER = "gender";
	public static final String MALE = "male";
	public static final String FEMALE = "female";
	public static final String LOGIN_TYPE = "login_type";
	public static final String DOB = "dob";
	public static final String API_KEY_VALUE = "EFH127FL";
	public static final String API_KEY = "apiKey";
	public static final String NEW_APPS = "newApps";
	public static final int INT = 1;
	public static final int FLOW = 1;
	public static final int NOT_FLOW = 0;


	public static final int STATUS = 1;
	public static final int NOT_STATUS = 0;
	public static final String yourChannels = "yourChannels";
	public static final String followedChannels = "followedChannels";
	public static final String explorer = "explorer";
	public static final String type = "type";
	public static final String catId = "catId";
	public static final String catName = "catName";
	public static final String channelId = "channelId";
	public static final String postId = "postId";
	public static final String parentId = "parentId";
	public static final String channel = "channel";
	public static final String channelName = "channelName";
	public static final String channelDescription = "channelDescription";
	public static final String channelType = "channelType";
	public static final String channelMode = "channelMode";
	public static final String channelImage = "channelImage";
	public static final String channelStatus = "channelStatus";
	public static final String Post = "Post";
	public static final String description = "description";
	public static final String image = "image";
	public static final String Comment = "Comment";
	public static final String comment = "comment";
	public static final String username = "username";
	public static final String main_comment = "main_comment";
	public static final String childComments = "childComments";
	public static final String packageName = "packageName";
	public static final String appStatus = "appStatus";

	// parameters
	public final static String p_device_brand = "deviceBrand";
	public final static String p_device_type = "deviceType";
	public final static String p_device_os = "deviceOs";

	public final static String p_first_name = "first_name";
	public final static String p_last_name = "last_name";

	public final static String p_fb_id = "fb_id";
	public final static String p_mobile_no = "mobileNo";

	public final static String p_user = "user";
	public final static String p_categories = "categories";
	public final static String p_message = "message";
	public final static String p_app_list = "appList";
	public final static String p_appName = "appName";
	public final static String p_appPack = "appPack";
	public final static String p_app_url = "app_url";

	public final static String p_friendName = "friendName";
	public final static String p_friendId = "friendId";
	//public final static String p_channel_id = "channel_Id";
	public final static String p_contact_list = "contactList";

	public final static String p_channel_type = "channel_type";
	public final static String p_channel_mode = "channel_mode";
	public final static String p_fileToUpload = "fileToUpload";

	public final static String p_show_subscribe_feeds = "show_subscribe_feeds";
	public final static String p_allow_post = "allow_post";
	public final static String p_allow_search = "allow_search";
	public final static String p_allow_feeds = "allow_feeds";
	public final static String android = "Android";	
	public final static String p_receiver_id = "receiver_id";
	public final static String senderId = "senderId";
	public final static String requestType = "requestType";
	
	
	
	public final static String id = "id";
	public final static String fileSize = "fileSize";
	public final static String numDownloads = "numDownloads";
	public final static String datePublished = "datePublished";
	public final static String versionName = "versionName";
	public final static String operatingSystems = "operatingSystems";
	public final static String screenshots = "screenshots";
	public final static String rating = "rating";
	public final static String price = "price";
	public final static String category = "category";
	public final static String categoryName = "categoryName";
	public final static String priceCurrency = "priceCurrency";
	public final static String icon = "icon";
	public final static String count = "count";
	public final static String display = "display";
	public final static String supportEmail = "supportEmail";
	public final static String supportUrl = "supportUrl";
	public final static String inAppBilling = "inAppBilling";
	public final static String adsSupported = "adsSupported";
	public final static String value = "value";
	public final static String five = "five";
	public final static String four = "four";
	public final static String three = "three";
	public final static String two = "two";
	public final static String one = "one";
	public final static String storeCategory = "storeCategory";
	public final static String topDeveloper = "topDeveloper";
	public final static String editorsChoice = "editorsChoice";
	public final static String changelog = "changelog";
	public final static String contentRating = "contentRating";
	public final static String video = "video";
	public final static String unknown = "unknown";
	public final static String created = "created";
	public final static String modified = "modified";
	
	
	public final static String body = "body";
	public final static String author = "author";
	public final static String date = "date";
	
	public final static String postTitle = "postTitle";
	public final static String postImage = "postImage";
	public final static String postDescription = "postDescription";
	public final static String postCreatedTime = "postCreatedTime";
	//public final static String userName = "userName";
	public final static String commentTime = "commentTime";
	public final static String feeds = "feeds";
	
	public final static String package_ = "package";
	public final static String feedType = "feedType";
	public final static String YourCommunityApps = "YourCommunityApps";
	public final static String GAPPSSCommunityApps = "GAPPSSCommunityApps";
	public final static String freeApps = "freeApps";
	public final static String paidApps = "paidApps";
	public final static String topGrossingApps = "topGrossingApps";
	public final static String appId = "appId";
	public final static String appName = "appName";
	public final static String appPackageName = "appPackageName";
	public final static String appUrl = "appUrl";
	public final static String rank = "rank";
	
	public final static String brand = "brand";
	public final static String os = "os";
	public final static String userEmail = "userEmail";
	public final static String totalApps = "totalApps";
	public final static String totalCommanApps = "totalCommanApps";
	public final static String totalDiscoverApps = "totalDiscoverApps";
	public final static String user = "user";
	public final static String friendstr = "friend";
	public final static String appList = "appList";
	
	
	public final static int feedTypeLeaveChannel = 0;
	public final static int feedTypeJoinChannel = 1;
	public final static int feedTypePost = 2;
	public final static int feedTypeCreateChannel = 3;
	public final static int feedTypeFriendRequest = 4;
	public final static int feedTypeComment = 5;
	public static final String friendType = "isFriend";
	public static final String noFriendStr = "Gappss users";
	public static final String friendStr = "Friends";
	public static final String sentRequestStr = "Request sent";
	public static final String receivedReqFriendStr = "Received request";
	public final static int sentRequest = 0;
	public final static int receivedRequest = 1;
	public final static int friend = 2;
	public final static int noFriend = 3;
	public static final CharSequence allow = "Allow";
	public static final CharSequence waitingResponse = "Waiting response";
	public static final int accept = 1;
	public static final int deny = 2;
	public static final String contact = "contact";
	public static final String community = "Community";
	public static final String yourCommunity = "Your Community";
	public static final String gappssCommunity = "Gappss Community";
	public static final String free = "Free";
	public static final String paid = "Paid";
	public static final String topgrossing = "Topgrossing";
	public final static String feedsT = "Feeds";
	public static final String populorCat = "Populor Category";
	public static final String inbox = "Inbox";
	public static final String contactT = "Contact";
	public static final String aboutUs = "About Us";
	public static final String account = "Account";
	public static final String appCat = "App Categories";
	public static final String appDetail = "App Detail";
	public static final String compareProfile = "Compare Users Profile";
	public static final String friendInterestT = "Interests";
	public static final String frndActivity = "Activities";
	public static final String apps = "Apps";
	public static final String frndChannels = "Channels";
	public static final String shareApps = "Shared Apps";
	public static final String InboxSharedAppFragment = "InboxSharedAppFragment";
	public static final String InboxRequestFragment = "InboxRequestFragment";
	public static final String InboxPostFragment = "InboxPostFragment";
	public static final String Notification = "Notification";
	public static final String TearmAndConditionFragment = "TearmAndConditionFragment";
	public static final String MyAppShareActivity = "MyAppShareActivity";
	public static final String AllFeedsFragment = "AllFeedsFragment";
	public static final String FriendFeedsFragment = "FriendFeedsFragment";
	public static final String ChannelsFeedsFragment = "ChannelsFeedsFragment";
	public static final String FavoritesFeedsFragment = "FavoritesFeedsFragment";

	public static final String hot = "hot";
	public static final String rate = "rate";
	public static final String newly = "new";
	public static final String appPrice = "appPrice";
	
	public static final String TABLE_APP_DETAIL = "app_detail";
	public static final String COLUMN_ROW_ID = "rowId";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_AUTHOR = "author";
	public static final String COLUMN_SUPPORT_EMAIL = "supportEmail";
	public static final String COLUMN_SUPPORT_URL = "supportUrl";
	public static final String COLUMN_ICON = "icon";
	public static final String COLUMN_CATEGORY = "category";
	public static final String COLUMN_STORE_CATEGORY = "storeCategory";
	public static final String COLUMN_PRICE = "price";
	public static final String COLUMN_PRICE_CURRENCY = "priceCurrency";
	public static final String COLUMN_INAPP_BILLING = "inAppBilling";
	public static final String COLUMN_ADS_SUPPORTED = "adsSupported";
	public static final String COLUMN_RATING_COUNT = "count";
	public static final String COLUMN_RATING_DISPLAY = "display";
	public static final String COLUMN_RATING_VALUE = "value";
	public static final String COLUMN_RATING_FIVE = "five";
	public static final String COLUMN_RATING_FOUR = "four";
	public static final String COLUMN_RATING_THREE = "three";
	public static final String COLUMN_RATING_TWO = "two";
	public static final String COLUMN_RATING_ONE = "one";
	public static final String COLUMN_TOP_DEVELOPER = "topDeveloper";
	public static final String COLUMN_EDITORS_CHOICE = "editorsChoice";
	public static final String COLUMN_SCREENSHOTS = "screenshots";
	public static final String COLUMN_DESCRIPTION = "description";
	public static final String COLUMN_CHANGELOG = "changelog";
	public static final String COLUMN_DATE_PUBLISHED = "datePublished";
	public static final String COLUMN_FILE_SIZE = "fileSize";
	public static final String COLUMN_NUM_DOWNLOADS = "numDownloads";
	public static final String COLUMN_VERSION_NAME = "versionName";
	public static final String COLUMN_VIDEO = "video";
	public static final String COLUMN_OPERATING_SYSTEMS = "operatingSystems";
	public static final String COLUMN_CONTENT_RATING	 = "contentRating";
	public static final String COLUMN_PACKAGE	 = "packageId";
	public static final String COLUMN_UNKNOWN	 = "unknown";
	
	public static final String COLUMN_CREATED	 = "created";
	public static final String COLUMN_MODIFIED	 = "modified";
	public static final String COLUMN_STATUS	 = "status";
	public static final String COLUMN_RANK	 = "rank";
	public static final String COLUMN_APP_STATUS	 = "appStatus";
	
	
	
}
