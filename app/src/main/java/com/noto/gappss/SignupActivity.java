package com.noto.gappss;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.noto.gappss.adapter.GenderAdapter;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Gender;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.preference.MySharedPreferences;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class SignupActivity extends BaseFragmentActivity implements OnClickListener {

	private ImageView fb_signup;
	//private EditText signup_mobile_no;
	private EditText signup_email;
	private EditText signup_username;
	private EditText signup_password;
	//private EditText signup_confirm_password;
	private EditText signup_dob;
	private TextView signup_gender;
	private Button signup;
	private CallbackManager callbackManager;
	private String dob = "";
	ArrayList<Gender> arrayList;
	private int dataFlow;
	private TextView signup_term_condition; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// getSupportActionBar().hide();
		enableBackButton();
		setTitleOnAction(getResources().getString(R.string.signup_upercase));
		setContentView(R.layout.activity_signup);
		initControls(savedInstanceState);
		setValueOnUI();
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		fb_signup = (ImageView) findViewById(R.id.fb_signup);
		//signup_mobile_no = (EditText) findViewById(R.id.signup_mobile_no);
		signup_email = (EditText) findViewById(R.id.signup_email);
		signup_username = (EditText) findViewById(R.id.signup_username);
		signup_password = (EditText) findViewById(R.id.signup_password);
		//	signup_confirm_password = (EditText) findViewById(R.id.signup_confirm_password);
		signup_dob = (EditText) findViewById(R.id.signup_dob);
		signup_gender = (TextView) findViewById(R.id.signup_gender);
		signup = (Button) findViewById(R.id.signup);
		signup_term_condition = (TextView) findViewById(R.id.signup_term_condition);
		fb_signup.setOnClickListener(this);
		signup_dob.setOnClickListener(this);
		signup.setOnClickListener(this);
		signup_gender.setOnClickListener(this);
	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub
		/*signup_mobile_no.setText(MySharedPreferences.getInstance().getString(this, Constants.PH_NUMBER, ""));
		signup_mobile_no.setFocusable(false);
		signup_mobile_no.setEnabled(false);*/
		//ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,genderArray);  
		//aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
		//String[] genderArray = {"Gender", "Male", "Female"};
	/*	arrayList = new ArrayList<Gender>();
		Gender gender = new Gender();
		gender.setGender("Gender");
		arrayList.add(gender);
		Gender gender2 = new Gender();
		gender2.setGender("Male");
		arrayList.add(gender2);
		Gender gender3 = new Gender();
		gender3.setGender("Female");
		arrayList.add(gender3);
		GenderAdapter adapter = new GenderAdapter(this, arrayList);
		//Setting the ArrayAdapter data on the Spinner  
		signup_gender.setAdapter(adapter); */
		// signup_gender.setPromptId(0);
		
		
		signup_term_condition.setText(Html.fromHtml("By signing up, you agree to GAPPSS "+"<font color='#142D8A'>"+"Terms and condition of Use,Privacy Policy</font> and "+"<font color='#142D8A'>"+"Mobile Terms</font>."));
	}

	@Override
	public void onBackPressed() {
		startMyActivity(LoginSignupActivity.class, null);
		super.onBackPressed();		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.fb_signup:

			fbSignup();

			break;
		case R.id.signup_dob:

			openDateDialog();

			break;
		case R.id.signup_gender:

			alertDialogGender();

			break;
		case R.id.signup:
			//Log.i("GENDER", genderArray[signup_gender.getSelectedItemPosition()]);
			/*if (signup_mobile_no.getText().toString().trim().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_ph_username_not_null));
			}else */if (signup_username.getText().toString().trim().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_username_not_null));
			}else if (signup_email.getText().toString().trim().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_email_not_null));
			}else if (!UtilsClass.isEmailValid(signup_email.getText().toString().trim())) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_email_valid));
			}else if (signup_password.getText().toString().trim().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_password_not_null));
			}else if (signup_password.getText().toString().trim().length() < 5) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_password_minimum));
			}/*else if (signup_confirm_password.getText().toString().trim().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_confirm_pass));
			}else if (!signup_password.getText().toString().trim().equalsIgnoreCase(signup_confirm_password.getText().toString().trim())) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_pass_not_match));
			}*/else if (signup_dob.getText().toString().trim().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_birthdate_not_null));
			}else if (signup_gender.getText().toString().trim().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_gender));
			}else if (UtilsClass.isConnectingToInternet(this)) {				
				HashMap<String, Object> params = new HashMap<String, Object>();			
				params.put(Constants.password, signup_password.getText().toString().trim());
				loginCommon(params, Constants.login_type_normal, signup_username.getText().toString().trim(), signup_email.getText().toString().trim(),signup_gender.getText().toString().trim(), dob);
			}else {
				UtilsClass.plsStartInternet(this);	
			}
			break;
		}

	}


	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if(super.callBackFromApi(object, act, requstCode)){
			JSONObject jsonObject = (JSONObject) object;
			dataFlow = jsonObject.optInt(Constants.dataToFollow);
			String statusText = jsonObject.optString(Constants.p_message);
			if(dataFlow == 1){
				JSONObject response = jsonObject.optJSONObject(Constants.p_user);

				switch (requstCode) {
				case Constants.signup:
					//{"response":{"UserLastInsertId":"65","status":true,"statusMessage":"Congatualtion you have created account successfully.For more detail please check you email account."}}
					//Log.i("Login", jsonObject.toString());
					ToastCustomClass.showToast(this, statusText);
					//MySharedPreferences.getInstance().putStringKeyValueD(this, Constants.USER_ID, response.optString("UserLastInsertId"));
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.USER_ID, response.optString(Constants.USER_ID));
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.EMAIL, response.optString(Constants.EMAIL));
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.USER_NAME, response.optString(Constants.USER_NAME));
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.DOB, response.optString(Constants.DOB));
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.PH_NUMBER, response.optString(Constants.PH_NUMBER));
					MySharedPreferences.getInstance().putBooleanKeyValue(this, Constants.LOGIN, true);
					if (response.optInt(Constants.GENDER) == 1) {
						MySharedPreferences.getInstance().putStringKeyValue(this, Constants.GENDER, Constants.MALE);
					}else{
						MySharedPreferences.getInstance().putStringKeyValue(this, Constants.GENDER, Constants.FEMALE);
					}
					startMyActivity(AddCategoryActivity.class, null);
					finish();
					break;
				case Constants.forgetPass:
					//{"response":{"status":true,"statusMessage":"Please check your email for reset Password!"}}
					//Log.i("Login", jsonObject.toString());
					UtilsClass.showAlertDialog(this, statusText, "Message");
					break;
				}
			}
			else{
				UtilsClass.showAlertDialog(this, statusText, "Message");
			}
		}else if(object != null){
			JSONObject jsonObject = (JSONObject) object;
			String statusText = jsonObject.optString(Constants.p_message);
			UtilsClass.showAlertDialog(this, statusText, "Message");
		}
		return true;
	}

	public void fbSignup(){
		callbackManager = CallbackManager.Factory.create();
		// Set permissions 
		LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email","public_profile"));

		LoginManager.getInstance().registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult loginResult) {

				//System.out.println("Success");
				//Log.i("ACCESS TOKEN1", "="+loginResult.getAccessToken().getToken());

				//Log.i("ACCESS TOKEN3", "="+loginResult.getAccessToken().getPermissions());
				GraphRequest request =  GraphRequest.newMeRequest(
						loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
							@Override
							public void onCompleted(JSONObject object, GraphResponse response) {
								if (response.getError() != null) {
									// handle error
									//Log.d("ERROR","ERROR");

								} else {

									//Log.d("SUCCESS","");
									//Log.i("JSONOBJECT", object.toString());
									//Log.i("GRAPH RESPONSE", response.toString());
									//{"id":"1640502796227231","name":"Leander Daniel","first_name":"Leander","last_name":"Daniel","email":"leander.daniel1027@gmail.com","gender":"female"}


									if(object != null && object.optString("id") != null){
										//Log.d("object","object");
										HashMap<String, Object> params = new HashMap<String, Object>();
										params.put(Constants.p_fb_id, object.optString("id"));
										loginCommon(params, Constants.login_type_fb, object.optString("name"), object.optString("email"), object.optString("gender"), "");
									}
								}
							}
						});
				Bundle parameters = new Bundle();
				// parameters.putString("fields", "id,name,first_name,last_name,email,birthday,gender,link,location");
				parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday,location");
				request.setParameters(parameters);
				request.executeAsync();
			}

			@Override
			public void onCancel() {
				Log.d("oncancel","On cancel");
				LoginManager.getInstance().logOut();
			}

			@Override
			public void onError(FacebookException error) { 
				Log.d("onerror",error.toString());
				LoginManager.getInstance().logOut();
			}
		});
	}
	
	private void loginCommon(HashMap<String, Object> params, int loginType, String name, String email, String gender, String dob) {
		String[] url = {URLsClass.baseUrl+URLsClass.service_type_register};		
		params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
		String[] sptName = name.split(" ");
		params.put(Constants.p_first_name, name.split(" ")[0]);
		if(sptName.length > 1)
			params.put(Constants.p_last_name, name.split(" ")[1]);
		
		params.put(Constants.COUNTRY_CODE,  MySharedPreferences.getInstance().getString(SignupActivity.this, Constants.COUNTRY_CODE, ""));
		params.put(Constants.p_mobile_no,  MySharedPreferences.getInstance().getString(SignupActivity.this, Constants.PH_NUMBER, ""));
		params.put(Constants.EMAIL,  email);	
		params.put(Constants.DOB, dob);	
		if (gender.equalsIgnoreCase("male")) {
			params.put(Constants.GENDER,  1);
		}else if (gender.equalsIgnoreCase("female")) {
			params.put(Constants.GENDER,  2);
		}else {
			params.put(Constants.GENDER, 1);
		}
		params.put(Constants.type, loginType);
		
		params.put(Constants.deviceId, UtilsClass.getAndroidDevic(this));
		params.put(Constants.p_device_brand, UtilsClass.getDeviceModel(this));
		params.put(Constants.p_device_type, Constants.android);
		params.put(Constants.p_device_os, UtilsClass.getDeviceSDKVersion(this));

		serviceCaller(SignupActivity.this, url, params, true, false, Constants.signup, true);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//Facebook
		if(callbackManager != null)
			callbackManager.onActivityResult(requestCode, resultCode, data);  
	}

	private void openDateDialog(){	
		UtilsClass.showCurrentDatePickerDialog(this, new OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				String  day = ""+dayOfMonth;
				String  month = ""+(monthOfYear+1);
				if (dayOfMonth < 9) {
					day = "0"+dayOfMonth;
				}
				if (monthOfYear < 9) {
					month = "0"+(monthOfYear+1);
				}

				long currentDate = UtilsClass.getCurrentTimeInMili();
				long selectedDate = UtilsClass.getMilisecondsFromDate(day+"-"+month+"-"+year);
				//Log.i("DATEEEEE", currentDate+"==="+selectedDate);
				if (selectedDate >= currentDate) {
					ToastCustomClass.showToast(SignupActivity.this, getResources().getString(R.string.val_birthdate_valid));
				}else {
					signup_dob.setText(day+"-"+month+"-"+year);
					dob = year+"-"+month+"-"+day;
				}
			}
		});

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action buttons
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();			
			return true;
		}
		return true;
	}
	
	public void alertDialogGender(){
		final String[] arrayString = {"Male", "Female"};
		AlertDialog.Builder builder = new AlertDialog.Builder(
				this);
		builder.setTitle("Gender");
		

		builder.setItems(arrayString,
				new DialogInterface.OnClickListener() {
					@SuppressWarnings("deprecation")
					public void onClick(DialogInterface dialog, int position) {
						// Toast.makeText(getApplicationContext(),
						
				     // ToastCustomClass.showToast(SignupActivity.this, arrayString[position]);
                      signup_gender.setText(arrayString[position]);
					}
				});
		AlertDialog alert = builder.create();
		// alert.getWindow().setLayout(50, 50);
		alert.show();
	}

}
