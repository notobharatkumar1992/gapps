package com.noto.gappss;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.fragments.CreatePostFragment;
import com.noto.gappss.fragments.ExplorerChannelsFragment;
import com.noto.gappss.fragments.NewAppInstallFragment;
import com.noto.gappss.fragments.YourChannelDetailFragment;
import com.noto.gappss.utilities.ToastCustomClass;

public class CreatePostActivity extends BaseFragmentActivity{

			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		enableBackButton();
		setTitleOnAction(getResources().getString(R.string.create_post));
		setContentView(R.layout.activity_container);
		replaceFragement(CreatePostFragment.newInstance(getBundle()), CreatePostFragment.TAG);
	}
	
	
	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected Boolean callBackFromApi(Object object, Activity act,
			int requstCode) {
		// TODO Auto-generated method stub
		
		JSONObject jObject;
		String message ="";
		switch (requstCode){
		case Constants.createPost:
			jObject = (JSONObject) object;
			message = jObject.optString("message");
			Log.i("CREATE POST", jObject.toString());
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				
			}else {
				ToastCustomClass.showToast(this, message);
				 
				CreatePostFragment freg = (CreatePostFragment) getFragmentByTag(CreatePostFragment.TAG);
				if (freg != null) {
					freg.response();
				}
				
			}
			break;
		}
		return super.callBackFromApi(object, act, requstCode);
	}

}
