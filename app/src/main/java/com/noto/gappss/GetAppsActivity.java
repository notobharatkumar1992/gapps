package com.noto.gappss;

import android.os.Bundle;

import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.fragments.CompareProfileFragment;

public class GetAppsActivity extends BaseFragmentActivity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		setTitleOnAction(getResources().getString(R.string.get_apps));
		enableBackButton();
		replaceFragement(CompareProfileFragment.newInstance(getBundle()), CompareProfileFragment.TAG);
	}
	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub
		
	}

}
