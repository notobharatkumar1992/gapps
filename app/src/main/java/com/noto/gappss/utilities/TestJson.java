package com.noto.gappss.utilities;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.noto.gappss.baseClasses.Constants;

public class TestJson {

	public static JSONObject getJsonFormat(){
		
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put(Constants.status, 1);
			jsonObject.put(Constants.dataToFollow, 1);
			jsonObject.put(Constants.p_message, "Tesing json object");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonObject;
		
	}
	
	public static JSONObject getGappssCommunity(){
		
		JSONObject jsonObject = getJsonFormat();
		JSONArray jsonArray = new JSONArray();
		
		String[] arrayList = {"All","Games"};
		try {
			for (int i = 0; i < arrayList.length; i++) {
				JSONObject jsonObjectAll = new JSONObject();
				jsonObjectAll.put(Constants.category, arrayList[i]);
				
				JSONArray jsonArrayFree = new JSONArray();
				JSONObject jsonObjectFree = new JSONObject();
				jsonObjectFree.put(Constants.appName , "TestApps free");
				jsonObjectFree.put(Constants.description , "Test description");
				jsonObjectFree.put(Constants.appPackageName , "asdf");
				jsonArrayFree.put(0,jsonObjectFree);
				jsonObjectAll.put(Constants.freeApps, jsonArrayFree);
				
				JSONArray jsonArrayPaid = new JSONArray();
				JSONObject jsonObjectPaid = new JSONObject();
				jsonObjectPaid.put(Constants.appName , "TestApps paid");
				jsonObjectPaid.put(Constants.description , "Test description");
				jsonObjectPaid.put(Constants.appPackageName , "asdf");
				jsonArrayPaid.put(0,jsonObjectPaid);
				jsonObjectAll.put(Constants.paidApps, jsonArrayPaid);
				
				JSONArray jsonArrayGrosing = new JSONArray();
				JSONObject jsonObjectGrosing = new JSONObject();
				jsonObjectGrosing.put(Constants.appName , "TestApps grosing");
				jsonObjectGrosing.put(Constants.description , "Test description");
				jsonObjectGrosing.put(Constants.appPackageName , "asdf");
				jsonArrayGrosing.put(0,jsonObjectGrosing);
				jsonObjectAll.put(Constants.topGrossingApps, jsonArrayGrosing);
				
				jsonArray.put(i,jsonObjectAll);
			}
			jsonObject.put(Constants.YourCommunityApps, jsonArray);
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonObject;
		
	}
	

}
