package com.noto.gappss;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.fragments.FriendInterestFragment;
import com.noto.gappss.fragments.GetAppsFragment;
import com.noto.gappss.fragments.MyAppShareFragment;

public class InterestedAppsActivity extends BaseFragmentActivity {
	public static String isApps = "isApp";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		enableBackButton();
		Bundle bundle = getBundle();
		boolean isApp = bundle.getBoolean(isApps);
		if(isApp)
			replaceFragement(GetAppsFragment.newInstance(bundle),GetAppsFragment.TAG);
		else
			replaceFragement(FriendInterestFragment.newInstance(bundle), FriendInterestFragment.TAG);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
	}

	@Override
	protected void setValueOnUI() {
	}

	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment, int requstCode) {
		if (super.callBackFromApi(object, fragment, requstCode)) {
			Object res = callBackCommon(object, requstCode, fragment);
			if(res != null){
				ArrayList<ApplicationBean> list = (ArrayList<ApplicationBean>) res; 
				GetAppsFragment getAppF = (GetAppsFragment)fragment;
				getAppF.setApps(list);
			}
		}
		return true;
	}

	@Override
	protected Boolean callBackFromApi(Object object, Activity act,	int requstCode) {
		if (super.callBackFromApi(object, act, requstCode)) {
			callBackCommon(object, requstCode, act);
		}
		return true;
	}

	private Object callBackCommon(Object object, int requstCode, Object luncher) {
		String message = "";
		JSONObject jObject;		
		switch (requstCode) {
		case Constants.getFriendAllApps:
			jObject = (JSONObject) object;
			JSONArray jArr = jObject.optJSONArray(Constants.list);
			ArrayList<ApplicationBean> listApps = new ArrayList<ApplicationBean>();
			if(jArr != null){				
				ApplicationBean bean = null;
				JSONObject jBean = null;
				for (int i = 0; i < jArr.length(); i++) {
					jBean = jArr.optJSONObject(i);
					bean = new ApplicationBean();
					bean.setId(jBean.optInt(Constants.appId));
					bean.setTitle(jBean.optString(Constants.appName));
					bean.setPackageName(jBean.optString(Constants.appPackageName));
					bean.setApplogo(jBean.optString(Constants.appUrl));
					bean.setDescription(jBean.optString(Constants.description));
					bean.setInstalled(false);
					bean.setHot(jBean.optLong(Constants.hot));
					bean.setRate(jBean.optDouble(Constants.rate));
					bean.setPrice(jBean.optDouble(Constants.price));
					bean.setNewlyDate(jBean.optLong(Constants.newly));
					listApps.add(bean);
				}				
			}				
			return listApps; 
		}

		return null;
	}
	/*	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.id.shar_app, menu);

		return super.onCreateOptionsMenu(menu);
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;    
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		BaseFragment topF = getTopFragment();
		if(topF != null && !topF.onBackPressedListener()){
			removeTopFragement();
			// disableBackButton();
			invalidateOptionsMenu();
			setTitleOnAction(MyAppShareFragment.TAG);
		}
		super.onBackPressed();
	}
}
