package com.noto.gappss;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.AppCategory;
import com.noto.gappss.fragments.AddCategoryFragment;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.utilities.ToastCustomClass;

public class AddCategoryActivity extends BaseFragmentActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		addFragement(AddCategoryFragment.getInstance(), AddCategoryFragment.TAG, false);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {

	}

	@Override
	protected void setValueOnUI() {

	}

	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,	int requstCode) {
		if(super.callBackFromApi(object, fragment, requstCode)){
			commonCallBack(requstCode, object);
		}
		return true;
	}

	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if(super.callBackFromApi(object, act, requstCode)){
			commonCallBack(requstCode, object);
		}
		return true;
	}
	
	private void commonCallBack(int requstCode, Object object){
		switch (requstCode) {
		case Constants.getCat:
			JSONObject jObject = (JSONObject) object;
			//Log.i("App catagory", jObject.toString());
			if(jObject.optInt(URLsClass.status, Constants.NOT_STATUS) == Constants.STATUS){
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray("list");
					ArrayList<AppCategory> arrayList = new ArrayList<AppCategory>();
					for (int i = 0; i<jsonArray.length() ; i++) {
						AppCategory appCategory = new AppCategory();
						appCategory.setCatId(jsonArray.optJSONObject(i).optString("catId"));
						appCategory.setCatName(jsonArray.optJSONObject(i).optString("catName"));
						appCategory.setCatLogo(jsonArray.optJSONObject(i).optString("catImage"));
						appCategory.setChecked(jsonArray.optJSONObject(i).optInt("isChecked") == 1 ? true : false);
						arrayList.add(appCategory);
					}

					AddCategoryFragment freg = (AddCategoryFragment) getFragmentByTag(AddCategoryFragment.TAG);
					if(freg != null)
						freg.serviceResponse(arrayList);
				}
			}	
			break;
		case Constants.setCat:
			//{"status":1," ":"Set Successfully.","dataflow":0}
			JSONObject jsonObj = (JSONObject) object;
			//Log.i("App catagory", jsonObj.toString());
			ToastCustomClass.showToast(this, jsonObj.optString(Constants.p_message));
			AddCategoryFragment freg = (AddCategoryFragment) getFragmentByTag(AddCategoryFragment.TAG);
			if(freg != null)
				freg.serviceResponseSetCat(true);			
			break;
		}	
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.ok, menu);
		return super.onCreateOptionsMenu(menu);
	}*/
}
