package com.noto.gappss;

import android.os.Bundle;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.fragments.AppDetailFragment;
import com.noto.gappss.fragments.GappssCommunityFragment;

public class AppDetailActivity extends BaseFragmentActivity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		setTitleOnAction(getResources().getString(R.string.app_detail));
		enableBackButton();
		replaceFragement(AppDetailFragment.newInstance(getBundle()), AppDetailFragment.TAG);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub
		
	}

}
