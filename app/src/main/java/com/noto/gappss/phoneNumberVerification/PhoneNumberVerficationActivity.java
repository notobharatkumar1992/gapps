package com.noto.gappss.phoneNumberVerification;

import android.content.Context;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.noto.gappss.LoginSignupActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.SpinnerAdapter;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Country;
import com.noto.gappss.preference.MySharedPreferences;
import com.noto.gappss.utilities.UtilsClass;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class PhoneNumberVerficationActivity extends BaseFragmentActivity implements OnItemSelectedListener, OnClickListener {
    private AutoCompleteTextView search;
    private Spinner spinner;
    private TextView codes;
    private EditText et_phone;
    private TextView send;
    private LinearLayout validation_layout;
    private RelativeLayout verify_layout;
    private Button btn_try_again;


    private String phone_no;
    private String message;
    private int code;
    private ArrayList<Country> arrayList = new ArrayList<Country>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_no);
        setTitle(getResources().getString(R.string.verify_no));
        search = (AutoCompleteTextView) findViewById(R.id.search);
        spinner = (Spinner) findViewById(R.id.spinner);
        codes = (TextView) findViewById(R.id.code);
        et_phone = (EditText) findViewById(R.id.ph_number);
        send = (TextView) findViewById(R.id.ok);
        verify_layout = (RelativeLayout) findViewById(R.id.verify_layout);
        validation_layout = (LinearLayout) findViewById(R.id.validation_layout);
        btn_try_again = (Button) findViewById(R.id.btn_try_again);
        btn_try_again.setOnClickListener(this);
        send.setOnClickListener(this);
        spinner.setOnItemSelectedListener(this);
        addCounteryInSpinner();
    }

	/*
     * @Override public boolean onCreateOptionsMenu(Menu menu) {
	 * 
	 * // Inflate the menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.main, menu); return true; }
	 * 
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { // Handle
	 * action bar item clicks here. The action bar will // automatically handle
	 * clicks on the Home/Up button, so long // as you specify a parent activity
	 * in AndroidManifest.xml. int id = item.getItemId(); if (id ==
	 * R.id.action_settings) { return true; } return
	 * super.onOptionsItemSelected(item); }
	 */

    public String generateSMS(int code) {
        String sms;
        sms = "Your Gappss verification code is " + code;
        return sms;
    }

    public int generateCode() {
        int code;
        Random ran = new Random();
        code = 100000 + ran.nextInt(900000);
        return code;
    }

    public void sendSMS(final Context context, final String phoneNumber, final String message) {
        Log.d("test", "sendSMS => " + phoneNumber + ", " + message);
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phoneNumber, null, message, null, null);

		/*Handler handler = new Handler();
        Runnable r = new Runnable() {
			public void run() {
				deletemsg(context, phoneNumber, message);
			}
		};
		handler.postDelayed(r, 5000);*/
    }

	/*public static void deletemsg(Context context, String phoneNumber,
            String message) {
		try {
			Uri uriSms = Uri.parse("content://sms/");
			Cursor c = context.getContentResolver().query(
					uriSms,
					new String[] { "_id", "thread_id", "address", "person",
							"date", "body" }, null, null, null);
			Log.e("Cursor has items?", "" + c.moveToFirst());
			if (c != null && c.moveToFirst()) {
				do {
					long id = c.getLong(0);
					long threadId = c.getLong(1);
					String address = c.getString(2);
					String person = c.getString(3);
					String date = c.getString(4);
					String body = c.getString(5);

					Log.e("Cursor Values",
							"\n0-->" + c.getColumnName(0) + "-->"
									+ c.getString(0) + "\n1-->"
									+ c.getColumnName(1) + "-->"
									+ c.getString(1) + "\n2-->"
									+ c.getColumnName(2) + "-->"
									+ c.getString(2) + "\n3-->"
									+ c.getColumnName(3) + "-->"
									+ c.getString(3) + "\n4-->"
									+ c.getColumnName(4) + "-->"
									+ c.getString(4) + "\n5-->"
									+ c.getColumnName(5) + "-->"
									+ c.getString(5));

					Log.e(message, body);
					if (message.equals(body)) {
						context.getContentResolver().delete(
								Uri.parse("content://sms/"), "_id=" + id, null);
						Log.e("Log", "Deleteing.....");
						break;
					}
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (verify_layout.getVisibility() == View.INVISIBLE) {
            verify_layout.setVisibility(View.VISIBLE);
            validation_layout.setVisibility(View.GONE);
        } else {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                String ph_no = et_phone.getText().toString().trim();
                ph_no = ph_no.replaceFirst("^0+(?!$)", "");
                int length = ph_no.length();
                if (length == 9)
                    ph_no = "0" + ph_no;
                length = ph_no.length();
                if (length >= 10) {
                    if (true) {//if(isPhoneSimOk()){//if(true){//isPhoneSimOk()
                        UtilsClass.hideKeyBoard(this, et_phone);
                        //String mobNo = et_phone.getText().toString().trim();
                        phone_no = codes.getText().toString() + ph_no;
                        code = generateCode();
                        message = generateSMS(code);
                        Verify verify = new Verify();
                        Bundle bundle = new Bundle();
                        bundle.putString("sms", message);
                        bundle.putInt("code", code);
                        bundle.putString(Constants.PH_NUMBER, phone_no);
                        verify.setArguments(bundle);
                        addFragement(verify, Verify.TAG, true);

                        MySharedPreferences.getInstance().putStringKeyValue(this, Constants.PH_NUMBER, ph_no);
                        MySharedPreferences.getInstance().putStringKeyValue(this, Constants.COUNTRY_CODE, codes.getText().toString());

                        // This is test code for client
                        if (ph_no.equalsIgnoreCase("0123456789") ||
                                ph_no.equalsIgnoreCase("0633031052") ||
                                ph_no.equalsIgnoreCase("9602487039") ||
                                ph_no.equalsIgnoreCase("9929572110") ||
                                ph_no.equalsIgnoreCase("8302510544")) {//8302510544//9602487039
                            MySharedPreferences.getInstance().putBooleanKeyValue(this, Constants.VERIFY, true);
                            startMyActivity(LoginSignupActivity.class, null);
                            finish();
                        } else {
                            sendSMS(PhoneNumberVerficationActivity.this, phone_no, message);
                        }
                        //////////////----------=============
                        //finish();
                    } else {
                        Toast.makeText(PhoneNumberVerficationActivity.this, "No simcard detected please insert simcard", Toast.LENGTH_LONG).show();
                    }
                } else {
                    verify_layout.setVisibility(View.INVISIBLE);
                    validation_layout.setVisibility(View.VISIBLE);
                /*Toast.makeText(PhoneNumberVerficationActivity.this,
                        "Enter Valid Phone Number", Toast.LENGTH_LONG)
						.show();*/
                }
                break;
            case R.id.btn_try_again:
                verify_layout.setVisibility(View.VISIBLE);
                validation_layout.setVisibility(View.INVISIBLE);
                break;
        }
    }

    public void addCounteryInSpinner() {
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        String netIso = tm.getNetworkCountryIso();
        String simIso = tm.getSimCountryIso();
        int itemPos = 0;
        try {
            JSONArray jsonArray = new JSONArray(readSavedData());
            final String[] arrayString = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                String name = jsonArray.optJSONObject(i).optString("name");
                String code = jsonArray.optJSONObject(i).optString("code");
                if (code.equalsIgnoreCase(netIso))
                    itemPos = i;
                Country countery = new Country();
                countery.setName(name);
                countery.setCode(jsonArray.optJSONObject(i).optString("dial_code"));
                arrayList.add(countery);

                arrayString[i] = name;
            }

            ArrayAdapter arrAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayString);
            search.setAdapter(arrAdapter);

            search.setThreshold(1);

            search.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String con = (String) ((TextView) view).getText();
                    for (int i = 0; i < arrayList.size(); i++) {
                        if (arrayList.get(i).getName().equalsIgnoreCase(con)) {
                            spinner.setSelection(i);
                            break;
                        }
                    }

                    UtilsClass.hideKeyBoard(PhoneNumberVerficationActivity.this, search);
                }
            });

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        SpinnerAdapter adapter = new SpinnerAdapter(PhoneNumberVerficationActivity.this, arrayList);
        spinner.setAdapter(adapter);
        spinner.setSelection(itemPos);
        codes.setText(arrayList.get(itemPos).getCode());
    }

    public String readSavedData() {
        StringBuffer datax = new StringBuffer("");
        BufferedReader buffreader = null;
        try {

            buffreader = new BufferedReader(new InputStreamReader(getAssets()
                    .open("countrycode.txt"), "UTF-8"));

            String readString = buffreader.readLine();
            while (readString != null) {
                datax.append(readString);
                readString = buffreader.readLine();
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (buffreader != null) {
                try {
                    buffreader.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return datax.toString();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        codes.setText(arrayList.get(position).getCode());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub

    }

    private boolean isPhoneSimOk() {
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        /*If Tablet then no sim*/
        if (telMgr.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE)
            return false;

        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                // do something
                Log.i("SIM", "SIM_STATE_ABSENT");
                return false;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                // do something
                Log.i("SIM", "SIM_STATE_NETWORK_LOCKED");
                return false;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                // do something
                Log.i("SIM", "SIM_STATE_PIN_REQUIRED");
                return false;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                // do something
                Log.i("SIM", "SIM_STATE_PUK_REQUIRED");
                return false;
            case TelephonyManager.SIM_STATE_READY:
                // do something
                Log.i("SIM", "SIM_STATE_READY");
                return true;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                // do something
                Log.i("SIM", "SIM_STATE_UNKNOWN");
                return true;
        }

        return false;
    }

    @Override
    protected void initControls(Bundle savedInstanceState) {

    }

    @Override
    protected void setValueOnUI() {

    }
}
