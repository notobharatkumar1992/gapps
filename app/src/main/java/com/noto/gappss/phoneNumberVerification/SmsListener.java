package com.noto.gappss.phoneNumberVerification;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.noto.gappss.LoginSignupActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.preference.MySharedPreferences;

public class SmsListener extends BroadcastReceiver {

	/*public SmsListener() {
		super();
	}*/

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("Verify", "onReceive");
		if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
			Log.i("GOT IT", "GET MSG");
			Bundle bundle = intent.getExtras(); // ---get the SMS message
			// passed in---
			SmsMessage[] msgs = null;
			String msg_from;
			if (bundle != null) {
				// ---retrieve the SMS message received---
				try {
					Object[] pdus = (Object[]) bundle.get("pdus");
					msgs = new SmsMessage[pdus.length];
					for (int i = 0; i < msgs.length; i++) {
						msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
						msg_from = msgs[i].getOriginatingAddress();
						String msgBody = msgs[i].getMessageBody();
						
						Log.i("msg_from", ""+msg_from);
						Log.i("msgBody", ""+msgBody);

						if(Verify.verify(context, msgBody)){
							MySharedPreferences.getInstance().putBooleanKeyValue(context, Constants.VERIFY, true);
							Intent goToHome = new Intent(context, LoginSignupActivity.class);
							goToHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
					        context.startActivity(goToHome);
					      ( (Activity)context).finish();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}