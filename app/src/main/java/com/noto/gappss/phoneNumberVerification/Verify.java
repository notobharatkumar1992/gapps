package com.noto.gappss.phoneNumberVerification;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;


public class Verify extends BaseFragment {

	private EditText et_code;
	private Button verify,resend;
	private static String sms;
	ProgressBar mProgressBar;
	CountDownTimer mCountDownTimer;
	//TextView waiting;
	private SmsListener reciever = new SmsListener();;
	String phone_no=null,message = null;
	int i = 60;
	Bundle b;
	private View textNote;
	public static String TAG = "Verify";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_verify_progress, null);
		initUi(view);
		return view;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		//time = (TextView)findViewById(R.id.time);
		//	waiting = (TextView)findViewById(R.id.waiting);

	}

	@Override
	public void onStart() { 	
		super.onStart();
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.provider.Telephony.SMS_RECEIVED");
		filter.setPriority(2147483647);
		getActivity().registerReceiver(reciever, filter);
		//registerReceiver(reciever, filter, "android.permission.RECEIVE_SMS", null);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	public static boolean verify(Context context, String message) {
		if (message.equals(sms)) {
			Toast.makeText(context, "Verified",	Toast.LENGTH_LONG).show();
			((Activity)context).finish();
			//startHome(context);
			return true;
		} else {
			Toast.makeText(context, "Invalid Code",	Toast.LENGTH_LONG).show();
			return false;
		}
	}

	@Override
	public void onStop() {	
		super.onStop();
		getActivity().unregisterReceiver(reciever);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		//unregisterReceiver(reciever);
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	protected void initUi(View view) {
		b = getArguments();
		sms = b.getString("sms");
		phone_no = b.getString(Constants.PH_NUMBER);

		textNote =  view.findViewById(R.id.textNote);
		et_code = (EditText) view.findViewById(R.id.et_code);
		verify = (Button) view.findViewById(R.id.verify);
		resend = (Button)view.findViewById(R.id.resend);
		mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar);
		mProgressBar.setProgress(i);
	    mCountDownTimer = new CountDownTimer(60000, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {
				i--;
				mProgressBar.setProgress(i);
				//time.setText(i+" Seconds");
			}

			@Override
			public void onFinish() {
				// Do what you want
				//TEST CODE
				/*if(getActivity() != null){
					MySharedPreferences.getInstance().putBooleanKeyValue(getActivity(), Constants.VERIFY, true);
					Intent goToHome = new Intent(getActivity(), LoginSignupActivity.class);
					goToHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(goToHome);
					getActivity().finish();
				}	*/							i--;
				mProgressBar.setProgress(i);

				et_code.setVisibility(View.VISIBLE);				
				verify.setVisibility(View.VISIBLE);
				resend.setVisibility(View.VISIBLE);
				//waiting.setVisibility(View.INVISIBLE);
				//time.setVisibility(View.INVISIBLE);
				textNote.setVisibility(View.INVISIBLE);
				mProgressBar.setVisibility(View.INVISIBLE);
			}
		};
		mCountDownTimer.start();


		verify.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String code = "Your verification code is " + et_code.getText().toString().trim();
				verify(getActivity(), code);
			}
		});

		resend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				removeTopFragment();
				//((PhoneNumberVerficationActivity)getActivity()).sendSMS(getActivity(),phone_no, sms);
			}
		});		
	}

	@Override
	protected void setValueOnUi() {
	}

	@Override
	protected void setListener() {
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}

	@Override
	public void onDestroyView() {
		if(mCountDownTimer != null)			
			mCountDownTimer.cancel();

		mCountDownTimer = null;
		super.onDestroyView();
	}

	@Override
	public void onDetach() {
		if(mCountDownTimer != null)			
			mCountDownTimer.cancel();

		mCountDownTimer = null;
		super.onDetach();
	}
}
