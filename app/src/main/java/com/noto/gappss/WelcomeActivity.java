package com.noto.gappss;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.TextView;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.phoneNumberVerification.PhoneNumberVerficationActivity;

public class WelcomeActivity extends BaseFragmentActivity implements
OnClickListener {

	private TextView agree_and_continue;
	private TextView decline;
	private WebView wv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);		
		setTitleOnAction(getResources().getString(R.string.welcome));
		setContentView(R.layout.activity_welcome);
		initControls(savedInstanceState);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		agree_and_continue = (TextView) findViewById(R.id.agree_and_continue);
		decline = (TextView) findViewById(R.id.decline);		
		wv = (WebView) findViewById(R.id.webView1);  
		wv.loadUrl("file:///android_asset/privacy.html"); 
		agree_and_continue.setOnClickListener(this);
		decline.setOnClickListener(this);
	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.agree_and_continue:
			//startMyActivity(VerifyNumberActivity.class, null);
			//startMyActivity(LoginSignupActivity.class, null);
			startMyActivity(PhoneNumberVerficationActivity.class, null);
			finish();
			break;

		case R.id.decline:
			finish();
			break;
		}

	}

}
