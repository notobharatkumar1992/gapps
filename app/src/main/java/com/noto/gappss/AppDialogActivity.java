package com.noto.gappss;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.noto.gappss.adapter.AppDialogAdapter;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.networkTask.AppDetailAsyncTask.CallBackListener;
import com.noto.gappss.utilities.ToastCustomClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AppDialogActivity extends BaseFragmentActivity implements OnItemClickListener {

    private GridView gridView;
    private ArrayList<ApplicationBean> arrayList = new ArrayList<ApplicationBean>();
    private AppDialogAdapter adapter;
    //private ApplicationBean applicationBean = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        enableBackButton();
        setTitleOnAction(getResources().getString(R.string.app));
        setContentView(R.layout.activity_app_dialog);

        gridView = (GridView) findViewById(R.id.gridView);

        gridView.setOnItemClickListener(this);

        new DownloadAppData().execute("");
    }

    @Override
    protected void initControls(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void setValueOnUI() {
        // TODO Auto-generated method stub

    }

    private class DownloadAppData extends AsyncTask<String, Void, String> {
        private ProgressDialog mProgressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(AppDialogActivity.this);
            mProgressDialog.setMessage("Please wait...\nSearching installed applications");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... data) {
            exportApks(AppDialogActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(String str) {
            // Close progressdialog
            mProgressDialog.dismiss();
            if (arrayList.size() > 0) {
                adapter = new AppDialogAdapter(AppDialogActivity.this, arrayList);
                gridView.setAdapter(adapter);
            }
        }
    }


    @SuppressLint("NewApi")
    public void exportApks(Context context) {
        PackageManager pm = context.getPackageManager();
        List<PackageInfo> pkginfolist = pm.getInstalledPackages(PackageManager.GET_ACTIVITIES);
        List<ApplicationInfo> appinfoList = pm.getInstalledApplications(0);

        if (pkginfolist.size() > 0) {
            //installedApp = new HashMap<String, ApplicationBean>();
            //  progress.progress(BackupTask.MESSAGE_COUNT, pkginfolist.size());
            for (int x = 0; x < pkginfolist.size(); x++) {
                ApplicationBean app = new ApplicationBean();
                ApplicationInfo appInfo = appinfoList.get(x);
                String appname = pm.getApplicationLabel(appInfo).toString();
                String pkgName = appInfo.packageName;

                Drawable icon = null;
                try {
                    icon = pm.getApplicationIcon(appInfo.packageName);
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }

                app.setTitle(appname);
                app.setPackageName(pkgName);
                app.setApplogo(icon);
                app.setInstalled(true);
                app.setStatus(1);
                if (!isSystemPackage(appInfo))
                    //Log.i("APP INFO", app.toString());
                    arrayList.add(app);
                //installedApp.put(pkgName, app);

                //Log.i("APKS", "Name:"+appname+"\n "+"Package Name:"+pkgName+"\n Icon:"+icon);
            }
            //serviceResponseGetFriendAppVisibility(installedApp);
        }
    }

    private boolean isSystemPackage(ApplicationInfo applicationInfo) {
        return ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
    }

    CallBackListener callBackListener = new CallBackListener() {

        @Override
        public void callback(Object object) {
            try {
                JSONObject jsonObject = new JSONObject((String) object);
                String error = jsonObject.optString("error");
                if (error == null || error.equals("")) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.response, jsonObject.toString());
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    ToastCustomClass.showToast(AppDialogActivity.this, error);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ok, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cart:
                ArrayList<ApplicationBean> list = new ArrayList<ApplicationBean>();
                StringBuffer appPcks = new StringBuffer();
                for (int i = 0; i < arrayList.size(); i++) {
                    if (arrayList.get(i).isChecked()) {
                    /*ApplicationBean applicationBean = ;
                    appPcks.append(arrayList.get(i).getPackageName());
					app
					appPcks.append(",");*/
                        list.add(arrayList.get(i));
                    }
                }
                MyApplication.getApplication().setApplicationBean(list);
                Intent intent = new Intent();
                //intent.putExtra(Constants.response,jsonObject.toString());
                setResult(Activity.RESULT_OK, intent);
                finish();
			/*
			AppDetailAsyncTask appDetailAsyncTask = new AppDetailAsyncTask(this, appPcks.toString(), AppDetailAsyncTask.applicationDetails, false);
			appDetailAsyncTask.setCallBackListener(callBackListener);
			appDetailAsyncTask.execute("");*/
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
