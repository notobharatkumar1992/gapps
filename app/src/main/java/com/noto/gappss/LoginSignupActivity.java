package com.noto.gappss;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.noto.gappss.baseClasses.BaseFragmentActivity;

public class LoginSignupActivity extends BaseFragmentActivity implements OnClickListener {
	private LinearLayout login;
	private LinearLayout signup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().hide();
		// enableBackButton();
		// setTitleOnAction(getResources().getString(R.string.legal_conditon_act));
		setContentView(R.layout.activity_login_signup);
		initControls(savedInstanceState);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		login = (LinearLayout) findViewById(R.id.login);
		signup = (LinearLayout) findViewById(R.id.signup);
		login.setOnClickListener(this);
		signup.setOnClickListener(this);
	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action buttons
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login:
			startMyActivity(LoginActivity.class, null);
			//startMyActivity(CreateChannelActivity.class, null);
			finish();
			break;

		case R.id.signup:
			startMyActivity(SignupActivity.class, null);
			finish();
			break;
		}
	}
}
