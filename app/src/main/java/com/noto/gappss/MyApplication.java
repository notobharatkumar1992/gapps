package com.noto.gappss;

import android.app.Application;
import android.graphics.Typeface;
import android.util.Base64;

import com.facebook.FacebookSdk;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.beanClasses.Contact;

import java.util.ArrayList;
import java.util.Date;

public class MyApplication extends Application {
//	public static boolean withDummyData = false;

    //private ApiResponse apiResponce;
    private double currentLatitude;
    private double currentLongitude;
    // Screen in Pixels
    private int widthPixel = 1280;
    private int heightPixel = 1920;
    public static long locationDetectionTimeInMin;
    private Date date = null;
    private ArrayList<Contact> listContact;
    private static MyApplication myApplication;
    public static Typeface typeJACKPORT_REGULAR_NCV;
    public static Typeface typeFaceOrgano;
    public static Typeface typeFaceSkipLegDay;
    public static Typeface typeFaceStarzy_Darzy;
    public static ArrayList<ApplicationBean> applicationBean;

    public static String base64EncodedCredentials = "Basic " + Base64.encodeToString(("admin" + ":" + "noto@123").getBytes(), Base64.NO_WRAP);

    @Override
    public void onCreate() {
        super.onCreate();
        // Get Date ----------------
        date = new Date();

        myApplication = this;
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    public static MyApplication getApplication() {
        return myApplication;
    }

    public double getCurrentLongitude() {
        return currentLongitude;
    }

    public void setCurrentLongitude(double currentLongitude) {
        this.currentLongitude = currentLongitude;
    }

    public double getCurrentLatitude() {
        return currentLatitude;
    }

    public void setCurrentLatitude(double currentLatitude) {
        this.currentLatitude = currentLatitude;
    }

    public int getWidthPixel() {
        return widthPixel;
    }

    public void setWidthPixel(int widthPixel) {
        this.widthPixel = widthPixel;
    }

    public int getHeightPixel() {
        return heightPixel;
    }

    public void setHeightPixel(int heightPixel) {
        this.heightPixel = heightPixel;
    }

    public void setApplicationBean(ArrayList<ApplicationBean> applicationBean) {
        this.applicationBean = applicationBean;
    }

    public ArrayList<ApplicationBean> getApplicationBean() {
        return this.applicationBean;
    }

    public void setContactList(ArrayList<Contact> list) {
        this.listContact = list;
    }

    public ArrayList<Contact> getContactList() {
        return this.listContact;
    }

	/*public void addApisResponce(String url, String apiResponce) {		 
        this.apiResponce = getApisResponce();
		this.apiResponce.addResponce(url, apiResponce);
	}*/
	
	/*public ApiResponse getApisResponce(){
		if(apiResponce == null)
			apiResponce = new ApiResponse();
		return apiResponce;
	}*/
}

