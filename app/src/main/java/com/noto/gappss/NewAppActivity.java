package com.noto.gappss;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.AppDetails;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.beanClasses.FriendApplicationBean;
import com.noto.gappss.db.DataBaseQueriesClass;
import com.noto.gappss.fragments.AppStatusFragment;
import com.noto.gappss.fragments.NewAppInstallFragment;
import com.noto.gappss.networkTask.URLsClass;


public class NewAppActivity extends BaseFragmentActivity {


	private ArrayList<ApplicationBean> frndsAppList;

	public ArrayList<ApplicationBean> getFriendsAppList(){
		return frndsAppList;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		replaceFragement(NewAppInstallFragment.getInstance(), NewAppInstallFragment.TAG);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {

	}

	@Override
	protected void setValueOnUI() {

	}

	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,	int requstCode) {
		if(super.callBackFromApi(object, fragment, requstCode)){
			callBackCommon(object, requstCode, fragment);
		}
		return true;
	}

	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if(super.callBackFromApi(object, act, requstCode)){
			callBackCommon(object, requstCode, act);
		}
		return true;
	}

	private void callBackCommon(Object object, int requstCode, Object luncher){
		String message = "";
		JSONObject jObject;		
		switch (requstCode) {		
		case Constants.getAppVisibility:
			jObject = (JSONObject) object;
			message = jObject.optString(Constants.p_message);
			HashMap<String, ApplicationBean> mapOfApp = new HashMap<String, ApplicationBean>();
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				//Log.i("App catagory", "2");
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);
				PackageManager  pm = getPackageManager();
				for (int i = 0; i<jsonArray.length() ; i++) {
					JSONObject jObj = jsonArray.optJSONObject(i);
					String pkg = jObj.optString(Constants.p_appPack);
					Drawable icon = null;
					try {
						icon = pm.getApplicationIcon(pkg);
					} catch (NameNotFoundException e) {						
					}		

					ApplicationBean appCategory = new ApplicationBean();
					appCategory.setTitle(jObj.optString(Constants.p_appName));
					appCategory.setPackageName(jObj.optString(Constants.p_appPack));
					appCategory.setStatus(jObj.optInt(Constants.status));
					appCategory.setApplogo(icon);
					//Log.i("CATTTTTTT", appCategory.toString()+"\n\n value="+jObj);
					mapOfApp.put(pkg, appCategory);
				}

				if(luncher instanceof NewAppInstallFragment){
					NewAppInstallFragment freg = (NewAppInstallFragment) luncher;				
					freg.serviceResponseGetAppVisibility(mapOfApp);
				}else{
					AppStatusFragment freg = (AppStatusFragment) luncher;					
					freg.serviceResponseGetAppVisibility(mapOfApp);
				}
			}else {
				if(luncher instanceof NewAppInstallFragment){
					NewAppInstallFragment freg = (NewAppInstallFragment) getFragmentByTag(NewAppInstallFragment.TAG);
					freg.serviceResponseGetAppVisibility(mapOfApp);
				}else{
					AppStatusFragment freg = (AppStatusFragment) luncher;
					freg.serviceResponseGetAppVisibility(mapOfApp);
				}
			}
			break;
		case Constants.setAppVisibility:{
			//{"status":1," ":"Set Successfully.","dataflow":0}
			JSONObject jsonObj = (JSONObject) object;
			//Log.i("App catagory", jsonObj.toString());
			JSONArray jsonArray = jsonObj.optJSONArray(Constants.appList);
			if (jsonArray != null && jsonArray.length() > 0) {
				ArrayList<AppDetails> arrayList = new ArrayList<AppDetails>();
				for (int i = 0; i < jsonArray.length(); i++) {
					AppDetails appDetails = new AppDetails();
					JSONObject jsonObject = jsonArray.optJSONObject(i);
					appDetails.setAdsSupported(jsonObject.optString(Constants.adsSupported));
					appDetails.setAuthor(jsonObject.optString(Constants.name));
					appDetails.setCategory(jsonObject.optString(Constants.category));
					appDetails.setChangelog(jsonObject.optString(Constants.changelog));
					appDetails.setContentRating(jsonObject.optString(Constants.contentRating));
					appDetails.setDatePublished(jsonObject.optString(Constants.datePublished));
					appDetails.setDescription(jsonObject.optString(Constants.description));
					appDetails.setEditorsChoice(jsonObject.optString(Constants.editorsChoice));
					appDetails.setFileSize(jsonObject.optString(Constants.fileSize));
					appDetails.setIcon(jsonObject.optString(Constants.icon));
					appDetails.setInAppBilling(jsonObject.optString(Constants.inAppBilling));
					appDetails.setName(jsonObject.optString(Constants.name));
					appDetails.setNumDownloads(jsonObject.optString(Constants.numDownloads));
					appDetails.setOperatingSystems(jsonObject.optString(Constants.operatingSystems));
					appDetails.setPackageId(jsonObject.optString(Constants.packageName));
					appDetails.setPrice(jsonObject.optString(Constants.price));
					appDetails.setRatingCount(jsonObject.optLong(Constants.count));
					appDetails.setRatingDisplay(jsonObject.optString(Constants.display));
					appDetails.setRatingFive(jsonObject.optLong(Constants.five));
					appDetails.setRatingFour(jsonObject.optLong(Constants.four));
					appDetails.setRatingThree(jsonObject.optLong(Constants.three));
					appDetails.setRatingTwo(jsonObject.optLong(Constants.two));
					appDetails.setRatingOne(jsonObject.optLong(Constants.one));
					appDetails.setRatingValue(jsonObject.optDouble(Constants.value));
					appDetails.setScreenshots(jsonObject.optString(Constants.screenshots));
					appDetails.setStoreCategory(jsonObject.optString(Constants.storeCategory));
					appDetails.setSupportEmail(jsonObject.optString(Constants.supportEmail));
					appDetails.setSupportUrl(jsonObject.optString(Constants.supportUrl));
					appDetails.setTopDeveloper(jsonObject.optString(Constants.topDeveloper));
					appDetails.setVersionName(jsonObject.optString(Constants.versionName));
					appDetails.setVideo(jsonObject.optString(Constants.video));
					
					appDetails.setUnknown(jsonObject.optInt(Constants.unknown));
					appDetails.setStatus(jsonObject.optString(Constants.status));
					appDetails.setCreated(jsonObject.optString(Constants.created));
					appDetails.setModified(jsonObject.optString(Constants.modified));
					appDetails.setRank(jsonObject.optString(Constants.rank));
					
					arrayList.add(appDetails);
					
				}
				DataBaseQueriesClass.getInstance().insertUpdateAppDetails(this, arrayList);
			}
			if(luncher instanceof NewAppInstallFragment){
				NewAppInstallFragment freg = (NewAppInstallFragment) getFragmentByTag(NewAppInstallFragment.TAG);
				freg.serviceResponseSetAppVisibility(true);
			}else if(luncher instanceof AppStatusFragment){
				AppStatusFragment freg = (AppStatusFragment) luncher;
				freg.serviceResponseSetAppVisibility(true);
			}			
		}
		break;
		case Constants.getFriendAppVisibility:
			jObject = (JSONObject) object;
			//Log.i("App catagory", jObject.toString());
			message = jObject.optString(Constants.p_message);
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);
				//	ArrayList<FriendApplicationBean> arrayList = new ArrayList<FriendApplicationBean>();
				frndsAppList = new ArrayList<ApplicationBean>();
				for (int i = 0; i<jsonArray.length() ; i++) {
					JSONObject innerJsonObject = jsonArray.optJSONObject(i);
					JSONArray innerJsonArray = innerJsonObject.optJSONArray(Constants.p_app_list);
					FriendApplicationBean applicationBeanFriend = new FriendApplicationBean();
					applicationBeanFriend.setFriendId(innerJsonObject.optString(Constants.p_friendId));
					applicationBeanFriend.setFriendName(innerJsonObject.optString(Constants.p_friendName));
					if (innerJsonArray != null) 
					{
						for (int j = 0; j < innerJsonArray.length(); j++) {
							ApplicationBean applicationBean = new ApplicationBean();
							applicationBean.setTitle(innerJsonArray.optJSONObject(j).optString(Constants.p_appName));
							applicationBean.setPackageName(innerJsonArray.optJSONObject(j).optString(Constants.p_appPack));
							frndsAppList.add(applicationBean);
						}
					}
				}

				if(frndsAppList != null && frndsAppList.size() > 0){
					if(luncher instanceof NewAppInstallFragment){
						NewAppInstallFragment freg = (NewAppInstallFragment) getFragmentByTag(NewAppInstallFragment.TAG);
						freg.serviceResponseGetFriendAppVisibility(frndsAppList);
					}else if(luncher instanceof AppStatusFragment){
						AppStatusFragment freg = (AppStatusFragment) luncher;
						freg.serviceResponseGetFriendAppVisibility(frndsAppList);
					}
				}
			}else {
				if(luncher instanceof NewAppInstallFragment){
					NewAppInstallFragment freg = (NewAppInstallFragment) getFragmentByTag(NewAppInstallFragment.TAG);
					freg.serviceResponseGetFriendAppVisibility(new ArrayList<ApplicationBean>());
				}else if(luncher instanceof AppStatusFragment){
					AppStatusFragment freg = (AppStatusFragment) luncher;
					freg.serviceResponseGetFriendAppVisibility(new ArrayList<ApplicationBean>());
				}					
			}		
			break;
		}	
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {		
		getMenuInflater().inflate(R.menu.ok, menu);	
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.cart:			
			NewAppInstallFragment f = (NewAppInstallFragment) getFragmentByTag(NewAppInstallFragment.TAG);
			if(f != null){
				((NewAppInstallFragment)f).setAppVisibility();
			}
			else{
				AppStatusFragment f1 = (AppStatusFragment) getFragmentByTag(AppStatusFragment.TAG);
				if(f1 != null)
					((AppStatusFragment)f1).setAppVisibility();
			}
			return true;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}*/
}
