package com.noto.gappss;

import android.os.Bundle;
import android.view.MenuItem;

import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.fragments.SettingFragment;

public class SettingActivity extends BaseFragmentActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		//	Bundle bundle = new Bundle();
		//bundle.putString(Constants.USER_ID, "");
		enableBackButton();
		//setTitleOnAction(getResources().getString(R.string.settings));
		addFragement(SettingFragment.newInstance(null), SettingFragment.TAG, false);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			BaseFragment topF = getTopFragment();
			if(topF != null && !topF.onBackPressedListener()){
				removeTopFragement();
				// disableBackButton();
				setTitleOnAction(getResources().getString(R.string.settings));
				return true;  
			}    
		}
		return super.onOptionsItemSelected(item);
	}

}
