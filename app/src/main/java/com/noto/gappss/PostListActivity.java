package com.noto.gappss;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.fragments.ChannelSettingFragment;
import com.noto.gappss.fragments.PostListFragment;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.utilities.ToastCustomClass;

public class PostListActivity extends BaseFragmentActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		//	Bundle bundle = new Bundle();
			//bundle.putString(Constants.USER_ID, "");
			enableBackButton();
			setTitleOnAction(getResources().getString(R.string.posts));
			addFragement(PostListFragment.newInstance(getBundle()), PostListFragment.TAG, false);
			
	}
	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub
		
	}
	

	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if(super.callBackFromApi(object, act, requstCode)){
			JSONObject jObject ;
			String message ="";
			switch (requstCode) {
			case Constants.getPostsByChannel:
				jObject = (JSONObject) object;
				
			    message = jObject.optString(Constants.p_message);
					if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
						PostListFragment freg = (PostListFragment) getFragmentByTag(PostListFragment.TAG);
						if(freg != null)
							freg.setPost(jObject);
					}else {
						ToastCustomClass.showToast(this, message);
					}
				
				break;
			case Constants.setChannelSettings:
				 jObject = (JSONObject) object;
				 message = jObject.optString(Constants.p_message);
					if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
						ToastCustomClass.showToast(this, message);
					}else {
						ToastCustomClass.showToast(this, message);
					}
				
				break;
			}
		}
		return true;
	}


}
