package com.noto.gappss;

import java.util.ArrayList;
import java.util.List;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.noto.gappss.adapter.SearchContactListAdapter;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.MainListLoader;



public class SearchActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<List<Contact>>{

	private static final String JARGON = "SEARCH";
	private String _curFilter="";
	private ListView listView;
	private ArrayList<Contact> contactList;
	private SearchContactListAdapter _adapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(R.style.Theme_AppCompat);
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.fragment_main_list);
		listView = (ListView) findViewById(R.id.listView);
		contactList = MyApplication.getApplication().getContactList();
		if(contactList != null){
			_adapter = new SearchContactListAdapter(this, contactList);
			listView.setAdapter(_adapter);
			
		}

		/*FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		SearchFragment fragment = new SearchFragment();
		fragmentTransaction.add(R.id.container, fragment);
		fragmentTransaction.commit();*/
		if(getIntent() != null && getIntent().getAction() != null)
			handleIntent(getIntent());
	}

	private void handleIntent(Intent intent){
		if(intent.getAction().equals(Intent.ACTION_SEARCH)){
			doSearch(intent.getStringExtra(SearchManager.QUERY));
		}else if(intent.getAction().equals(Intent.ACTION_VIEW)){
			getPlace(intent.getStringExtra(SearchManager.EXTRA_DATA_KEY));
		}
	}


	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		handleIntent(intent);
	}

	private void doSearch(String query){
		Bundle data = new Bundle();
		data.putString("query", query);
		getSupportLoaderManager().restartLoader(0, data, this);
	}

	private void getPlace(String query){
		Bundle data = new Bundle();
		data.putString("query", query);
		getSupportLoaderManager().restartLoader(1, data, this);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search, menu);

		// Get the SearchView and set the searchable configuration
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

		MenuItem searchItem = menu.findItem(R.id.i_search);

		SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

		// searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

		// Assumes current activity is the searchable activity
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public Loader<List<Contact>> onCreateLoader(int id, Bundle args) {
		return new MainListLoader(this, _curFilter);
	}

	@Override
	public void onLoadFinished(Loader<List<Contact>> loader, List<Contact> data) {
		// Set the new data in the adapter.
		//_adapter.setData(data);
		_adapter.setData(data);
	}

	@Override
	public void onLoaderReset(Loader<List<Contact>> loader) {
		// Android guides set data to null but I need check
		// what happen when I set data as null
		//_adapter.setData(new ArrayList<Contact>());
		_adapter.setData(contactList);
	}
}
