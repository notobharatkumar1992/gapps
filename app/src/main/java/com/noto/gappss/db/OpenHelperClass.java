package com.noto.gappss.db;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.noto.gappss.baseClasses.Constants;

public class OpenHelperClass extends SQLiteOpenHelper 
{
	public static final String DB_NAME="gappss_db.sqlite";
	private static final String TAG = "Open Helper";

	private SQLiteDatabase myDataBase; 

	private Context myContext;

	private String DB_PATH="/data/data/com.noto.gappss/databases/";

	private final String pathPrefix = "databases/";
	
	private final String tbl_cows = "tbl_cows";
	
	public OpenHelperClass(Context context) 
	{	 
	

		super(context, DB_NAME, null, 1);// 1? its Database Version
		Log.i("OpenHelperClass", "OpenHelperClass");
		if(android.os.Build.VERSION.SDK_INT >= 17){
			DB_PATH = context.getApplicationInfo().dataDir + "/"+pathPrefix;         
		}
		else
		{
			DB_PATH = "/data/data/" + context.getPackageName() + "/"+pathPrefix;
		}
		this.myContext = context;
	}	


	public SQLiteDatabase openDataBase() throws SQLException
	{
		Log.i("openDataBase", "openDataBase");
		//Open the database
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
		return myDataBase;
	}

	
	@Override
	public synchronized void close() 
	{
		Log.i("synchronized", "synchronized");
		if(myDataBase != null)
			myDataBase.close();

		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{	Log.i("onCreate", "onCreate");
	        //Create start route table
			String CREATE_START_ROUTE_TABLE = "CREATE TABLE " + Constants.TABLE_APP_DETAIL + "("
					+ Constants.COLUMN_ROW_ID + " INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE,"//0
					+ Constants.COLUMN_ADS_SUPPORTED + " VARCHAR,"//1
					+ Constants.COLUMN_AUTHOR + " TEXT,"//2
					+ Constants.COLUMN_CATEGORY + " VARCHAR,"//3
					+ Constants.COLUMN_CHANGELOG + " TEXT,"//4
					+ Constants.COLUMN_CONTENT_RATING + " VARCHAR,"//5
					+ Constants.COLUMN_DATE_PUBLISHED + " VARCHAR,"//6
					+ Constants.COLUMN_DESCRIPTION + " TEXT,"//7
					+ Constants.COLUMN_EDITORS_CHOICE + " VARCHAR,"//8
					+ Constants.COLUMN_FILE_SIZE + " TEXT,"//9
					+ Constants.COLUMN_ICON + " TEXT,"//10
					+ Constants.COLUMN_INAPP_BILLING + " VARCHAR,"//11
					+ Constants.COLUMN_NAME + " VARCHAR," //12
					+ Constants.COLUMN_NUM_DOWNLOADS + " TEXT,"//13
					+ Constants.COLUMN_OPERATING_SYSTEMS + " VARCHAR,"//14
					+ Constants.COLUMN_PACKAGE + " TEXT  NOT NULL  UNIQUE,"//15
					+ Constants.COLUMN_PRICE + " VARCHAR,"//16
					+ Constants.COLUMN_PRICE_CURRENCY + " VARCHAR,"//17
					+ Constants.COLUMN_RATING_COUNT + " long,"//18
					+ Constants.COLUMN_RATING_DISPLAY + " VARCHAR,"//19
					+ Constants.COLUMN_RATING_FIVE + " long,"//20
					+ Constants.COLUMN_RATING_FOUR + " long,"//21
					+ Constants.COLUMN_RATING_THREE + " long,"//22
					+ Constants.COLUMN_RATING_TWO + " long,"//23
					+ Constants.COLUMN_RATING_ONE + " long,"//24
					+ Constants.COLUMN_RATING_VALUE + " DOUBLE,"//25
					+ Constants.COLUMN_SCREENSHOTS + " TEXT,"//26
					+ Constants.COLUMN_STORE_CATEGORY + " VARCHAR,"//27
					+ Constants.COLUMN_SUPPORT_EMAIL + " TEXT,"//28
					+ Constants.COLUMN_SUPPORT_URL + " TEXT,"//29
					+ Constants.COLUMN_TOP_DEVELOPER + " VARCHAR,"//30
					+ Constants.COLUMN_VERSION_NAME + " VARCHAR,"//31
			        + Constants.COLUMN_VIDEO + " TEXT,"//32	
			        + Constants.COLUMN_UNKNOWN + " INTEGER,"//33
			        + Constants.COLUMN_CREATED + " VARCHAR,"//34
			        + Constants.COLUMN_MODIFIED + " VARCHAR,"//35
			        + Constants.COLUMN_STATUS + " VARCHAR,"//36
			        + Constants.COLUMN_RANK + " VARCHAR,"//37
			        + Constants.COLUMN_APP_STATUS + " INTEGER"+ ")";//38
  
			db.execSQL(CREATE_START_ROUTE_TABLE);	
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		Log.i("onUpgrade", "onUpgrade");
		if(newVersion > oldVersion){
			db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_APP_DETAIL);

			//Create tables again
			onCreate(db);		
		}
	}


	//Copy Database
	public void createDataBase() throws IOException
	{
		Log.i("createDataBase", "createDataBase");
		//If database not exists copy it from the assets
		boolean mDataBaseExist = checkDataBase();
		if(!mDataBaseExist)
		{
			this.getReadableDatabase();
			this.close();
			try 
			{
				//Copy the database from assests
				copyDataBase();
				Log.e(TAG, "createDatabase database created");
			} 
			catch (IOException mIOException) 
			{
				throw new Error("ErrorCopyingDataBase");
			}
		}
	}
	//Check that the database exists here: /data/data/your package/databases/Da Name
	private boolean checkDataBase()
	{
		Log.i("checkDataBase", "checkDataBase");
		File dbFile = new File(DB_PATH + DB_NAME);
		//Log.v("dbFile", dbFile + "   "+ dbFile.exists());
		return dbFile.exists();
	}

	//Copy the database from assets
	private void copyDataBase() throws IOException
	{
		Log.i("copyDataBase", "copyDataBase     ");
		InputStream mInput = myContext.getAssets().open(pathPrefix+DB_NAME);
		String outFileName = DB_PATH + DB_NAME;
		OutputStream mOutput = new FileOutputStream(outFileName);
		byte[] mBuffer = new byte[1024];
		int mLength;
		while ((mLength = mInput.read(mBuffer))>0)
		{
			mOutput.write(mBuffer, 0, mLength);
		}
		mOutput.flush();
		mOutput.close();
		mInput.close();
	}
}

