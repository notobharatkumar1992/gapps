package com.noto.gappss.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.AppDetails;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.utilities.UtilsClass;

public class DataBaseQueriesClass implements Constants {
	private static final String TAG = "DataBaseQuerires";
	private static DataBaseQueriesClass dbQueries;

	public static DataBaseQueriesClass getInstance() {
		if (dbQueries == null)
			dbQueries = new DataBaseQueriesClass();
		return dbQueries;
	}

	// private static ContentValues values;
	// private static SQLiteDatabase db;
	// private static OpenHelperClass openHelperClass;
	// static SQLiteDatabase db = null;
	public SQLiteDatabase getWritableDatabase(Context context) {
		// if(db == null){
		OpenHelperClass openHelperClass = new OpenHelperClass(context);
		SQLiteDatabase db = openHelperClass.getWritableDatabase();
		// }
		return db;
	}

	public SQLiteDatabase getReadableDataBase(Context context) {
		// if(db == null){
		OpenHelperClass openHelperClass = new OpenHelperClass(context);
		SQLiteDatabase db = openHelperClass.getReadableDatabase();
		// }
		return db;
	}

	public void deleteIntoTable(SQLiteDatabase dbLoc, String deleteQuery) {
		dbLoc.rawQuery(deleteQuery, null);
		closeResources(null, dbLoc);
	}

	public void createDatabase(Context mContext) throws SQLException {
		OpenHelperClass mDbHelper = new OpenHelperClass(mContext);
		try {
			mDbHelper.createDataBase();
		} catch (IOException mIOException) {
			Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
			throw new Error("UnableToCreateDatabase");
		}
	}

	public void closeResources(Cursor cursor, SQLiteDatabase db) {
		if (cursor != null) {
			cursor.close();
		}
		if (db != null) {
			db.close();
		}
	}

	public boolean isUpdateNeed(SQLiteDatabase db, String tblName,
			String columnName, String id) {
		Cursor cursor = null;
		try {
			// Select All Query
			String selectQuery = "SELECT * FROM " + tblName + " WHERE "
					+ columnName + " = '" + id + "'";
			Log.i("getAnimal", "=" + selectQuery);
			cursor = db.rawQuery(selectQuery, null);
		} catch (SQLiteException e) {
			Log.i("update", "=" + e.getMessage());
		} catch (Exception e) {
			Log.i("update", "=" + e.getMessage());
		}
		if (cursor != null && cursor.getCount() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public void insertUpdateAppDetails(Context context,ArrayList<AppDetails> arrayList){
		
		SQLiteDatabase db = getWritableDatabase(context);
		Log.i("insertAppDetails", "insertAppDetails");
		ContentValues values;

		try {
			for (int i = 0; i < arrayList.size(); i++) {
				
				
				String query = "SELECT * FROM " + TABLE_APP_DETAIL + " WHERE "
						+ COLUMN_PACKAGE + " = '" + arrayList.get(i).getPackageId() + "'";
				Cursor cursor = db.rawQuery(query, null);
				
				values = new ContentValues();
				AppDetails appDetails = arrayList.get(i);
				
				Log.i("DB TEST", arrayList.get(i).toString());
				
				values.put(COLUMN_ADS_SUPPORTED, appDetails.getAdsSupported());
				values.put(COLUMN_NAME, appDetails.getName());
				values.put(COLUMN_AUTHOR, appDetails.getAuthor());
				values.put(COLUMN_CATEGORY, appDetails.getCategory());
				values.put(COLUMN_CHANGELOG, appDetails.getChangelog());
				values.put(COLUMN_CONTENT_RATING, appDetails.getContentRating());
				values.put(COLUMN_DATE_PUBLISHED, appDetails.getDatePublished());
				values.put(COLUMN_DESCRIPTION, appDetails.getDescription());
				values.put(COLUMN_EDITORS_CHOICE, appDetails.getEditorsChoice());
				values.put(COLUMN_FILE_SIZE, appDetails.getFileSize());
				values.put(COLUMN_ICON, appDetails.getIcon());
				values.put(COLUMN_INAPP_BILLING, appDetails.getInAppBilling());
				values.put(COLUMN_NUM_DOWNLOADS, appDetails.getNumDownloads());
				values.put(COLUMN_OPERATING_SYSTEMS, appDetails.getOperatingSystems());
				values.put(COLUMN_PACKAGE, appDetails.getPackageId());
				values.put(COLUMN_PRICE, appDetails.getPrice());
				values.put(COLUMN_RATING_COUNT, appDetails.getRatingCount());
				values.put(COLUMN_RATING_DISPLAY, appDetails.getRatingDisplay());
				values.put(COLUMN_RATING_FIVE, appDetails.getRatingFive());
				values.put(COLUMN_RATING_FOUR, appDetails.getRatingFour());
				values.put(COLUMN_RATING_ONE, appDetails.getRatingOne());
				values.put(COLUMN_RATING_THREE, appDetails.getRatingThree());
				values.put(COLUMN_RATING_TWO, appDetails.getRatingTwo());
				values.put(COLUMN_RATING_VALUE, appDetails.getRatingValue());
				values.put(COLUMN_SCREENSHOTS, appDetails.getScreenshots());
				values.put(COLUMN_STORE_CATEGORY, appDetails.getStoreCategory());
				values.put(COLUMN_SUPPORT_EMAIL, appDetails.getSupportEmail());
				values.put(COLUMN_SUPPORT_URL, appDetails.getSupportUrl());
				values.put(COLUMN_TOP_DEVELOPER, appDetails.getTopDeveloper());
				values.put(COLUMN_VERSION_NAME, appDetails.getVersionName());

				values.put(COLUMN_UNKNOWN, appDetails.getUnknown());
				values.put(COLUMN_CREATED, appDetails.getCreated());
				values.put(COLUMN_MODIFIED, appDetails.getModified());
				values.put(COLUMN_RANK, appDetails.getRank());
				values.put(COLUMN_STATUS, appDetails.getStatus());
				

				if (cursor != null && cursor.getCount() > 0) {
					String pakId = arrayList.get(i).getPackageId();
					db.update(TABLE_APP_DETAIL, values, COLUMN_PACKAGE + " = ?",
							new String[] { pakId });
					Log.i("CONDITION", "IF");
				}else {
					Log.i("CONDITION", "ELSE");
					db.insert(TABLE_APP_DETAIL, null, values);
				}
				
				
			}
			
		} catch (SQLiteException e) {
			Log.i("insertAppDetails", "=" + e.getMessage());
		} catch (Exception e) {
			Log.i("insertAppDetails", "=" + e.getMessage());
		} finally {
			closeResources(null, db);
		}	
	}
	
	public ArrayList<AppDetails> getAppDetailList(Context context,String query){
		
		Cursor cursor = null;
		SQLiteDatabase db = getReadableDataBase(context);
		ArrayList<AppDetails> arrayList = new ArrayList<AppDetails>();
		try {
			
			cursor = db.rawQuery(query, null);
			if (cursor.moveToFirst()) {
				do {
					AppDetails appDetails = new AppDetails();

					appDetails.setRowId(cursor.getInt(0));
					appDetails.setCategory(cursor.getString(3));
					appDetails.setContentRating(cursor.getString(5));
					appDetails.setDatePublished(cursor.getString(6));
					appDetails.setDescription(cursor.getString(7));
					appDetails.setIcon(cursor.getString(10));
					appDetails.setName(cursor.getString(12));
					appDetails.setPackageId(cursor.getString(15));
					appDetails.setPrice(cursor.getString(16));
					appDetails.setRatingCount(cursor.getLong(18));
					appDetails.setRatingDisplay(cursor.getString(19));
					appDetails.setRatingFive(cursor.getLong(20));
					appDetails.setRatingFour(cursor.getLong(21));
					appDetails.setRatingThree(cursor.getLong(22));
					appDetails.setRatingTwo(cursor.getLong(23));
					appDetails.setRatingOne(cursor.getLong(24));
					appDetails.setRatingValue(cursor.getDouble(25));
					appDetails.setStoreCategory(cursor.getString(27));
					
					arrayList.add(appDetails);
				} while (cursor.moveToNext());
			}

		} catch (SQLiteException e) {
			Log.i("getFavourateList", "=" + e.getMessage());
		} catch (Exception e) {
			Log.i("getFavourateList", "=" + e.getMessage());
		} finally {
			closeResources(cursor, db);
		}
		// return contact list
		return arrayList;
	}
	
  public HashMap<String, ApplicationBean> getAppDetailHashMap(Context context){
		
		Cursor cursor = null;
		SQLiteDatabase db = getReadableDataBase(context);
		HashMap<String, ApplicationBean> mapOfApp = new HashMap<String, ApplicationBean>();
		String query = "SELECT * FROM " + TABLE_APP_DETAIL;
		try {
			
			cursor = db.rawQuery(query, null);
			Log.i("CURSOR", ""+cursor.getCount());
			if (cursor.moveToFirst()) {
				do {
					
					ApplicationBean appCategory = new ApplicationBean();
					appCategory.setTitle(cursor.getString(12));
					appCategory.setPackageName(cursor.getString(15));
					Log.i("getAppDetailHashMap", cursor.getString(15));
					//appCategory.setStatus(cursor.getString(0));
					appCategory.setApplogo(cursor.getString(10));
					appCategory.setStatus(cursor.getInt(38));
					Log.i("TESTING", appCategory.toString());
					mapOfApp.put(cursor.getString(15), appCategory);
				
				} while (cursor.moveToNext());
			}

		} catch (SQLiteException e) {
			Log.i("getFavourateList", "=" + e.getMessage());
		} catch (Exception e) {
			Log.i("getFavourateList", "=" + e.getMessage());
		} finally {
			closeResources(cursor, db);
		}
		//return contact list
		return mapOfApp;
	}	
	 
	public AppDetails getAppDetail(Context context,String query){
		Cursor cursor = null;
		SQLiteDatabase db = getReadableDataBase(context);
		
					AppDetails appDetails = new AppDetails();
                try
                {
                	cursor = db.rawQuery(query, null);
					appDetails.setRowId(cursor.getInt(0));
					appDetails.setAdsSupported(cursor.getString(1));
					appDetails.setAuthor(cursor.getString(2));
					appDetails.setCategory(cursor.getString(3));
					appDetails.setChangelog(cursor.getString(4));
					appDetails.setContentRating(cursor.getString(5));
					appDetails.setDatePublished(cursor.getString(6));
					appDetails.setDescription(cursor.getString(7));
					appDetails.setEditorsChoice(cursor.getString(8));
					appDetails.setFileSize(cursor.getString(9));  
					appDetails.setIcon(cursor.getString(10));
					appDetails.setInAppBilling(cursor.getString(11));
					appDetails.setName(cursor.getString(12));
					appDetails.setNumDownloads(cursor.getString(13));
					appDetails.setOperatingSystems(cursor.getString(14));
					appDetails.setPackageId(cursor.getString(15));
					appDetails.setPrice(cursor.getString(16));
					appDetails.setPriceCurrency(cursor.getString(17));
					appDetails.setRatingCount(cursor.getLong(18));
					appDetails.setRatingDisplay(cursor.getString(19));
					appDetails.setRatingFive(cursor.getLong(20));
					appDetails.setRatingFour(cursor.getLong(21));
					appDetails.setRatingThree(cursor.getLong(22));
					appDetails.setRatingTwo(cursor.getLong(23));
					appDetails.setRatingOne(cursor.getLong(24));
					appDetails.setRatingValue(cursor.getDouble(25));
					appDetails.setScreenshots(cursor.getString(26));
					appDetails.setStoreCategory(cursor.getString(27));
					appDetails.setSupportEmail(cursor.getString(28));
					appDetails.setSupportUrl(cursor.getString(29));
					appDetails.setTopDeveloper(cursor.getString(30));
					appDetails.setVersionName(cursor.getString(31));
					appDetails.setVideo(cursor.getString(32));
					
				

		} catch (SQLiteException e) {
			Log.i("getAppDetail", "=" + e.getMessage());
		} catch (Exception e) {
			Log.i("getAppDetail", "=" + e.getMessage());
		} finally {
			closeResources(cursor, db);
		}
		// return contact list
		return appDetails;
	}



	public void update(SQLiteDatabase db, String tblName, String columnName,
			String columnValue, ContentValues cv) {
		try {
			db.update(tblName, cv, columnName + " = ?",
					new String[] { columnValue });
		} catch (SQLiteException e) {
			Log.i("update", "=" + e.getMessage());
		} catch (Exception e) {
			Log.i("update", "=" + e.getMessage());
		}
	}

	
	// Delete favorites
	public int deleteAppDetail(String pakageId, Context context) {

		SQLiteDatabase db = getWritableDatabase(context);
		int ret = 0;
		try {

			ret = db.delete(TABLE_APP_DETAIL, COLUMN_PACKAGE + " = ?",
					new String[] { pakageId });

		} catch (SQLiteException e) {
			Log.i("deleteAppDetail", "=" + e.getMessage());

		} catch (Exception e) {
			Log.i("deleteAppDetail", "=" + e.getMessage());

		} finally {
			closeResources(null, db);
		}
		
		return ret;

	}

	

}
