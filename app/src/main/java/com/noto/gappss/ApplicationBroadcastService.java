package com.noto.gappss;

import java.util.HashSet;
import java.util.Set;

import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.preference.MySharedPreferences;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;

public class ApplicationBroadcastService extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
    	final String action = intent.getAction();
		if (action.equals(Intent.ACTION_PACKAGE_ADDED)) {
			Uri data = intent.getData();
			String pkgName = data.getEncodedSchemeSpecificPart();
			SharedPreferences file = MySharedPreferences.getInstance().getNewAppFile(context);
			Set<String> set = file.getStringSet(Constants.NEW_APPS, new HashSet<String>());
			set.add(pkgName);
			Editor editor = file.edit();			
			editor.putStringSet(Constants.NEW_APPS, set);
			editor.commit();
		}
    }
}