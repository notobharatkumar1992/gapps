package com.noto.gappss;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.fragments.InviteFriendFragment;
import com.noto.gappss.fragments.MyAppShareFragment;
import com.noto.gappss.utilities.ToastCustomClass;

public class MyAppShareActivity extends BaseFragmentActivity {
	public static final String TAG = Constants.MyAppShareActivity;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		enableBackButton();
		Bundle bundle = new Bundle();
		bundle.putString(Constants.type, TAG);
		replaceFragement(MyAppShareFragment.newInstance(bundle),MyAppShareFragment.TAG);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub

	}

	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,
			int requstCode) {
		if (super.callBackFromApi(object, fragment, requstCode)) {
			callBackCommon(object, requstCode, fragment);
		}
		return true;
	}

	@Override
	protected Boolean callBackFromApi(Object object, Activity act,
			int requstCode) {
		if (super.callBackFromApi(object, act, requstCode)) {
			callBackCommon(object, requstCode, act);
		}
		return true;
	}

	private void callBackCommon(Object object, int requstCode, Object luncher) {
		String message = "";
		JSONObject jObject;
		switch (requstCode) {
		case Constants.getAppVisibility:
			jObject = (JSONObject) object;
			message = jObject.optString(Constants.p_message);
			HashMap<String, ApplicationBean> mapOfApp = new HashMap<String, ApplicationBean>();
			if (jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW) {
				// Log.i("App catagory", "2");
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);
				if (luncher != null) {
					MyAppShareFragment freg = (MyAppShareFragment) luncher;
					freg.serviceResponse(jsonArray);
				}
			} else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getContactList:
			jObject = (JSONObject) object;
			message = jObject.optString("message");
			Log.i("App catagory", jObject.toString());
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);//JSONArray jsonArray = jObject.optJSONArray("user");

				ArrayList<Contact> arrayList = new ArrayList<>();
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObject = jsonArray.optJSONObject(i);
					Contact contact = new Contact();
					contact.setId(jsonObject.optString(Constants.USER_ID));
					contact.setName(jsonObject.optString(Constants.USER_NAME));
					contact.setMobileNo(jsonObject.optString(Constants.PH_NUMBER));
					arrayList.add(contact);
				}
				//InviteFriendFragment freg = (InviteFriendFragment) getFragmentByTag(ContactsFragment.TAG);
				if(luncher != null){
					InviteFriendFragment freg = (InviteFriendFragment)luncher;
					freg.serverResponsGetContactList(arrayList);
				}else {
					ToastCustomClass.showToast(this, message);
				}
			}
			break;

		}
	}
	/*	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.id.shar_app, menu);

		return super.onCreateOptionsMenu(menu);
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			BaseFragment topF = getTopFragment();
			if(topF != null && !topF.onBackPressedListener()){
				removeTopFragement();
				// disableBackButton();
				invalidateOptionsMenu();
				setTitleOnAction(MyAppShareFragment.TAG);
				return true;  
			}    
		}
		return super.onOptionsItemSelected(item);
	}



}
