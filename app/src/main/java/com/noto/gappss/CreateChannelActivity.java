package com.noto.gappss;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.fragments.CreateChannelFragment;
import com.noto.gappss.utilities.ToastCustomClass;

public class CreateChannelActivity extends BaseFragmentActivity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		//	Bundle bundle = new Bundle();
		//bundle.putString(Constants.USER_ID, "");
		enableBackButton();
		addFragement(CreateChannelFragment.newInstance(null), CreateChannelFragment.TAG, false);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
	}

	@Override
	protected void setValueOnUI() {
	}

	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,	int requstCode) {
		if(super.callBackFromApi(object, fragment, requstCode)){
			
		}
		return true;
	}

	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if(super.callBackFromApi(object, act, requstCode)){
			JSONObject jObject ;
			String message ="";
			switch (requstCode) {
			case Constants.createChannel:
                jObject = (JSONObject) object;
                message = jObject.optString("message");
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					int channelStatus = jObject.optInt(Constants.channelStatus);
					if (channelStatus == 1) {
					CreateChannelFragment freg = (CreateChannelFragment) getFragmentByTag(CreateChannelFragment.TAG);
					if(freg != null)
						freg.serviceResponse();
					}
					ToastCustomClass.showToast(this, message);
				}	else {
					ToastCustomClass.showToast(this, message);
				}			
				break;
			}
		}
		return true;
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.save, menu);
		return super.onCreateOptionsMenu(menu);
	}*/
}
