package com.noto.gappss;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.fragments.ChannelSettingFragment;
import com.noto.gappss.fragments.CreateChannelFragment;
import com.noto.gappss.fragments.ContactsFragment;
import com.noto.gappss.fragments.FollowedChannelsFragment;
import com.noto.gappss.fragments.InviteFriendFragment;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.utilities.ToastCustomClass;

public class InviteFriendActivity extends BaseFragmentActivity{


	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);		
		enableBackButton();
		setTitleOnAction(getResources().getString(R.string.invite_friend));
		addFragement(InviteFriendFragment.newInstance(getBundle()), ContactsFragment.TAG, false);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {

	}

	@Override
	protected void setValueOnUI() {

	}

	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,	int requstCode) {
		if(super.callBackFromApi(object, fragment, requstCode)){
			JSONObject jObject;
			String message ="";
			switch (requstCode) {
			case Constants.getContactList:
				jObject = (JSONObject) object;
				message = jObject.optString("message");
				Log.i("App catagory", jObject.toString());
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray(Constants.list);//JSONArray jsonArray = jObject.optJSONArray("user");
				/*	HashMap<Integer, ArrayList<Contact>> map = new HashMap<Integer, ArrayList<Contact>>();
					ArrayList<Integer> header = new ArrayList<Integer>();
					ArrayList<Contact> arrayList = null;
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jOb = jsonArray.optJSONObject(i);
						int userType = jOb.optInt(Constants.friendType);
						String userTypeStr = "";
						switch (userType) {
						case Constants.sentRequest:
							userTypeStr = Constants.sentRequestStr;
							break;
						case 1:
							userTypeStr = Constants.receivedReqFriendStr;
							break;
						case 2:
							userTypeStr = Constants.friendStr;
							break;
						case 3:
							userTypeStr = Constants.noFriendStr;
							break;
						}
						Contact invite = new Contact();
						invite.setId(jOb.optString("id"));
						invite.setName(jOb.optString(Constants.USER_NAME));
						invite.setMobileNo(jOb.optString(Constants.PH_NUMBER));
						invite.setType(userType);
						//invite.addType(userType);
						if(!map.containsKey(userTypeStr)){
							arrayList = new ArrayList<Contact>();							
							arrayList.add(invite);
							header.add(userType);
							map.put(userType, arrayList);	
						}else{
							map.get(userType).add(invite);
						}						
					}*/
					ArrayList<Contact> arrayList = new ArrayList<>();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.optJSONObject(i);
						Contact contact = new Contact();
						contact.setId(jsonObject.optString(Constants.USER_ID));
						contact.setName(jsonObject.optString(Constants.USER_NAME));
						contact.setMobileNo(jsonObject.optString(Constants.PH_NUMBER));
						arrayList.add(contact);
					}
					InviteFriendFragment freg = (InviteFriendFragment) getFragmentByTag(ContactsFragment.TAG);
					if(freg != null)
						freg.serverResponsGetContactList(arrayList);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.sendChannelInvitation:
				jObject = (JSONObject) object;
				message = jObject.optString(Constants.p_message);
				//	Log.i("App catagory", jObject.toString());
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray("list");
					ArrayList<Contact> arrayList = new ArrayList<Contact>();
					for (int i = 0; i < jsonArray.length(); i++) {
						Contact invite = new Contact();
						invite.setId("userId");
						invite.setStatus(jsonArray.optJSONObject(i).optInt("status"));
						invite.setMessage(jsonArray.optJSONObject(i).optString("message"));
						arrayList.add(invite);
					}
					//ContactsFragment freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
					if(fragment != null)
						((InviteFriendFragment)fragment).serverResponse();
						
					ToastCustomClass.showToast(this, message);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.sendInvitation:
				jObject = (JSONObject) object;
				message = jObject.optString("message");
				//	Log.i("App catagory", jObject.toString());
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray("list");
					ArrayList<Contact> arrayList = new ArrayList<Contact>();
					for (int i = 0; i < jsonArray.length(); i++) {
						Contact invite = new Contact();
						invite.setId("userId");
						invite.setStatus(jsonArray.optJSONObject(i).optInt("status"));
						invite.setMessage(jsonArray.optJSONObject(i).optString("message"));
						arrayList.add(invite);
					}
					ContactsFragment freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
					/*if(freg != null)
						freg.serverResponsGetContactList(arrayList);*/
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			}
		}
		return true;
	}

	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if(super.callBackFromApi(object, act, requstCode)){

		}
		return true;
	}
}
