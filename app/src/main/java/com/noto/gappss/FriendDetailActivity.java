package com.noto.gappss;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.fragments.CompareProfileFragment;
import com.noto.gappss.fragments.FriendAppsFragment;
import com.noto.gappss.fragments.FriendChannelListFragment;
import com.noto.gappss.fragments.FriendDetailFragment;
import com.noto.gappss.fragments.FriendFeedsFragment;
import com.noto.gappss.utilities.ToastCustomClass;

public class FriendDetailActivity extends BaseFragmentActivity{

	private Contact contact;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		initControls(savedInstanceState);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {		
		enableBackButton();
		replaceFragement(FriendDetailFragment.newInstance(getBundle()), FriendDetailFragment.TAG);
	}

	@Override
	protected void setValueOnUI() {

	}
	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,	int requstCode) {
		if (super.callBackFromApi(object, fragment, requstCode)) {
			JSONObject jObject;
			String message ="";
			switch (requstCode) {
			case Constants.getDevicesDetails:
				jObject = (JSONObject) object;
				message = jObject.optString(Constants.p_message);
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){

					if (fragment != null) 
						((CompareProfileFragment)fragment).setCompareData(jObject);

				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;

			case Constants.getFriendAllApps:
				jObject = (JSONObject) object;
				message = jObject.optString(Constants.p_message);
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray(Constants.list);
					if (fragment != null) 
						((FriendAppsFragment)fragment).setApps(jsonArray);

				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;

			case Constants.getFriendAllChannel:
				jObject = (JSONObject) object;
				message = jObject.optString(Constants.p_message);
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray(Constants.list);
					if (fragment != null) 
						((FriendChannelListFragment)fragment).setChannels(jsonArray);

				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.getFriendsFeeds:
				jObject = (JSONObject) object;
				message = jObject.optString(Constants.p_message);
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);
					//FollowedChannelsFragment mFragment = ((FollowedChannelsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
					if(fragment != null)
						((FriendFeedsFragment)fragment).setFeeds(jsonArray);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			}
		}
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {		
		switch (item.getItemId()) {
		case android.R.id.home:
			BaseFragment topF = getTopFragment();
			if(topF != null && !topF.onBackPressedListener()){
				removeTopFragement();
				contact = (Contact) getBundle().getSerializable(Constants.contact);
				setTitleOnAction(contact.getName());
				return true;  
			}
		}
		return super.onOptionsItemSelected(item);
	}
}
