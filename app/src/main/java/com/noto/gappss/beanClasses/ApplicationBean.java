package com.noto.gappss.beanClasses;

import java.io.Serializable;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;

import com.noto.gappss.R;

@SuppressLint("NewApi")
public class ApplicationBean implements Serializable{
	private int id=-1;
	private String des = "";	
	private Drawable applogo;
	private String title;
	private String packageName;
	private int status = 1;
	private boolean isInstalled = false;
	private String logoUrl;
	private double price;
	private long hot;
	private double rate;
	private long newlyDate;
	private boolean isChecked = false;
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public long getHot() {
		return hot;
	}
	public void setHot(long hot) {
		this.hot = hot;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public long getNewlyDate() {
		return newlyDate;
	}
	public void setNewlyDate(long newlyDate) {
		this.newlyDate = newlyDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public boolean isInstalled() {
		return isInstalled;
	}
	public void setInstalled(boolean isInstalled) {
		this.isInstalled = isInstalled;
	}
	public Drawable getApplogo(Context context) {
		if(applogo == null)
			applogo = context.getResources().getDrawable(R.drawable.ic_launcher);
		return applogo;
	}
	public void setApplogo(Drawable applogo) {
		this.applogo = applogo;
	}
	
	public void setApplogo(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	
	public String getApplogoUrl() {		
		return logoUrl;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "ApplicationBean [applogo=" + applogo + ", title=" + title
				+ ", packageName=" + packageName + ", status=" + status + "]";
	}
	public String getDescription() {
		return des;
	}
	public void setDescription(String des) {
		this.des = des;
	}
	public boolean isChecked() {
		return isChecked;
	}
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}	
}
