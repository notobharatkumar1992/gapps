package com.noto.gappss.beanClasses;

import java.io.Serializable;
import java.util.ArrayList;

public class ChannelDetail  implements Serializable{
	
	String channelId = "";
	String channelImage = "";
	String channelMode = "";
	String channelDescription = "";
	String channelName = "";
	String channelType = "";
	String createdOn = "";
	int isOwner = 0;
	int isSusbcribable = 0;
	int Members = 0;
	int totalPosts = 0;
	int visitors = 0;
	int Subscribers = 0;
	
	ArrayList<Post> postList = null;
	

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelImage() {
		return channelImage;
	}

	public void setChannelImage(String channelImage) {
		this.channelImage = channelImage;
	}

	public String getChannelMode() {
		return channelMode;
	}

	public void setChannelMode(String channelMode) {
		this.channelMode = channelMode;
	}

	public String getChannelDescription() {
		return channelDescription;
	}

	public void setChannelDescription(String channelDescription) {
		this.channelDescription = channelDescription;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public int getMembers() {
		return Members;
	}

	public void setMembers(int members) {
		Members = members;
	}

	
	public int getTotalPosts() {
		return totalPosts;
	}

	public void setTotalPosts(int totalPosts) {
		this.totalPosts = totalPosts;
	}

	public ArrayList<Post> getPostList() {
		return postList;
	}

	public void setPostList(ArrayList<Post> postList) {
		this.postList = postList;
	}

	public int getIsOwner() {
		return isOwner;
	}

	public void setIsOwner(int isOwner) {
		this.isOwner = isOwner;
	}

	public int getIsSusbcribable() {
		return isSusbcribable;
	}

	public void setIsSusbcribable(int isSusbcribable) {
		this.isSusbcribable = isSusbcribable;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public int getVisitors() {
		return visitors;
	}

	public void setVisitors(int visitors) {
		this.visitors = visitors;
	}

	public int getSubscribers() {
		return Subscribers;
	}

	public void setSubscribers(int subscribers) {
		Subscribers = subscribers;
	}
	
	
	
	

}
