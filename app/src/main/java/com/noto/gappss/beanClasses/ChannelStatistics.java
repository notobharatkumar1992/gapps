package com.noto.gappss.beanClasses;

public class ChannelStatistics {
	
	int totalPosts = 0;
	int totalSubscribers = 0;
	int totalVisitors = 0;
	
	
	public int getTotalPosts() {
		return totalPosts;
	}
	public void setTotalPosts(int totalPosts) {
		this.totalPosts = totalPosts;
	}
	public int getTotalSubscribers() {
		return totalSubscribers;
	}
	public void setTotalSubscribers(int totalSubscribers) {
		this.totalSubscribers = totalSubscribers;
	}
	public int getTotalVisitors() {
		return totalVisitors;
	}
	public void setTotalVisitors(int totalVisitors) {
		this.totalVisitors = totalVisitors;
	}
	
	
	

}
