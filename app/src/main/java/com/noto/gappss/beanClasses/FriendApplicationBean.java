package com.noto.gappss.beanClasses;

import java.util.ArrayList;

public class FriendApplicationBean {
	
	String friendName = "";
	String friendId = "";
	ArrayList<ApplicationBean> applicationBeans = null;
	
	
	public String getFriendName() {
		return friendName;
	}
	public void setFriendName(String friendName) {
		this.friendName = friendName;
	}
	public String getFriendId() {
		return friendId;
	}
	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}
	public ArrayList<ApplicationBean> getApplicationBeans() {
		return applicationBeans;
	}
	public void setApplicationBeans(ArrayList<ApplicationBean> applicationBeans) {
		this.applicationBeans = applicationBeans;
	}
	
	
	

}
