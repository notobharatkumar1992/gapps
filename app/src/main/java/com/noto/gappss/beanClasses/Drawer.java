package com.noto.gappss.beanClasses;

public class Drawer {
	private String title = "";
	private int textColorSelect ;
	private int textColorUnselect;
	private int iconSelect;
	private int iconUnselect;
	private boolean isChecked = false;
	
	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIconSelect() {
		return iconSelect;
	}

	public void setIconSelect(int iconSelect) {
		this.iconSelect = iconSelect;
	}

	public int getIconUnselect() {
		return iconUnselect;
	}

	public void setIconUnselect(int iconUnselect) {
		this.iconUnselect = iconUnselect;
	}

	public int getTextColorSelect() {
		return textColorSelect;
	}

	public void setTextColorSelect(int textColorSelect) {
		this.textColorSelect = textColorSelect;
	}

	public int getTextColorUnselect() {
		return textColorUnselect;
	}

	public void setTextColorUnselect(int textColorUnselect) {
		this.textColorUnselect = textColorUnselect;
	}
	
	

}
