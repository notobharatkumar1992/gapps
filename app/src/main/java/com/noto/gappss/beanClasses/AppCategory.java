package com.noto.gappss.beanClasses;

public class AppCategory {

	String catLogo;
	String catId = "";
	String catName = "";
	boolean isChecked = false;
	
	public String getCatLogo() {
		return catLogo;
	}
	public void setCatLogo(String catLogo) {
		this.catLogo = catLogo;
	}
	public String getCatId() {
		return catId;
	}
	public void setCatId(String catId) {
		this.catId = catId;
	}
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public boolean isChecked() {
		return isChecked;
	}
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
	
	@Override
	public String toString() {
		return "AppCategory [catLogo=" + catLogo + ", catId=" + catId
				+ ", catName=" + catName + ", isChecked=" + isChecked + "]";
	}
	
	
	

}
