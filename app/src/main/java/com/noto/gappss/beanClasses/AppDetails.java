package com.noto.gappss.beanClasses;

public class AppDetails {
	
	private int rowId;
	private String name = "";
	private String author ="";
	private String supportEmail ="";
	private String supportUrl ="";
	private String icon ="";
	private String category ="";
	private String storeCategory ="";
	private String price ="";
	private String priceCurrency ="";
	private String inAppBilling = "";
	private String adsSupported ="";
	private long ratingCount;
	private String ratingDisplay ="";
	private double ratingValue;
	private long ratingFive;
	private long ratingFour;
	private long ratingThree;
	private long ratingTwo;
	private long ratingOne;
	private String topDeveloper ="";
	private String editorsChoice ="";
	private String screenshots ="";
	private String description ="";
	private String changelog ="";
	private String datePublished ="";
	private String fileSize ="";
	private String numDownloads ="";
	private String versionName ="";
	private String video ="";
	private String operatingSystems ="";
	private String contentRating ="";
	private String packageId ="";
	private int unknown ;
	private String created ="";
	private String modified ="";
	private String status ="";
	private String rank ="";
	private int appStatus = 1;
	
	public int getUnknown() {
		return unknown;
	}
	public void setUnknown(int unknown) {
		this.unknown = unknown;
	}
	public int getRowId() {
		return rowId;
	}
	public void setRowId(int rowId) {
		this.rowId = rowId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getSupportEmail() {
		return supportEmail;
	}
	public void setSupportEmail(String supportEmail) {
		this.supportEmail = supportEmail;
	}
	public String getSupportUrl() {
		return supportUrl;
	}
	public void setSupportUrl(String supportUrl) {
		this.supportUrl = supportUrl;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getStoreCategory() {
		return storeCategory;
	}
	public void setStoreCategory(String storeCategory) {
		this.storeCategory = storeCategory;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getInAppBilling() {
		return inAppBilling;
	}
	public void setInAppBilling(String inAppBilling) {
		this.inAppBilling = inAppBilling;
	}
	public String getAdsSupported() {
		return adsSupported;
	}
	public void setAdsSupported(String adsSupported) {
		this.adsSupported = adsSupported;
	}
	public long getRatingCount() {
		return ratingCount;
	}
	public void setRatingCount(long ratingCount) {
		this.ratingCount = ratingCount;
	}
	public String getRatingDisplay() {
		return ratingDisplay;
	}
	public void setRatingDisplay(String ratingDisplay) {
		this.ratingDisplay = ratingDisplay;
	}
	public double getRatingValue() {
		return ratingValue;
	}
	public void setRatingValue(double ratingValue) {
		this.ratingValue = ratingValue;
	}
	public long getRatingFive() {
		return ratingFive;
	}
	public void setRatingFive(long ratingFive) {
		this.ratingFive = ratingFive;
	}
	public long getRatingFour() {
		return ratingFour;
	}
	public void setRatingFour(long ratingFour) {
		this.ratingFour = ratingFour;
	}
	public long getRatingThree() {
		return ratingThree;
	}
	public void setRatingThree(long ratingThree) {
		this.ratingThree = ratingThree;
	}
	public long getRatingTwo() {
		return ratingTwo;
	}
	public void setRatingTwo(long ratingTwo) {
		this.ratingTwo = ratingTwo;
	}
	public long getRatingOne() {
		return ratingOne;
	}
	public void setRatingOne(long ratingOne) {
		this.ratingOne = ratingOne;
	}
	public String getTopDeveloper() {
		return topDeveloper;
	}
	public void setTopDeveloper(String topDeveloper) {
		this.topDeveloper = topDeveloper;
	}
	public String getEditorsChoice() {
		return editorsChoice;
	}
	public void setEditorsChoice(String editorsChoice) {
		this.editorsChoice = editorsChoice;
	}
	public String getScreenshots() {
		return screenshots;
	}
	public void setScreenshots(String screenshots) {
		this.screenshots = screenshots;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getChangelog() {
		return changelog;
	}
	public void setChangelog(String changelog) {
		this.changelog = changelog;
	}
	public String getDatePublished() {
		return datePublished;
	}
	public void setDatePublished(String datePublished) {
		this.datePublished = datePublished;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getNumDownloads() {
		return numDownloads;
	}
	public void setNumDownloads(String numDownloads) {
		this.numDownloads = numDownloads;
	}
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getOperatingSystems() {
		return operatingSystems;
	}
	public String getPriceCurrency() {
		return priceCurrency;
	}
	public void setPriceCurrency(String priceCurrency) {
		this.priceCurrency = priceCurrency;
	}
	public String getVideo() {
		return video;
	}
	public void setVideo(String video) {
		this.video = video;
	}
	public void setOperatingSystems(String operatingSystems) {
		this.operatingSystems = operatingSystems;
	}
	public String getContentRating() {
		return contentRating;
	}
	public void setContentRating(String contentRating) {
		this.contentRating = contentRating;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public int getAppStatus() {
		return appStatus;
	}
	public void setAppStatus(int appStatus) {
		this.appStatus = appStatus;
	}
	@Override
	public String toString() {
		return "AppDetails [rowId=" + rowId + ", name=" + name + ", author="
				+ author + ", supportEmail=" + supportEmail + ", supportUrl="
				+ supportUrl + ", icon=" + icon + ", category=" + category
				+ ", storeCategory=" + storeCategory + ", price=" + price
				+ ", priceCurrency=" + priceCurrency + ", inAppBilling="
				+ inAppBilling + ", adsSupported=" + adsSupported
				+ ", ratingCount=" + ratingCount + ", ratingDisplay="
				+ ratingDisplay + ", ratingValue=" + ratingValue
				+ ", ratingFive=" + ratingFive + ", ratingFour=" + ratingFour
				+ ", ratingThree=" + ratingThree + ", ratingTwo=" + ratingTwo
				+ ", ratingOne=" + ratingOne + ", topDeveloper=" + topDeveloper
				+ ", editorsChoice=" + editorsChoice + ", screenshots="
				+ screenshots + ", description=" + description + ", changelog="
				+ changelog + ", datePublished=" + datePublished
				+ ", fileSize=" + fileSize + ", numDownloads=" + numDownloads
				+ ", versionName=" + versionName + ", video=" + video
				+ ", operatingSystems=" + operatingSystems + ", contentRating="
				+ contentRating + ", packageId=" + packageId + ", unknown="
				+ unknown + ", created=" + created + ", modified=" + modified
				+ ", status=" + status + ", rank=" + rank + "]";
	}
	
	
	

}
