package com.noto.gappss.beanClasses;

import java.io.Serializable;
import java.util.HashSet;

public class Contact implements Serializable{
	private String message = "";
	private int status = 0;
	private String id = "";
	private String name = "";
	private String img = "";
	private String mobileNo = "";
	private int type = -1;
	private boolean isChecked = false;
	//private static HashSet<Integer> typesSet = new HashSet<Integer>();

	/*public void addType(Integer type){
		typesSet.add(type);
	}
	
	public int countOfTypes(){
		return typesSet.size();
	}	
	*/
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public boolean isChecked() {
		return isChecked;
	}
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
	
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	@Override
	public String toString() {
		return "id="
				+ id + ", name=" + name + ", mobileNo=" + mobileNo
				+ ", isChecked=" + isChecked + "]";
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}





}
