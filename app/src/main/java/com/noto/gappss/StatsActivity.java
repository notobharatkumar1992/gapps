package com.noto.gappss;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelStatistics;
import com.noto.gappss.fragments.StatsFragment;
import com.noto.gappss.utilities.ToastCustomClass;

public class StatsActivity extends BaseFragmentActivity{
	
	@Override
		protected void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			enableBackButton();
			setContentView(R.layout.activity_container);
			replaceFragement(StatsFragment.newInstance(getBundle()), StatsFragment.TAG);
		}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if (super.callBackFromApi(object, act, requstCode)) {
			JSONObject jObject ;
			String message ="";
			switch (requstCode) {
			case Constants.channelStatistics:

				jObject = (JSONObject) object;
				//message = jObject.optString("message");
				//Log.i("App catagory", jObject.toString());
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONObject jsonObject = jObject.optJSONObject("response");
					ArrayList<ChannelStatistics> arrayList = new ArrayList<ChannelStatistics>();

					JSONObject jsonObject1 = jsonObject.optJSONObject("lastday");
					ChannelStatistics channelStatistics1 = new ChannelStatistics();
					channelStatistics1.setTotalPosts(jsonObject1.optInt("totalPosts"));
					channelStatistics1.setTotalSubscribers(jsonObject1.optInt("totalSubscribers"));
					channelStatistics1.setTotalVisitors(jsonObject1.optInt("totalVisitors"));
					arrayList.add(channelStatistics1);

					JSONObject jsonObject2 = jsonObject.optJSONObject("lastfourweek");
					ChannelStatistics channelStatistics2 = new ChannelStatistics();
					channelStatistics2.setTotalPosts(jsonObject2.optInt("totalPosts"));
					channelStatistics2.setTotalSubscribers(jsonObject2.optInt("totalSubscribers"));
					channelStatistics2.setTotalVisitors(jsonObject2.optInt("totalVisitors"));
					arrayList.add(channelStatistics2);

					JSONObject jsonObject3 = jsonObject.optJSONObject("lastweek");
					ChannelStatistics channelStatistics3 = new ChannelStatistics();
					channelStatistics3.setTotalPosts(jsonObject3.optInt("totalPosts"));
					channelStatistics3.setTotalSubscribers(jsonObject3.optInt("totalSubscribers"));
					channelStatistics3.setTotalVisitors(jsonObject3.optInt("totalVisitors"));
					arrayList.add(channelStatistics3);

					JSONObject jsonObject4 = jsonObject.optJSONObject("previousmonth");
					ChannelStatistics channelStatistics4 = new ChannelStatistics();
					channelStatistics4.setTotalPosts(jsonObject4.optInt("totalPosts"));
					channelStatistics4.setTotalSubscribers(jsonObject4.optInt("totalSubscribers"));
					channelStatistics4.setTotalVisitors(jsonObject4.optInt("totalVisitors"));
					arrayList.add(channelStatistics4);
					StatsFragment freg = (StatsFragment) getFragmentByTag(StatsFragment.TAG);
					if(freg != null)
						freg.serverResponse(arrayList);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			}
			
		}
		
		return true;
	}
	

}
