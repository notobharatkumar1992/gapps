package com.noto.gappss;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.fragments.GappssCommunityFragment;
import com.noto.gappss.utilities.ToastCustomClass;

public class GappssCommunityActivity extends BaseFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		enableBackButton();
		replaceFragement(GappssCommunityFragment.newInstance(getBundle()),
				GappssCommunityFragment.TAG);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub

	}

	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,
			int requstCode) {
		// TODO Auto-generated method stub
		if (super.callBackFromApi(object, fragment, requstCode)) {
			JSONObject jObject;
			String message = "";
			switch (requstCode) {
			case Constants.getGappssCommunityApps:

				jObject = (JSONObject) object;
				message = jObject.optString("message");
				Log.i("getGappssCommunityApps", "getGappssCommunityApps");
				if (jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW) {
					JSONArray jsonObject = jObject
							.optJSONArray(Constants.GAPPSSCommunityApps);
					// FollowedChannelsFragment mFragment =
					// ((FollowedChannelsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
					if (fragment != null)
						((GappssCommunityFragment) fragment)
								.setCommunity(jsonObject);
					Log.i("getGappssCommunityApps11", "getGappssCommunityApps");

				} else {
					if (fragment != null)
						((GappssCommunityFragment) fragment).setCommunity(null);
					ToastCustomClass.showToast(this, message);
				}
				break;

			}
		}
		return true;
	}

}
