package com.noto.gappss;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.fragments.ChannelSettingFragment;
import com.noto.gappss.fragments.CreateChannelFragment;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.utilities.ToastCustomClass;

public class ChannelSettingActivity extends BaseFragmentActivity{
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
	//	Bundle bundle = new Bundle();
		//bundle.putString(Constants.USER_ID, "");
		enableBackButton();
		setTitleOnAction(getResources().getString(R.string.channelSettings));
		addFragement(ChannelSettingFragment.newInstance(null), ChannelSettingFragment.TAG, false);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {

	}

	@Override
	protected void setValueOnUI() {

	}

	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,	int requstCode) {
		if(super.callBackFromApi(object, fragment, requstCode)){
			

		}
		return true;
	}

	@Override//{"status":1,"dataflow":0,"message":"Your channel settings have been updated."}

	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if(super.callBackFromApi(object, act, requstCode)){
			JSONObject jObject ;
			String message ="";
			switch (requstCode) {
			case Constants.getChannelSettings:
				jObject = (JSONObject) object;
				
			    message = jObject.optString(Constants.p_message);
					if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
						ChannelSettingFragment freg = (ChannelSettingFragment) getFragmentByTag(ChannelSettingFragment.TAG);
						if(freg != null)
							freg.serviceResponse(jObject);
					}else {
						ToastCustomClass.showToast(this, message);
					}
				
				break;
			case Constants.setChannelSettings:
				 jObject = (JSONObject) object;
				 message = jObject.optString(Constants.p_message);
					if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
						ToastCustomClass.showToast(this, message);
					}else {
						ToastCustomClass.showToast(this, message);
					}
				
				break;
			}
		}
		return true;
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.save, menu);

		return super.onCreateOptionsMenu(menu);
	}*/
}
