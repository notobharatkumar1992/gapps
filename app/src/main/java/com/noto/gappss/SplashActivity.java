package com.noto.gappss;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.preference.MySharedPreferences;

public class SplashActivity extends BaseFragmentActivity implements OnClickListener {
    private static final int SPLASH_DURATION = 1000; // 2 seconds
    private RelativeLayout rel_top;
    private boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_splash);
        initControls(savedInstanceState);
        setValueOnUI();
        //Log.i("hii", null);
        //startMyActivity(HomeActivity.class, null);
        startHome();
        //startMyActivity(HomeActivityTest.class, null);
        //Log.i("Android Device VERSION Name", UtilsClass.getDeviceSDKVersionName(this));
    }

    @Override
    protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
        if (super.callBackFromApi(object, act, requstCode)) {
            switch (requstCode) {

            }
        }

        return true;
    }


    @Override
    protected void initControls(Bundle savedInstanceState) {


    }

    @Override
    protected void setValueOnUI() {
        /*View lay[] = {findViewById(R.id.imageView1)};
        UtilsClass.animationLittleUp(this, lay, 1000, 500);*/
        setHeightWidth();
    }


    private void setHeightWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int widthPixel = displayMetrics.widthPixels;
        int heightPixel = displayMetrics.heightPixels;
        //System.out.println("In Pixels Width : "+widthPixel+" Height : "+heightPixel);

        MyApplication.getApplication().setWidthPixel(widthPixel);
        MyApplication.getApplication().setHeightPixel(heightPixel);
    }

    /*@Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        bundle.putInt(com.noto.mapitrealtor.baseClasses.Constants.position,(Integer) v.getTag());
        startMyActivity(HomeActivity.class, bundle);
        finish();
    }*/
    @Override
    public void onClick(View v) {

    }

    private void startHome() {
        //registerReceiver();
        Handler handler = new Handler();
        // run a thread after 2 seconds to start the home screen
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // LoginAct
                if (!MySharedPreferences.getInstance().getBoolean(SplashActivity.this, Constants.VERIFY, false)) {
                    startMyActivity(WelcomeActivity.class, null);
                    //startMyActivity(FriendDetailActivity.class, null);
                } else if (!MySharedPreferences.getInstance().getBoolean(SplashActivity.this, Constants.LOGIN, false)) {
                    startMyActivity(LoginSignupActivity.class, null);
                    //startMyActivity(HomeActivity.class, null);
                } else {
                    startMyActivity(NewAppActivity.class, null);
                }
                finish();
            }
        }, SPLASH_DURATION);
    }

	/*void registerReceiver() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_PACKAGE_ADDED);
		filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
		filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
		filter.addDataScheme("package_name"); 
		ApplicationBroadcastService br = new ApplicationBroadcastService();
		registerReceiver(br, filter);
	}	*/
}
