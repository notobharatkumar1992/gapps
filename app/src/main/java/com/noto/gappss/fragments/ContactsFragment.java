package com.noto.gappss.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import com.noto.gappss.FriendDetailActivity;
import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.SearchActivity;
import com.noto.gappss.adapter.ContactAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class ContactsFragment extends BaseFragment implements OnItemClickListener{
	private ListView listView;
	private SearchView search;
	//TextView btn_invite;
	public static String TAG = Constants.contactT;
	ContactAdapter adapter;
	//ArrayList<AppCategory> arrayList = new ArrayList<AppCategory>();
	HashMap<String, Contact> mapContacts = new HashMap<String, Contact>();
	private ArrayList<Contact> list;
	int itemClickPosition = -1;
	public ContactsFragment(){		
	}

	public static ContactsFragment newInstance(Bundle bundle) {
		ContactsFragment fragment = new ContactsFragment();
		if(bundle !=  null)
			fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		//disableBackButton();
		setHasOptionsMenu(true);
		setTitleOnAction(TAG, false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//return super.onCreateView(inflater, container, savedInstanceState);
		//setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_contacts, null);
		initUi(view);
		setListener();
		setValueOnUi();
		//Log.i("sdf", null);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}	

	@Override
	public void onClick(View v) {
	}

	@Override
	protected void initUi(View view) {
		search = (SearchView) view.findViewById(R.id.search);
		listView = (ListView) view.findViewById(R.id.list_item);		
	}

	@Override
	protected void setValueOnUi() {
		new DownloadContact().execute("");
	}

	@Override
	protected void setListener() {		
		listView.setOnItemClickListener(this);
		listView.setItemsCanFocus(true);

		//btn_invite.setOnClickListener(this);
		listView.setTextFilterEnabled(true);
		search.setIconifiedByDefault(false);
		search.setQueryHint("Search Contacts");
		search.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String query) {
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				if (TextUtils.isEmpty(newText.toString())) {
					listView.clearTextFilter();
					//Log.i("if onQueryTextChange", "onQueryTextChange=="+newText.toString());
				} else {
					listView.setFilterText(newText.toString());
					//Log.i("else onQueryTextChange", "onQueryTextChange=="+newText.toString());
				}
				return true;
			}
		});
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}

	private class DownloadContact extends AsyncTask<String, Void, String> {
		private ProgressDialog mProgressDialog;
		//ArrayList<Invite> arrayList = new ArrayList<>();
		String contacts="";
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(getActivity());
			// Set progressdialog title
			mProgressDialog.setTitle("Please wait");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading contacts...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected String doInBackground(String... data) {
			contacts = getContactWithAllPhoneNumber(getActivity());
			return null;
		}

		@Override
		protected void onPostExecute(String str) {
			// Close progressdialog
			mProgressDialog.dismiss();
			ApiManager.getInstance().getContactList(ContactsFragment.this, contacts);
		}
	}

	/*	public void serverResponsGetContactList(ArrayList<Contact> arrayList){
		Log.i("Testing", "=="+arrayList.size());
		for (int i = 0; i < arrayList.size(); i++) {
			Contact contact = mapContacts.get(arrayList.get(i).getMobileNo());
			arrayList.get(i).setName(contact.getName());
		}

		adapter = new InviteAdapter(ContactsFragment.this, arrayList);
		list_item.setAdapter(adapter);

		//We will finish the fragment here
		//removeTopFragment();


	}*/

	/*public void serverResponsInviteFriend(ArrayList<Contact> arrayList){
		adapter.notifyList(arrayList);
	}*/

	public  String getContactWithAllPhoneNumber(Context context) {
		StringBuffer totalNumber = new StringBuffer();
		//MySharedPreferences.getInstance().getString(getActivity(), Constants.p_mobile_no, "");
		totalNumber.append("9929572110"+"|");
		totalNumber.append("8302510544"+"|");
		totalNumber.append("0633031052"+"|");
		totalNumber.append("0641424322"+"|");
		totalNumber.append("0613359720"+"|");

		Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
		while (phones.moveToNext())
		{
			String displayName = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
			String mobile = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			if (mobile != null && !mobile.equals("")) {				
				if(isValidMobile(mobile)) {
					try {
						mobile = mobile.substring(mobile.length() - 10);
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
					totalNumber.append(mobile+"|");
					Contact invite = new Contact();
					invite.setName(displayName);
					invite.setMobileNo(mobile);
					mapContacts.put(mobile, invite);									
				}
			}
		}
		phones.close();	
		return UtilsClass.removeStringLastChar(totalNumber, '|');
	}

	/*public  String getDigitString(String string){

		char c;
		StringBuffer strBuff = new StringBuffer();
		for (int i = 0; i < string.length() ; i++) {
			c = string.charAt(i);

			if (Character.isDigit(c)) {
				strBuff.append(c);
			}
		}
		return strBuff.toString();
	}*/
	private boolean isValidMobile(String phone) 
	{
		return android.util.Patterns.PHONE.matcher(phone).matches();   
	}
	/*private boolean isValidMobileSe(String phone2) 
	{
	    boolean check=false;
	    if(!Pattern.matches("[a-zA-Z]+", text))
	    {
	        if(phone2.length() < 6 || phone2.length() > 13)
	        {
	            check = false;
	            txtPhone.setError("Not Valid Number");
	        }
	        else
	        {
	            check = true;
	        }
	    }
	    else
	    {
	        check=false;
	    }
	    return check;
	}*/


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		//ToastCustomClass.showToast(getActivity(), "TESTING");
		Contact contact = (Contact) view.getTag(R.id.invite_title);
		int pos = (Integer)view.getTag(R.id.invite_checkbox);
		contact.getType();
		if(contact != null){

			switch (contact.getType()) {
			case Constants.sentRequest:
				ToastCustomClass.showToast(getActivity(), getActivity().getResources().getString(R.string.not_frnd));
				break;
			case Constants.receivedRequest:			
				ToastCustomClass.showToast(getActivity(), getActivity().getResources().getString(R.string.not_frnd));
				break;
			case Constants.noFriend:
				itemClickPosition = pos;
				showAlertDialog(getActivity(), contact.getName()+" "+getActivity().getResources().getString(R.string.not_frnd_request), "Alert!", "Send",contact.getId());
				break;
			case Constants.friend:
				Bundle bundle = new Bundle();
				bundle.putSerializable(Constants.contact, contact);
				startMyActivity(FriendDetailActivity.class, bundle);
				break;
			} 

		}
	}

	public void serverResponsGetContactList(ArrayList<Integer> header,	HashMap<Integer, ArrayList<Contact>> map) {
		/*for (int i = 0; i < arrayList.size(); i++) {
			Contact contact = mapContacts.get(arrayList.get(i).getMobileNo());
			arrayList.get(i).setName(contact.getName());
		}*/

		list = new ArrayList<Contact>();
		Set<Integer> keySet = map.keySet();
		Iterator<Integer> iterator = keySet.iterator();
		while (iterator.hasNext()) {
			Integer type = (Integer) iterator.next();
			list.addAll(map.get(type));			
		}
		MyApplication.getApplication().setContactList(list);
		adapter = new ContactAdapter(ContactsFragment.this, header, map);
		listView.setAdapter(adapter);
	}

	public void serverResponsSentRequest() {
		/*for (int i = 0; i < arrayList.size(); i++) {
			Contact contact = mapContacts.get(arrayList.get(i).getMobileNo());
			arrayList.get(i).setName(contact.getName());
		}*/

		ArrayList<Contact> sendList = adapter.getFriendList(Constants.sentRequest);
		ArrayList<Contact> noFrndList = adapter.getFriendList(Constants.noFriend);
		if (noFrndList != null) {
			for (int i = 0; i < noFrndList.size(); i++) {
				Contact contact = noFrndList.get(i);
				if (contact.isChecked()) {
					contact.setChecked(false);
					contact.setType(Constants.sentRequest);
					sendList.add(noFrndList.remove(i));
				}
			}			
		}

		adapter.notifyDataSetChanged();
	}

	public void serverResponsAcceptCancelRequest(int requestType) {
		/*for (int i = 0; i < arrayList.size(); i++) {
			Contact contact = mapContacts.get(arrayList.get(i).getMobileNo());
			arrayList.get(i).setName(contact.getName());
		}*/
		ArrayList<Contact> recievedList = adapter.getFriendList(Constants.receivedRequest);
		ArrayList<Contact> noFrndList = adapter.getFriendList(requestType);
		if (recievedList != null) {
			for (int i = 0; i < recievedList.size(); i++) {
				Contact contact = recievedList.get(i);
				if (contact.isChecked()) {
					contact.setChecked(false);					
					contact.setType(requestType);
					noFrndList.add(recievedList.remove(i));
				}
			}				
		}
		adapter.notifyDataSetChanged();
	}



	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		//super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.contact, menu);				
		// Get the SearchView and set the searchable configuration
		/*SearchView searchView = (SearchView) menu.findItem(R.id.menu_search) .getActionView();
		searchView.setOnQueryTextListener(this);*/
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.i_invite:
			sendInvitation();
			break;
		case R.id.i_search:			
			//startMyActivity(SearchActivity.class, null);
			
			if (search.getVisibility() == View.VISIBLE) {
				search.setVisibility(View.GONE);
			}else {
				search.setVisibility(View.VISIBLE);
			}
			break;
		}
		return false;
	}

	private void sendInvitation(){
		if (adapter != null) {
			ArrayList<Contact> arrayList = adapter.getNoFriendList();
			if (arrayList != null && arrayList.size() > 0) {
				StringBuffer receiverId = new StringBuffer();
				for (int i = 0; i < arrayList.size(); i++) {
					if (arrayList.get(i).isChecked()) {
						receiverId.append(arrayList.get(i).getId());
						receiverId.append("|");
					}
				}
				if(receiverId.length() > 0){
					String result = UtilsClass.removeStringLastChar(receiverId, '|');
					ApiManager.getInstance().sendInvitation((BaseFragmentActivity)getActivity(),result,Constants.sendInvitation);
				}
				else{
					ToastCustomClass.showToast(getActivity(), "Please select contact!");
				}
			}
		}	
	}

	public AlertDialog showAlertDialog(Context context,String msg,String title,String button,final String string){

		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
		alertBuilder.setTitle(title);
		alertBuilder.setMessage(msg);			
		alertBuilder.setNeutralButton("Cancel", null);	        
		alertBuilder.setPositiveButton(button, new OnClickListener() {				
			@Override
			public void onClick(DialogInterface dialog, int which) {					
				ApiManager.getInstance().sendInvitation((BaseFragmentActivity)getActivity(),string,Constants.sendInvitationAlertDialog);
			}
		});

		AlertDialog alertDialog = alertBuilder.create();
		alertDialog.show();
		return alertDialog;
	}

	public void serverResponsSentRequestAlertDialog() {
		ArrayList<Contact> sendList = adapter.getFriendList(Constants.sentRequest);
		ArrayList<Contact> noFrndList = adapter.getFriendList(Constants.noFriend);
		if (noFrndList != null) {
			Contact contact = noFrndList.get(itemClickPosition);
			contact.setChecked(false);
			contact.setType(Constants.sentRequest);
			sendList.add(noFrndList.remove(itemClickPosition));			
		}

		adapter.notifyDataSetChanged();
	}
}
