package com.noto.gappss.fragments;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noto.gappss.AppDialogActivity;
import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.CameraGallery;
import com.noto.gappss.utilities.GetRealPath;
import com.noto.gappss.utilities.ToastCustomClass;

import eu.janmuller.android.simplecropimage.CropImage;

public class CreatePostFragment extends BaseFragment {

	public static String TAG = "CREATE_POST";

	private ImageView img_logo;
	private EditText edit_title;
	private EditText edit_des;
	private TextView btn_get_app_link;
	private TextView btn_post;
	//private ImageView app_logo;
	//private TextView app_name;
	private String path = "";
	private StringBuffer appPack = null;
	private StringBuffer appName = null;
	private StringBuffer appUrl = null;
	private StringBuffer cat = null;
	private String fileUrl = "";
	private String cId = "";

	private LinearLayout lin;


	public CreatePostFragment() {
	}

	public static CreatePostFragment newInstance(Bundle bundle) {
		CreatePostFragment fragment = new CreatePostFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// return super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_create_post, null);
		initUi(view);
		setListener();
		//Log.i("asdf", null);
		return view;
	}

	@Override
	public void onAttach(Activity activity) {	
		super.onAttach(activity);
		Bundle bundle = getArguments();
		cId  = bundle.getString(Constants.channelId);		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.img_logo:
			new CameraGallery().openImageSource(getActivity() , this);
			break;
		case R.id.btn_get_app_link:			
			startActivityForResult(new Intent(getActivity(),AppDialogActivity.class),Constants.requestCode);
			break;
		case R.id.btn_post:
			if (edit_title.getText().toString().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(getActivity(), getActivity().getResources().getString(R.string.val_title));
			}else if (edit_des.getText().toString().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(getActivity(), getActivity().getResources().getString(R.string.val_post_des));
			}else {				
				ApiManager.getInstance().createPost((BaseFragmentActivity)getActivity(), cId, edit_title.getText().toString(), edit_des.getText().toString(), appPack.toString(), appName.toString(), appUrl.toString(), cat.toString(), path);
			}
			break;
		}
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		img_logo = (ImageView) view.findViewById(R.id.img_logo);
		edit_title = (EditText) view.findViewById(R.id.edit_title);
		edit_des = (EditText) view.findViewById(R.id.edit_des);
		btn_get_app_link = (TextView) view.findViewById(R.id.btn_get_app_link);
		btn_post = (TextView) view.findViewById(R.id.btn_post);
		lin = (LinearLayout) view.findViewById(R.id.lin);

	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		img_logo.setOnClickListener(this);
		btn_get_app_link.setOnClickListener(this);
		btn_post.setOnClickListener(this);

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.i("onActivityResult", "resultCode==" + resultCode
				+ "  requestCode==" + requestCode + " Activity.RESULT_OK="
				+ Activity.RESULT_OK);
		if (resultCode == Activity.RESULT_OK) {

			switch (requestCode) {
			case CameraGallery.REQUEST_CODE_GALLERY: {
				Log.i("REQUEST_CODE_GALLERY", "REQUEST_CODE_GALLERY");
				try {
					CameraGallery cameraAct = new CameraGallery();
					/*
					 * InputStream inputStream =
					 * getActivity().getContentResolver
					 * ().openInputStream(data.getData());
					 * Log.i("REQUEST_CODE_GALLERY",
					 * "REQUEST_CODE_GALLERY=="+cameraAct.mFileTemp.getPath());
					 * FileOutputStream fileOutputStream = new
					 * FileOutputStream(cameraAct.mFileTemp.getPath());
					 * cameraAct.copyStream(inputStream, fileOutputStream);
					 * fileOutputStream.close(); inputStream.close();
					 */

					String realpath;
					if (Build.VERSION.SDK_INT < 11) {
						realpath = GetRealPath.getRealPathFromURI_BelowAPI11(
								getActivity(), data.getData());

					} else if (Build.VERSION.SDK_INT < 19) {
						realpath = GetRealPath.getRealPathFromURI_API11to18(
								getActivity(), data.getData());

					} else {
						realpath = GetRealPath.getRealPathFromURI_API19(
								getActivity(), data.getData());

					}

					// cameraAct.startCropImage(getActivity(), null,
					// cameraAct.mFileTemp.getPath());
					Log.i("PATHSSSSSSSS", "===" + realpath);
					//this.path = realpath;
					cameraAct.startCropImage(getActivity(), this, realpath);
				} catch (Exception e) {
					Log.e(TAG, "Error while creating temp file", e);
				}
				break;
			}
			case CameraGallery.REQUEST_CODE_TAKE_PICTURE: {
				Log.i("REQUEST_CODE_TAKE_PICTURE", "REQUEST_CODE_TAKE_PICTURE");
				CameraGallery cameraAct = new CameraGallery();
				// String action = data.getAction();
				// Parcelable parcelable = data.getParcelableExtra("data");
				// Bitmap bit = (Bitmap) data.getExtras().get("data");
				// Uri selectedImageUri = Uri.fromFile(CameraGallery.mFileTemp);
				// Uri selectedImageUri = data.getData();
				if (CameraGallery.mFileTemp != null) {
					cameraAct.startCropImage(getActivity(), this,
							CameraGallery.mFileTemp.getPath());
				}
				break;
			}
			case CameraGallery.REQUEST_CODE_CROP_IMAGE: {
				Log.i("REQUEST_CODE_CROP_IMAGE", "REQUEST_CODE_CROP_IMAGE");
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				// String path = CameraGallery.mFileTemp.getPath();
				// String caption = data.getStringExtra(CropImage.IMAGE_TITLE);
				if (path == null) {
					return;
				}
				try {
					this.path = path;
					/*
					 * int width =
					 * (MyApplication.getApplication().getWidthPixel()/3)-40;
					 * LinearLayout layout = new LinearLayout(getActivity());
					 * layout.setOrientation(LinearLayout.HORIZONTAL);
					 * layout.setLayoutParams(new
					 * LayoutParams(LayoutParams.WRAP_CONTENT,
					 * LayoutParams.WRAP_CONTENT));
					 * 
					 * LinearLayout.LayoutParams imageViewLayoutParams = new
					 * LinearLayout.LayoutParams(width, width);
					 * imageViewLayoutParams.setMargins(0, 0, 20, 0);
					 * 
					 * ImageView imageView = new ImageView(getActivity());
					 */
					// img_logo.setLayoutParams(imageViewLayoutParams);
					// img_logo.setBackgroundResource(R.drawable.image_fram9);
					//Log.i("PATHSSSSS", path);
					//ToastCustomClass.showToast(getActivity(), "Set Image");
					img_logo.setImageBitmap(BitmapFactory.decodeFile(path));
					img_logo.setScaleType(ScaleType.CENTER_CROP);

					CameraGallery.mFileTemp = null;

				} catch (Exception e) {
					e.printStackTrace();
					ToastCustomClass.showToast(getActivity(),"Something went wrong!");
				}
				break;
			}
			case Constants.requestCode: {
				//https://play.google.com/store/apps/details?id=com.mobisystems.msdict.embedded.wireless.oxford.dictionaryofenglish
				//Bundle bundle = data.getBundleExtra(Constants.bundleArg);
				LayoutInflater inflator = LayoutInflater.from(getActivity());
				//String res = data.getStringExtra(Constants.response);
				ArrayList<ApplicationBean> appList = MyApplication.getApplication().getApplicationBean();
				if (appList != null && appList.size() > 0) {
					appPack = new StringBuffer();
					appName = new StringBuffer();
					appUrl = new StringBuffer();
					cat = new StringBuffer();
					for (int i = 0; i < appList.size(); i++) {						
						View view = inflator.inflate(R.layout.app_block, null);
						ImageView app_logo = (ImageView) view.findViewById(R.id.app_logo);
						//TextView app_name = (TextView) view.findViewById(R.id.app_name);
						app_logo.setVisibility(View.VISIBLE);
						//app_name.setVisibility(View.VISIBLE);
						app_logo.setImageDrawable(appList.get(i).getApplogo(getActivity()));
						//app_name.setText(appList.get(i).getTitle());
						//app_name.set
						if(i < appList.size()-1){
							this.appName.append(appList.get(i).getTitle()+",");
							this.appPack.append(appList.get(i).getPackageName()+",");
							this.appUrl.append("https://play.google.com/store/apps/details?id="+appList.get(i).getPackageName()+",");
						}
						else{
							this.appName.append(appList.get(i).getTitle());
							this.appPack.append(appList.get(i).getPackageName());
							this.appUrl.append("https://play.google.com/store/apps/details?id="+appList.get(i).getPackageName());
						}
						//this.appPack = applicationBean.getPackageName();
						//this.appUrl = "https://play.google.com/store/apps/details?id="+applicationBean.getPackageName();
						lin.addView(view);
					}
				}
				//	Log.i("ACTIVITY RESULTTTTTTT", applicationBean.toString());
				/*JSONObject jsonObject = new JSONObject(data.getStringExtra(Constants.response));
				this.cat = jsonObject.optString("category");*/
				break;
			}
			}
		}
	}

	public void response() {
		Intent intent=new Intent();  
		intent.putExtra("Result","ok");  
		getActivity().setResult(Activity.RESULT_OK,intent);  
		getActivity().finish();
	}
}
