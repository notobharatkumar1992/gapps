package com.noto.gappss.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noto.gappss.ChannelSettingActivity;
import com.noto.gappss.CreatePostActivity;
import com.noto.gappss.InviteFriendActivity;
import com.noto.gappss.PostListActivity;
import com.noto.gappss.R;
import com.noto.gappss.StatsActivity;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelDetail;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.preference.MySharedPreferences;
import com.squareup.picasso.Picasso;

public class YourChannelDetailFragment extends BaseFragment {

	public static String TAG = "MY_CHANNEL_DETAIL";
	private TextView text_subs;
	private TextView text_post;
	private TextView text_visit;
	private TextView btn_stats;
	//private TextView btn_invite;
	//private TextView btn_addPost;
	private String cId = "";
	private RelativeLayout postLayout;
	private TextView btn_post_first;
	private TextView post_title;
	private TextView post_des;
	private ImageView post_img;
	private String postId = "";
	private RelativeLayout rel_title_post;
	private RelativeLayout lin_left;
	private boolean postList = false;
	private ChannelDetail channel;
	private String userId;

	public YourChannelDetailFragment() {
	}

	public static YourChannelDetailFragment newInstance(Bundle bundle) {
		YourChannelDetailFragment fragment = new YourChannelDetailFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// return super.onCreateView(inflater, container, savedInstanceState);
		 setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_my_channel_name, null);
		initUi(view);		
		setListener();
		return view;
	}

	
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		// TODO Auto-generated method stub
		super.setUserVisibleHint(isVisibleToUser);
		//ToastCustomClass.showToast(getActivity(), "onStart");
		//Log.i("vvvvvvvvvvvvvvvv", "setUserVisibleHint"+isVisibleToUser);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setValueOnUi();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {		
		case R.id.btn_stats:
			//if (!postId.equals("")) {
				Bundle bundle = new Bundle();
				bundle.putString(Constants.channelId, cId);
				bundle.putString(Constants.postId, postId);
				//addFragment(StatsFragment.newInstance(bundle), StatsFragment.TAG, true);
				startMyActivity(StatsActivity.class, bundle);
			//}			
			break;
		case R.id.lin_left:
			if (postList) {
				Bundle bundle1 = new Bundle();
				bundle1.putString(Constants.channelId, cId);
				bundle1.putSerializable(Constants.channel, channel);
				//startMyActivity(FollowingChannelDetailActivity.class, bundle);
				startMyActivity(PostListActivity.class, bundle1);		
			}else {
				/*if (cId != null && !cId.equalsIgnoreCase("")) {
					Bundle bundle1 = new Bundle();
					bundle1.putString(Constants.channelId, cId);
					startMyActivity(CreatePostActivity.class, bundle1);
				}*/
				if (cId != null && !cId.equalsIgnoreCase("")) {
					Bundle bundle1 = new Bundle();
					bundle1.putString(Constants.channelId, cId);
					//startMyActivity(CreatePostActivity.class, bundle);
					Intent intent = new Intent(getActivity(),CreatePostActivity.class);
					intent.putExtra(Constants.bundleArg, bundle1);
					startActivityForResult(intent, Constants.requestCode);
				}
			}
			break;
		}
	}

	@Override
	public void onAttach(Activity activity) {	
		super.onAttach(activity);
		Bundle bundle = getArguments();
		channel =(ChannelDetail) bundle.getSerializable(Constants.channel);
		userId = bundle.getString(Constants.USER_ID);
		cId = bundle.getString(Constants.channelId);		
	}

	@Override
	protected void initUi(View view) {
		setTitleOnAction(getString(R.string.channels), false);
		LinearLayout stats = (LinearLayout) view.findViewById(R.id.lin_second);
		stats.setOnClickListener(this);
		postLayout = (RelativeLayout) view.findViewById(R.id.lin_left);
		post_title = (TextView) view.findViewById(R.id.post_title);
		post_des = (TextView) view.findViewById(R.id.post_des);
		post_img = (ImageView) view.findViewById(R.id.post_img);
		btn_post_first = (TextView) view.findViewById(R.id.btn_post_first);
		text_subs = (TextView) view.findViewById(R.id.text_subs);
		text_post = (TextView) view.findViewById(R.id.text_post);
		text_visit = (TextView) view.findViewById(R.id.text_visit);
		btn_stats = (TextView) view.findViewById(R.id.btn_stats);
		rel_title_post = (RelativeLayout) view.findViewById(R.id.rel_title_post);
		lin_left = (RelativeLayout) view.findViewById(R.id.lin_left);
		//btn_invite = (TextView) view.findViewById(R.id.btn_invite);
		//btn_addPost = (TextView) view.findViewById(R.id.btn_addPost);
	}

	@Override
	protected void setValueOnUi() {		
		ApiManager.getInstance().getUserChannelDetail((BaseFragmentActivity)getActivity(),cId, userId);
	}

	@Override
	protected void setListener() {
		//btn_addPost.setOnClickListener(this);
		//btn_invite.setOnClickListener(this);
		btn_stats.setOnClickListener(this);
		lin_left.setOnClickListener(this);
	}
	public void serviceResponse(ChannelDetail channelDetail){
		if (channelDetail != null) {
			setTitleOnAction(channelDetail.getChannelName(), false);
			text_subs.setText(channelDetail.getSubscribers()+" Subscribers");
			text_post.setText(channelDetail.getTotalPosts()+" Post");
			text_visit.setText(channelDetail.getVisitors()+" Visits");

			if (channelDetail.getPostList() != null && channelDetail.getPostList().size() > 0) {
				postList = true;
				rel_title_post.setVisibility(View.VISIBLE);  
				postId = channelDetail.getPostList().get(0).getId();
				post_title.setText(Html.fromHtml(channelDetail.getPostList().get(0).getTitle()));
				post_des.setText(Html.fromHtml(channelDetail.getPostList().get(0).getDescription()));
				//Log.i("Testing1  ", "=="+channelDetail.getPostList().get(0).getImage());
				if (!channelDetail.getPostList().get(0).getImage().equals("")) {
					//.i("Testing2", "=="+channelDetail.getPostList().get(0).getImage());
					Picasso.with(getActivity()).load(channelDetail.getPostList().get(0).getImage()).placeholder(getActivity().getResources().getDrawable(R.drawable.noimg)).into(post_img);
				}
			}else {
				
				post_img.setImageDrawable(getResources().getDrawable(R.drawable.plus));
			}
		}
	}

	@Override
	public boolean onBackPressedListener() {
		return true;
	}

	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.your_channel, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
*/	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		//ToastCustomClass.showToast(getActivity(), "clicked");
		switch (item.getItemId()) {
		case R.id.i_channelSetting:
			startMyActivity(ChannelSettingActivity.class, null);
			break;
		case R.id.i_invite:
			Bundle bundle1 = new Bundle();
			bundle1.putString(Constants.channelId, cId);
			bundle1.putString(Constants.type, YourChannelDetailFragment.TAG);
			startMyActivity(InviteFriendActivity.class, bundle1);
			break;
		case R.id.i_addPost:
			if (cId != null && !cId.equalsIgnoreCase("")) {
				Bundle bundle = new Bundle();
				bundle.putString(Constants.id, cId);
				//startMyActivity(CreatePostActivity.class, bundle);
				Intent intent = new Intent(getActivity(),CreatePostActivity.class);
				intent.putExtra(Constants.bundleArg, bundle);
				startActivityForResult(intent, Constants.requestCode);
			}
			
			break;
		case R.id.i_delChannel:
			if (cId != null && !cId.equalsIgnoreCase("")) {
				ApiManager.getInstance().deleteChannel((BaseFragmentActivity)getActivity(), cId);
			}
			
			break;

		
		}
		return super.onOptionsItemSelected(item);
	}

	public void responseDeleteChannel() {
		// TODO Auto-generated method stub
		
		 Intent intent=new Intent();  
         intent.putExtra("Result","ok");  
         getActivity().setResult(Activity.RESULT_OK,intent);  
		 getActivity().finish();
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.i("onActivityResult", "adsf1 requestCode "+requestCode+" resultCode"+resultCode);
		if (requestCode == Constants.requestCode && resultCode == Activity.RESULT_OK) {
			Log.i("onActivityResult", "adsf2");
			String string = data.getStringExtra("Result");
			ApiManager.getInstance().getUserChannelDetail((BaseFragmentActivity)getActivity(),cId, userId);
		}
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.your_channel, menu);
	}
}
