package com.noto.gappss.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.adapter.AllCategoryAdapter;
import com.noto.gappss.adapter.PopularCatAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.AppCategory;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.preference.MySharedPreferences;

public class HomePopularCatagoryFragment extends BaseFragment{

	public static final String TAG = Constants.populorCat;

	private SearchView search;
	private GridView gridView;
	private ListView listView;
	AllCategoryAdapter adapter;
	PopularCatAdapter gridAdapter;
	private ArrayList<AppCategory> arrayList;

	public HomePopularCatagoryFragment() {

	}

	public static HomePopularCatagoryFragment newInstance(Bundle bundle) {
		HomePopularCatagoryFragment fragment = new HomePopularCatagoryFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//disableBackButton();
		setHasOptionsMenu(true);
		String name = MySharedPreferences.getInstance().getString(getActivity(), Constants.USER_NAME, "Guest");
		setTitleOnAction(name, false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// return super.onCreateView(inflater, container, savedInstanceState);
		// setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_home_popular_cat, null);
		initUi(view);		
		setListener();
		setValueOnUi();
		// Log.i("sdf", null);

		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub

		search = (SearchView) view.findViewById(R.id.search);
		gridView = (GridView) view.findViewById(R.id.gridView);
		listView = (ListView) view.findViewById(R.id.listView);

	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		ApiManager.getInstance().getCategories((BaseFragmentActivity)getActivity());

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				replaceFragment(ChannelFragment.newInstance(null), ChannelFragment.TAG);
			}
		});
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public void serviceResponse(ArrayList<AppCategory> arrayList) {
		// TODO Auto-generated method stub
		if (arrayList.size() > 0 && arrayList != null) {

			RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (MyApplication.getApplication().getHeightPixel()/4));
			layoutParams.addRule(RelativeLayout.BELOW, R.id.popular_text);
			layoutParams.setMargins(3, 3, 3, 3);
			gridView.setLayoutParams(layoutParams);

			ArrayList<AppCategory> appCategories = new ArrayList<AppCategory>();
			for (int i = 0; i < 6 ; i++) {
				try {
					appCategories.add(arrayList.get(i));
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}

			}
			Log.i("TESTINGGGGGGGG", appCategories.size()+"=");
			gridAdapter = new PopularCatAdapter(this, appCategories);
			gridView.setAdapter(gridAdapter);
			this.arrayList = arrayList;
			adapter = new AllCategoryAdapter(this, arrayList);
			listView.setAdapter(adapter);

		}


	}

	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		//super.onCreateOptionsMenu(menu, inflater);		
		inflater.inflate(R.menu.search, menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.i_search:			
			search.setVisibility(View.VISIBLE);
			break;			
		
		}	
		return false;
	}
}
