package com.noto.gappss.fragments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.ActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.noto.gappss.AppDetailActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.ChartAppAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.TestJson;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class GappssCommunityFragment extends BaseFragment{


	private FragmentTabHost mTabHost;
	public static final String TAG = Constants.gappssCommunity;
	TabWidget widget;
	HorizontalScrollView hs;
	private ListView listView;
	private TextView btnFree;
	private TextView btnPaid;
	private TextView btnGrossing;
	private ChartAppAdapter adapter;
	private int jsonArrayPosition = -1;
	private JSONArray jsonArrayTop;
	private LinearLayout topll;
	private int flag = 0;
	public GappssCommunityFragment() {

	}

	public static GappssCommunityFragment newInstance(Bundle bundle) {
		GappssCommunityFragment fragment = new GappssCommunityFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		//fragment.setUserVisibleHint(true);
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTitleOnAction(Constants.gappssCommunity, false);
		setHasOptionsMenu(true);
		//getActionBar().removeAllTabs();
		//getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_gappss_community, container, false);
		//initView(rootView);
		initUi(rootView);
		setListener();
		setValueOnUi();
		return rootView;
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnFree:
			btnFree();
			break;
		case R.id.btnPaid:
			btnPaid();
			break;
		case R.id.btnGrossing:
			btnGrossing();
			break;
		}			
	}

	@Override
	protected void initUi(View view) {
		listView = (ListView) view.findViewById(R.id.listView);
		btnFree = (TextView) view.findViewById(R.id.btnFree);
		btnPaid = (TextView) view.findViewById(R.id.btnPaid);
		btnGrossing = (TextView) view.findViewById(R.id.btnGrossing);
		topll = (LinearLayout) view.findViewById(R.id.topll);
	}

	@Override
	protected void setValueOnUi() {
		//testMethod();
		// setCommunity(null);
		// Log.i("TESING", TestJson.getGappssCommunity().toString());
		Bundle bundle = getArguments();
		String countryCode = bundle.getString(Constants.countryCode);
		       flag  = bundle.getInt(Constants.flag);
		ApiManager.getInstance().getGappssCommunityApps(this,countryCode.trim().replace(" ", ""));

	}

	@Override
	protected void setListener() {
		btnFree.setOnClickListener(this);
		btnPaid.setOnClickListener(this);
		btnGrossing.setOnClickListener(this);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String packag = (String) view.getTag(R.id.link);
				Bundle bundle = new Bundle();
				bundle.putString(Constants.package_, packag);
				startMyActivity(AppDetailActivity.class, bundle);
			}
		});
	}

	@Override
	public boolean onBackPressedListener() {
		return true;
	}

	private void initView(View rootView) {
		mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);

		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		hs = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView);

		mTabHost.setup(getActivity(), getChildFragmentManager(),android.R.id.tabcontent);
		mTabHost.addTab(mTabHost.newTabSpec("0").setIndicator("Test"),DummyFragment.class, null);


		/*  mTabHost.addTab(mTabHost.newTabSpec("All").setIndicator("All"),DummyFragment.class, null);
		        mTabHost.addTab(mTabHost.newTabSpec("Games").setIndicator("Games"),DummyFragment.class, null);
		        mTabHost.addTab(mTabHost.newTabSpec("Business").setIndicator("Business"),DummyFragment.class, null);
		        mTabHost.addTab(mTabHost.newTabSpec("Utility").setIndicator("Utility"),DummyFragment.class, null);
		        mTabHost.addTab(mTabHost.newTabSpec("Shopping").setIndicator("Shopping"),DummyFragment.class, null);
		        mTabHost.addTab(mTabHost.newTabSpec("hiii").setIndicator("hiii"));
		        mTabHost.addTab(mTabHost.newTabSpec("hello").setIndicator("hello"));
		        for (int i = 0; i < 5; i++) {

			        TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
		            tv.setTextColor(Color.parseColor("#ffffff"));
				}

		        mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

					@Override
					public void onTabChanged(String tabId) {
						// TODO Auto-generated method stub
						ToastCustomClass.showToast(getActivity(), ""+tabId);
						btnFree();

					}
				});*/

	}

	public void btnFree(){
		btnFree.setTextColor(getActivity().getResources().getColor(R.color.action_bar));
		btnPaid.setTextColor(getActivity().getResources().getColor(R.color.white));
		btnGrossing.setTextColor(getActivity().getResources().getColor(R.color.white));
		if (jsonArrayPosition >= 0 ) {
			JSONArray array = jsonArrayTop.optJSONObject(jsonArrayPosition).optJSONArray(Constants.freeApps);
			if (array != null ) {
				adapter.setList(array);	
			}else {
				adapter.setList(new JSONArray());
			}
		}
	}
	public void btnPaid(){
		btnFree.setTextColor(getActivity().getResources().getColor(R.color.white));
		btnPaid.setTextColor(getActivity().getResources().getColor(R.color.action_bar));
		btnGrossing.setTextColor(getActivity().getResources().getColor(R.color.white));
		if (jsonArrayPosition >= 0 ) {
			JSONArray array = jsonArrayTop.optJSONObject(jsonArrayPosition).optJSONArray(Constants.paidApps);
			if (array != null ) {
				adapter.setList(array);	
			}else {
				adapter.setList(new JSONArray());
			}

		}
	}

	public void btnGrossing(){
		btnFree.setTextColor(getActivity().getResources().getColor(R.color.white));
		btnPaid.setTextColor(getActivity().getResources().getColor(R.color.white));
		btnGrossing.setTextColor(getActivity().getResources().getColor(R.color.action_bar));
		if (jsonArrayPosition >= 0 ) {
			JSONArray array = jsonArrayTop.optJSONObject(jsonArrayPosition).optJSONArray(Constants.topGrossingApps);
			if (array != null ) {
				adapter.setList(array);	
			}else {
				adapter.setList(new JSONArray());
			}
		}
	}

	public void testMethod(){
		JSONArray array = new JSONArray();
		for (int i = 0; i < 5; i++) {
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put(Constants.channelId, "1");
				jsonObject.put(Constants.image, "");
				jsonObject.put(Constants.title, "Binary Signal");
				jsonObject.put(Constants.description, "Binary signal app description");
				jsonObject.put("link", "https://play.google.com/store/apps/details?id=com.binarysignals&hl=en");
				array.put(i, jsonObject);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		adapter = new ChartAppAdapter(this, array);
		listView.setAdapter(adapter);
	}

	public void setCommunity(JSONArray jsonArray) {
		// TODO Auto-generated method stub
		//JSONObject jsonObject = TestJson.getGappssCommunity();
		//jsonArray = jsonObject.optJSONArray(Constants.YourCommunityApps);
		Log.i("getGappssCommunityApps22", "getGappssCommunityApps");
		jsonArrayTop = jsonArray;
		if (jsonArray != null) {
			//jsonArrayTop = jsonArray;
			//mTabHost.getTabWidget().removeAllViews();//View(mTabHost.getTabWidget().getChildTabViewAt(0));

			View view = getActivity().getLayoutInflater().inflate(R.layout.tabhost_layout, null);
			mTabHost = (FragmentTabHost) view.findViewById(android.R.id.tabhost);
			widget = (TabWidget) view.findViewById(android.R.id.tabs);
			mTabHost.setup(getActivity(), getChildFragmentManager(),android.R.id.tabcontent);



			for (int i = 0; i < jsonArray.length(); i++) {
				Log.i("Testing", ""+i);
				mTabHost.addTab(mTabHost.newTabSpec(""+i).setIndicator(jsonArray.optJSONObject(i).optString(Constants.categoryName)),DummyFragment.class, null);

				TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
				View v = mTabHost.getTabWidget().getChildAt(i);
				UtilsClass.setTabTextColor(getActivity(), tv, v);
			}

			topll.addView(view);
			mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

				@Override
				public void onTabChanged(String tabId) {
					// TODO Auto-generated method stub
					//ToastCustomClass.showToast(getActivity(), ""+tabId);
					//btnFree();
					jsonArrayPosition =  Integer.parseInt(tabId);
					btnFree();

				}
			});


			JSONArray jsonArrayFree =jsonArray.optJSONObject(0).optJSONArray(Constants.freeApps);
			if (jsonArrayFree != null) {
				jsonArrayPosition = 0;
				adapter = new ChartAppAdapter(this, jsonArrayFree);
				listView.setAdapter(adapter);
			}
		}

	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		if (flag > 0) {
			menu.add("testing").setIcon(getActivity().getResources().getDrawable(flag)).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}
		}
		
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}
}
