package com.noto.gappss.fragments;

import org.json.JSONArray;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;

import com.noto.gappss.R;
import com.noto.gappss.adapter.ChartAppAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.utilities.UtilsClass;

public class InboxFragment extends BaseFragment{

	private FragmentTabHost mTabHost;
	public static final String TAG = Constants.inbox;
	TabWidget widget;
	HorizontalScrollView hs;
	private ListView listView;
	private ChartAppAdapter adapter;

	public InboxFragment() {

	}

	public static InboxFragment newInstance(Bundle bundle) {
		InboxFragment fragment = new InboxFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		//fragment.setUserVisibleHint(true);
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub		
       super.onCreate(savedInstanceState);		
	   //disableBackButton();
		setTitleOnAction(TAG, false);		
		//setHasOptionsMenu(true);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_inbox,container, false);
		initView(rootView);
		initUi(rootView);
		setListener();
		setValueOnUi();
		return rootView;
	}

	private void initView(View rootView) {
		mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);

		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		hs = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView);


		mTabHost.setup(getActivity(), getChildFragmentManager(),android.R.id.tabcontent);

		mTabHost.addTab(mTabHost.newTabSpec("All").setIndicator("All"),InboxAllFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("sharedapps").setIndicator("Shared Apps"),InboxSharedAppFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("Posts").setIndicator("Posts"),InboxPostFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("Requests").setIndicator("Requests"),InboxRequestFragment.class, null);

		for (int i = 0; i < 4; i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
			View v = mTabHost.getTabWidget().getChildAt(i);
			UtilsClass.setTabTextColor(getActivity(), tv, v);
		}

		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				// TODO Auto-generated method stub
				//ToastCustomClass.showToast(getActivity(), ""+tabId);


			}
		});

	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();		
		inflater.inflate(R.menu.setting, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}*/
}
