package com.noto.gappss.fragments;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.noto.gappss.R;
import com.noto.gappss.YourChannelDetailActivity;
import com.noto.gappss.adapter.FriendChannelAdapter;
import com.noto.gappss.adapter.YourChannelsAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelDetail;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.ApiManager;

public class FriendChannelListFragment extends BaseFragment{
	
	public static final String TAG = Constants.frndChannels;
	public String title = "Channel";
	private FriendChannelAdapter adapter;
	private JSONArray jsonArray;
	private ListView listView;
	private Contact contact;

	public FriendChannelListFragment(){			
		//Log.i(TAG, "Root Cat Contructor");
	}

	public static FriendChannelListFragment newInstance(Bundle bundle) {
		FriendChannelListFragment fragment = new FriendChannelListFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		fragment.setUserVisibleHint(true);
		return fragment;
	}
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		setHasOptionsMenu(true);
		setTitleOnAction(TAG, false);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_friend_channel_list, null);
		initUi(view);
		setListener();
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		contact = (Contact) getArguments().getSerializable(Constants.contact);
		setValueOnUi();	
	}
	
	@Override
	public void onResume() {
		super.onResume();		
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {		
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
	}

	public void setChannels(JSONArray jsonArray){	
		if(jsonArray != null && listView != null){
			this.jsonArray = jsonArray;
			adapter = new FriendChannelAdapter(this,  jsonArray);			
			listView.setAdapter(adapter);
		}
	}

	@Override
	public void onClick(View v) {
		
	}

	@Override
	protected void initUi(View view) {
		setTitleOnAction("Channels", false);
		listView = (ListView) view.findViewById(R.id.listView);
	}

	@Override
	protected void setValueOnUi() {
		ApiManager.getInstance().getFriendAllChannel(this, contact.getId());
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		jsonArray = null;
	}

	@Override
	protected void setListener() {
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				JSONObject jO =(JSONObject) view.getTag(R.id.text_channel);
				ChannelDetail detail = new ChannelDetail();				
				detail.setChannelId(jO.optString(Constants.channelId));
				detail.setChannelName(jO.optString(Constants.channelName));
				detail.setChannelDescription(jO.optString(Constants.channelDescription));
				detail.setChannelType(jO.optString(Constants.channelType));
				detail.setChannelMode(jO.optString(Constants.channelMode));
				detail.setChannelImage(jO.optString(Constants.channelImage));

				Bundle bundle = new Bundle();
				bundle.putString(Constants.USER_ID, contact.getId());
				bundle.putString(Constants.channelId, detail.getChannelId());
				bundle.putSerializable(Constants.channel, detail);

				//startMyActivity(YourChannelDetailActivity.class, bundle);
				Intent intent = new Intent(getActivity(),YourChannelDetailActivity.class);
				intent.putExtra(Constants.bundleArg, bundle);
				startActivityForResult(intent, Constants.requestCodeYourChannelDetail);
			}
		});	
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}

	/*@Override  
	public void onActivityResult(int requestCode, int resultCode, Intent data)  
	{  
		super.onActivityResult(requestCode, resultCode, data);  
		// check if the request code is same as what is passed  here it is 2  
		if(resultCode == Activity.RESULT_OK)  
		{  
			switch (requestCode) {
			case Constants.requestCode:
				//ToastCustomClass.showToast(getActivity(), data.getStringExtra("Result")); 
				if (data.getStringExtra("Result").equals("ok")) {
					ApiManager.getInstance().getYourChannels(this);
					//create_channel_empty.setVisibility(View.GONE);
					//rel_second.setVisibility(View.VISIBLE);
				}
				break;
			case Constants.requestCodeYourChannelDetail:
				//ToastCustomClass.showToast(getActivity(), "102="+data.getStringExtra("Result")); 
				if (data.getStringExtra("Result").equals("ok")) {
					ApiManager.getInstance().getYourChannels(this);
					//create_channel_empty.setVisibility(View.GONE);
					//rel_second.setVisibility(View.VISIBLE);
				}
				break;
			}
		}  
	}*/ 

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {	
		//inflater.inflate(R.menu.create_channel, menu);
		super.onCreateOptionsMenu(menu, inflater);		
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {			
		return false;
	}	

	public void refreshList() {
		adapter.refreshList();
	}
}
