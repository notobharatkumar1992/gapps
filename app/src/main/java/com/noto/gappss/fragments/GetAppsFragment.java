package com.noto.gappss.fragments;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONObject;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabWidget;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.adapter.ChartAppAdapter;
import com.noto.gappss.adapter.InterestedAppsAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.UtilsClass;

public class GetAppsFragment extends BaseFragment {

	public static final String TAG = "GET_APPS_FAR";
	private FragmentTabHost mTabHost;

	TabWidget widget;
	HorizontalScrollView hs;
	private ListView listView;
	private ArrayList<ApplicationBean> list;
	private Contact contact;
	private InterestedAppsAdapter adapter;

	public GetAppsFragment() {

	}

	public static GetAppsFragment newInstance(Bundle bundle) {
		GetAppsFragment fragment = new GetAppsFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		// fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_get_appps, container, false);
		initUi(view);
		setListener();
		setValueOnUi();
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initUi(View view) {
		listView = (ListView) view.findViewById(R.id.listView);

		mTabHost = (FragmentTabHost) view.findViewById(android.R.id.tabhost);
		widget = (TabWidget) view.findViewById(android.R.id.tabs);
		hs = (HorizontalScrollView) view.findViewById(R.id.horizontalScrollView);

		mTabHost.setup(getActivity(), getChildFragmentManager(),android.R.id.tabcontent);
		mTabHost.addTab(mTabHost.newTabSpec("0").setIndicator(getActivity().getResources().getString(R.string.all)),DummyFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator(getActivity().getResources().getString(R.string.hot_apps)),DummyFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("2").setIndicator(getActivity().getResources().getString(R.string.rated)),DummyFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("3").setIndicator(getActivity().getResources().getString(R.string.newly)),DummyFragment.class, null);

		for (int i = 0; i < 4; i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
			View v = mTabHost.getTabWidget().getChildAt(i);
			UtilsClass.setTabTextColor(getActivity(), tv, v);
		}	

		contact =(Contact) getArguments().getSerializable(Constants.contact);
	}

	@Override
	protected void setValueOnUi() {
		ApiManager.getInstance().getFriendAllApps(this, contact.getId());
	}

	@Override
	protected void setListener() {
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				int key = Integer.parseInt(tabId);
				ArrayList<ApplicationBean> tempList = (ArrayList<ApplicationBean>) list.clone();
				switch (key) {
				case 0:			
					adapter.setList(list);
					break;
				case 1:
					Collections.sort(tempList, new Comparator<ApplicationBean>() {
						@Override
						public int compare(ApplicationBean lhs,	ApplicationBean rhs) {							 
							return lhs.getHot() < rhs.getHot() ? -1 : lhs.getHot() == rhs.getHot() ? 0 : 1;
						}
					});					
					adapter.setList(tempList);
					break;
				case 2:
					Collections.sort(tempList, new Comparator<ApplicationBean>() {
						@Override
						public int compare(ApplicationBean lhs, ApplicationBean rhs) {
							return	lhs.getRate() < rhs.getRate() ? -1 : lhs.getRate() == rhs.getRate() ? 0 : 1;
						}
					});				
					adapter.setList(tempList);
					break;
				case 3:
					Collections.sort(tempList, new Comparator<ApplicationBean>() {
						@Override
						public int compare(ApplicationBean lhs,	ApplicationBean rhs) {							 
							return lhs.getNewlyDate() < rhs.getNewlyDate() ? -1 : lhs.getNewlyDate() == rhs.getNewlyDate() ? 0 : 1;
						}
					});					
					adapter.setList(tempList);
					break;
				}
			}
		});
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}

	public void setApps(ArrayList<ApplicationBean> list){
		this.list = list;
		if (list != null) {
			adapter = new InterestedAppsAdapter(this, list);
			listView.setAdapter(adapter);
		}
	}
}
