package com.noto.gappss.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import com.noto.gappss.GappssCommunityActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.CountryAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Country;

public class CountryFragment extends BaseFragment{

	public static final String TAG = "COUNTRY_FAR";
	private ListView listView;
	private JSONArray jsonArray;
	private SearchView searchCountry;
	public CountryFragment() {

	}

	public static CountryFragment newInstance(Bundle bundle) {
		CountryFragment fragment = new CountryFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTitleOnAction(Constants.gappssCommunity, false);
		setHasOptionsMenu(true);
		enableBackButton();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_country, container, false);
		initUi(view);
		setListener();
		setValueOnUi();
		return view;
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	protected void initUi(View view) {
		listView = (ListView) view.findViewById(R.id.listView);
		searchCountry = (SearchView) view.findViewById(R.id.searchCountry);
	}

	@Override
	protected void setValueOnUi() {
		addCounteryInList();
	}

	@Override
	protected void setListener() {
		listView.setTextFilterEnabled(true);
		searchCountry.setIconifiedByDefault(false);
		searchCountry.setQueryHint("Search...");
		searchCountry.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				if (TextUtils.isEmpty(newText.toString())) {
					listView.clearTextFilter();
					//Log.i("if onQueryTextChange", "onQueryTextChange=="+newText.toString());
				} else {
					listView.setFilterText(newText.toString());
					//Log.i("else onQueryTextChange", "onQueryTextChange=="+newText.toString());
				}
				return true;
			}
		});

		
		
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String dialCode = (String) view.getTag(R.id.text_title);
				int flg =  (int) view.getTag(R.id.img_logo);
				Bundle bundle = new Bundle();
				bundle.putString(Constants.countryCode, dialCode);
				bundle.putInt(Constants.flag,flg);
				startMyActivity(GappssCommunityActivity.class, bundle);				
			}
		});		
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}	

	public void addCounteryInList(){
		try {
			jsonArray = new JSONArray(readSavedData());
			Log.i("Testing", jsonArray.toString()); 
			//final String[] arrayString = new String[jsonArray.length()];
			Country country = null;
			ArrayList<Country> arrList = new ArrayList<Country>();
			for (int i = 0; i < jsonArray.length(); i++) {
				country = new Country();
				JSONObject jC = jsonArray.optJSONObject(i);
				String name = jC.optString("name");
				String code = jC.optString("code").toLowerCase();
				String dial_code = jC.optString("dial_code");
				int resID = getResources().getIdentifier(code , "drawable", getActivity().getPackageName());
				if(code.equalsIgnoreCase("do"))
					resID = getResources().getIdentifier("do1" , "drawable", getActivity().getPackageName());

				country.setName(name);
				country.setFlag(resID);
				country.setDialCode(dial_code);
				
				arrList.add(country);
			}
			CountryAdapter adapter = new CountryAdapter(this, arrList);
			//ArrayAdapter arrAdapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,arrayString);
			listView.setAdapter(adapter);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String readSavedData() {
		StringBuffer datax = new StringBuffer("");
		BufferedReader buffreader = null;
		try {

			buffreader = new BufferedReader(new InputStreamReader(getActivity().getAssets().open("countrycode.txt"), "UTF-8"));

			String readString = buffreader.readLine();
			while (readString != null) {
				datax.append(readString);
				readString = buffreader.readLine();
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (buffreader != null) {
				try {
					buffreader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return datax.toString();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		//super.onCreateOptionsMenu(menu, inflater);		
		inflater.inflate(R.menu.search, menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.i_search:			
			
			if (searchCountry.getVisibility() == View.VISIBLE) {
				searchCountry.setVisibility(View.GONE);
			}else {
				searchCountry.setVisibility(View.VISIBLE);
			}
			break;			
		
		}	
		return false;
	}

}
