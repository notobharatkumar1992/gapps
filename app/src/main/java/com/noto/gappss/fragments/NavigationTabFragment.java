package com.noto.gappss.fragments;
import android.R.transition;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.utilities.UtilsClass;

/**
 * Fragment used for managing interactions for and presentation of a navigation
 * drawer. See the <a href=
 * "https://developer.android.com/design/patterns/navigation-drawer.html#Interaction"
 * > design guidelines</a> for a complete explanation of the behaviors
 * implemented here.
 */
public class NavigationTabFragment extends Fragment {

	/**
	 * Remember the position of the selected item.
	 */
	private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

	/**
	 * Per the design guidelines, you should show the drawer on launch until the
	 * user manually expands it. This shared preference tracks this.
	 */
	private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

	/**
	 * A pointer to the current callbacks instance (the Activity).
	 */


	private View mFragmentContainerView;

	private int mCurrentSelectedPosition = 3;
	private boolean mFromSavedInstanceState;
	private boolean mUserLearnedDrawer;

	private NavigationDrawerCallbacks mCallbacks;

	private View tabsRoot;
	private TextView btn_chart;
	private TextView btn_feeds;
	private TextView btn_channels;
	private TextView btn_inbox;
	private TextView btn_contacts;

	private boolean flag = true;

	private RelativeLayout root;

	private LinearLayout tabGroup;

	private View view;

	public NavigationTabFragment() {
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Read in the flag indicating whether or not the user has demonstrated
		// awareness of the
		// drawer. See PREF_USER_LEARNED_DRAWER for details.
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

		if (savedInstanceState != null) {
			mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
			mFromSavedInstanceState = true;
		}else{
			mCurrentSelectedPosition = -1;
		}

		// Select either the default item (0) or the last selected item.
		//selectItem(mCurrentSelectedPosition);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Indicate that this fragment would like to influence the set of
		// actions in the action bar.
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		tabsRoot =  inflater.inflate(R.layout.tab_layout, container, false);
		root = (RelativeLayout) tabsRoot.findViewById(R.id.root);
		btn_chart = (TextView) tabsRoot.findViewById(R.id.btn_chart);
		btn_feeds = (TextView) tabsRoot.findViewById(R.id.btn_feeds);
		btn_channels = (TextView) tabsRoot.findViewById(R.id.btn_channels);
		btn_inbox = (TextView) tabsRoot.findViewById(R.id.btn_inbox);
		btn_contacts = (TextView) tabsRoot.findViewById(R.id.btn_contacts);
		
		
		btn_chart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				selectItem(0);				
			}
		});

		btn_feeds.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				selectItem(1);				
			}
		});	

		btn_channels.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				selectItem(2);				
			}
		});

		btn_inbox.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				selectItem(3);				
			}
		});

		btn_contacts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				selectItem(4);				
			}
		});

		//btn_chart.setSelected(true);
		

		return tabsRoot;
	}

	
	

	/*public boolean isDrawerOpen() {
		return mDrawerLayout != null
				&& mDrawerLayout.isDrawerOpen(mFragmentContainerView);
	}
	 */
	/**
	 * Users of this fragment must call this method to set up the navigation
	 * drawer interactions.
	 *
	 * @param fragmentId
	 *            The android:id of this fragment in its activity's layout.
	 * @param drawerLayout
	 *            The DrawerLayout containing this fragment's UI.
	 */
	public void setUp(int pos, int fragmentId) {
		mFragmentContainerView = getActivity().findViewById(fragmentId);
		selectItem(pos);
		
	}

	public void buttonChartSelect(){
		btn_chart.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_game_select, 0, 0);
		btn_chart.setTextColor(getResources().getColor(R.color.bottom_tab_text_selected));
		
		btn_feeds.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_feeds_unselect, 0, 0);
		btn_feeds.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));

		btn_channels.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_channel_unselect, 0, 0);
		btn_channels.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_inbox.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_inbox_unselect, 0, 0);
		btn_inbox.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_contacts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_contact_unselect, 0, 0);
		btn_contacts.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		//UtilsClass.underConstruction(getActivity());
	}
	public void buttonFeedsSelect(){
		btn_chart.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_game_unselect, 0, 0);
		btn_chart.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_feeds.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_feeds_select, 0, 0);
		btn_feeds.setTextColor(getResources().getColor(R.color.bottom_tab_text_selected));

		btn_channels.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_channel_unselect, 0, 0);
		btn_channels.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_inbox.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_inbox_unselect, 0, 0);
		btn_inbox.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_contacts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_contact_unselect, 0, 0);
		btn_contacts.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		//UtilsClass.underConstruction(getActivity());
		//replaceFragement(FeedsFragment.newInstance(null), FeedsFragment.TAG);
	}
	public void buttonChannelsSelect() throws NullPointerException{
		btn_chart.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_game_unselect, 0, 0);
		btn_chart.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_feeds.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_feeds_unselect, 0, 0);
		btn_feeds.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));

		btn_channels.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_channel_select, 0, 0);
		btn_channels.setTextColor(getResources().getColor(R.color.bottom_tab_text_selected));
		
		btn_inbox.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_inbox_unselect, 0, 0);
		btn_inbox.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_contacts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_contact_unselect, 0, 0);
		btn_contacts.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
	}
	public void buttonInboxSelect(){
		btn_chart.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_game_unselect, 0, 0);
		btn_chart.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_feeds.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_feeds_unselect, 0, 0);
		btn_feeds.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));

		btn_channels.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_channel_unselect, 0, 0);
		btn_channels.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_inbox.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_inbox_select, 0, 0);
		btn_inbox.setTextColor(getResources().getColor(R.color.bottom_tab_text_selected));
		
		btn_contacts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_contact_unselect, 0, 0);
		btn_contacts.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		UtilsClass.underConstruction(getActivity());
	}
	public void buttonContactsSelect(){
		btn_chart.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_game_unselect, 0, 0);
		btn_chart.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_feeds.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_feeds_unselect, 0, 0);
		btn_feeds.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));

		btn_channels.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_channel_unselect, 0, 0);
		btn_channels.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_inbox.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_inbox_unselect, 0, 0);
		btn_inbox.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		
		btn_contacts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btn_contact_select, 0, 0);
		btn_contacts.setTextColor(getResources().getColor(R.color.bottom_tab_text_selected));
	}
	private void selectItem(int position) {
		mCurrentSelectedPosition = position;
		
		if (position == 0) {
			buttonChartSelect();		
		}
		else if (position == 1) {
			try {
				buttonFeedsSelect();
			} catch (NullPointerException e) {
				// TODO: handle exception
			}
				
		}
		else if (position == 2) {
			try {
				buttonChannelsSelect();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}	
		}
		else if (position == 3) {
			buttonInboxSelect();
		}
		else if (position == 4) {
			buttonContactsSelect();
		}
		
		if (mCallbacks != null) {
			mCallbacks.onNavigationDrawerItemSelected(position);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallbacks = (NavigationDrawerCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(
					"Activity must implement NavigationDrawerCallbacks.");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Forward the new configuration the drawer toggle component.
		//mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// If the drawer is open, show the global app actions in the action bar.
		// See also
		// showGlobalContextActionBar, which controls the top-left area of the
		// action bar.
		/*if (mDrawerLayout != null && isDrawerOpen()) {
			inflater.inflate(R.menu.global, menu);
			showGlobalContextActionBar();
		}*/
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/*if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}*/

		/*if (item.getItemId() == R.id.action_example) {
			Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT)
					.show();
			return true;
		}*/

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Per the navigation drawer design guidelines, updates the action bar to
	 * show the global app 'context', rather than just what's in the current
	 * screen.
	 */
	/*private void showGlobalContextActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setTitle(R.string.app_name);
	}*/

	/*private ActionBar getActionBar() {
		return ((ActionBarActivity) getActivity()).getSupportActionBar();
	}*/

	/**
	 * Callbacks interface that all activities using this fragment must
	 * implement.
	 */
	public static interface NavigationDrawerCallbacks {
		/**
		 * Called when an item in the navigation drawer is selected.
		 */
		void onNavigationDrawerItemSelected(int position);
	}
	
	public void replaceFragement(Fragment fragment, String tagValue){
		FragmentTransaction transaction = getFragmentTransaction();
		transaction.replace(R.id.container, fragment, tagValue).commit();
	}
	
	public FragmentTransaction getFragmentTransaction(){
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		//transaction.setCustomAnimations(R.anim.slide_in_down, R.anim.slide_out_up);
		transaction.setCustomAnimations(R.anim.slide_in_down, R.anim.slide_out_up, R.anim.slide_out_down, R.anim.slide_in_up);
		return transaction;
	}

}
