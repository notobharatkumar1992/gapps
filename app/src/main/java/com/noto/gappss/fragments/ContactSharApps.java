package com.noto.gappss.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TabWidget;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.utilities.UtilsClass;

public class ContactSharApps extends BaseFragment{
	
	   private FragmentTabHost mTabHost;
	   public static final String TAG = Constants.contactT;
	   TabWidget widget;
	   HorizontalScrollView hs;
	   private String id = "";
	   public ContactSharApps() {
			
		}

		public static ContactSharApps newInstance(Bundle bundle) {
			ContactSharApps fragment = new ContactSharApps();
			if (bundle != null)
				fragment.setArguments(bundle);
			//fragment.setUserVisibleHint(true);
			return fragment;
		}
		
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setTitleOnAction(TAG, false);
			//setHasOptionsMenu(true);
			//getActionBar().removeAllTabs();
			//getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		}
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			 View rootView = inflater.inflate(R.layout.fragment_tabs,container, false);
			 initView(rootView);
			 initUi(rootView);
			 setListener();
			 setValueOnUi();
			 return rootView;
		}
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
			Bundle bundle = getArguments();
			id = bundle.getString(Constants.id);
			Log.i("idddddddddd", id);
		}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}
	
	 private void initView(View rootView) {
	        mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);

	        widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
	        hs = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView);


	        mTabHost.setup(getActivity(), getChildFragmentManager(),android.R.id.tabcontent);
	        
	        mTabHost.addTab(mTabHost.newTabSpec("0").setIndicator("CONTACTS"),InviteFriendFragment.class, getArguments());
	        mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator("CHANNELS"),DummyFragment.class, null);
	       
	        for (int i = 0; i < 2; i++) {

	        	TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
				View v = mTabHost.getTabWidget().getChildAt(i);
				UtilsClass.setTabTextColor(getActivity(), tv, v);
			} 
	}

}
