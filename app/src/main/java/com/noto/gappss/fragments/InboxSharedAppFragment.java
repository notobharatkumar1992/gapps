package com.noto.gappss.fragments;

import org.json.JSONArray;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.noto.gappss.AppDetailActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.ChartAppAdapter;
import com.noto.gappss.adapter.InboxSharedAppsAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;

public class InboxSharedAppFragment extends BaseFragment{

	public static final String TAG = Constants.InboxSharedAppFragment;
	private ListView listView;
    private InboxSharedAppsAdapter adapter;
	public InboxSharedAppFragment() {

	}

	public static InboxSharedAppFragment newInstance(Bundle bundle) {
		InboxSharedAppFragment fragment = new InboxSharedAppFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		// fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_friend_apps, container,
				false);
		initUi(view);
		setListener();
		setValueOnUi();
		return view;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		listView = (ListView) view.findViewById(R.id.listView);
		
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		ApiManager.getInstance().getShareApps(this);
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String packag = (String) view.getTag(R.id.link);
				Bundle bundle = new Bundle();
				bundle.putString(Constants.package_, packag);
				startMyActivity(AppDetailActivity.class, bundle);					
			}
		});
		
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setList(JSONArray jsonArray) {
		// TODO Auto-generated method stub
		if (jsonArray != null) {
			adapter = new InboxSharedAppsAdapter(this, jsonArray);
			listView.setAdapter(adapter);
			
		}
		
	}

}
