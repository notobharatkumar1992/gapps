package com.noto.gappss.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Contact;

public class FriendDetailFragment extends BaseFragment{
	public static final String TAG = "FRIEND_DETAIL_FAR";
	private ImageView imgUser;
	private TextView tv_userName;
	private TextView tv_alllow;
	private TextView tv_accessible;
	private Button btn_channels;
	private Button btn_apps;
	private Button btn_activity;
	private Button btn_interest;
	private Button btn_compareProfile;
	private String userId = "";
	private Contact contact;
	private Bundle bundle;

	public FriendDetailFragment() {
	}

	public static FriendDetailFragment newInstance(Bundle bundle) {
		FriendDetailFragment fragment = new FriendDetailFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_friend_detail, container, false);
		initUi(view);
		setListener();		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {	
		super.onActivityCreated(savedInstanceState);
		setValueOnUi();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_channels:
			addFragment(FriendChannelListFragment.newInstance(bundle), FriendChannelListFragment.TAG, true);
			break;
		case R.id.btn_apps:
			addFragment(FriendAppsFragment.newInstance(bundle), FriendAppsFragment.TAG, true);
			break;
		case R.id.btn_activity:			
			bundle.putInt(Constants.type,2);
			addFragment(FriendFeedsFragment.newInstance(bundle), FriendFeedsFragment.TAG, true);
			break;
		case R.id.btn_interest:
			addFragment(FriendInterestFragment.newInstance(bundle), FriendInterestFragment.TAG, true);
			break;
		case R.id.btn_compareProfile:
			//startMyActivity(FriendDataActivity.class, null);
			addFragment(CompareProfileFragment.newInstance(bundle), CompareProfileFragment.TAG, true);	       
			break;
		}
	}

	@Override
	protected void initUi(View view) {
		imgUser = (ImageView) view.findViewById(R.id.imgUser);
		tv_userName = (TextView) view.findViewById(R.id.tv_userName);
		tv_alllow = (TextView) view.findViewById(R.id.tv_alllow);
		tv_accessible = (TextView) view.findViewById(R.id.tv_accessible);
		btn_channels = (Button) view.findViewById(R.id.btn_channels);
		btn_apps = (Button) view.findViewById(R.id.btn_apps);
		btn_activity = (Button) view.findViewById(R.id.btn_activity);
		btn_interest = (Button) view.findViewById(R.id.btn_interest);
		btn_compareProfile = (Button) view.findViewById(R.id.btn_compareProfile);

	}

	@Override
	protected void setValueOnUi() {
		bundle = getArguments();
		contact = (Contact) bundle.getSerializable(Constants.contact);
		setTitleOnAction(contact.getName(), false);
		tv_userName.setText(contact.getName());
	}

	@Override
	protected void setListener() {
		btn_channels.setOnClickListener(this);
		btn_apps.setOnClickListener(this);
		btn_activity.setOnClickListener(this);
		btn_interest.setOnClickListener(this);
		btn_compareProfile.setOnClickListener(this);
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}
}
