package com.noto.gappss.fragments;

import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TabWidget;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.utilities.UtilsClass;

public class ChannelFragment extends BaseFragment{

	private FragmentTabHost mTabHost;
	public static final String TAG = "CHANNEL_FAR";
	TabWidget widget;
	HorizontalScrollView hs;
	public ChannelFragment() {		
	}

	public static ChannelFragment newInstance(Bundle bundle) {
		ChannelFragment fragment = new ChannelFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		//fragment.setUserVisibleHint(true);
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setHasOptionsMenu(true);
		//getActionBar().removeAllTabs();
		//getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_tabs,container, false);
		initView(rootView);



		return rootView;
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	private void initView(View rootView) {
		mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);

		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		widget.setStripEnabled(false);
		hs = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView);

		mTabHost.setup(getActivity(), getChildFragmentManager(),android.R.id.tabcontent);

		mTabHost.addTab(mTabHost.newTabSpec(ExplorerChannelsFragment.TAG).setIndicator("EXPLORER"),ExplorerChannelsFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec(FollowedChannelsFragment.TAG).setIndicator("FOLLOWER"),FollowedChannelsFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec(YourChannelsFragment.TAG).setIndicator("YOUR CHANNEL"),YourChannelsFragment.class, null);
		for (int i = 0; i < 3; i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
			View v = mTabHost.getTabWidget().getChildAt(i);
			UtilsClass.setTabTextColor(getActivity(), tv, v);   
		} 
	}



}
