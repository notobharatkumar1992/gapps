package com.noto.gappss.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.noto.gappss.R;
import com.noto.gappss.adapter.InboxRequestAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.ToastCustomClass;

public class InboxRequestFragment extends BaseFragment{
	
	public static final String TAG = Constants.InboxRequestFragment;
	private ListView listView;
    private InboxRequestAdapter adapter;
	public InboxRequestFragment() {

	}

	public static InboxRequestFragment newInstance(Bundle bundle) {
		InboxRequestFragment fragment = new InboxRequestFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		//fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_friend_apps, container,false);
		initUi(view);
		setListener();
		setValueOnUi();
		return view;
	}
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		listView = (ListView) view.findViewById(R.id.listView);
		
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		ApiManager.getInstance().getRequests(this);
		
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setList(ArrayList<Contact> arrayList) {
		// TODO Auto-generated method stub
		adapter = new InboxRequestAdapter(this, arrayList);
		listView.setAdapter(adapter);
		
	}

	public void serverResponsAcceptCancelRequest(int friend) {
		// TODO Auto-generated method stub
		ToastCustomClass.showToast(getActivity(), "=="+friend);
		
	}

}
