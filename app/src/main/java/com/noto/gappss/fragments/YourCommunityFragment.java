package com.noto.gappss.fragments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabWidget;

import com.noto.gappss.AppDetailActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.ChartAppAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class YourCommunityFragment extends BaseFragment{
	private FragmentTabHost mTabHost;
	public static final String TAG = Constants.yourCommunity;
	TabWidget widget;
	HorizontalScrollView hs;
	private ListView listView;
	private ChartAppAdapter adapter;
	private JSONArray jsonArrayFree;
	private JSONArray jsonArrayPaid;
	private JSONArray jsonArrayTopGrossing;
	public YourCommunityFragment() {

	}

	public static YourCommunityFragment newInstance(Bundle bundle) {
		YourCommunityFragment fragment = new YourCommunityFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		//fragment.setUserVisibleHint(true);
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitleOnAction(Constants.yourCommunity, false);
		enableBackButton();
		//setHasOptionsMenu(true);
		//getActionBar().removeAllTabs();
		//getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_your_community,container, false);
		initView(rootView);
		initUi(rootView);
		setListener();
		setValueOnUi();
		return rootView;
	}


	@Override
	public void onClick(View v) {
	}

	@Override
	protected void initUi(View view) {
		listView = (ListView) view.findViewById(R.id.listView);
	}

	@Override
	protected void setValueOnUi() {
		// testMethod();
		ApiManager.getInstance().getYourCommunityApps(this);
	}

	@Override
	protected void setListener() {
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String packag = (String) view.getTag(R.id.link);
				Bundle bundle = new Bundle();
				bundle.putString(Constants.package_, packag);
				startMyActivity(AppDetailActivity.class, bundle);					
			}
		});
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}

	private void initView(View rootView) {
		mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);
		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		hs = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView);
		mTabHost.setup(getActivity(), getChildFragmentManager(),android.R.id.tabcontent);		        
		mTabHost.addTab(mTabHost.newTabSpec(Constants.free).setIndicator(Constants.free),DummyFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec(Constants.paid).setIndicator(Constants.paid),DummyFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec(Constants.topgrossing).setIndicator(Constants.topgrossing),DummyFragment.class, null);

		for (int i = 0; i < 3; i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
			View v = mTabHost.getTabWidget().getChildAt(i);
			UtilsClass.setTabTextColor(getActivity(), tv, v);
		}

		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				// TODO Auto-generated method stub
				//ToastCustomClass.showToast(getActivity(), ""+tabId);
				if (tabId.equals(Constants.free)) {
					if (jsonArrayFree != null) {
						adapter.setList(jsonArrayFree);
					}
				}else if (tabId.equals(Constants.paid)) {
					if (jsonArrayPaid != null) {
						adapter.setList(jsonArrayPaid);
					}
				}else if (tabId.equals(Constants.topgrossing)) {
					if (jsonArrayTopGrossing != null) {
						adapter.setList(jsonArrayTopGrossing);
					}
				}

			}
		});

	}


	public void testMethod(){
		JSONArray array = new JSONArray();
		for (int i = 0; i < 5; i++) {
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put(Constants.channelId, "1");
				jsonObject.put(Constants.image, "");
				jsonObject.put(Constants.title, "Binary Signal");
				jsonObject.put(Constants.description, "Binary Signal app description");
				jsonObject.put("link", "https://play.google.com/store/apps/details?id=com.binarysignals&hl=en");
				array.put(i, jsonObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		adapter = new ChartAppAdapter(this, array);
		listView.setAdapter(adapter);
	}

	public void setCommunity(JSONObject jsonObject) {
		if (jsonObject != null) {
			jsonArrayFree = jsonObject.optJSONArray(Constants.freeApps);
			jsonArrayPaid = jsonObject.optJSONArray(Constants.paidApps);
			jsonArrayTopGrossing = jsonObject.optJSONArray(Constants.topGrossingApps);

			if (jsonArrayFree != null) {
				adapter = new ChartAppAdapter(this, jsonArrayFree);
				listView.setAdapter(adapter);
			}
		}
	}
	


}
