package com.noto.gappss.fragments;

import org.json.JSONObject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.noto.gappss.InterestedAppsActivity;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.RoundedImageView;

public class CompareProfileFragment extends BaseFragment{
	public static final String TAG = Constants.compareProfile;
	private RoundedImageView userOneImg;
	private RoundedImageView userTwoImg;

	private TextView userOneName;
	private TextView userTwoName;
	private TextView userOneBtn;
	private TextView userTwoBtn;

	private TextView totalUserOne;
	private TextView commonUserOne;
	private TextView discoverUserOne;

	private TextView totalUserTwo;
	private TextView commonUserTwo;
	private TextView discoverUserTwo;

	private TextView brandUserOne;
	private TextView typeUserOne;
	private TextView osUserOne;

	private TextView brandUserTwo;
	private TextView typeUserTwo;
	private TextView osUserTwo;
	private Contact contact;

	public CompareProfileFragment() {

	}

	public static CompareProfileFragment newInstance(Bundle bundle) {
		CompareProfileFragment fragment = new CompareProfileFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		//fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		setTitleOnAction(TAG, false);
		((BaseFragmentActivity)getActivity()).enableBackButton();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_compare_profile, container, false);
		initUi(view);
		setListener();
		return view;
	}

	@Override
	public void onClick(View v) {

	}

	@Override
	protected void initUi(View view) {
		userOneImg = (RoundedImageView) view.findViewById(R.id.userOneImg);
		userTwoImg = (RoundedImageView) view.findViewById(R.id.userTwoImg);

		userOneName = (TextView) view.findViewById(R.id.userOneName);
		userTwoName = (TextView) view.findViewById(R.id.userTwoName);
		userOneBtn = (TextView) view.findViewById(R.id.userOneBtn);
		userTwoBtn = (TextView) view.findViewById(R.id.userTwoBtn);

		totalUserOne = (TextView) view.findViewById(R.id.totalUserOne);
		commonUserOne = (TextView) view.findViewById(R.id.commonUserOne);
		discoverUserOne = (TextView) view.findViewById(R.id.discoverUserOne);

		totalUserTwo = (TextView) view.findViewById(R.id.totalUserTwo);
		commonUserTwo = (TextView) view.findViewById(R.id.commonUserTwo);
		discoverUserTwo = (TextView) view.findViewById(R.id.discoverUserTwo);

		brandUserOne = (TextView) view.findViewById(R.id.brandUserOne);
		typeUserOne = (TextView) view.findViewById(R.id.typeUserOne);
		osUserOne = (TextView) view.findViewById(R.id.osUserOne);

		brandUserTwo = (TextView) view.findViewById(R.id.brandUserTwo);
		typeUserTwo = (TextView) view.findViewById(R.id.typeUserTwo);
		osUserTwo = (TextView) view.findViewById(R.id.osUserTwo);


	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		contact = (Contact) getArguments().getSerializable(Constants.contact);
		setValueOnUi();	
	}

	@Override
	protected void setValueOnUi() {
		ApiManager.getInstance().getDevicesDetails(this, contact.getId());
	}

	@Override
	protected void setListener() {

	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}

	public void setCompareData(JSONObject jObject) {
		if (jObject != null) {
			JSONObject user = jObject.optJSONObject(Constants.user);
			JSONObject friend = jObject.optJSONObject(Constants.friendstr);
			if(user != null){
				userOneName.setText(user.optString(Constants.USER_NAME));
				totalUserOne.setText(user.optString(Constants.totalApps));
				commonUserOne.setText(user.optString(Constants.totalCommanApps));
				discoverUserOne.setText(user.optString(Constants.totalDiscoverApps));
				brandUserOne.setText(user.optString(Constants.brand));
				typeUserOne.setText(user.optString(Constants.type));
				osUserOne.setText(user.optString(Constants.os));
			}
			if(friend != null){
				userTwoName.setText(friend.optString(Constants.USER_NAME));
				totalUserTwo.setText(friend.optString(Constants.totalApps));
				commonUserTwo.setText(friend.optString(Constants.totalCommanApps));
				discoverUserTwo.setText(friend.optString(Constants.totalDiscoverApps));
				brandUserTwo.setText(friend.optString(Constants.brand));
				typeUserTwo.setText(friend.optString(Constants.type));
				osUserTwo.setText(friend.optString(Constants.os));
			}
		}		
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.compare_profile, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Bundle bundle = new Bundle();	
		bundle.putSerializable(Constants.contact, contact);
		switch (item.getItemId()) {
		case R.id.i_int_channels:
			bundle.putBoolean(InterestedAppsActivity.isApps, false);
			startMyActivity(InterestedAppsActivity.class, bundle);
			return true;
		case R.id.i_int_apps:
			bundle.putBoolean(InterestedAppsActivity.isApps, true);
			startMyActivity(InterestedAppsActivity.class, bundle);
			return true;	
		}
		return super.onOptionsItemSelected(item);
	}
}
