package com.noto.gappss.fragments;

import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.networkTask.URLsClass;

public class ChannelSettingFragment extends BaseFragment{

	private CheckBox checkbox_one;
	private CheckBox checkbox_two;
	private CheckBox checkbox_three;
	private CheckBox checkbox_four;



	public static final String TAG = "CHANNEL_SETTING";

	public ChannelSettingFragment() {
	}

	public static ChannelSettingFragment newInstance(Bundle bundle) {
		ChannelSettingFragment fragment = new ChannelSettingFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// return super.onCreateView(inflater, container, savedInstanceState);
		setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_channel_setting, null);
		initUi(view);		
		setListener();
		setValueOnUi();
		// Log.i("sdf", null);

		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub

		checkbox_one = (CheckBox) view.findViewById(R.id.checkbox_one);
		checkbox_two = (CheckBox) view.findViewById(R.id.checkbox_two);
		checkbox_three = (CheckBox) view.findViewById(R.id.checkbox_three);
		checkbox_four = (CheckBox) view.findViewById(R.id.checkbox_four);



	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		ApiManager.getInstance().getChannelSettings((BaseFragmentActivity)getActivity());

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub


	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public void serviceResponse(JSONObject response) {
		// TODO Auto-generated method stub
		Log.i("RESPONSEEEEEEE", response.toString());
		if (response != null) {

			checkbox_one.setChecked(response.optString(Constants.p_show_subscribe_feeds).equalsIgnoreCase("1") ? true : false);
			checkbox_two.setChecked(response.optString(Constants.p_allow_post).equalsIgnoreCase("1") ? true : false);
			checkbox_three.setChecked(response.optString(Constants.p_allow_search).equalsIgnoreCase("1") ? true : false);
			checkbox_four.setChecked(response.optString(Constants.p_allow_feeds).equalsIgnoreCase("1") ? true : false);
			
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.save:
			String show_subscribe_feeds = checkbox_one.isChecked() ? "1" : "0";
			String allow_post = checkbox_two.isChecked() ? "1" : "0";
			String allow_search = checkbox_three.isChecked() ? "1" : "0";
			String allow_feeds = checkbox_four.isChecked() ? "1" : "0";
			ApiManager.getInstance().setChannelSettings((BaseFragmentActivity)getActivity(), show_subscribe_feeds, allow_post, allow_search, allow_feeds);
			break;

		default:
			getActivity().finish();
			break;
		}


		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {	
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.save, menu);
	}
}
