package com.noto.gappss.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.noto.gappss.HomeActivityTest;
import com.noto.gappss.NewAppActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.NewAppAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.db.DataBaseQueriesClass;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.preference.MySharedPreferences;
import com.noto.gappss.utilities.UtilsClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class NewAppInstallFragment extends BaseFragment {
	private ListView new_app_list;
	private SearchView new_app_search;
	private TextView btn_get_apps;
	private TextView btn_app_status,testing;
	public static String TAG = "NEW_APP";
	NewAppAdapter adapter;
	//ArrayList<ApplicationBean> fromServer = new ArrayList<ApplicationBean>();
	//ArrayList<ApplicationBean> installedApp =  null;
	HashMap<String, ApplicationBean> installedApp = null;

	//ArrayList<ApplicationBean> installedApp =  new ArrayList<ApplicationBean>();;

	public NewAppInstallFragment(){		
	}

	public static NewAppInstallFragment getInstance(){
		NewAppInstallFragment fragment = new NewAppInstallFragment();
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_new_app, null);
		initUi(view);
		setListener();
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);		
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setValueOnUi();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_get_apps:
			btn_get_apps.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_appstatus));
			btn_app_status.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_getapps));
			if(getActivity() instanceof NewAppActivity){
				ArrayList<ApplicationBean> listFrndsApp = ((NewAppActivity)getActivity()).getFriendsAppList();
				if(listFrndsApp != null)
					serviceResponseGetFriendAppVisibility(listFrndsApp);
				else
					getFriendAppVisibility();
			}else{
				getFriendAppVisibility();
			}			
			break;
		case R.id.btn_app_status:
			btn_app_status.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_appstatus));
			btn_get_apps.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_getapps));

			ArrayList<ApplicationBean> list = new ArrayList<ApplicationBean>();
			Set<String> keySet = installedApp.keySet();
			Iterator<String> iterator = keySet.iterator();
			while (iterator.hasNext()) {
				String key = (String) iterator.next();
				ApplicationBean app = installedApp.get(key);
				list.add(app);
			}
			Bundle bundle = new Bundle();
			bundle.putSerializable(Constants.list, list);
			replaceFragment(AppStatusFragment.getInstance(bundle), AppStatusFragment.TAG);
			break;
		}
	}

	@Override
	protected void initUi(View view) {
		view.findViewById(R.id.lin).setVisibility(View.GONE);
		new_app_list =(ListView) view.findViewById(R.id.new_app_list);
		new_app_search = (SearchView) view.findViewById(R.id.new_app_search);
		btn_get_apps = (TextView) view.findViewById(R.id.btn_get_apps);
		btn_app_status = (TextView) view.findViewById(R.id.btn_app_status);
	}

	@Override
	protected void setValueOnUi() {
		new DownloadAppData().execute("");
	}

	@Override
	protected void setListener() {
		btn_get_apps.setOnClickListener(this);
		btn_app_status.setOnClickListener(this);

		new_app_list.setTextFilterEnabled(true);
		new_app_search.setIconifiedByDefault(false);
		new_app_search.setQueryHint("Search...");
		new_app_search.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				if (TextUtils.isEmpty(newText.toString())) {
					new_app_list.clearTextFilter();
					//Log.i("if onQueryTextChange", "onQueryTextChange=="+newText.toString());
				} else {
					new_app_list.setFilterText(newText.toString());
					//Log.i("else onQueryTextChange", "onQueryTextChange=="+newText.toString());
				}
				return true;
			}
		});
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}

	public void getAppVisibility(){
		//ApiManager.getInstance().getAppVisibility(this);
		/*	String urlsArr[] = {URLsClass.service_type_getAppVisibility};
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
		params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(getActivity(), Constants.USER_ID, ""));
		serviceCaller(this, urlsArr, params, true, false, Constants.getAppVisibility, true);*/
		
		serviceResponseGetAppVisibility(DataBaseQueriesClass.getInstance().getAppDetailHashMap(getActivity()));
	}

	public void setAppVisibility(){		
		StringBuffer appList = new StringBuffer();
		if (adapter == null) {
			return;
		}
		ArrayList<ApplicationBean> appStatus = adapter.getList();
		if (appStatus != null) {
			for (int i = 0; i < appStatus.size(); i++) {
				ApplicationBean app = appStatus.get(i);
				installedApp.put(app.getPackageName(), app);
				//appList = appList+URLsClass.p_appName+":"+appStatus.get(i).getTitle()+"|"+URLsClass.p_appPack+":"+appStatus.get(i).getPackageName()+"|"+URLsClass.p_app_url+":"+""+"|"+URLsClass.status+"="+(appStatus.get(i).isInstalled() ? appStatus.get(i).getStatus() : 3)+",";
			}

			Set<String> keySet = installedApp.keySet();
			Iterator<String> iterator = keySet.iterator();
			while (iterator.hasNext()) {
				String key = (String) iterator.next();
				ApplicationBean app = installedApp.get(key);
				appList.append(Constants.p_appName+":"+app.getTitle()+"|"+Constants.p_appPack+":"+app.getPackageName()+"|"+Constants.p_app_url+":"+""+"|"+URLsClass.status+"="+(app.isInstalled() ? app.getStatus() : 3));
				appList.append(",");
			}

			String result = UtilsClass.removeStringLastChar(appList,',');
			Log.i("sdf", result);


			String urlsArr[] = {URLsClass.service_type_setAppVisibility};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(getActivity(), Constants.USER_ID, ""));
			params.put(Constants.p_app_list,  new String(Base64.encode(result.getBytes(), Base64.DEFAULT)));
			serviceCaller(this, urlsArr, params, true, false, Constants.setAppVisibility, true);
		}
	}

	public void getFriendAppVisibility(){
		String urlsArr[] = {URLsClass.service_type_getFriendsAppVisibility};		
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
		params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(getActivity(), Constants.USER_ID, ""));
		serviceCaller(this, urlsArr, params, true, false, Constants.getFriendAppVisibility, true);
	}
	public void serviceResponseGetAppVisibility(HashMap<String, ApplicationBean> mapOfApp){
		//this.fromServer = arrayList;
		ArrayList<ApplicationBean> newAppList = new ArrayList<ApplicationBean>();
		if (mapOfApp != null) {
			Set<String> keySet = installedApp.keySet();
			Iterator<String> iterator = keySet.iterator();			
			while (iterator.hasNext()) {
				String key = (String) iterator.next();				
				if(!mapOfApp.containsKey(key)){
					newAppList.add(installedApp.get(key));				
				}else{
					ApplicationBean app = installedApp.get(key);
					app.setStatus(mapOfApp.get(key).getStatus());
					installedApp.put(key, app);
				}					
			}		

			Set<String> keySetServer = mapOfApp.keySet();
			if(keySetServer.size() > 0){
				Iterator<String> iteratorServer = keySetServer.iterator();
				while (iteratorServer.hasNext()) {
					String keyServer = (String) iteratorServer.next();
					if(!installedApp.containsKey(keyServer)){
						ApplicationBean app = mapOfApp.get(keyServer);
						app.setStatus(3);
						app.setInstalled(false);
						installedApp.put(keyServer, app);
					}
				}
			}		


			if(newAppList.size() > 0)
				setAdapterValues(newAppList, 0);
			else{
				//startMyActivity(HomeActivity.class, null);
				startMyActivity(HomeActivityTest.class, null);//for testing
				getActivity().finish();
			}
		}

		setTitleOnAction("Your New Apps ("+newAppList.size()+")", false);
	}
	private void setAdapterValues(ArrayList<ApplicationBean> installedApp, int dataState){
		adapter = new NewAppAdapter(this, installedApp);
		adapter.setAdapterDataState(dataState);			
		new_app_list.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	public void serviceResponseSetAppVisibility( boolean res){		
		if (res) {
			
			//startMyActivity(HomeActivity.class, null);
			startMyActivity(HomeActivityTest.class, null); //for testing
			getActivity().finish();
		}
	}
	public void serviceResponseGetFriendAppVisibility(ArrayList<ApplicationBean> arrayList){		
		adapter = new NewAppAdapter(this, arrayList);
		adapter.setAdapterDataState(1);
		new_app_list.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}




	private class DownloadAppData extends AsyncTask<String, Void, String> {
		private ProgressDialog mProgressDialog;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(getActivity());
			mProgressDialog.setMessage("Please wait...\nSearching new installed applications");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setCancelable(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected String doInBackground(String... data) {
			exportApks(getActivity());
			return null;
		}

		@Override
		protected void onPostExecute(String str) {
			// Close progressdialog
			mProgressDialog.dismiss();	
			getAppVisibility();
		}
	}

	@SuppressLint("NewApi")
	public void exportApks(Context context){

		PackageManager  pm = context.getPackageManager();
		List<PackageInfo> pkginfolist = pm.getInstalledPackages(PackageManager.GET_ACTIVITIES);
		List<ApplicationInfo> appinfoList = pm.getInstalledApplications(0);


		installedApp = new HashMap<String, ApplicationBean>();
		//  progress.progress(BackupTask.MESSAGE_COUNT, pkginfolist.size());
		for (int x = 0; x < pkginfolist .size(); x++) {
			ApplicationBean app = new ApplicationBean();
			ApplicationInfo appInfo = appinfoList.get(x); 
			String appname = pm.getApplicationLabel(appInfo).toString();
			String pkgName = appInfo.packageName;

			Drawable icon = null;
			try {
				icon = pm.getApplicationIcon(appInfo.packageName);
			} catch (NameNotFoundException e) {
				//e.printStackTrace();
			}

			app.setTitle(appname);
			app.setPackageName(pkgName);
			app.setApplogo(icon);
			app.setInstalled(true);
			app.setStatus(1);
			if(!isSystemPackage(appInfo))
				installedApp.put(pkgName, app);
		}
	}

	private boolean isSystemPackage(ApplicationInfo applicationInfo) {
		return ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.ok, menu);		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.cart:
			setAppVisibility();
			return true;			
		}		
		return super.onOptionsItemSelected(item);
	}
}

