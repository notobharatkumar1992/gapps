package com.noto.gappss.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.noto.gappss.FriendDetailActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.InviteAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.AppCategory;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class InviteFriendFragment extends BaseFragment implements OnItemClickListener{

	private ListView list_item;
	private SearchView search;
	//TextView btn_invite;
	public static String TAG = "CONTACTS_FRA";
	InviteAdapter adapter;
	private TextView btn_invite;
	ArrayList<AppCategory> arrayList = new ArrayList<AppCategory>();
	HashMap<String, Contact> mapContacts = new HashMap<String, Contact>();
	private String cId = "";
	private String frgType;

	public InviteFriendFragment(){		
	}

	public static InviteFriendFragment newInstance(Bundle bundle) {
		InviteFriendFragment fragment = new InviteFriendFragment();
		if(bundle !=  null)
			fragment.setArguments(bundle);
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);	
		 setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//return super.onCreateView(inflater, container, savedInstanceState);
		//setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_invite, null);
		initUi(view);
		setListener();
		setValueOnUi();
		//Log.i("sdf", null);
		return view;
	}


	@Override
	public void onClick(View v) {
     switch (v.getId()) {
	 case R.id.btn_invite:
		 if (frgType.equals(MyAppShareFragment.TAG)) {
				UtilsClass.underConstruction(getActivity());
			}else {
				sendInvitation();
			}
		
		break;

	  }
	}

	@Override
	protected void initUi(View view) {
		search = (SearchView) view.findViewById(R.id.search);
		list_item = (ListView) view.findViewById(R.id.list_item);
		btn_invite = (TextView) view.findViewById(R.id.btn_invite);
		//	btn_invite = (TextView) view.findViewById(R.id.btn_invite);
	}

	@Override
	protected void setValueOnUi() {
		if (frgType.equals(MyAppShareFragment.TAG)) {
			btn_invite.setText(getActivity().getResources().getString(R.string.share_apps));
		}else {
			btn_invite.setText(getActivity().getResources().getString(R.string.invite));
		}
		new DownloadContact().execute("");
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

		btn_invite.setOnClickListener(this);
		list_item.setOnItemClickListener(this);
		list_item.setItemsCanFocus(true);

		//btn_invite.setOnClickListener(this);
		list_item.setTextFilterEnabled(true);
		search.setIconifiedByDefault(false);
		search.setQueryHint("Search Contacts");
		search.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				if (TextUtils.isEmpty(newText.toString())) {
					list_item.clearTextFilter();
					//Log.i("if onQueryTextChange", "onQueryTextChange=="+newText.toString());
				} else {
					list_item.setFilterText(newText.toString());
					//Log.i("else onQueryTextChange", "onQueryTextChange=="+newText.toString());
				}
				return true;
			}
		});



	}
	
	@Override
	public void onAttach(Activity activity) {	
		super.onAttach(activity);
		Bundle bundle = getArguments();
		cId  = bundle.getString(Constants.id);
		frgType  = bundle.getString(Constants.type);
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}

	private class DownloadContact extends AsyncTask<String, Void, String> {
		private ProgressDialog mProgressDialog;
		//ArrayList<Invite> arrayList = new ArrayList<>();
		String contacts="";
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(getActivity());
			// Set progressdialog title
			mProgressDialog.setTitle("Please wait");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading contacts...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected String doInBackground(String... data) {
			contacts = getContactWithAllPhoneNumber(getActivity());
			return null;
		}

		@Override
		protected void onPostExecute(String str) {
			// Set the bitmap into ImageView
			// Close progressdialog
			mProgressDialog.dismiss();
			
			ApiManager.getInstance().getContactList(InviteFriendFragment.this, contacts);

		}
	}



	public  String getContactWithAllPhoneNumber(Context context) {

		StringBuffer totalNumber = new StringBuffer();
		//MySharedPreferences.getInstance().getString(getActivity(), Constants.p_mobile_no, "");
	/*	totalNumber.append("9929572110"+"|");
		totalNumber.append("8302510544"+"|");
		totalNumber.append("0633031052"+"|");
		totalNumber.append("0641424322"+"|");
		totalNumber.append("0613359720"+"|");*/
		
		Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
		while (phones.moveToNext())
		{
			String displayName = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
			String mobile = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			if (mobile != null && !mobile.equals("")) {				
				if(isValidMobile(mobile)) {
					try {
						mobile = mobile.substring(mobile.length() - 10);
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
					totalNumber.append(mobile+"|");
					Contact invite = new Contact();
					invite.setName(displayName);
					invite.setMobileNo(mobile);
					mapContacts.put(mobile, invite);									
				}
			}
		}
		phones.close();	
		return UtilsClass.removeStringLastChar(totalNumber, '|');


	}
	
	private boolean isValidMobile(String phone) 
	{
		return android.util.Patterns.PHONE.matcher(phone).matches();   
	}

	public  String getDigitString(String string){

		char c;
		StringBuffer strBuff = new StringBuffer();
		for (int i = 0; i < string.length() ; i++) {
			c = string.charAt(i);

			if (Character.isDigit(c)) {
				strBuff.append(c);
			}
		}
		return strBuff.toString();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		//ToastCustomClass.showToast(getActivity(), "TESTING");
		String userId = (String) view.getTag(R.id.invite_title);
		Bundle bundle = new Bundle();
		bundle.putString(Constants.USER_ID, userId);
		startMyActivity(FriendDetailActivity.class, bundle);
	}

	public void serverResponsGetContactList(ArrayList<Contact> arrayList) {
		/*for (int i = 0; i < arrayList.size(); i++) {
			Contact contact = mapContacts.get(arrayList.get(i).getMobileNo());
			arrayList.get(i).setName(contact.getName());
		}*/

		adapter = new InviteAdapter(InviteFriendFragment.this, arrayList);
		list_item.setAdapter(adapter);
	}
	
	public void serverResponsSentRequest() {
	
/*
		ArrayList<Contact> arrayList = adapter.getNoFriendList();
		if (arrayList != null && arrayList.size() > 0) {
			for (int i = 0; i < arrayList.size(); i++) {
				if (arrayList.get(i).isChecked()) {
					arrayList.get(i).setType(Constants.sentRequest);					
				}
			}			
		}
		
		adapter.notifyDataSetChanged();*/
	}

	

	private void sendInvitation(){
		if (adapter != null) {
			ArrayList<Contact> arrayList = adapter.getList();
			if (arrayList != null && arrayList.size() > 0) {
				StringBuffer receiverId = new StringBuffer();
				for (int i = 0; i < arrayList.size(); i++) {
					if (arrayList.get(i).isChecked()) {
						receiverId.append(arrayList.get(i).getId());
						receiverId.append("|");
					}
				}
				if(receiverId.length() > 0){
					String result = UtilsClass.removeStringLastChar(receiverId, '|');
					if (frgType.equals(YourChannelDetailFragment.TAG)) {
						ApiManager.getInstance().sendChannelInvitation(this, result, cId);	
					}else {
						
					}
					
				}
				else{
					ToastCustomClass.showToast(getActivity(), "Please select contact!");
				}
			}
		}	
	}
	
	public void serverResponse(){
		getActivity().finish();
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		//super.onCreateOptionsMenu(menu, inflater);		
		
		if (frgType.equals(YourChannelDetailFragment.TAG)) {
			inflater.inflate(R.menu.search, menu);
		}else {
			super.onCreateOptionsMenu(menu, inflater);
			inflater.inflate(R.menu.search, menu);
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.i_search:			
			if (search.getVisibility() == View.VISIBLE) {
				search.setVisibility(View.GONE);
			}else {
				search.setVisibility(View.VISIBLE);
			}
			break;			
		
		}	
		return false;
	}
	

}
