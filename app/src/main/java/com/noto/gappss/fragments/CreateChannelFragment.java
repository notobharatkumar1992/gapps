package com.noto.gappss.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RadioGroup;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.CameraGallery;
import com.noto.gappss.utilities.GetRealPath;
import com.noto.gappss.utilities.ToastCustomClass;

import eu.janmuller.android.simplecropimage.CropImage;


public class CreateChannelFragment extends BaseFragment{
	
	private ImageView img_logo;
	private RadioGroup redio_group_one;
	private RadioGroup redio_group_two;
	private EditText edit_fullnane;
	private EditText edit_comp_name;
	private EditText edit_comp_url;
	private EditText edit_number;
	private EditText edit_channel_desc;
	
	
	public static final String TAG = "CREATE_CHANNEL";
	private String path = "";
	
	public CreateChannelFragment() {
	}

	public static CreateChannelFragment newInstance(Bundle bundle) {
		CreateChannelFragment fragment = new CreateChannelFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// return super.onCreateView(inflater, container, savedInstanceState);
		 setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_create_channel, null);
		initUi(view);		
		setListener();
		// Log.i("sdf", null);
		
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.img_logo:
			
			new CameraGallery().openImageSource(getActivity() , this);
			break;
		}
		
	}

	@Override
	protected void initUi(View view) {
		setTitleOnAction(getResources().getString(R.string.create_channel),false);
		img_logo = (ImageView) view.findViewById(R.id.img_logo);
		redio_group_one = (RadioGroup) view.findViewById(R.id.redio_group_one);
		redio_group_two = (RadioGroup) view.findViewById(R.id.redio_group_two);
		edit_fullnane = (EditText) view.findViewById(R.id.edit_fullnane);
		edit_comp_name = (EditText) view.findViewById(R.id.edit_comp_name);
		edit_comp_url = (EditText) view.findViewById(R.id.edit_comp_url);
		edit_number = (EditText) view.findViewById(R.id.edit_number);
		edit_channel_desc = (EditText) view.findViewById(R.id.edit_channel_desc);		
	}

	@Override
	protected void setValueOnUi() {
		
	}

	@Override
	protected void setListener() {
		img_logo.setOnClickListener(this);
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}

	public void serviceResponse() {

		Intent intent=new Intent();  
        intent.putExtra("Result","ok");  
        getActivity().setResult(Activity.RESULT_OK,intent);  
		getActivity().finish();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.save:			
			 String channelType = "";
			 String channelMode = "";
			// Bitmap bitmap = img_logo.getDrawable().get//((BitmapDrawable)img_logo.getDrawable()).getBitmap();
			 img_logo.buildDrawingCache();
			 Bitmap bmap = img_logo.getDrawingCache();
			 
			switch (redio_group_one.getCheckedRadioButtonId()) {
			case R.id.radio_business:
				channelType = "1";
				break;

	        case R.id.radio_personal:
	        	channelType = "2";
				break;
			}
			
			switch (redio_group_two.getCheckedRadioButtonId()) {
			case R.id.radio_open:
				channelMode = "1";
				break;

	        case R.id.radio_invite:
	        	channelMode = "2";
				break;
			}
			
			if (edit_fullnane.getText().toString().trim().equals("")) {
				ToastCustomClass.showToast(getActivity(), getActivity().getResources().getString(R.string.val_channel_name));
			}else if (edit_channel_desc.getText().toString().trim().equals("")){
				ToastCustomClass.showToast(getActivity(), getActivity().getResources().getString(R.string.val_channel_des));
			}else {
				Log.i("CropImage", "CropImage=="+path);
				ApiManager.getInstance().createChannel((BaseFragmentActivity)getActivity(), edit_fullnane.getText().toString().trim(), edit_channel_desc.getText().toString().trim(), channelType, channelMode,path);
			}			
			break;
		default:
			 Intent intent=new Intent();  
             intent.putExtra("Result","ok");  
             getActivity().setResult(Constants.requestCode,intent);  
			 getActivity().finish();
			break;
		}		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.i("onActivityResult", "resultCode=="+resultCode+"  requestCode=="+requestCode+" Activity.RESULT_OK="+Activity.RESULT_OK);
		if(resultCode == Activity.RESULT_OK){
			switch(requestCode){
			case CameraGallery.REQUEST_CODE_GALLERY:{
				Log.i("REQUEST_CODE_GALLERY", "REQUEST_CODE_GALLERY");
				try {
					CameraGallery cameraAct = new CameraGallery();
					/*InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
					Log.i("REQUEST_CODE_GALLERY", "REQUEST_CODE_GALLERY=="+cameraAct.mFileTemp.getPath());
					FileOutputStream fileOutputStream = new FileOutputStream(cameraAct.mFileTemp.getPath());
					cameraAct.copyStream(inputStream, fileOutputStream);
					fileOutputStream.close();
					inputStream.close();*/
					
					String realpath;
					if (Build.VERSION.SDK_INT < 11) {
						realpath = GetRealPath.getRealPathFromURI_BelowAPI11(getActivity(),
								data.getData());
					

					} else if (Build.VERSION.SDK_INT < 19) {
						realpath = GetRealPath.getRealPathFromURI_API11to18(getActivity(),
								data.getData());
						

					} else {
						realpath = GetRealPath.getRealPathFromURI_API19(getActivity(),
								data.getData());
						

					}

					//cameraAct.startCropImage(getActivity(), null, cameraAct.mFileTemp.getPath());
					Log.i("PATHSSSSSSSS", "==="+realpath);
					cameraAct.startCropImage(getActivity(), this, realpath);
				} catch (Exception e) {
					Log.e(TAG, "Error while creating temp file", e);
				}
				break;
			}
			case CameraGallery.REQUEST_CODE_TAKE_PICTURE:{
				Log.i("REQUEST_CODE_TAKE_PICTURE", "REQUEST_CODE_TAKE_PICTURE");
				CameraGallery cameraAct = new CameraGallery();	
					//String action = data.getAction();
					//Parcelable parcelable = data.getParcelableExtra("data");
					//Bitmap bit = (Bitmap) data.getExtras().get("data");
					//Uri selectedImageUri = Uri.fromFile(CameraGallery.mFileTemp);
					//Uri selectedImageUri = data.getData();
					if(CameraGallery.mFileTemp != null){					
						cameraAct.startCropImage(getActivity(), this, CameraGallery.mFileTemp.getPath());
					}
				break;
			}
			case CameraGallery.REQUEST_CODE_CROP_IMAGE:{
				Log.i("REQUEST_CODE_CROP_IMAGE", "REQUEST_CODE_CROP_IMAGE");
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				//String path = CameraGallery.mFileTemp.getPath();
				//String caption = data.getStringExtra(CropImage.IMAGE_TITLE);
				if (path == null) {
					return;
				}
				try{
					this.path = path;
					/*int width = (MyApplication.getApplication().getWidthPixel()/3)-40;
					LinearLayout layout = new LinearLayout(getActivity());
					layout.setOrientation(LinearLayout.HORIZONTAL);
					layout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

					LinearLayout.LayoutParams imageViewLayoutParams = new LinearLayout.LayoutParams(width, width);
					imageViewLayoutParams.setMargins(0, 0, 20, 0);

					ImageView imageView = new ImageView(getActivity());*/
					//img_logo.setLayoutParams(imageViewLayoutParams);
					//img_logo.setBackgroundResource(R.drawable.image_fram9);
				//	Log.i("PATHSSSSS", path);
					//ToastCustomClass.showToast(getActivity(), "Set Image");
					img_logo.setImageBitmap(BitmapFactory.decodeFile(path));
					img_logo.setScaleType(ScaleType.CENTER_CROP);
							

					CameraGallery.mFileTemp = null;
	
				} catch (Exception e) {
					e.printStackTrace();
					ToastCustomClass.showToast(getActivity(), "Something went wrong!");
				}
				break;
			}
			}
		}
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.save, menu);
	}
}
