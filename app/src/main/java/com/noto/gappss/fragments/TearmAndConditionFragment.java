package com.noto.gappss.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.URLsClass;

public class TearmAndConditionFragment extends BaseFragment {

	public static final String TAG = Constants.TearmAndConditionFragment;
	private WebView webView;

	public TearmAndConditionFragment() {

	}

	public static TearmAndConditionFragment newInstance(Bundle bundle) {
		TearmAndConditionFragment fragment = new TearmAndConditionFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		// fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_tearm_and_condition,
				container, false);
		initUi(view);
		setListener();
		setValueOnUi();
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		webView = (WebView) view.findViewById(R.id.webView);

	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		Bundle bundle = getArguments();
		int type = bundle.getInt(Constants.type);
		switch (type) {
		case 1:
			setTitleOnAction(getActivity().getResources().getString(R.string.terms_and_conditions), false);
			webView.loadUrl(URLsClass.tearmAndCondition);
			break;
		case 2:
			setTitleOnAction(getActivity().getResources().getString(R.string.privacyPolicy), false);
			webView.loadUrl(URLsClass.privacyPolicy);
			break;
		case 3:
			setTitleOnAction(getActivity().getResources().getString(R.string.openSourceLic), false);
			webView.loadUrl(URLsClass.openSourceLicenses);
			break;
		}

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

}
