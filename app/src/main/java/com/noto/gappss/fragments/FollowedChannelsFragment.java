package com.noto.gappss.fragments;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;

import com.noto.gappss.ExplorerChannelDetailActivity;
import com.noto.gappss.FollowingChannelDetailActivity;
import com.noto.gappss.PostListActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.FollowingChannelsAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelDetail;
import com.noto.gappss.networkTask.ApiManager;

public class FollowedChannelsFragment extends BaseFragment{
	public static final String TAG = "FOLLOWED_CH";
	public String title = "Channel";
	private JSONArray jsonArray;
	private ListView listView;
	private FollowingChannelsAdapter adapter;
	private SearchView new_app_search;

	public FollowedChannelsFragment(){			
		//Log.i(TAG, "Root Cat Contructor");
	}

	public static FollowedChannelsFragment newInstance(JSONArray jsonArray){
		FollowedChannelsFragment rootF = new FollowedChannelsFragment();
		rootF.jsonArray = jsonArray;
		return rootF;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_friend_channel_list, null);
		initUi(view);
		setListener();
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		setValueOnUi();	
		
	}

	

	@Override
	public void onSaveInstanceState(Bundle outState) {		
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

	}

	public void setChannels(JSONArray explorer){
	
		jsonArray = explorer;
		adapter = new FollowingChannelsAdapter(this, explorer);
		listView.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {

	}

	@Override
	protected void initUi(View view) {
		new_app_search = (SearchView) view.findViewById(R.id.new_app_search);
		setTitleOnAction(getString(R.string.channels), false);
		listView = (ListView) view.findViewById(R.id.listView);
	}

	@Override
	protected void setValueOnUi() {
		//if(jsonArray == null)
			ApiManager.getInstance().getFollowedChannels(this);
		/*else
			setChannels(jsonArray);*/
		//setChannels(jsonArray);
	}
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		jsonArray = null;
	}

	@Override
	protected void setListener() {
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				JSONObject jO =(JSONObject) view.getTag(R.id.text_channel);
				ChannelDetail detail = new ChannelDetail();				
				detail.setChannelId(jO.optString(Constants.channelId));
				detail.setChannelName(jO.optString(Constants.channelName));
				detail.setChannelDescription(jO.optString(Constants.channelDescription));
				detail.setChannelType(jO.optString(Constants.channelType));
				detail.setChannelMode(jO.optString(Constants.channelMode));
				detail.setChannelImage(jO.optString(Constants.channelImage));

				Bundle bundle = new Bundle();
				bundle.putString(Constants.channelId, detail.getChannelId());

				bundle.putSerializable(Constants.channel, detail);
				//startMyActivity(FollowingChannelDetailActivity.class, bundle);
				startMyActivity(PostListActivity.class, bundle);
			}
		});
	}

	@Override
	public boolean onBackPressedListener() {
		return true;
	}

	public void refreshList() {
		// TODO Auto-generated method stub
		adapter.refreshList();
		
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		//super.onCreateOptionsMenu(menu, inflater);		
		inflater.inflate(R.menu.search, menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.i_search:			
		
			if (new_app_search.getVisibility() == View.VISIBLE) {
				new_app_search.setVisibility(View.GONE);
			}else {
				new_app_search.setVisibility(View.VISIBLE);
			}
			break;			
		
		}	
		return false;
	}
}
