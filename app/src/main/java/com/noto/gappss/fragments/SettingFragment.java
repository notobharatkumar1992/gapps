package com.noto.gappss.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.preference.MySharedPreferences;
import com.noto.gappss.utilities.UtilsClass;

public class SettingFragment extends BaseFragment {

	public static String TAG = "SETTING";
	private TextView account;
	private TextView notification;
	private TextView about;
	private TextView tab_setting;
	private TextView tab;
	final String[] arrayString = {"Apps", "Feeds", "Channels", "Inbox", "Contacts"};
	public SettingFragment() {
		
	}

	public static SettingFragment newInstance(Bundle bundle) {
		SettingFragment fragment = new SettingFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitleOnAction(getActivity().getResources().getString(R.string.settings), false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_setting, null);
		initUi(view);
		setValueOnUi();
		setListener();

		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.account:
			addFragment(AccountFragment.newInstance(null), AccountFragment.TAG,true);
			UtilsClass.underConstruction(getActivity());
			break;
		case R.id.notification:
			addFragment(NotificationFragment.newInstance(null), NotificationFragment.TAG,true);
			UtilsClass.underConstruction(getActivity());
			break;
		case R.id.about:
			addFragment(AboutUsFragment.newInstance(null), AboutUsFragment.TAG,true);
			UtilsClass.underConstruction(getActivity());
			break;
		case R.id.tab:
			alertDialogTab();
			break;
		}

	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		account = (TextView) view.findViewById(R.id.account);
		notification = (TextView) view.findViewById(R.id.notification);
		about = (TextView) view.findViewById(R.id.about);
		tab_setting= (TextView) view.findViewById(R.id.tab_setting);
		tab = (TextView) view.findViewById(R.id.tab);
		

	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		tab_setting.setText(arrayString[MySharedPreferences.getInstance().getInteger(getActivity(), Constants.drawerTab, 1)]);
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		account.setOnClickListener(this);
		notification.setOnClickListener(this);
		about.setOnClickListener(this);
		tab.setOnClickListener(this);

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void alertDialogTab(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Set open startup page.");
		

		builder.setItems(arrayString,
				new DialogInterface.OnClickListener() {
					@SuppressWarnings("deprecation")
					public void onClick(DialogInterface dialog, int position) {
						// Toast.makeText(getApplicationContext(),
						
				     // ToastCustomClass.showToast(SignupActivity.this, arrayString[position]);
						MySharedPreferences.getInstance().putIntegerKeyValue(getActivity(), Constants.drawerTab, position);
						tab_setting.setText(arrayString[position]);
					}
				});
		AlertDialog alert = builder.create();
		// alert.getWindow().setLayout(50, 50);
		alert.show();
	}


}
