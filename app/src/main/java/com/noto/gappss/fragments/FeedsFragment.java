package com.noto.gappss.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabWidget;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class FeedsFragment extends BaseFragment{

	private FragmentTabHost mTabHost;
	public static final String TAG = Constants.feedsT;
	TabWidget widget;
	HorizontalScrollView hs;
	public FeedsFragment() {

	}

	public static FeedsFragment newInstance(Bundle bundle) {
		FeedsFragment fragment = new FeedsFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		fragment.setUserVisibleHint(true);
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//disableBackButton();
		//String name = MySharedPreferences.getInstance().getString(getActivity(), Constants.USER_NAME, "Guest");
		setTitleOnAction(TAG, false);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_tabs,container, false);
		initView(rootView);

		/*mTabHost = (FragmentTabHost)rootView.findViewById(android.R.id.tabhost);
	        mTabHost.setup(getActivity(), getChildFragmentManager(), R.id.realtabcontent);

	        mTabHost.addTab(mTabHost.newTabSpec(AllFeedsFragment.TAG).setIndicator("All"),
	        		AllFeedsFragment.class, null);
	        mTabHost.addTab(mTabHost.newTabSpec(FriendFeedsFragment.TAG).setIndicator("Friends"),
	        		FriendFeedsFragment.class, null);
	        mTabHost.addTab(mTabHost.newTabSpec("fragmentd").setIndicator("Channels"),
	        		AllFeedsFragment.class, null);
	        mTabHost.addTab(mTabHost.newTabSpec("fragmentd").setIndicator("Favorites"),
	        		AllFeedsFragment.class, null);

		 */

		return rootView;
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	private void initView(View rootView) {
		mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);

		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		hs = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView);


		mTabHost.setup(getActivity(), getChildFragmentManager(),android.R.id.tabcontent);
		Bundle bundle = new Bundle();
		bundle.putInt(Constants.type,1);
		mTabHost.addTab(mTabHost.newTabSpec(AllFeedsFragment.TAG).setIndicator("All"),AllFeedsFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec(FriendFeedsFragment.TAG).setIndicator("Friends"),FriendFeedsFragment.class, bundle);
		mTabHost.addTab(mTabHost.newTabSpec(ChannelsFeedsFragment.TAG).setIndicator("Channels"),ChannelsFeedsFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec(FavoritesFeedsFragment.TAG).setIndicator("Favorites"),FavoritesFeedsFragment.class, null);

		for (int i = 0; i < 4; i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
			View v = mTabHost.getTabWidget().getChildAt(i);
			UtilsClass.setTabTextColor(getActivity(), tv, v);			
		}
		
	}

	/*	 @Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {	
			inflater.inflate(R.menu.setting, menu);
			super.onCreateOptionsMenu(menu, inflater);		
		}


		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {
			case R.id.i_settings:			
				startMyActivity(SettingActivity.class, null);
				break;			
			default:
				UtilsClass.underConstruction(getActivity());
				break;
			}	
			return false;
		}*/
	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {	
		menu.clear();		
		inflater.inflate(R.menu.setting, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}*/
}
