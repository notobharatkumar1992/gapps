package com.noto.gappss.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.utilities.UtilsClass;

public class AboutUsFragment extends BaseFragment{

	public static final String TAG = Constants.aboutUs;
	private EditText editT_feedback;
	private Button btn_sendFeedback;
	private Button btn_terms;
	private Button btn_privacyPolicy;
	private Button btn_openSourceLic;
	private TextView tv_version;
	private TextView tv_googlePlay;

	public AboutUsFragment() {

	}

	public static AboutUsFragment newInstance(Bundle bundle) {
		AboutUsFragment fragment = new AboutUsFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		// fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTitleOnAction(TAG, false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_about_us, container,
				false);
		initUi(view);
		setListener();
		setValueOnUi();
		return view;
	}

	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Bundle bundle = new Bundle();
		switch (v.getId()) {
		case R.id.btn_terms:
			bundle.putInt(Constants.type, 1);
	    	addFragment(TearmAndConditionFragment.newInstance(bundle), TearmAndConditionFragment.TAG,true);
			break;
	    case R.id.btn_privacyPolicy:
	    	bundle.putInt(Constants.type, 2);
	    	addFragment(TearmAndConditionFragment.newInstance(bundle), TearmAndConditionFragment.TAG,true);
			break;
	    case R.id.btn_openSourceLic:
	    	bundle.putInt(Constants.type, 3);
	    	addFragment(TearmAndConditionFragment.newInstance(bundle), TearmAndConditionFragment.TAG,true);
		
		    break;
	    case R.id.btn_sendFeedback:
		
		     break;
	    case R.id.tv_googlePlay:
		
		     break;
		}
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		
		editT_feedback = (EditText) view.findViewById(R.id.editT_feedback);
		btn_sendFeedback = (Button) view.findViewById(R.id.btn_sendFeedback);
		btn_terms = (Button) view.findViewById(R.id.btn_terms);
		btn_privacyPolicy = (Button) view.findViewById(R.id.btn_privacyPolicy);
		btn_openSourceLic = (Button) view.findViewById(R.id.btn_openSourceLic);
		tv_version = (TextView) view.findViewById(R.id.tv_version);
		tv_googlePlay = (TextView) view.findViewById(R.id.tv_googlePlay);
		
		
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
      tv_version.setText("Version : "+UtilsClass.getAppVersion(getActivity()));
		
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		btn_terms.setOnClickListener(this);
		btn_privacyPolicy.setOnClickListener(this);
		btn_openSourceLic.setOnClickListener(this);
		btn_sendFeedback.setOnClickListener(this);
		tv_googlePlay.setOnClickListener(this);
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

}
