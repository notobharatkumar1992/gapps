package com.noto.gappss.fragments;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.noto.gappss.ChannelSettingActivity;
import com.noto.gappss.CommentScreenActivity;
import com.noto.gappss.CreateChannelActivity;
import com.noto.gappss.FollowingChannelDetailActivity;
import com.noto.gappss.PostListActivity;
import com.noto.gappss.R;
import com.noto.gappss.YourChannelDetailActivity;
import com.noto.gappss.adapter.PostListAdapter;
import com.noto.gappss.adapter.YourChannelsAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelDetail;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class PostListFragment extends BaseFragment{

	public static final String TAG = "POST_LIST_FRA";
	public String title = "Channel";
	private PostListAdapter adapter;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private ListView listView;
	private String cId;
	private ChannelDetail channel;

	public PostListFragment(){			
		//Log.i(TAG, "Root Cat Contructor");
	}

	public static PostListFragment newInstance(Bundle bundle){
		PostListFragment fragment = new PostListFragment();
		if(bundle !=  null)
			fragment.setArguments(bundle);
		return fragment;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setHasOptionsMenu(true);
	}
	

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_post_list, null);
		listView = (ListView) view.findViewById(R.id.listView);
		setListener();
       
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Bundle bundle = getArguments();
		channel =(ChannelDetail) bundle.getSerializable(Constants.channel);
		cId = channel.getChannelId();
		setValueOnUi();	
	}

	

	@Override
	public void onSaveInstanceState(Bundle outState) {		
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

	}

	public void setPost(JSONObject explorer){	
		Log.i("setChannels", explorer.toString());
		if(explorer != null && listView != null){
			jsonObject = explorer;
			jsonArray = explorer.optJSONArray(Constants.Post);
			
			adapter = new PostListAdapter(this,  jsonArray);			
			listView.setAdapter(adapter);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_create:
			//();
			break;
		}
	}

	@Override
	protected void initUi(View view) {

	}

	@Override
	protected void setValueOnUi() {
		if(jsonObject == null)
			ApiManager.getInstance().getPostsByChannel((BaseFragmentActivity)getActivity(),cId);
		else
			setPost(jsonObject);
		//setChannels(jsonArray);
	}

	@Override
	protected void setListener() {
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Bundle bundle = new Bundle();
			//bundle.putString(Constants.channelId, channel.getChannelId());
            bundle.putString(Constants.postId, jsonArray.optJSONObject(position).toString());
			
			startMyActivity(CommentScreenActivity.class, bundle);
			}
		});	
	}

	@Override
	public boolean onBackPressedListener() {
		return true;
	}
/*
	@Override  
	public void onActivityResult(int requestCode, int resultCode, Intent data)  
	{  
		super.onActivityResult(requestCode, resultCode, data);  
		// check if the request code is same as what is passed  here it is 2  
		if(resultCode == Activity.RESULT_OK)  
		{  
			switch (requestCode) {
			case Constants.requestCode:
				ToastCustomClass.showToast(getActivity(), data.getStringExtra("Result")); 
				if (data.getStringExtra("Result").equals("ok")) {
					ApiManager.getInstance().getYourChannels((BaseFragmentActivity)getActivity());
					//create_channel_empty.setVisibility(View.GONE);
					//rel_second.setVisibility(View.VISIBLE);
				}
				break;
			case Constants.requestCodeYourChannelDetail:
				ToastCustomClass.showToast(getActivity(), "102="+data.getStringExtra("Result")); 
				if (data.getStringExtra("Result").equals("ok")) {
					ApiManager.getInstance().getYourChannels((BaseFragmentActivity)getActivity());
					//create_channel_empty.setVisibility(View.GONE);
					//rel_second.setVisibility(View.VISIBLE);
				}
				break;

		
			}
			
		}  
	} */

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		super.onCreateOptionsMenu(menu, inflater);		
		inflater.inflate(R.menu.subscribe_channel_post, menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.i_channel_profile:			
			 
			Bundle bundle = new Bundle();
			bundle.putString(Constants.channelId, channel.getChannelId());

			bundle.putSerializable(Constants.channel, channel);
			startMyActivity(FollowingChannelDetailActivity.class, bundle);
			
			break;

		case R.id.i_channel_settings:
			startMyActivity(ChannelSettingActivity.class, null);
			break;
			
			default:
				UtilsClass.underConstruction(getActivity());
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}


	


}
