package com.noto.gappss.fragments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.noto.gappss.FollowingChannelDetailActivity;
import com.noto.gappss.R;
import com.noto.gappss.R.id;
import com.noto.gappss.adapter.CommentScreenAdapter;
import com.noto.gappss.adapter.PostListAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelDetail;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.utilities.ToastCustomClass;
import com.squareup.picasso.Picasso;

public class CommentScreenFragment extends BaseFragment{


	public static final String TAG = "COMMENT_SCREEN_FRA";
	public String title = "Channel";
	private CommentScreenAdapter adapter;
	private JSONArray jsonArray;
	private JSONObject jsonObjectBundle;
	private JSONObject jsonObject;
	private ListView listView;
	private String postId = "";
	private CheckBox like;
	private EditText message;
	private ImageView tick_btn;
	

	public CommentScreenFragment(){			
		//Log.i(TAG, "Root Cat Contructor");
	}

	public static CommentScreenFragment newInstance(Bundle bundle){
		CommentScreenFragment fragment = new CommentScreenFragment();
		if(bundle !=  null)
			fragment.setArguments(bundle);
		return fragment;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setHasOptionsMenu(true);
	}
	

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_comment_screen, null);
		listView = (ListView) view.findViewById(R.id.listViews);
		message = (EditText) view.findViewById(R.id.message);
		tick_btn = (ImageView) view.findViewById(R.id.tick_btn);
		
		setListener();
		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Bundle bundle = getArguments();
		String post = bundle.getString(Constants.postId);

		try {
			jsonObjectBundle = new JSONObject(post);
			postId = jsonObjectBundle.optString("id");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setValueOnUi();	
	}

	

	@Override
	public void onSaveInstanceState(Bundle outState) {		
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

	}

	public void setPost(JSONObject explorer){
		
		Log.i("setChannels", explorer.toString());
		if(explorer != null && listView != null){
			jsonObject = explorer;
			jsonArray = explorer.optJSONArray(Constants.Comment);
			
			if (jsonArray != null) {
				adapter = new CommentScreenAdapter(this, jsonArray);			
				listView.setAdapter(adapter);
			}else {
				adapter = new CommentScreenAdapter(this, new JSONArray());			
				listView.setAdapter(adapter);
			}
			
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_create:
			
			break;
			
		case R.id.tick_btn:
			
			if (message.getText().toString().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(getActivity(), getActivity().getResources().getString(R.string.val_comment));
			}else {
				ApiManager.getInstance().createComment((BaseFragmentActivity)getActivity(), postId, message.getText().toString(), "0");
			}
			
			break;
		}
	}

	@Override
	protected void initUi(View view) {

	}

	@Override
	protected void setValueOnUi() {
		adapter = new CommentScreenAdapter(this,new JSONArray());
		//ToastCustomClass.showToast(getActivity(), "hello");		
		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View view2 = layoutInflater.inflate(R.layout.item_comment_list_header, null);
		TextView title = (TextView) view2.findViewById(R.id.title);
		TextView description = (TextView) view2.findViewById(R.id.description);
		ImageView img = (ImageView) view2.findViewById(R.id.img);
	    like = (CheckBox) view2.findViewById(R.id.like);
	    Log.i("IMAGE", "==="+jsonObjectBundle.optString(Constants.image));
	    if (!jsonObjectBundle.optString(Constants.image).equals("")) {
			Picasso.with(getActivity()).load(jsonObjectBundle.optString(Constants.image)).placeholder(R.drawable.noimg).into(img);
		}
	    title.setText(Html.fromHtml("@"+jsonObjectBundle.optString(Constants.username)));
	    description.setText(Html.fromHtml(jsonObjectBundle.optString(Constants.description)));
		
		listView.addHeaderView(view2,null,false);
		
		listView.setAdapter(adapter);
		if(jsonObject == null)
			ApiManager.getInstance().postDetail((BaseFragmentActivity)getActivity(),postId);
		else
			setPost(jsonObject);
		//setChannels(jsonArray);
	}

	@Override
	protected void setListener() {
		tick_btn.setOnClickListener(this);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				

		
				
			}
		});	
	}

	@Override
	public boolean onBackPressedListener() {
		return true;
	}
/*
	@Override  
	public void onActivityResult(int requestCode, int resultCode, Intent data)  
	{  
		super.onActivityResult(requestCode, resultCode, data);  
		// check if the request code is same as what is passed  here it is 2  
		if(resultCode == Activity.RESULT_OK)  
		{  
			switch (requestCode) {
			case Constants.requestCode:
				ToastCustomClass.showToast(getActivity(), data.getStringExtra("Result")); 
				if (data.getStringExtra("Result").equals("ok")) {
					ApiManager.getInstance().getYourChannels((BaseFragmentActivity)getActivity());
					//create_channel_empty.setVisibility(View.GONE);
					//rel_second.setVisibility(View.VISIBLE);
				}
				break;
			case Constants.requestCodeYourChannelDetail:
				ToastCustomClass.showToast(getActivity(), "102="+data.getStringExtra("Result")); 
				if (data.getStringExtra("Result").equals("ok")) {
					ApiManager.getInstance().getYourChannels((BaseFragmentActivity)getActivity());
					//create_channel_empty.setVisibility(View.GONE);
					//rel_second.setVisibility(View.VISIBLE);
				}
				break;

		
			}
			
		}  
	} */

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		super.onCreateOptionsMenu(menu, inflater);		
		inflater.inflate(R.menu.subscribe_channel_post, menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.i_channel_profile:			
			 
			/*Bundle bundle = new Bundle();
			bundle.putString(Constants.channelId, channel.getChannelId());

			bundle.putSerializable(Constants.channel, channel);
			startMyActivity(FollowingChannelDetailActivity.class, bundle);*/
			break;

		}
		return super.onOptionsItemSelected(item);
	}

	public void addListItem(JSONObject jsonObject2) {
		// TODO Auto-generated method stub
		Log.d("TESTTTTTTTTTTTTT", "====="+jsonObject2.toString());
		message.setText("");
		adapter.refreshList(jsonObject2);
		
		
	}


	


}
