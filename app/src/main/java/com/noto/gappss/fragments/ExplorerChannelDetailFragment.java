package com.noto.gappss.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelDetail;
import com.noto.gappss.beanClasses.Post;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.ToastCustomClass;
import com.squareup.picasso.Picasso;

public class ExplorerChannelDetailFragment extends BaseFragment{

	public static String TAG = "EXPLORER_CHANNEL_DETAIL";
	
	private ImageView image_logo;
	private TextView text_post_data;
	private TextView text_members_data;
	private TextView text_created_data;
	private TextView text_desc_data;
	private TextView text_channel;
	private TextView btn_join_leave;
	private LinearLayout post_list;

	private String cId;

	private ChannelDetail channel;
	
	public ExplorerChannelDetailFragment(){		
	}
	
	public static ExplorerChannelDetailFragment newInstance(Bundle bundle) {
		ExplorerChannelDetailFragment fragment = new ExplorerChannelDetailFragment();
		if(bundle !=  null)
			fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//return super.onCreateView(inflater, container, savedInstanceState);
		//setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_explorer_channel_detalis, null);
		initUi(view);
		setListener();
		return view;
	}

	@Override
	public void onClick(View v) {	
		if (btn_join_leave.getText().equals(getActivity().getResources().getString(R.string.join_channel))) {
			ApiManager.getInstance().joinChannel((BaseFragmentActivity)getActivity(), cId);
		}else {
			ApiManager.getInstance().leaveChannel((BaseFragmentActivity)getActivity(), cId);
		}
	}
	
	@Override
	public void onAttach(Activity activity) {	
		super.onAttach(activity);			
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {	
		super.onActivityCreated(savedInstanceState);
		Bundle bundle = getArguments();
		channel =(ChannelDetail) bundle.getSerializable(Constants.channel);
		cId = channel.getChannelId();	
		setValueOnUi();
	}

	@Override
	protected void initUi(View view) {
		setTitleOnAction(getString(R.string.channel_details), false);
		image_logo = (ImageView) view.findViewById(R.id.image_logo);
		text_post_data = (TextView) view.findViewById(R.id.text_post_datass);
		text_members_data = (TextView) view.findViewById(R.id.text_members_data);
		text_created_data = (TextView) view.findViewById(R.id.text_created_data);
		text_desc_data = (TextView) view.findViewById(R.id.text_desc_data);
		text_channel = (TextView) view.findViewById(R.id.text_channel);
		btn_join_leave = (TextView) view.findViewById(R.id.btn_join_leave);
		post_list = (LinearLayout) view.findViewById(R.id.post_list);
	}

	@Override
	protected void setValueOnUi() {
		//setChannelDetail(channel);
	   ApiManager.getInstance().getChannelDetail((BaseFragmentActivity)getActivity(), cId);
	}

	@Override
	protected void setListener() {
		btn_join_leave.setOnClickListener(this);
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}
	
	public void serviceResponsLeaveChannel(){
		 btn_join_leave.setText(getResources().getString(R.string.join_channel));
	 }
    public void serviceResponsJoinChannel(){
    	 btn_join_leave.setText(getResources().getString(R.string.leave_channel));
	 }
	
	public void setChannelDetail(ChannelDetail channelDetail){
		Log.i("setChannelDetail", channelDetail.toString());
		if (channelDetail.getChannelImage() != null && !channelDetail.getChannelImage().equalsIgnoreCase("")) {
			 Picasso.with(getActivity()).load(channelDetail.getChannelImage()).placeholder(getActivity().getResources().getDrawable(R.drawable.demo)).into(image_logo);
		}
		
		 text_post_data.setText(": "+channelDetail.getTotalPosts());
		 text_members_data.setText(": "+channelDetail.getMembers());
		 text_created_data.setText(": "+channelDetail.getCreatedOn());
		 text_desc_data.setText(Html.fromHtml(channelDetail.getChannelDescription()));
		 text_channel.setText(Html.fromHtml(channelDetail.getChannelName()));
		 
		 if (channelDetail.getIsOwner() == 0) {
			 btn_join_leave.setVisibility(View.VISIBLE);
			
			 if (channelDetail.getIsSusbcribable() == 0) {
				 btn_join_leave.setText(getActivity().getResources().getString(R.string.join_channel));
			}else {
				btn_join_leave.setText(getActivity().getResources().getString(R.string.leave_channel));
			}
		}
		 
		if (channelDetail.getPostList() != null && channelDetail.getPostList().size() > 0) {
			ArrayList<Post> arrayList = channelDetail.getPostList();
			
		
			
			for (int i = 0; i < arrayList.size(); i++) {
				View view = getActivity().getLayoutInflater().inflate(R.layout.item_post, null);
				TextView post_title = (TextView) view.findViewById(R.id.post_title);
				TextView post_des = (TextView) view.findViewById(R.id.post_des);
				
				post_title.setText(Html.fromHtml(arrayList.get(i).getTitle()));
				post_des.setText(Html.fromHtml(arrayList.get(i).getDescription()));
				
				post_list.addView(view);
			}
		}
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
}
