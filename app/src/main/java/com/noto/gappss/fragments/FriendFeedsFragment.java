package com.noto.gappss.fragments;

import org.json.JSONArray;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.noto.gappss.R;
import com.noto.gappss.adapter.FriendsFeedAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.ApiManager;

public class FriendFeedsFragment extends BaseFragment {

	public static final String TAG = Constants.FriendFeedsFragment;
    private ListView listView;
    private FriendsFeedAdapter adapter;
    private int type = -1;
	private Contact contact;
	public FriendFeedsFragment() {

	}

	public static FriendFeedsFragment newInstance(Bundle bundle) {
		FriendFeedsFragment fragment = new FriendFeedsFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTitleOnAction(TAG, false);
		Log.i("FriendFeedsFragment", "FriendFeedsFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.friend_feeds_fragment, container,false);
		initUi(view);
		setListener();
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Bundle bundle = getArguments();
		contact = (Contact) bundle.getSerializable(Constants.contact);
		type = bundle.getInt(Constants.type);
		setValueOnUi();	
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
       listView = (ListView) view.findViewById(R.id.listView);
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		
		
		if (type == 1) {
			ApiManager.getInstance().getFriendsFeeds(this);
		}else if (type == 2) {
			ApiManager.getInstance().getFriendsActivites(this,"131");
		}
		

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setFeeds(JSONArray jsonArray) {
		// TODO Auto-generated method stub
		if (jsonArray != null) {
			adapter = new FriendsFeedAdapter(this, jsonArray);
			listView.setAdapter(adapter);
		}
		
	}

}
