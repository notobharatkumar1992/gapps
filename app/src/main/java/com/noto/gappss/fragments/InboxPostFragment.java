package com.noto.gappss.fragments;

import org.json.JSONArray;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.noto.gappss.R;
import com.noto.gappss.adapter.InboxPostAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;

public class InboxPostFragment extends BaseFragment{

	public static final String TAG = Constants.InboxPostFragment;
	private ListView listView;
	private InboxPostAdapter adapter;

	public InboxPostFragment() {

	}

	public static InboxPostFragment newInstance(Bundle bundle) {
		InboxPostFragment fragment = new InboxPostFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		// fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_friend_apps, container,false);
		initUi(view);
		setListener();
		setValueOnUi();
		return view;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		listView = (ListView) view.findViewById(R.id.listView);
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		ApiManager.getInstance().getFriendsAllPosts(this);
		
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setList(JSONArray jsonObject) {
		// TODO Auto-generated method stub
		if (jsonObject != null) {
			adapter = new InboxPostAdapter(this, jsonObject);
			listView.setAdapter(adapter);
			
		}
		
	}

}
