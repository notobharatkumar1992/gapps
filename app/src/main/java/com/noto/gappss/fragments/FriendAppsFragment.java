package com.noto.gappss.fragments;

import org.json.JSONArray;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.adapter.FriendAppsAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.ApiManager;

public class FriendAppsFragment extends BaseFragment{


	public static final String TAG = Constants.apps;
	private ListView listView;
	private FriendAppsAdapter adapter;
	private Contact contact;
	public FriendAppsFragment() {
		
	}
	
	public static FriendAppsFragment newInstance(Bundle bundle) {
		FriendAppsFragment fragment = new FriendAppsFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		//fragment.setUserVisibleHint(true);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTitleOnAction(TAG, false);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_friend_apps, container, false);
		initUi(view);
		setListener();
		return view;
	}
	
	@Override
	public void onClick(View v) {
		
	}

	@Override
	protected void initUi(View view) {
		listView = (ListView) view.findViewById(R.id.listView);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		contact = (Contact) getArguments().getSerializable(Constants.contact);
		setValueOnUi();	
	}

	@Override
	protected void setValueOnUi() {
		ApiManager.getInstance().getFriendAllApps(this, contact.getId());
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setApps(JSONArray jsonArray) {
		// TODO Auto-generated method stub
		if (jsonArray !=null && listView != null) {			
			adapter = new FriendAppsAdapter(this, jsonArray);
			listView.setAdapter(adapter);
		}		
	}
}
