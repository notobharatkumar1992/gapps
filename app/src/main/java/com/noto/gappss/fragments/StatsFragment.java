package com.noto.gappss.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;

import com.noto.gappss.HomeActivity;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelStatistics;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class StatsFragment extends BaseFragment{
	
	private TextView text_subs_data;
	private TextView text_visit_data;
	private TextView text_post_data;
	private TextView text_comments_data;
	private TextView text_like_data;
	private TextView text_visits_data;
	public static String TAG = "STATS";
	private ActionBar actionBar;
	private String channelId = "";
	private String postId = "";
	private FragmentTabHost mTabHost;
	TabWidget widget;
	HorizontalScrollView hs;
	ArrayList<ChannelStatistics> arrayList = new ArrayList<ChannelStatistics>();

	public StatsFragment(){		
	}
	
	public static StatsFragment newInstance(Bundle bundle) {
		StatsFragment fragment = new StatsFragment();
		if(bundle !=  null)
			fragment.setArguments(bundle);
		return fragment;
	}
	

	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//return super.onCreateView(inflater, container, savedInstanceState);
		//setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_states, null);
		
		initUi(view);
		setValueOnUi();
		setListener();
		//Log.i("sdf", null);
		setTitleOnAction("Stats", false);
	
		return view;
	}
	
	@Override
	public void onAttach(Activity activity) {	
		super.onAttach(activity);
		Bundle bundle = getArguments();
		channelId = bundle.getString(Constants.channelId);	
		postId = bundle.getString(Constants.postId);	
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		createTab(view);
		text_subs_data = (TextView) view.findViewById(R.id.text_subs_data);
		text_visit_data = (TextView) view.findViewById(R.id.text_visit_data);
		text_post_data = (TextView) view.findViewById(R.id.text_post_data);
		text_comments_data = (TextView) view.findViewById(R.id.text_comments_data);
		text_like_data = (TextView) view.findViewById(R.id.text_like_data);
		text_visits_data = (TextView) view.findViewById(R.id.text_visits_data);
		
		/*actionBar = getActionBar();
		actionBar.removeAllTabs();
		customTabFragment();*/
		
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		ApiManager.getInstance().channelStatistics((BaseFragmentActivity)getActivity(), postId, channelId);
		
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		
	}
	
	public void serverResponse(ArrayList<ChannelStatistics> arrayList){
		this.arrayList = arrayList;
		if (arrayList !=null && arrayList.size() > 0) {
			/*
			text_subs_data.setText(arrayList.get(0).getTotalSubscribers()+"");
			text_visit_data.setText(arrayList.get(0).getTotalVisitors()+"");
			text_post_data.setText(arrayList.get(0).getTotalPosts()+"");*/
			
		}
		
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
	
	
		
		return true;
	}
	
	private void createTab(View rootView) {
		mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);
		widget = (TabWidget) rootView.findViewById(android.R.id.tabs);
		hs = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView);
		mTabHost.setup(getActivity(), getChildFragmentManager(),android.R.id.tabcontent);
		
		String array[] = getResources().getStringArray(R.array.stats_action_tab_array);
		mTabHost.addTab(mTabHost.newTabSpec("0").setIndicator(array[0]),DummyFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator(array[1]),DummyFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("2").setIndicator(array[2]),DummyFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("3").setIndicator(array[3]),DummyFragment.class, null);

		for (int i = 0; i < 4; i++) {
			TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
			View v = mTabHost.getTabWidget().getChildAt(i);
			UtilsClass.setTabTextColor(getActivity(), tv, v);
		}

		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				// TODO Auto-generated method stub
				//ToastCustomClass.showToast(getActivity(), ""+tabId);
				int tab = Integer.parseInt(tabId);
				switch (tab) {
				case 0:

					try {
        				
						text_subs_data.setText(arrayList.get(0).getTotalSubscribers());
						text_visit_data.setText(arrayList.get(0).getTotalVisitors());
						text_post_data.setText(arrayList.get(0).getTotalPosts());
						
					} catch (Exception e) {
						// TODO: handle exception
						//Log.i("Exception", e.getMessage());
					}
					break;
                case 1:
                	try {
        				
        				text_subs_data.setText(arrayList.get(1).getTotalSubscribers());
            			text_visit_data.setText(arrayList.get(1).getTotalVisitors());
            			text_post_data.setText(arrayList.get(1).getTotalPosts());
						
					} catch (Exception e) {
						// TODO: handle exception
						//Log.i("Exception", e.getMessage());
					}
					break;
                case 2:
                	try {
        				
        				text_subs_data.setText(arrayList.get(2).getTotalSubscribers());
            			text_visit_data.setText(arrayList.get(2).getTotalVisitors());
            			text_post_data.setText(arrayList.get(2).getTotalPosts());
						
					} catch (Exception e) {
						// TODO: handle exception
						//Log.i("Exception", e.getMessage());
					}
	                break;
                case 3:
                	try {
        				
        				text_subs_data.setText(arrayList.get(3).getTotalSubscribers());
            			text_visit_data.setText(arrayList.get(3).getTotalVisitors());
            			text_post_data.setText(arrayList.get(3).getTotalPosts());
						
					} catch (Exception e) {
						// TODO: handle exception
						//Log.i("Exception", e.getMessage());
					}
        		
	                break;
				}

 

			}
		});

	}


	
	@SuppressWarnings("deprecation")
	public void customTabFragment() {
		if (actionBar != null) {
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		}

		String array[] = getResources().getStringArray(R.array.stats_action_tab_array);
		for (int i = 0; i < array.length; i++) {
			//String styledText = "<font color='red'>"+array[i]+"</font>.";
			actionBar.addTab(actionBar.newTab().setText(array[i]).setTabListener(new TabListener() {
				
				
				public void onTabSelected(Tab tab, FragmentTransaction ft) {
								
					switch (tab.getPosition()) {
					case 0:
						
						
						
						break;

                    case 1:
                			
                		
						break;
                    case 2:
                    	
            		
	                    break;
	                    
                    case 3:
                    	
                    	
	                    break;
					}

				}

				public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				
				}

				public void onTabReselected(Tab tab, FragmentTransaction ft) {
					// User selected the already selected tab. Usually do nothing.
				}
			}));
		}   

	}
	
	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		

		
		
	}
	
}
