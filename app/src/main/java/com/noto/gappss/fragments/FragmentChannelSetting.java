package com.noto.gappss.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.utilities.ToastCustomClass;

public class FragmentChannelSetting extends BaseFragment implements OnCheckedChangeListener{
	
	private CheckBox sub_channel_act_feeds;
	private CheckBox per_allow_cmt_onpost;
	private CheckBox per_show_channel_in_search;
	private CheckBox per_channel_act_feeds;
	
	
	
	public FragmentChannelSetting() {
	}

	public static FragmentChannelSetting newInstance(Bundle bundle) {
		FragmentChannelSetting fragment = new FragmentChannelSetting();
		if (bundle != null)
			fragment.setArguments(bundle);
		fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// return super.onCreateView(inflater, container, savedInstanceState);
		// setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_channel_setting, null);
		initUi(view);		
		setListener();
		// Log.i("sdf", null);
		
		return view;
	}

	

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		
	    sub_channel_act_feeds = (CheckBox) view.findViewById(R.id.checkbox_one);
	    CheckBox per_allow_cmt_onpost = (CheckBox) view.findViewById(R.id.checkbox_one);
	    CheckBox per_show_channel_in_search = (CheckBox) view.findViewById(R.id.checkbox_one);
	    CheckBox per_channel_act_feeds = (CheckBox) view.findViewById(R.id.checkbox_one);
		
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		sub_channel_act_feeds.setOnCheckedChangeListener(this);
		per_allow_cmt_onpost.setOnCheckedChangeListener(this);
		per_show_channel_in_search.setOnCheckedChangeListener(this);
		per_channel_act_feeds.setOnCheckedChangeListener(this);
		
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		
		switch (buttonView.getId()) {
		case R.id.checkbox_one:
			//ToastCustomClass.showToast(getActivity(), "checkbox_one");
			break;

        case R.id.checkbox_two:
        	//ToastCustomClass.showToast(getActivity(), "checkbox_two");
			break;
        case R.id.checkbox_three:
        	//ToastCustomClass.showToast(getActivity(), "checkbox_three");
	        break;
        case R.id.checkbox_four:
        	//ToastCustomClass.showToast(getActivity(), "checkbox_four");
	        break;
		}
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

}
