package com.noto.gappss.fragments;

import org.json.JSONArray;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.noto.gappss.R;
import com.noto.gappss.adapter.FavoritesFeedAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;

public class FavoritesFeedsFragment extends BaseFragment{

	public static final String TAG = Constants.FavoritesFeedsFragment;
    private ListView listView;
    private FavoritesFeedAdapter adapter;
	public FavoritesFeedsFragment() {

	}

	public static FavoritesFeedsFragment newInstance(Bundle bundle) {
		FavoritesFeedsFragment fragment = new FavoritesFeedsFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		fragment.setUserVisibleHint(true);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Log.i("FavoritesFeedsFragment", "FavoritesFeedsFragment");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.favorites_feeds_fragment, container,false);
		initUi(view);
		setListener();
		setValueOnUi();
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		listView = (ListView) view.findViewById(R.id.listView);
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		ApiManager.getInstance().getFavorateFeeds(this);
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setFeeds(JSONArray jsonArray) {
		// TODO Auto-generated method stub
		if (jsonArray != null) {
			adapter = new FavoritesFeedAdapter(this, jsonArray);
			listView.setAdapter(adapter);
			  
		}
	}

}
