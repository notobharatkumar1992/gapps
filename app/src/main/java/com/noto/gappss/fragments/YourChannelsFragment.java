package com.noto.gappss.fragments;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.noto.gappss.CreateChannelActivity;
import com.noto.gappss.R;
import com.noto.gappss.YourChannelDetailActivity;
import com.noto.gappss.adapter.YourChannelsAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelDetail;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.preference.MySharedPreferences;
import com.noto.gappss.utilities.UtilsClass;

public class YourChannelsFragment extends BaseFragment{
	public static final String TAG = "YOUR_CH";
	private RelativeLayout create_channel_empty;
	private RelativeLayout rel_second;
	public String title = "Channel";
	private YourChannelsAdapter adapter;
	private JSONArray jsonArray;
	private TextView btn_create;
	private ListView listView;
	private SearchView new_app_search;

	public YourChannelsFragment(){			
		//Log.i(TAG, "Root Cat Contructor");
	}

	public static YourChannelsFragment newInstance(JSONArray jsonArray){
		YourChannelsFragment rootF = new YourChannelsFragment();
		rootF.jsonArray = jsonArray;
		//Log.i("JSONDATAT", "====="+jsonArray.toString());
		return rootF;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		setHasOptionsMenu(true);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_your_channel, null);
		initUi(view);
		setListener();
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		setValueOnUi();	
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {		
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);
	}

	public void setChannels(JSONArray explorer){	
		if(explorer != null && listView != null){
			jsonArray = explorer;
			adapter = new YourChannelsAdapter(this,  explorer);			
			listView.setAdapter(adapter);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_create:
			createChannel();
			break;
		}
	}

	@Override
	protected void initUi(View view) {
		setTitleOnAction("Your Channels", false);
		new_app_search = (SearchView) view.findViewById(R.id.new_app_search);
		listView = (ListView) view.findViewById(R.id.yourList);
		create_channel_empty = (RelativeLayout) view.findViewById(R.id.create_channel_empty);
		//rel_second = (RelativeLayout) view.findViewById(R.id.rel_second);
		btn_create = (TextView) view.findViewById(R.id.btn_create);
		listView.setEmptyView(create_channel_empty);
	}

	@Override
	protected void setValueOnUi() {
		//if(jsonArray == null)
		ApiManager.getInstance().getYourChannels(this);
		/*else
			setChannels(jsonArray);*/
		//setChannels(jsonArray);
	}
	@Override
	public void onDetach() {
		super.onDetach();
		jsonArray = null;
	}

	@Override
	protected void setListener() {
		btn_create.setOnClickListener(this);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				JSONObject jO =(JSONObject) view.getTag(R.id.text_channel);
				ChannelDetail detail = new ChannelDetail();				
				detail.setChannelId(jO.optString(Constants.channelId));
				detail.setChannelName(jO.optString(Constants.channelName));
				detail.setChannelDescription(jO.optString(Constants.channelDescription));
				detail.setChannelType(jO.optString(Constants.channelType));
				detail.setChannelMode(jO.optString(Constants.channelMode));
				detail.setChannelImage(jO.optString(Constants.channelImage));

				Bundle bundle = new Bundle();
				bundle.putString(Constants.USER_ID, MySharedPreferences.getInstance().getString(getActivity(), Constants.USER_ID, ""));
				bundle.putString(Constants.channelId, detail.getChannelId());
				bundle.putSerializable(Constants.channel, detail);

				//startMyActivity(YourChannelDetailActivity.class, bundle);
				Intent intent = new Intent(getActivity(),YourChannelDetailActivity.class);
				intent.putExtra(Constants.bundleArg, bundle);
				startActivityForResult(intent, Constants.requestCodeYourChannelDetail);
			}
		});	
	}

	@Override
	public boolean onBackPressedListener() {
		return true;
	}

	@Override  
	public void onActivityResult(int requestCode, int resultCode, Intent data)  
	{  
		super.onActivityResult(requestCode, resultCode, data);  
		// check if the request code is same as what is passed  here it is 2  
		if(resultCode == Activity.RESULT_OK)  
		{  
			switch (requestCode) {
			case Constants.requestCode:
				//ToastCustomClass.showToast(getActivity(), data.getStringExtra("Result")); 
				if (data.getStringExtra("Result").equals("ok")) {
					ApiManager.getInstance().getYourChannels(this);
					//create_channel_empty.setVisibility(View.GONE);
					//rel_second.setVisibility(View.VISIBLE);
				}
				break;
			case Constants.requestCodeYourChannelDetail:
				//ToastCustomClass.showToast(getActivity(), "102="+data.getStringExtra("Result")); 
				if (data.getStringExtra("Result").equals("ok")) {
					ApiManager.getInstance().getYourChannels(this);
					//create_channel_empty.setVisibility(View.GONE);
					//rel_second.setVisibility(View.VISIBLE);
				}
				break;
			}
		}  
	} 

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		//super.onCreateOptionsMenu(menu, inflater);		
		inflater.inflate(R.menu.create_channel, menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.i_CreateChannel:			
			createChannel();
			break;	
		case R.id.i_search:			
			if (new_app_search.getVisibility() == View.VISIBLE) {
				new_app_search.setVisibility(View.GONE);
			}else {
				new_app_search.setVisibility(View.VISIBLE);
			}
			break;	
		default:
			UtilsClass.underConstruction(getActivity());
			break;
		}	
		return false;
	}

	private void createChannel(){
		Intent intent = new Intent(getActivity(), CreateChannelActivity.class);
		startActivityForResult(intent, Constants.requestCode);
	}

	public void refreshList() {
		adapter.refreshList();
	}
}
