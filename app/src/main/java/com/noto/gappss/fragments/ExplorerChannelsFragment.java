package com.noto.gappss.fragments;

import org.json.JSONArray;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.noto.gappss.R;
import com.noto.gappss.adapter.CategoryPagerAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.preference.MySharedPreferences;

public class ExplorerChannelsFragment extends BaseFragment{
	public static final String TAG = "EXPLORER_CH";
	static ViewPager mViewPager;		
	public String title = "Channel";
	private CategoryPagerAdapter adapter;
	private JSONArray jsonArray;
	private SearchView new_app_search;

	public ExplorerChannelsFragment(){			
		//Log.i(TAG, "Root Cat Contructor");
	}

	public static ExplorerChannelsFragment newInstance(JSONArray jsonArray){
		ExplorerChannelsFragment rootF = new ExplorerChannelsFragment();
		rootF.jsonArray = jsonArray;
		rootF.setUserVisibleHint(true);
		return rootF;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		setHasOptionsMenu(true);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragement_root_categories, null);
		initUi(view);	
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		setValueOnUi();	
	}



	/*@Override
	public void onSaveInstanceState(Bundle outState) {		
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);

	}*/

	public void setChannels(JSONArray explorer){
		if(explorer != null && mViewPager != null){
			adapter = new CategoryPagerAdapter(getActivity(), getFragmentManager(), explorer, false);
			mViewPager.setAdapter(adapter);	
		}
	}

	@Override
	public void onClick(View v) {

	}

	@Override
	protected void initUi(View view) {
		new_app_search = (SearchView) view.findViewById(R.id.new_app_search);
		//setTitleOnAction(getString(R.string.channels), false);
		String name = MySharedPreferences.getInstance().getString(getActivity(), Constants.USER_NAME, "Guest");
		//setTitleOnAction(name==null? "Exlorer" : name, false);
		setTitleOnAction(name.trim().isEmpty()? getString(R.string.channels) : name, false);
		mViewPager = (ViewPager) view.findViewById(R.id.pager);
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int arg0) {
				//subCategoryPagerAdapter.notifyDataSetChanged();
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				//mViewPager.setCurrentItem(arg2);
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});	
	}

	@Override
	protected void setValueOnUi() {
		ApiManager.getInstance().getExploreChannels(this);
	}


	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		jsonArray = null;
	}

	@Override
	protected void setListener() {

	}

	@Override
	public boolean onBackPressedListener() {
		return true;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		//super.onCreateOptionsMenu(menu, inflater);		
		inflater.inflate(R.menu.search, menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.i_search:			
			
			if (new_app_search.getVisibility() == View.VISIBLE) {
				new_app_search.setVisibility(View.GONE);
			}else {
				new_app_search.setVisibility(View.VISIBLE);
			}
			break;			
		
		}	
		return false;
	}
}
