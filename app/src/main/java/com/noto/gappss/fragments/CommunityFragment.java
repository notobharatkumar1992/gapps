package com.noto.gappss.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.SettingActivity;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.preference.MySharedPreferences;
import com.noto.gappss.utilities.UtilsClass;

public class CommunityFragment extends BaseFragment{
	
	public static final String TAG = Constants.community;
	private TextView btn_yourCommunity;
	private TextView btn_gappssCommunity;
	private TextView btn_yourApps;
	public CommunityFragment() {
		
	}

	public static CommunityFragment newInstance(Bundle bundle) {
		CommunityFragment fragment = new CommunityFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitleOnAction(Constants.community, false);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_community, container, false);
		initUi(view);
		setListener();
		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_yourApps:
			Bundle bundle = new Bundle();
			bundle.putString(Constants.type, TAG);
			addFragment(MyAppShareFragment.newInstance(bundle), MyAppShareFragment.TAG, true);
			break;
		case R.id.btn_yourCommunity:
			addFragment(YourCommunityFragment.newInstance(null), YourCommunityFragment.TAG, true);
			break;

		case R.id.btn_gappssCommunity:
			addFragment(CountryFragment.newInstance(null), CountryFragment.TAG, true);
			break;
		}		
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		btn_yourCommunity = (TextView) view.findViewById(R.id.btn_yourCommunity);
		btn_gappssCommunity = (TextView) view.findViewById(R.id.btn_gappssCommunity);		
		btn_yourApps = (TextView) view.findViewById(R.id.btn_yourApps);
	}

	@Override
	protected void setValueOnUi() {
	}

	@Override
	protected void setListener() {
		btn_yourCommunity.setOnClickListener(this);
		btn_gappssCommunity.setOnClickListener(this);		
		btn_yourApps.setOnClickListener(this);
	}

	@Override
	public boolean onBackPressedListener() {		
		return false;
	}
	
/*	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {	
		inflater.inflate(R.menu.setting, menu);
		super.onCreateOptionsMenu(menu, inflater);		
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.i_settings:			
			startMyActivity(SettingActivity.class, null);
			break;			
		default:
			UtilsClass.underConstruction(getActivity());
			break;
		}	
		return false;
	}*/
	
	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.setting, menu);
		super.onCreateOptionsMenu(menu, inflater);		
	}*/
}
