package com.noto.gappss.fragments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.adapter.AppDetailReviewAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.AppDetailAsyncTask;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.networkTask.AppDetailAsyncTask.CallBackListener;
import com.noto.gappss.utilities.ToastCustomClass;
import com.squareup.picasso.Picasso;

public class AppDetailFragment extends BaseFragment{

	public static final String TAG = Constants.appDetail;
	private ListView listView;
	private AppDetailReviewAdapter adapter;
	private String packg = "";
	private boolean flag = true;
	public AppDetailFragment() {

	}

	public static AppDetailFragment newInstance(Bundle bundle) {
		AppDetailFragment fragment = new AppDetailFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		//fragment.setUserVisibleHint(true);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_app_detail, container, false);
		initUi(view);
		setValueOnUi();
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		Bundle bundle = getArguments();
		packg = bundle.getString(Constants.package_);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		listView = (ListView) view.findViewById(R.id.listView);
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		getAppData();
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public void getAppData(){

		AppDetailAsyncTask appDetailAsyncTask = new AppDetailAsyncTask(getActivity(), packg,AppDetailAsyncTask.applicationDetails, false);
		appDetailAsyncTask.setCallBackListener(callBackListener);
		appDetailAsyncTask.execute("");

	}


	CallBackListener callBackListener = new CallBackListener() {

		@Override
		public void callback(Object object) {
			// TODO Auto-generated method stub
			//{"error":"Sorry, I cannot find that!"}

			Log.i("CALLBACKLISTENER", (String)object);

			try {
				JSONObject jsonObject = new JSONObject((String)object);
				String error = jsonObject.optString("error");
				if (error == null || error.equals("")) {
                    
                    if (jsonObject != null) {
					
					View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_app_detail_header, null);

					ImageView imgV_= (ImageView) view.findViewById(R.id.imgV_);
					TextView tv_appName = (TextView) view.findViewById(R.id.tv_appName);
					TextView tv_des = (TextView) view.findViewById(R.id.tv_des);
					TextView btn_install = (TextView) view.findViewById(R.id.btn_install);
					TextView tv_date = (TextView) view.findViewById(R.id.tv_date);
					TextView appSize = (TextView) view.findViewById(R.id.appSize);
					TextView downloads = (TextView) view.findViewById(R.id.downloads);
					TextView ratingCount = (TextView) view.findViewById(R.id.ratingCount);
					RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
					HorizontalScrollView HVscreenShot = (HorizontalScrollView) view.findViewById(R.id.HVscreenShot);
					LinearLayout ImgContainer = (LinearLayout) view.findViewById(R.id.ImgContainer);

					btn_install.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent i = new Intent(Intent.ACTION_VIEW);
							i.setData(Uri.parse(URLsClass.googleAppUrl+packg));
							getActivity().startActivity(i);	
						}
					});

					String des = jsonObject.optString(Constants.description);
					// des = des.replace("\n", "<br>");
					tv_appName.setText(Html.fromHtml(jsonObject.optString(Constants.name)));
					tv_des.setText(Html.fromHtml(des));
					tv_date.setText(jsonObject.optString(Constants.datePublished));
					appSize.setText(jsonObject.optString(Constants.fileSize));
					downloads.setText(jsonObject.optString(Constants.numDownloads)+" Downloads");
					try {
						ratingBar.setRating(Float.parseFloat(jsonObject.optJSONObject(Constants.rating).optString(Constants.display)));
						ratingCount.setText(jsonObject.optJSONObject(Constants.rating).optString(Constants.count));
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (NullPointerException e) {
						// TODO: handle exception
						Log.i("NullPointerException", e.getMessage());
					}					 

					if (jsonObject.optString(Constants.icon) != null && !jsonObject.optString(Constants.icon).equals("")) {
						Picasso.with(getActivity()).load(jsonObject.optString(Constants.icon)).placeholder(R.drawable.noimg).into(imgV_);
					}

					int width = (MyApplication.getApplication().getWidthPixel()/2)-20;
					LinearLayout.LayoutParams imageViewLayoutParams = new LinearLayout.LayoutParams(width, width);
					imageViewLayoutParams.setMargins(0, 0, 20, 0);


					/*LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
					layoutParams.setMargins(0, 0, 0, 15);*/

					JSONArray jsonArray = jsonObject.optJSONArray(Constants.screenshots);
					if (jsonArray != null ) {
						for (int i = 0; i < jsonArray.length(); i++) {							 
							ImageView imageView = new ImageView(getActivity());
							imageView.setLayoutParams(imageViewLayoutParams);
							imageView.setBackgroundResource(R.drawable.noimg);
							// imageView.setImageBitmap(BitmapFactory.decodeFile(path));
							imageView.setScaleType(ScaleType.CENTER_CROP);							 
							Picasso.with(getActivity()).load(jsonArray.optString(i)).placeholder(R.drawable.noimg).into(imageView);
							ImgContainer.addView(imageView);
						}
					}

					adapter = new AppDetailReviewAdapter(AppDetailFragment.this,new JSONArray());
					listView.addHeaderView(view,null,false);
					listView.setAdapter(adapter);

					AppDetailAsyncTask appDetailAsyncTask = new AppDetailAsyncTask(getActivity(), packg,AppDetailAsyncTask.applicationReviews, false);
					appDetailAsyncTask.setCallBackListener(callBackListenerReview);
					appDetailAsyncTask.execute("");
					
					}
				}else {
					ToastCustomClass.showToast(getActivity(), error);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	};


	CallBackListener callBackListenerReview = new CallBackListener() {
		@Override
		public void callback(Object object) {
			Log.i("CALLBACKLISTENER", (String)object);
			try {
				JSONArray jsonArray = new JSONArray((String)object);
				if (jsonArray != null) {
					if (jsonArray.length() > 0) {
						adapter.refreshList(jsonArray);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
				JSONObject jsonObject;
				try {
					jsonObject = new JSONObject((String)object);
					String error = jsonObject.optString("error");
					if (error != null && error.equals("")) {
						ToastCustomClass.showToast(getActivity(), error);
					}
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			}
		}
	};
}
