package com.noto.gappss.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.noto.gappss.NewAppActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.AppCategoryAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.AppCategory;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.preference.MySharedPreferences;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class AddCategoryFragment extends BaseFragment {
	private ListView listView;
	public static String TAG = Constants.appCat;
	AppCategoryAdapter adapter;
	ArrayList<AppCategory> arrayList = new ArrayList<AppCategory>();
	
	public AddCategoryFragment(){		
	}
	
	public static AddCategoryFragment getInstance(){
		AddCategoryFragment fragment = new AddCategoryFragment();
		return fragment;
	}
	

	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//return super.onCreateView(inflater, container, savedInstanceState);
		setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_add_category, null);
		initUi(view);		
		return view;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {	
		super.onActivityCreated(savedInstanceState);
		setValueOnUi();
	}
	

	@Override
	public void onClick(View v) {

	}

	@Override
	protected void initUi(View view) {
		setTitleOnAction(getString(R.string.app_cat), false);
		listView =(ListView) view.findViewById(R.id.listCategories);		
	}

	@Override
	protected void setValueOnUi() {
		//getCategories();
		ApiManager.getInstance().getCategories((BaseFragmentActivity)getActivity());
	}

	@Override
	protected void setListener() {

	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}
	
	/*public void getCategories(){
		String urlsArr[] = {URLsClass.service_type_get_categories};
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(getActivity(), Constants.USER_ID, ""));
		params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
		serviceCaller(this, urlsArr, params, true, false, Constants.getCat, true);
	}*/
	
	public void addCategories(){
		StringBuffer categories = new StringBuffer();
		for (int i = 0; i < arrayList.size(); i++) {
			if (arrayList.get(i).isChecked()) {
				categories.append(arrayList.get(i).getCatId());
				categories.append(",");
			}
		}
		
		if (categories.equals("")) {
			ToastCustomClass.showToast(getActivity(), "Please select categories.");
		}else {
			String result = UtilsClass.removeStringLastChar(categories,',');
			//Log.i("categories", categories);
			String urlsArr[] = {URLsClass.service_type_set_categories};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(getActivity(), Constants.USER_ID, ""));
			params.put(Constants.p_categories, result);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
			serviceCaller(this, urlsArr, params, true, false, Constants.setCat, true);
		}
	}
	public void serviceResponse(ArrayList<AppCategory> arrayList){
		this.arrayList = arrayList;
	    adapter = new AppCategoryAdapter(this, arrayList);
		listView.setAdapter(adapter);
		
	}
	public void serviceResponseSetCat(boolean res){
		startMyActivity(NewAppActivity.class, null);	
		getActivity().finish();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		//ToastCustomClass.showToast(getActivity(), "ok");
		this.arrayList = adapter.getList();
		//ToastCustomClass.showToast(getActivity(), ""+arrayList.get(0).isChecked());
		addCategories();
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.ok, menu);
	}
}
