/*package com.noto.gappss.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import com.noto.gappss.R;
import com.noto.gappss.adapter.ContactAdapter;
import com.noto.gappss.adapter.SearchContactListAdapter;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.MainListLoader;

public class SearchFragment extends ListFragment implements LoaderManager.LoaderCallbacks<List<Contact>>, OnQueryTextListener {
	//TextView btn_invite;
	public static String TAG = "SEARCH_FRA";
	ContactAdapter adapter;
	//ArrayList<AppCategory> arrayList = new ArrayList<AppCategory>();
	HashMap<String, Contact> mapContacts = new HashMap<String, Contact>();
	private List<Contact> list;
	private SearchContactListAdapter _adapter;
	private String _curFilter;

	public SearchFragment(){		
	}

	public static SearchFragment newInstance(Bundle bundle) {
		SearchFragment fragment = new SearchFragment();
		if(bundle !=  null)
			fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);	
		setHasOptionsMenu(true);		
	}	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_main_list, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// set an empty filter
		_curFilter = null;

		// has menu
		setHasOptionsMenu(true);

		// set empty adapter
		_adapter = new SearchContactListAdapter(getActivity(), new ArrayList<Contact>());
		setListAdapter(_adapter);

		// start out with a progress indicator.
		//setListShown(false);

		// prepare the loader.  Either re-connect with an existing one, or start a new one.
		getLoaderManager().initLoader(0, null, this);
	}

	@Override
	public void onCreateOptionsMenu (Menu menu, MenuInflater inflater) {
		// Inflate the options menu from XML
		inflater.inflate(R.menu.search, menu);

		// Get the SearchView and set the searchable configuration
		SearchView searchView = (SearchView) menu.findItem(R.id.i_search).getActionView();
		searchView.setOnQueryTextListener(this);
		
		 // Associate searchable configuration with the SearchView
	    SearchManager searchManager =
	           (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
	    SearchView searchView =
	            (SearchView) menu.findItem(R.id.search).getActionView();
	    searchView.setSearchableInfo(
	            searchManager.getSearchableInfo(getActivity().getComponentName()));

		super.onCreateOptionsMenu(menu, inflater);
	}

	// LoaderManager methods
	// -----------------------------------------------------------------------------------------
	@Override
	public Loader<List<Contact>> onCreateLoader(int id, Bundle args) {
		return new MainListLoader(getActivity(), _curFilter);
	}

	@Override
	public void onLoadFinished(Loader<List<Contact>> loader, List<Contact> data) {
		// Set the new data in the adapter.
		_adapter.setData(data);

		// The list should now be shown.
		if (isResumed()) {
		//	setListShown(true);
		} else {
			//setListShownNoAnimation(true);
		}
	}

	@Override
	public void onLoaderReset(Loader<List<Contact>> loader) {
		// Android guides set data to null but I need check
		// what happen when I set data as null
		_adapter.setData(new ArrayList<Contact>());
	}

	// OnQueryTextListener methods
	// -----------------------------------------------------------------------------------------
	@Override
	public boolean onQueryTextChange(String newText) {
		// Called when the action bar search text has changed. sUpdate
		// the search filter, and restart the loader to do a new query
		// with this filter.
		_curFilter = !TextUtils.isEmpty(newText) ? newText : null;
		_adapter.setQuery(_curFilter);
		getLoaderManager().restartLoader(0, null, this);
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// Don't care about this.
		return false;
	}
}
*/