package com.noto.gappss.fragments;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewDebug.ExportedProperty;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.noto.gappss.ExplorerChannelDetailActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.ChannelsAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelDetail;
import com.noto.gappss.preference.MySharedPreferences;

public class ChannelGridFragment extends BaseFragment{
	public static String TAG = "CHANNEL_DETAIL";
	private GridView gridView;
	private JSONArray jsonArray;
	private ChannelsAdapter adapter;
	private boolean isYourChannel = false;

	public ChannelGridFragment(){		
	}

	public static ChannelGridFragment newInstance(JSONArray jsonArray, boolean isYourChannel) {
		ChannelGridFragment fragment = new ChannelGridFragment();
		fragment.jsonArray = jsonArray;
		fragment.isYourChannel = isYourChannel;
		//fragment.setUserVisibleHint(true);
		/*if(jsonObject !=  null)
			fragment.setArguments(jsonObject);*/
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);	
		//setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.grid_view, null);
		initUi(view);
		setListener();
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {	
		super.onActivityCreated(savedInstanceState);


	}

	@Override
	public void onClick(View v) {

	}

	@Override
	public void onResume() {
		super.onResume();
		setValueOnUi();
	}

	@Override
	protected void initUi(View view) {
		String name = MySharedPreferences.getInstance().getString(getActivity(), Constants.USER_NAME, "Guest");
		//setTitleOnAction(name==null? "Exlorer" : name, false);
		setTitleOnAction(name.trim().isEmpty()? getString(R.string.channels) : name, false);
		gridView = (GridView) view.findViewById(R.id.gridView);

	}

	@Override
	public void onAttach(Activity activity) {	
		super.onAttach(activity);

	}

	@Override
	protected void setValueOnUi() {
		adapter = new ChannelsAdapter(this, jsonArray);
		gridView.setAdapter(adapter);
	}

	@Override
	protected void setListener() {
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				//ToastCustomClass.showToast(getActivity(), "Pos "+position);
				JSONObject jO =(JSONObject) view.getTag(R.id.text_channel);
				ChannelDetail detail = new ChannelDetail();				
				detail.setChannelId(jO.optString(Constants.channelId));
				detail.setChannelName(jO.optString(Constants.channelName));
				detail.setChannelDescription(jO.optString(Constants.channelDescription));
				detail.setChannelType(jO.optString(Constants.channelType));
				detail.setChannelMode(jO.optString(Constants.channelMode));
				detail.setChannelImage(jO.optString(Constants.channelImage));

				Bundle bundle = new Bundle();
				bundle.putSerializable(Constants.channel, detail);


				startMyActivity(ExplorerChannelDetailActivity.class, bundle);
				//addFragment(ExplorerChannelDetailFragment.newInstance(bundle), ExplorerChannelDetailFragment.TAG, true);
			}
		});
	}

	@Override
	public boolean onBackPressedListener() {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return false;
	}
}
