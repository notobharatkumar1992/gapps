package com.noto.gappss.fragments;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.noto.gappss.AppDetailActivity;
import com.noto.gappss.MyAppShareActivity;
import com.noto.gappss.R;
import com.noto.gappss.adapter.MyAppShareAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class MyAppShareFragment extends BaseFragment {

	public static String TAG = Constants.shareApps;
	private ArrayList<ApplicationBean> combindArrayList = new ArrayList<ApplicationBean>();
	private ArrayList<ApplicationBean> publicArrayList = new ArrayList<ApplicationBean>();
	private ArrayList<ApplicationBean> privateArrayList = new ArrayList<ApplicationBean>();

	private ListView new_app_list;
	private SearchView new_app_search;
	private TextView btn_get_apps;
	private TextView btn_app_status;
	private TextView tv_public;
	private TextView tv_private;
	private LinearLayout lin;
	private MyAppShareAdapter adapter;
	private LinearLayout lin_bottom;
	private String type;



	public MyAppShareFragment() {
	}

	public static MyAppShareFragment newInstance(Bundle bundle) {
		MyAppShareFragment fragment = new MyAppShareFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		fragment.setUserVisibleHint(true);
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);		
		
		//setHasOptionsMenu(true);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.fragment_new_app, null);
		initUi(view);
		setListener();
		setValueOnUi();
		return view;
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_app_status:
			getAppStatus();
			break;
		case R.id.btn_get_apps:
			getAppFriends();
			break;
		case R.id.tv_public:
			setPublicAppList();
			break;
		case R.id.tv_private:
			setPrivateAppList();
			break;
		}
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub

		lin = (LinearLayout) view.findViewById(R.id.lin);
		new_app_list =(ListView) view.findViewById(R.id.new_app_list);
		new_app_search = (SearchView) view.findViewById(R.id.new_app_search);
		btn_get_apps = (TextView) view.findViewById(R.id.btn_get_apps);
		btn_app_status = (TextView) view.findViewById(R.id.btn_app_status);
		tv_public = (TextView) view.findViewById(R.id.tv_public);
		tv_private = (TextView) view.findViewById(R.id.tv_private);
		lin_bottom = (LinearLayout) view.findViewById(R.id.lin_bottom);

	

	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		Bundle bundle = getArguments();
	    type = bundle.getString(Constants.type);
		if (type.equals(CommunityFragment.TAG)) {
			enableBackButton();
			lin_bottom.setVisibility(View.GONE);
			setTitleOnAction("Your apps", false);
			lin.setVisibility(View.VISIBLE);
		}else {
			setTitleOnAction(TAG, false);
			lin.setVisibility(View.GONE);
		}
		ApiManager.getInstance().getAppVisibility(this);

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

		btn_get_apps.setOnClickListener(this);
		btn_app_status.setOnClickListener(this);
		tv_public.setOnClickListener(this);
		tv_private.setOnClickListener(this);

		new_app_list.setTextFilterEnabled(true);
		new_app_search.setIconifiedByDefault(false);
		new_app_search.setQueryHint("Search...");
		new_app_search.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				if (TextUtils.isEmpty(newText.toString())) {
					new_app_list.clearTextFilter();
					//Log.i("if onQueryTextChange", "onQueryTextChange=="+newText.toString());
				} else {
					new_app_list.setFilterText(newText.toString());
					//Log.i("else onQueryTextChange", "onQueryTextChange=="+newText.toString());
				}
				return true;
			}
		});

		new_app_list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String packag = (String) view.getTag(R.id.link);
				Bundle bundle = new Bundle();
				bundle.putString(Constants.package_, packag);
				startMyActivity(AppDetailActivity.class, bundle);					
			}
		});
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public void serviceResponse(JSONArray jsonArray) {
		// TODO Auto-generated method stub

		if (jsonArray != null) {

			PackageManager pm = getActivity().getPackageManager();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jObj = jsonArray.optJSONObject(i);
				String pkg = jObj.optString(Constants.p_appPack);
				Drawable icon = null;
				try {
					icon = pm.getApplicationIcon(pkg);
				} catch (NameNotFoundException e) {
				}

				ApplicationBean appCategory = new ApplicationBean();
				appCategory.setTitle(jObj.optString(Constants.p_appName));
				appCategory.setPackageName(jObj.optString(Constants.p_appPack));
				appCategory.setStatus(jObj.optInt(Constants.status));
				appCategory.setApplogo(icon);

				if (jObj.optInt(Constants.status) == 1) {
					combindArrayList.add(appCategory);
					publicArrayList.add(appCategory);
				} else if (jObj.optInt(Constants.status) == 2) {
					combindArrayList.add(appCategory);
					privateArrayList.add(appCategory);
				}
			}
			
			if (type.equals(CommunityFragment.TAG)) {
				adapter = new MyAppShareAdapter(this, publicArrayList,type);
				new_app_list.setAdapter(adapter);
			}else {
				adapter = new MyAppShareAdapter(this, combindArrayList,type);
				new_app_list.setAdapter(adapter);
			}
			

		}

	}


	private void setPublicAppList(){
		tv_private.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		tv_public.setTextColor(getResources().getColor(R.color.action_bar));
		adapter.setList(publicArrayList);
	}

	private void setPrivateAppList(){
		tv_private.setTextColor(getResources().getColor(R.color.action_bar));
		tv_public.setTextColor(getResources().getColor(R.color.bottom_tab_text_unselected));
		adapter.setList(privateArrayList);
	}

	private void getAppFriends(){
		addFragment(GetAppsFragment.newInstance(null), GetAppsFragment.TAG, true);
	}

	private void getAppStatus(){
		lin.setVisibility(View.VISIBLE);
		btn_app_status.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_appstatus));
		btn_get_apps.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_getapps));
		setPublicAppList();

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		
		if (type.equals(CommunityFragment.TAG)) {
			inflater.inflate(R.menu.search, menu);
		}else {
			super.onCreateOptionsMenu(menu, inflater);
			inflater.inflate(R.menu.shar_app, menu);
		}
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.shar_app:
			if (adapter != null) {
				ArrayList<ApplicationBean> applicationBeans = adapter.getList();
				//StringBuffer appId = "";
				if(applicationBeans != null){
					StringBuffer appId = new StringBuffer();
					for (int i = 0; i < applicationBeans.size(); i++) {
						if (applicationBeans.get(i).isInstalled()) {
							Log.i("test", applicationBeans.get(i).getTitle());
							appId.append(applicationBeans.get(i).getPackageName()+"|");
						}
					}
					if (appId.length() > 0) {
						String id = UtilsClass.removeStringLastChar(appId, '|');
						Bundle bundle = new Bundle();
						bundle.putString(Constants.id, id);
						bundle.putString(Constants.type, MyAppShareFragment.TAG);
						addFragment(ContactSharApps.newInstance(bundle), ContactSharApps.TAG, true);
					}else {
						ToastCustomClass.showToast(getActivity(), "Please select app!");
					}
				}
			}			
			break;
		case R.id.i_search:
			
			if (new_app_search.getVisibility() == View.VISIBLE) {
				new_app_search.setVisibility(View.GONE);
			}else {
				new_app_search.setVisibility(View.VISIBLE);
			}
				break;
		}
		return super.onOptionsItemSelected(item);
	}




}
