package com.noto.gappss.fragments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.noto.gappss.R;
import com.noto.gappss.adapter.FriendInterestAdapter;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;

public class FriendInterestFragment extends BaseFragment{


	public static final String TAG = Constants.friendInterestT;
	private ListView listView;
	private FriendInterestAdapter adapter;
	public FriendInterestFragment() {
		
	}
	
	public static FriendInterestFragment newInstance(Bundle bundle) {
		FriendInterestFragment fragment = new FriendInterestFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		//fragment.setUserVisibleHint(true);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTitleOnAction(TAG, false);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_friend_interest, container, false);
		initUi(view);
		setListener();
		setValueOnUi();
		return view;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		listView = (ListView) view.findViewById(R.id.listView);
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub
		setChannels(new JSONArray());
	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void setChannels(JSONArray array){
		JSONArray array2 = new JSONArray();
		//JSONObject jsonObject = new JSONObject();
		try {
			array2.put(0, new JSONObject());
			array2.put(1, new JSONObject());
			array2.put(2, new JSONObject());
			adapter = new FriendInterestAdapter(this, array2);
			listView.setAdapter(adapter);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}
