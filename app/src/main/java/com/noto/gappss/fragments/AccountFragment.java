package com.noto.gappss.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.CameraGallery;
import com.noto.gappss.utilities.GetRealPath;
import com.noto.gappss.utilities.ToastCustomClass;

import eu.janmuller.android.simplecropimage.CropImage;

public class AccountFragment extends BaseFragment {
	public static String TAG = Constants.account;
	private ImageView userImg;
	private EditText username;
	private EditText email;
	private EditText dob;
	private EditText phoneNumber;
	private EditText gender;
	private Spinner countryPlayStore;
	private EditText yourInterest;
	private Button  logOut;
	private TextView tag;
	private String path = "";
	public AccountFragment() {
	}

	public static AccountFragment newInstance(Bundle bundle) {
		AccountFragment fragment = new AccountFragment();
		if (bundle != null)
			fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		setTitleOnAction(TAG, false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_account, null);
		initUi(view);
		setValueOnUi();
		setListener();

		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.logOut:

			break;
		case R.id.userImg:

			break;
		case R.id.tag:
			new CameraGallery().openImageSource(getActivity() , this);
			break;
		
		}

	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		tag = (TextView) view.findViewById(R.id.tag);
		username = (EditText) view.findViewById(R.id.username);
		email = (EditText) view.findViewById(R.id.email);
		dob = (EditText) view.findViewById(R.id.dob);
		phoneNumber = (EditText) view.findViewById(R.id.phoneNumber);
		gender = (EditText) view.findViewById(R.id.gender);
		countryPlayStore = (Spinner) view.findViewById(R.id.countryPlayStore);
		yourInterest = (EditText) view.findViewById(R.id.yourInterest);
		logOut = (Button) view.findViewById(R.id.logOut);
		userImg = (ImageView) view.findViewById(R.id.userImg);

	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		logOut.setOnClickListener(this);
		userImg.setOnClickListener(this);
		tag.setOnClickListener(this);
		

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.save:
			
			break;

		default:
			getActivity().finish();
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.save, menu);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.i("onActivityResult", "resultCode=="+resultCode+"  requestCode=="+requestCode+" Activity.RESULT_OK="+Activity.RESULT_OK);
		if(resultCode == Activity.RESULT_OK){
			switch(requestCode){
			case CameraGallery.REQUEST_CODE_GALLERY:{
				Log.i("REQUEST_CODE_GALLERY", "REQUEST_CODE_GALLERY");
				try {
					CameraGallery cameraAct = new CameraGallery();
					/*InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
					Log.i("REQUEST_CODE_GALLERY", "REQUEST_CODE_GALLERY=="+cameraAct.mFileTemp.getPath());
					FileOutputStream fileOutputStream = new FileOutputStream(cameraAct.mFileTemp.getPath());
					cameraAct.copyStream(inputStream, fileOutputStream);
					fileOutputStream.close();
					inputStream.close();*/
					
					String realpath;
					if (Build.VERSION.SDK_INT < 11) {
						realpath = GetRealPath.getRealPathFromURI_BelowAPI11(getActivity(),
								data.getData());
					

					} else if (Build.VERSION.SDK_INT < 19) {
						realpath = GetRealPath.getRealPathFromURI_API11to18(getActivity(),
								data.getData());
						

					} else {
						realpath = GetRealPath.getRealPathFromURI_API19(getActivity(),
								data.getData());
						

					}

					//cameraAct.startCropImage(getActivity(), null, cameraAct.mFileTemp.getPath());
					Log.i("PATHSSSSSSSS", "==="+realpath);
					cameraAct.startCropImage(getActivity(), this, realpath);
				} catch (Exception e) {
					Log.e(TAG, "Error while creating temp file", e);
				}
				break;
			}
			case CameraGallery.REQUEST_CODE_TAKE_PICTURE:{
				Log.i("REQUEST_CODE_TAKE_PICTURE", "REQUEST_CODE_TAKE_PICTURE");
				CameraGallery cameraAct = new CameraGallery();	
					//String action = data.getAction();
					//Parcelable parcelable = data.getParcelableExtra("data");
					//Bitmap bit = (Bitmap) data.getExtras().get("data");
					//Uri selectedImageUri = Uri.fromFile(CameraGallery.mFileTemp);
					//Uri selectedImageUri = data.getData();
					if(CameraGallery.mFileTemp != null){					
						cameraAct.startCropImage(getActivity(), this, CameraGallery.mFileTemp.getPath());
					}
				break;
			}
			case CameraGallery.REQUEST_CODE_CROP_IMAGE:{
				Log.i("REQUEST_CODE_CROP_IMAGE", "REQUEST_CODE_CROP_IMAGE");
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				//String path = CameraGallery.mFileTemp.getPath();
				//String caption = data.getStringExtra(CropImage.IMAGE_TITLE);
				if (path == null) {
					return;
				}
				try{
					this.path = path;
					/*int width = (MyApplication.getApplication().getWidthPixel()/3)-40;
					LinearLayout layout = new LinearLayout(getActivity());
					layout.setOrientation(LinearLayout.HORIZONTAL);
					layout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

					LinearLayout.LayoutParams imageViewLayoutParams = new LinearLayout.LayoutParams(width, width);
					imageViewLayoutParams.setMargins(0, 0, 20, 0);

					ImageView imageView = new ImageView(getActivity());*/
					//img_logo.setLayoutParams(imageViewLayoutParams);
					//img_logo.setBackgroundResource(R.drawable.image_fram9);
				//	Log.i("PATHSSSSS", path);
					//ToastCustomClass.showToast(getActivity(), "Set Image");
					userImg.setImageBitmap(BitmapFactory.decodeFile(path));
					userImg.setScaleType(ScaleType.CENTER_CROP);
							

					CameraGallery.mFileTemp = null;
	
				} catch (Exception e) {
					e.printStackTrace();
					ToastCustomClass.showToast(getActivity(), "Something went wrong!");
				}
				break;
			}
			}
		}
	}

}
