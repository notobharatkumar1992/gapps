package com.noto.gappss;

import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.fragments.CommentScreenFragment;
import com.noto.gappss.fragments.PostListFragment;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.utilities.ToastCustomClass;

public class CommentScreenActivity extends BaseFragmentActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		//	Bundle bundle = new Bundle();
			//bundle.putString(Constants.USER_ID, "");
			enableBackButton();
			setTitleOnAction(getResources().getString(R.string.comments));
			addFragement(CommentScreenFragment.newInstance(getBundle()), CommentScreenFragment.TAG, false);
			
	}
	
	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 
	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub
		
	}
	
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if(super.callBackFromApi(object, act, requstCode)){
			JSONObject jObject ;
			String message ="";
			switch (requstCode) {
			case Constants.postDetail:
				jObject = (JSONObject) object;
				
			    message = jObject.optString(Constants.p_message);
					if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
						CommentScreenFragment freg = (CommentScreenFragment) getFragmentByTag(CommentScreenFragment.TAG);
						if(freg != null)
							freg.setPost(jObject.optJSONObject(Constants.Post));
					}else {
						ToastCustomClass.showToast(this, message);
					}
					
				break;
			case Constants.setChannelSettings:
				 jObject = (JSONObject) object;
				 message = jObject.optString(Constants.p_message);
					if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
						ToastCustomClass.showToast(this, message);
					}else {
						ToastCustomClass.showToast(this, message);
					}
				
				break;
				
			case Constants.createComment:
				//10-09 03:17:55.427: I/System.out(30192): Responce  {"status":1,"dataflow":0,"message":"You have commented on post."}

				jObject = (JSONObject) object;
				 message = jObject.optString(Constants.p_message);
					if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
						ToastCustomClass.showToast(this, message);
						JSONObject jsonObject = jObject.optJSONObject(Constants.Comment);
						
							CommentScreenFragment freg = (CommentScreenFragment) getFragmentByTag(CommentScreenFragment.TAG);
							if(freg != null)
								freg.addListItem(jsonObject);
						
					}else {
						ToastCustomClass.showToast(this, message);
						
					}
				
				break;
			}
		}
		return true;
	}


}
