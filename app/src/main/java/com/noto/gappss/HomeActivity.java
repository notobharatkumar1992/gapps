package com.noto.gappss;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.AppCategory;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.fragments.AllFeedsFragment;
import com.noto.gappss.fragments.ChannelFragment;
import com.noto.gappss.fragments.ChannelsFeedsFragment;
import com.noto.gappss.fragments.CommunityFragment;
import com.noto.gappss.fragments.ContactsFragment;
import com.noto.gappss.fragments.ExplorerChannelsFragment;
import com.noto.gappss.fragments.FavoritesFeedsFragment;
import com.noto.gappss.fragments.FeedsFragment;
import com.noto.gappss.fragments.FollowedChannelsFragment;
import com.noto.gappss.fragments.FriendFeedsFragment;
import com.noto.gappss.fragments.HomePopularCatagoryFragment;
import com.noto.gappss.fragments.InboxFragment;
import com.noto.gappss.fragments.NavigationTabFragment;
import com.noto.gappss.fragments.YourChannelsFragment;
import com.noto.gappss.fragments.YourCommunityFragment;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class HomeActivity extends BaseFragmentActivity implements NavigationTabFragment.NavigationDrawerCallbacks {
	private String fragmentType = "";
	private ActionBar actionBar;
	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationTabFragment mNavigationDrawerFragment;
	private int time = 0;
	private JSONArray jyourChannels;
	private JSONArray jfollowedChannels;
	private JSONArray jexplorer;
	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;
	protected boolean isFirstTime = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//	setContentView(R.layout.activity_drawer_home);
		setContentView(R.layout.activity_tabs_home);
		//getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		//getSupportActionBar().setHomeButtonEnabled(false);
		initControls(savedInstanceState);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		actionBar = getSupportActionBar();
		mNavigationDrawerFragment = (NavigationTabFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_tab);
		// Set up the drawer.	
		mNavigationDrawerFragment.setUp(2, R.id.navigation_tab);
	}

	@Override
	protected void setValueOnUI() {
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		invalidateOptionsMenu();
		// update the main content by replacing fragments
		switch (position) {
		case 0:
			replaceFragement(CommunityFragment.newInstance(null), CommunityFragment.TAG);
			break;
		case 1:
			replaceFragement(FeedsFragment.newInstance(null), FeedsFragment.TAG);
			break;
		case 2:
			replaceFragement(HomePopularCatagoryFragment.newInstance(null), HomePopularCatagoryFragment.TAG);
			//replaceFragement(ChannelFragment.newInstance(null), ChannelFragment.TAG);
			
			break;
		case 3:
			replaceFragement(InboxFragment.newInstance(null), InboxFragment.TAG);
			break;
		case 4:
			//startMyActivity(InviteFriendActivity.class, null);
			replaceFragement(ContactsFragment.newInstance(null), ContactsFragment.TAG);
			break;
		}		
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {	
		super.onPostCreate(savedInstanceState);
		//mNavigationDrawerFragment.mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		//mNavigationDrawerFragment.mDrawerToggle.onConfigurationChanged(newConfig);
	}

	public void onSectionAttached(int number) {		
		mTitle = getResources().getStringArray(R.array.drawer_items)[number];		
	}

	/*public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}*/

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {	
		super.onActivityResult(arg0, arg1, arg2);		
	}

	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if (super.callBackFromApi(object, act, requstCode)) {
			commonCallBack(object, requstCode, act);
		}
		return true; 
	}


	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,	int requstCode) {
		if (super.callBackFromApi(object, fragment, requstCode)) {
			commonCallBack(object, requstCode, fragment);			
		}
		return true;
	}

	private void commonCallBack(Object object, int requestCode, Object fragment2){
		JSONObject jObject;
		String message ="";
		jObject = (JSONObject) object;
		message = jObject.optString(Constants.message);
		switch (requestCode){
		case Constants.getSelCatChannels:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONObject jList = jObject.optJSONObject(Constants.list);
				if(jList != null){
					jyourChannels = jList.optJSONArray(Constants.yourChannels);
					jfollowedChannels = jList.optJSONArray(Constants.followedChannels);
					jexplorer = jList.optJSONArray(Constants.explorer);
					ExplorerChannelsFragment mFragment = null;
					if(fragment2 instanceof Fragment)
						mFragment =(ExplorerChannelsFragment) fragment2;
					else
						mFragment = ((ExplorerChannelsFragment)getFragmentByTag(ExplorerChannelsFragment.TAG));

					if (mFragment != null) {
						mFragment.setChannels(jexplorer);
					}
				}
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getCat:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);
				ArrayList<AppCategory> arrayList = new ArrayList<AppCategory>();
				for (int i = 0; i<jsonArray.length() ; i++) {
					AppCategory appCategory = new AppCategory();
					appCategory.setCatId(jsonArray.optJSONObject(i).optString("catId"));
					appCategory.setCatName(jsonArray.optJSONObject(i).optString("catName"));
					appCategory.setCatLogo(jsonArray.optJSONObject(i).optString("catImage"));
					appCategory.setChecked(jsonArray.optJSONObject(i).optInt("isChecked") == 1 ? true : false);
					arrayList.add(appCategory);
				}
				HomePopularCatagoryFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(HomePopularCatagoryFragment) fragment2;
				else
					freg = (HomePopularCatagoryFragment) getFragmentByTag(HomePopularCatagoryFragment.TAG);

				freg.serviceResponse(arrayList);
			}
			break;

		case Constants.deleteChannel:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				int channelStatus = jObject.optInt(Constants.channelStatus);
				if (channelStatus == 1) {
					YourChannelsFragment freg = null;
					if(fragment2 instanceof Fragment)
						freg =(YourChannelsFragment) fragment2;
					else
						freg = (YourChannelsFragment) getFragmentByTag(YourChannelsFragment.TAG);

					freg.refreshList();
				}
				ToastCustomClass.showToast(this, message);
			}else {

				ToastCustomClass.showToast(this, message);
			}

			break;
		case Constants.leaveChannel:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				int channelStatus = jObject.optInt(Constants.channelStatus);
				if (channelStatus == 1) {
					FollowedChannelsFragment freg = null;
					if(fragment2 instanceof Fragment)
						freg =(FollowedChannelsFragment) fragment2;
					else
						freg = (FollowedChannelsFragment) getFragmentByTag(FollowedChannelsFragment.TAG);

					freg.refreshList();
				}
				ToastCustomClass.showToast(this, message);

			}else {
				ToastCustomClass.showToast(this, message); 
			}
			break;
		case Constants.getExploreChannels:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				jexplorer = jObject.optJSONArray(Constants.explorer);
				ExplorerChannelsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(ExplorerChannelsFragment) fragment2;
				else
					freg = (ExplorerChannelsFragment) getFragmentByTag(ExplorerChannelsFragment.TAG);

				freg.setChannels(jexplorer);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getYourChannels:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				jyourChannels = jObject.optJSONArray(Constants.yourChannels);
				YourChannelsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(YourChannelsFragment) fragment2;
				else
					freg = ((YourChannelsFragment)getFragmentByTag(YourChannelsFragment.TAG));

				freg.setChannels(jyourChannels);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getFollowedChannels:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				jfollowedChannels = jObject.optJSONArray(Constants.followedChannels);

				FollowedChannelsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(FollowedChannelsFragment) fragment2;
				else
					freg = ((FollowedChannelsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));

				freg.setChannels(jfollowedChannels);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getChannelFeeds:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);
				ChannelsFeedsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(ChannelsFeedsFragment) fragment2;
				else
					freg = ((ChannelsFeedsFragment)getFragmentByTag(ChannelsFeedsFragment.TAG));

				freg.setFeeds(jsonArray);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getFavorate:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);
				FavoritesFeedsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(FavoritesFeedsFragment) fragment2;
				else
					freg = ((FavoritesFeedsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
				freg.setFeeds(jsonArray);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getFriendsFeeds:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);

				FriendFeedsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(FriendFeedsFragment) fragment2;
				else
					freg = ((FriendFeedsFragment)getFragmentByTag(FriendFeedsFragment.TAG));
				if(freg != null)
					((FriendFeedsFragment)freg).setFeeds(jsonArray);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getAllFeeds:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);

				AllFeedsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(AllFeedsFragment) fragment2;
				else
					freg = ((AllFeedsFragment)getFragmentByTag(AllFeedsFragment.TAG));

				freg.setFeeds(jsonArray);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getContactList:			
			Log.i("App catagory", jObject.toString());
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);//JSONArray jsonArray = jObject.optJSONArray("user");
				HashMap<Integer, ArrayList<Contact>> map = new HashMap<Integer, ArrayList<Contact>>();
				ArrayList<Integer> header = new ArrayList<Integer>();
				ArrayList<Contact> arrayList = null;
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jOb = jsonArray.optJSONObject(i);
					int userType = jOb.optInt(Constants.friendType);

					Contact invite = new Contact();
					invite.setId(jOb.optString(Constants.USER_ID));
					invite.setName(jOb.optString(Constants.USER_NAME));
					invite.setMobileNo(jOb.optString(Constants.PH_NUMBER));
					invite.setType(userType);
					//invite.addType(userType);
					if(!map.containsKey(userType)){
						arrayList = new ArrayList<Contact>();							
						arrayList.add(invite);
						header.add(userType);
						map.put(userType, arrayList);	
					}else{
						map.get(userType).add(invite);
					}						
				}
				ContactsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(ContactsFragment) fragment2;
				else

					freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
				if(freg != null)
					freg.serverResponsGetContactList(header, map);			
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.sendInvitationAlertDialog:	
			ContactsFragment freg = null;
			if(fragment2 instanceof Fragment)
				freg =(ContactsFragment) fragment2;
			else
				freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
			if(freg != null)
				freg.serverResponsSentRequestAlertDialog();
			break;
		case Constants.sendInvitation:	
			
			if(fragment2 instanceof Fragment)
				freg =(ContactsFragment) fragment2;
			else
				freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
			if(freg != null)
				freg.serverResponsSentRequest();
			break;
		case Constants.denyInvitation:	
			if(fragment2 instanceof Fragment)
				freg =(ContactsFragment) fragment2;
			else
				freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
			freg.serverResponsAcceptCancelRequest(Constants.noFriend);
			break;
		case Constants.acceptInvitation:
			if(fragment2 instanceof Fragment)
				freg =(ContactsFragment) fragment2;
			else
				freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
			if(freg != null)
				freg.serverResponsAcceptCancelRequest(Constants.friend);
			break;
		case Constants.getYourCommunityApps:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONObject jsonObject = jObject.optJSONObject(Constants.YourCommunityApps);
				YourCommunityFragment freg1 = null;
				if(fragment2 instanceof Fragment)
					freg1 =(YourCommunityFragment) fragment2;
				else
					freg1 = ((YourCommunityFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
				if(freg1 != null)
					freg1.setCommunity(jsonObject);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		}
	}

	/*	@SuppressWarnings("deprecation")
	public void customTab(String frg) {
		fragmentType = frg;
		if (actionBar != null) {
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		}

		String array[] = getResources().getStringArray(R.array.home_action_tab_array);
		for (int i = 0; i < array.length; i++) {
			//String styledText = "<font color='red'>"+array[i]+"</font>.";
			actionBar.addTab(actionBar.newTab().setText(array[i]).setTabListener(new TabListener() {
				private BaseFragment mFragment;


				public void onTabSelected(Tab tab, FragmentTransaction ft) {
					// Check if the fragment is already initialized
					 if (mFragment == null) {
				            // If not, instantiate and add it to the activity
				            mFragment = Fragment.instantiate(HomeActivity.this, ChannelsFragment.class.getName());
				            ft.add(R.id.container, mFragment, ChannelsFragment.TAG);
				        } else {
				            // If it exists, simply attach it in order to show it
				            ft.attach(mFragment);
				        }					
					//  if (fragmentType.equals("HomeActivity")) {
					removeTopFragement();
					switch (tab.getPosition()) {
					case 0:
						mFragment = ((ExplorerChannelsFragment)getFragmentByTag(ExplorerChannelsFragment.TAG));
    						if(mFragment == null)
						if(isFirstTime){
							replaceFragement(HomePopularCatagoryFragment.newInstance(null), HomePopularCatagoryFragment.TAG);
							isFirstTime = false;
						}else{
							replaceFragement(ExplorerChannelsFragment.newInstance(jexplorer), ExplorerChannelsFragment.TAG);
						}

						break;
					case 1:
						mFragment = ((FollowedChannelsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
    						if(mFragment == null)
						replaceFragement(FollowedChannelsFragment.newInstance(jfollowedChannels), FollowedChannelsFragment.TAG);
						else
    							((FollowedChannelsFragment)mFragment).setChannels(jfollowedChannels);
						break;
					case 2:
						mFragment = ((YourChannelsFragment)getFragmentByTag(YourChannelsFragment.TAG));
    						if(mFragment == null)
						replaceFragement(YourChannelsFragment.newInstance(jyourChannels), YourChannelsFragment.TAG);
						else
    							((YourChannelsFragment)mFragment).setChannels(jyourChannels);
						break;
					};
					//}

					//replaceFragement(ChannelsFragment.getInstance(tab.getPosition()), ChannelsFragment.TAG);
				}

				public void onTabUnselected(Tab tab, FragmentTransaction ft) {
					if (mFragment != null) {
						// Detach the fragment, because another one is being attached
						ft.detach(mFragment);
					}
				}

				public void onTabReselected(Tab tab, FragmentTransaction ft) {
					// User selected the already selected tab. Usually do nothing.
				}
			}));
		}	
	}*/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {	
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.setting, menu);
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;			
		case R.id.i_settings:			
			startMyActivity(SettingActivity.class, null);
			break;		
		case R.id.i_share_apps:			
			startMyActivity(MyAppShareActivity.class, null);
			break;
		case R.id.i_helps:			
			UtilsClass.underConstruction(this);
			break;
		case R.id.i_edit_user:			
			UtilsClass.underConstruction(this);
			break;
		}	
		return false;
	}


	@Override
	public void onBackPressed() {	
		super.onBackPressed();
		int framentCount = getSupportFragmentManager().getBackStackEntryCount();
		if(framentCount == 0)
		{			
			disableBackButton();
		}

		//getSupportActionBar().	
	}
}
