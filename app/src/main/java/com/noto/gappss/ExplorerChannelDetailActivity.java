package com.noto.gappss;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ChannelDetail;
import com.noto.gappss.beanClasses.Post;
import com.noto.gappss.fragments.ExplorerChannelDetailFragment;
import com.noto.gappss.fragments.ExplorerChannelsFragment;
import com.noto.gappss.fragments.FollowedChannelsFragment;
import com.noto.gappss.fragments.YourChannelDetailFragment;
import com.noto.gappss.utilities.ToastCustomClass;

public class ExplorerChannelDetailActivity extends BaseFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container);
		enableBackButton();
		replaceFragement(ExplorerChannelDetailFragment.newInstance(getBundle()), ExplorerChannelDetailFragment.TAG);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {

	}

	@Override
	protected void setValueOnUI() {

	}

	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if (super.callBackFromApi(object, act, requstCode)) {
			JSONObject jObject ;
			String message ="";
			switch (requstCode) {
			case Constants.getChannelDetail:
				jObject = (JSONObject) object;
				message = jObject.optString("message");
				//Log.i("", jObject.toString());
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					Log.i("GET CHANNEL DETAIL", jObject.toString());
					JSONObject jsonObject = jObject.optJSONObject("channel");
					ChannelDetail channelDetail = new ChannelDetail();
					channelDetail.setChannelDescription(jsonObject.optString("channelDescription"));
					channelDetail.setChannelId(jsonObject.optString("channelId"));
					channelDetail.setChannelImage(jsonObject.optString("channelImage"));
					channelDetail.setChannelMode(jsonObject.optString("channelMode"));
					channelDetail.setChannelName(jsonObject.optString("channelName"));
					channelDetail.setChannelType(jsonObject.optString("channelType"));
					channelDetail.setCreatedOn(jsonObject.optString("createdOn"));
					channelDetail.setMembers(jsonObject.optInt("Members"));
					channelDetail.setTotalPosts(jsonObject.optInt("totalPosts"));
					channelDetail.setIsOwner(jsonObject.optInt("isOwner"));
					channelDetail.setIsSusbcribable(jsonObject.optInt("isSusbcribed"));
					JSONArray jsonArray = jsonObject.optJSONArray("Posts");
					ArrayList<Post> arrayList = new ArrayList<Post>();
					if (jsonArray !=null) {
						for (int i = 0; i < jsonArray.length(); i++) {
							Post post = new Post();
							post.setCreated(jsonArray.optJSONObject(i).optString("created"));
							post.setDescription(jsonArray.optJSONObject(i).optString("description"));
							post.setId(jsonArray.optJSONObject(i).optString("id"));
							post.setImage(jsonArray.optJSONObject(i).optString("image"));
							post.setTitle(jsonArray.optJSONObject(i).optString("title"));
							arrayList.add(post);
						}
					}
					channelDetail.setPostList(arrayList);
					ExplorerChannelDetailFragment freg = (ExplorerChannelDetailFragment) getFragmentByTag(ExplorerChannelDetailFragment.TAG);
					if(freg != null)
						freg.setChannelDetail(channelDetail);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
				
			case Constants.joinChannel:
				jObject = (JSONObject) object;
				message = jObject.optString("message");
				//Log.i("App catagory", jObject.toString());
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					int channelStatus = jObject.optInt(Constants.channelStatus);
					if (channelStatus == 1) {
						ExplorerChannelDetailFragment freg = (ExplorerChannelDetailFragment) getFragmentByTag(ExplorerChannelDetailFragment.TAG);
						if(freg != null)
							freg.serviceResponsJoinChannel();
					}
					ToastCustomClass.showToast(this, message);
					
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.leaveChannel:
				jObject = (JSONObject) object;
				message = jObject.optString("message");
				//Log.i("App catagory", jObject.toString());
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					int channelStatus = jObject.optInt(Constants.channelStatus);
					if (channelStatus == 1) {
						ExplorerChannelDetailFragment freg = (ExplorerChannelDetailFragment) getFragmentByTag(ExplorerChannelDetailFragment.TAG);
						if(freg != null)
							freg.serviceResponsLeaveChannel();
					}
					ToastCustomClass.showToast(this, message);
					
				}else {
					ToastCustomClass.showToast(this, message); 
				}
				break;
			}
			
		}
		
		return true;
	}	
}
