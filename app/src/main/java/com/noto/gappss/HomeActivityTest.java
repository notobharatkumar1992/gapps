package com.noto.gappss;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.AppCategory;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.fragments.AllFeedsFragment;
import com.noto.gappss.fragments.ChannelFragment;
import com.noto.gappss.fragments.ChannelsFeedsFragment;
import com.noto.gappss.fragments.CommunityFragment;
import com.noto.gappss.fragments.ContactsFragment;
import com.noto.gappss.fragments.ExplorerChannelsFragment;
import com.noto.gappss.fragments.FavoritesFeedsFragment;
import com.noto.gappss.fragments.FeedsFragment;
import com.noto.gappss.fragments.FollowedChannelsFragment;
import com.noto.gappss.fragments.FriendFeedsFragment;
import com.noto.gappss.fragments.HomePopularCatagoryFragment;
import com.noto.gappss.fragments.InboxFragment;
import com.noto.gappss.fragments.InboxPostFragment;
import com.noto.gappss.fragments.InboxRequestFragment;
import com.noto.gappss.fragments.InboxSharedAppFragment;
import com.noto.gappss.fragments.MyAppShareFragment;
import com.noto.gappss.fragments.SettingFragment;
import com.noto.gappss.fragments.YourChannelsFragment;
import com.noto.gappss.fragments.YourCommunityFragment;
import com.noto.gappss.preference.MySharedPreferences;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class HomeActivityTest extends BaseFragmentActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;
	private String id = "";
	private String key = "";
	private boolean flag = true;

	private static final int TIME_INTERVAL = 1000; // # milliseconds, desired time passed between two back presses.
	private long mBackPressed;

	private JSONArray jexplorer;

	private JSONArray jyourChannels;

	private JSONArray jfollowedChannels;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_main_one);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
		//mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,	(DrawerLayout) findViewById(R.id.drawer_layout_main));	
		mNavigationDrawerFragment.selectItem(MySharedPreferences.getInstance().getInteger(this, Constants.drawerTab, 1), 0);	
	}


	@Override
	public void onNavigationDrawerItemSelected(int position, int child) {
		// TODO Auto-generated method stub
		//ToastCustomClass.showToast(this, "onNavigationDrawerItemSelected=="+position);
		switch (position) {
		case 0:
			replaceFragement(CommunityFragment.newInstance(null), CommunityFragment.TAG);	
			break;
		case 1:
			replaceFragement(FeedsFragment.newInstance(null), FeedsFragment.TAG);
			break;
		case 2:
			if (flag) {
				replaceFragement(HomePopularCatagoryFragment.newInstance(null), HomePopularCatagoryFragment.TAG);
				flag = false;
			}else {
				replaceFragement(ChannelFragment.newInstance(null), ChannelFragment.TAG);
			}
			
			break;
		case 3:
			replaceFragement(InboxFragment.newInstance(null), InboxFragment.TAG);
			break;
		case 4:
			//startMyActivity(InviteFriendActivity.class, null);
			replaceFragement(ContactsFragment.newInstance(null), ContactsFragment.TAG);
			break;
			/*case 5:
			//startMyActivity(InviteFriendActivity.class, null);
			replaceFragement(SettingFragment.newInstance(null), SettingFragment.TAG);
			break;*/
		case 100:
			replaceFragement(HomePopularCatagoryFragment.newInstance(null), HomePopularCatagoryFragment.TAG);
			break;
		}
	}


	@Override
	protected void onPostCreate(Bundle savedInstanceState) {	
		super.onPostCreate(savedInstanceState);
		mNavigationDrawerFragment.mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mNavigationDrawerFragment.mDrawerToggle.onConfigurationChanged(newConfig);
	}

	public void onSectionAttached(int number) {		
		mTitle = getResources().getStringArray(R.array.navDrawerItems)[number];		
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {	
		super.onActivityResult(arg0, arg1, arg2);		
	}



	@Override
	public void onBackPressed() {  
		finish();
	}

	/*@Override
	 public boolean onOptionsItemSelected(MenuItem item) {
		 if (mNavigationDrawerFragment.mDrawerToggle.onOptionsItemSelected(item)) {
	          return true;
	        }
	  return super.onOptionsItemSelected(item);
	 }*/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {	
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.setting, menu);
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			BaseFragment topF = getTopFragment();
			if(topF != null && !topF.onBackPressedListener()){
				removeTopFragement();
				disableBackButton();
				return true;  
			}else {
				if (mNavigationDrawerFragment.mDrawerToggle.onOptionsItemSelected(item)) {
					return true;
				}
			}

			break;			
		case R.id.i_settings:			
			startMyActivity(SettingActivity.class, null);
			break;		
		case R.id.i_share_apps:			
			startMyActivity(MyAppShareActivity.class, null);
			break;
		case R.id.i_helps:			
			UtilsClass.underConstruction(this);
			break;
		case R.id.i_edit_user:			
			UtilsClass.underConstruction(this);
			break;
		}	
		return false;
	}


	/*	
	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if (super.callBackFromApi(object, act, requstCode)) {
			commonCallBack(object, requstCode);
		}
		return true; 
	}


	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,	int requstCode) {
		if (super.callBackFromApi(object, fragment, requstCode)) {
			commonCallBack(object, requstCode);
			switch (requstCode){	
			case Constants.getExploreChannels:
				JSONObject jObject = (JSONObject) object;
				String message = jObject.optString("message");

				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					jexplorer = jObject.optJSONArray(Constants.explorer);
					if(fragment != null)
						((ExplorerChannelsFragment)fragment).setChannels(jexplorer);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.getYourChannels:
				jObject = (JSONObject) object;
				message = jObject.optString("message");
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					jyourChannels = jObject.optJSONArray(Constants.yourChannels);
					//YourChannelsFragment mFragment = ((YourChannelsFragment)getFragmentByTag(YourChannelsFragment.TAG));
					if(fragment != null)
						((YourChannelsFragment)fragment).setChannels(jyourChannels);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.getFollowedChannels:
				jObject = (JSONObject) object;
				message = jObject.optString("message");
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					jfollowedChannels = jObject.optJSONArray(Constants.followedChannels);
					//FollowedChannelsFragment mFragment = ((FollowedChannelsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
					if(fragment != null)
						((FollowedChannelsFragment)fragment).setChannels(jfollowedChannels);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.getChannelFeeds:
				jObject = (JSONObject) object;
				message = jObject.optString("message");
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);

					if(fragment != null)
						((ChannelsFeedsFragment)fragment).setFeeds(jsonArray);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.getFavorate:
				jObject = (JSONObject) object;
				message = jObject.optString("message");
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);
					//FollowedChannelsFragment mFragment = ((FollowedChannelsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
					if(fragment != null)
						((FavoritesFeedsFragment)fragment).setFeeds(jsonArray);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.getFriendsFeeds:
				jObject = (JSONObject) object;
				message = jObject.optString("message");
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);
					//FollowedChannelsFragment mFragment = ((FollowedChannelsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
					if(fragment != null)
						((FriendFeedsFragment)fragment).setFeeds(jsonArray);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.getAllFeeds:

				jObject = (JSONObject) object;
				message = jObject.optString("message");
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);
					//FollowedChannelsFragment mFragment = ((FollowedChannelsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
					if(fragment != null)
						((AllFeedsFragment)fragment).setFeeds(jsonArray);


				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.getContactList:
				jObject = (JSONObject) object;
				message = jObject.optString(Constants.message);
				Log.i("App catagory", jObject.toString());
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray(Constants.list);//JSONArray jsonArray = jObject.optJSONArray("user");
					HashMap<Integer, ArrayList<Contact>> map = new HashMap<Integer, ArrayList<Contact>>();
					ArrayList<Integer> header = new ArrayList<Integer>();
					ArrayList<Contact> arrayList = null;
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jOb = jsonArray.optJSONObject(i);
						int userType = jOb.optInt(Constants.friendType);

						Contact invite = new Contact();
						invite.setId(jOb.optString(Constants.USER_ID));
						invite.setName(jOb.optString(Constants.USER_NAME));
						invite.setMobileNo(jOb.optString(Constants.PH_NUMBER));
						invite.setType(userType);
						//invite.addType(userType);
						if(!map.containsKey(userType)){
							arrayList = new ArrayList<Contact>();							
							arrayList.add(invite);
							header.add(userType);
							map.put(userType, arrayList);	
						}else{
							map.get(userType).add(invite);
						}						
					}
					ContactsFragment freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
					if(freg != null)
						freg.serverResponsGetContactList(header, map);
										JSONArray jsonArray = jObject.optJSONArray("user");
					ArrayList<Contact> arrayList = new ArrayList<Contact>();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jOb = jsonArray.optJSONObject(i);
						Contact invite = new Contact();
						invite.setId(jOb.optString("id"));
						invite.setName(jOb.optString(Constants.USER_NAME));
						invite.setMobileNo(jOb.optString(Constants.PH_NUMBER));
						invite.setType(jOb.optInt(Constants.friendType));
						invite.addType(jOb.optInt(Constants.friendType));
						arrayList.add(invite);
					}
					ContactsFragment freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
					if(freg != null)
						freg.serverResponsGetContactList(arrayList);
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.sendInvitation:
				jObject = (JSONObject) object;
				message = jObject.optString(Constants.message);
				//	Log.i("App catagory", jObject.toString());
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONArray jsonArray = jObject.optJSONArray(Constants.list);
					ArrayList<Contact> arrayList = new ArrayList<Contact>();
					for (int i = 0; i < jsonArray.length(); i++) {
						Contact invite = new Contact();
						invite.setId(Constants.USER_ID);
						invite.setStatus(jsonArray.optJSONObject(i).optInt(Constants.status));
						invite.setMessage(jsonArray.optJSONObject(i).optString(Constants.message));
						arrayList.add(invite);
					}
					ContactsFragment freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
					if(freg != null)
						freg.serverResponsSentRequest();
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.acceptInvitation:
				jObject = (JSONObject) object;
				message = jObject.optString(Constants.message);
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){					
					ContactsFragment freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
					if(freg != null)
						freg.serverResponsSentRequest();
				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			case Constants.getYourCommunityApps:

				jObject = (JSONObject) object;
				message = jObject.optString(Constants.message);
				if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
					JSONObject jsonObject = jObject.optJSONObject(Constants.YourCommunityApps);
					//FollowedChannelsFragment mFragment = ((FollowedChannelsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
					if(fragment != null)
						((YourCommunityFragment)fragment).setCommunity(jsonObject);


				}else {
					ToastCustomClass.showToast(this, message);
				}
				break;
			}
		}

		return isTest;
	}

	private void commonCallBack(Object object, int requestCode){
		JSONObject jObject;
		String message ="";
		switch (requestCode){
		case Constants.getSelCatChannels:
			jObject = (JSONObject) object;
			message = jObject.optString(Constants.message);
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONObject jList = jObject.optJSONObject(Constants.list);
				if(jList != null){
					jyourChannels = jList.optJSONArray(Constants.yourChannels);
					jfollowedChannels = jList.optJSONArray(Constants.followedChannels);
					jexplorer = jList.optJSONArray(Constants.explorer);	
					ExplorerChannelsFragment mFragment = ((ExplorerChannelsFragment)getFragmentByTag(ExplorerChannelsFragment.TAG));
					if(mFragment == null)
						replaceFragement(ExplorerChannelsFragment.newInstance(jexplorer), ExplorerChannelsFragment.TAG);
					else
						((ExplorerChannelsFragment)mFragment).setChannels(jexplorer);
					if (mFragment != null) {
						mFragment.setChannels(jexplorer);

					}
				}
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getCat:
			jObject = (JSONObject) object;
			//Log.i("App catagory", jObject.toString());

			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray("list");
				ArrayList<AppCategory> arrayList = new ArrayList<AppCategory>();
				for (int i = 0; i<jsonArray.length() ; i++) {
					AppCategory appCategory = new AppCategory();
					appCategory.setCatId(jsonArray.optJSONObject(i).optString("catId"));
					appCategory.setCatName(jsonArray.optJSONObject(i).optString("catName"));
					appCategory.setCatLogo(jsonArray.optJSONObject(i).optString("catImage"));
					appCategory.setChecked(jsonArray.optJSONObject(i).optInt("isChecked") == 1 ? true : false);
					arrayList.add(appCategory);
				}

				HomePopularCatagoryFragment freg = (HomePopularCatagoryFragment) getFragmentByTag(HomePopularCatagoryFragment.TAG);
				if(freg != null)
					freg.serviceResponse(arrayList);
			}

			break;

		case Constants.deleteChannel:
			jObject = (JSONObject) object;
			Log.i("Delete channel", jObject.toString());
			message = jObject.optString("message");
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				int channelStatus = jObject.optInt(Constants.channelStatus);
				if (channelStatus == 1) {
					YourChannelsFragment freg = (YourChannelsFragment) getFragmentByTag(YourChannelsFragment.TAG);
					if(freg != null)
						freg.refreshList();
				}
				ToastCustomClass.showToast(this, message);

			}else {

				ToastCustomClass.showToast(this, message);
			}

			break;
		case Constants.leaveChannel:
			jObject = (JSONObject) object;
			message = jObject.optString("message");
			//Log.i("App catagory", jObject.toString());
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				int channelStatus = jObject.optInt(Constants.channelStatus);
				if (channelStatus == 1) {
					FollowedChannelsFragment freg = (FollowedChannelsFragment) getFragmentByTag(FollowedChannelsFragment.TAG);
					if(freg != null)
						freg.refreshList();
				}
				ToastCustomClass.showToast(this, message);

			}else {
				ToastCustomClass.showToast(this, message); 
			}
			break;
		}
	}

	 */


	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if (super.callBackFromApi(object, act, requstCode)) {
			commonCallBack(object, requstCode, act);
		}
		return true; 
	}


	@Override
	protected Boolean callBackFromApi(Object object, Fragment fragment,	int requstCode) {
		if (super.callBackFromApi(object, fragment, requstCode)) {
			commonCallBack(object, requstCode, fragment);			
		}
		return true;
	}

	private void commonCallBack(Object object, int requestCode, Object fragment2){
		JSONObject jObject;
		String message ="";
		jObject = (JSONObject) object;
		message = jObject.optString(Constants.message);
		switch (requestCode){
		case Constants.getSelCatChannels:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONObject jList = jObject.optJSONObject(Constants.list);
				if(jList != null){
					jyourChannels = jList.optJSONArray(Constants.yourChannels);
					jfollowedChannels = jList.optJSONArray(Constants.followedChannels);
					jexplorer = jList.optJSONArray(Constants.explorer);
					ExplorerChannelsFragment mFragment = null;
					if(fragment2 instanceof Fragment)
						mFragment =(ExplorerChannelsFragment) fragment2;
					else
						mFragment = ((ExplorerChannelsFragment)getFragmentByTag(ExplorerChannelsFragment.TAG));

					if (mFragment != null) {
						mFragment.setChannels(jexplorer);
					}
				}
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getCat:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);
				ArrayList<AppCategory> arrayList = new ArrayList<AppCategory>();
				for (int i = 0; i<jsonArray.length() ; i++) {
					AppCategory appCategory = new AppCategory();
					appCategory.setCatId(jsonArray.optJSONObject(i).optString("catId"));
					appCategory.setCatName(jsonArray.optJSONObject(i).optString("catName"));
					appCategory.setCatLogo(jsonArray.optJSONObject(i).optString("catImage"));
					appCategory.setChecked(jsonArray.optJSONObject(i).optInt("isChecked") == 1 ? true : false);
					arrayList.add(appCategory);
				}
				HomePopularCatagoryFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(HomePopularCatagoryFragment) fragment2;
				else
					freg = (HomePopularCatagoryFragment) getFragmentByTag(HomePopularCatagoryFragment.TAG);

				freg.serviceResponse(arrayList);
			}
			break;

		case Constants.deleteChannel:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				int channelStatus = jObject.optInt(Constants.channelStatus);
				if (channelStatus == 1) {
					YourChannelsFragment freg = null;
					if(fragment2 instanceof Fragment)
						freg =(YourChannelsFragment) fragment2;
					else
						freg = (YourChannelsFragment) getFragmentByTag(YourChannelsFragment.TAG);

					freg.refreshList();
				}
				ToastCustomClass.showToast(this, message);
			}else {

				ToastCustomClass.showToast(this, message);
			}

			break;
		case Constants.leaveChannel:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				int channelStatus = jObject.optInt(Constants.channelStatus);
				if (channelStatus == 1) {
					FollowedChannelsFragment freg = null;
					if(fragment2 instanceof Fragment)
						freg =(FollowedChannelsFragment) fragment2;
					else
						freg = (FollowedChannelsFragment) getFragmentByTag(FollowedChannelsFragment.TAG);

					freg.refreshList();
				}
				ToastCustomClass.showToast(this, message);

			}else {
				ToastCustomClass.showToast(this, message); 
			}
			break;
		case Constants.getExploreChannels:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				jexplorer = jObject.optJSONArray(Constants.explorer);
				ExplorerChannelsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(ExplorerChannelsFragment) fragment2;
				else
					freg = (ExplorerChannelsFragment) getFragmentByTag(ExplorerChannelsFragment.TAG);

				freg.setChannels(jexplorer);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getYourChannels:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				jyourChannels = jObject.optJSONArray(Constants.yourChannels);
				YourChannelsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(YourChannelsFragment) fragment2;
				else
					freg = ((YourChannelsFragment)getFragmentByTag(YourChannelsFragment.TAG));

				freg.setChannels(jyourChannels);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getFollowedChannels:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				jfollowedChannels = jObject.optJSONArray(Constants.followedChannels);

				FollowedChannelsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(FollowedChannelsFragment) fragment2;
				else
					freg = ((FollowedChannelsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));

				freg.setChannels(jfollowedChannels);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getChannelFeeds:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);
				ChannelsFeedsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(ChannelsFeedsFragment) fragment2;
				else
					freg = ((ChannelsFeedsFragment)getFragmentByTag(ChannelsFeedsFragment.TAG));

				freg.setFeeds(jsonArray);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getFavorate:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);
				FavoritesFeedsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(FavoritesFeedsFragment) fragment2;
				else
					freg = ((FavoritesFeedsFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
				freg.setFeeds(jsonArray);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getFriendsFeeds:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);

				FriendFeedsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(FriendFeedsFragment) fragment2;
				else
					freg = ((FriendFeedsFragment)getFragmentByTag(FriendFeedsFragment.TAG));
				if(freg != null)
					((FriendFeedsFragment)freg).setFeeds(jsonArray);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getAllFeeds:			
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.feeds);

				AllFeedsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(AllFeedsFragment) fragment2;
				else
					freg = ((AllFeedsFragment)getFragmentByTag(AllFeedsFragment.TAG));

				freg.setFeeds(jsonArray);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getContactList:			
			Log.i("App catagory", jObject.toString());
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);//JSONArray jsonArray = jObject.optJSONArray("user");
				HashMap<Integer, ArrayList<Contact>> map = new HashMap<Integer, ArrayList<Contact>>();
				ArrayList<Integer> header = new ArrayList<Integer>();
				ArrayList<Contact> arrayList = null;
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jOb = jsonArray.optJSONObject(i);
					int userType = jOb.optInt(Constants.friendType);

					Contact invite = new Contact();
					invite.setId(jOb.optString(Constants.USER_ID));
					invite.setName(jOb.optString(Constants.USER_NAME));
					invite.setMobileNo(jOb.optString(Constants.PH_NUMBER));
					invite.setType(userType);
					//invite.addType(userType);
					if(!map.containsKey(userType)){
						arrayList = new ArrayList<Contact>();							
						arrayList.add(invite);
						header.add(userType);
						map.put(userType, arrayList);	
					}else{
						map.get(userType).add(invite);
					}						
				}
				ContactsFragment freg = null;
				if(fragment2 instanceof Fragment)
					freg =(ContactsFragment) fragment2;
				else

					freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
				if(freg != null)
					freg.serverResponsGetContactList(header, map);			
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.sendInvitation:	
			ContactsFragment freg = null;
			if(fragment2 instanceof Fragment)
				freg =(ContactsFragment) fragment2;
			else
				freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
			if(freg != null)
				freg.serverResponsSentRequest();
			break;
		case Constants.sendInvitationAlertDialog:	
			if(fragment2 instanceof Fragment)
				freg =(ContactsFragment) fragment2;
			else
				freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
			if(freg != null)
				freg.serverResponsSentRequestAlertDialog();
			break;
		case Constants.denyInvitation:	
			if(fragment2 instanceof Fragment)
				freg =(ContactsFragment) fragment2;
			else
				freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
			freg.serverResponsAcceptCancelRequest(Constants.noFriend);
			break;
		case Constants.acceptInvitationInbox:
			InboxRequestFragment fregInbox = null;
			if(fragment2 instanceof Fragment)
				fregInbox =(InboxRequestFragment) fragment2;
			else
				fregInbox = (InboxRequestFragment) getFragmentByTag(ContactsFragment.TAG);
			if(fregInbox != null)
				fregInbox.serverResponsAcceptCancelRequest(Constants.friend);
			break;
		case Constants.denyInvitationInbox:	
			if(fragment2 instanceof Fragment)
				fregInbox =(InboxRequestFragment) fragment2;
			else
				fregInbox = (InboxRequestFragment) getFragmentByTag(ContactsFragment.TAG);
			fregInbox.serverResponsAcceptCancelRequest(Constants.noFriend);
			break;
		case Constants.acceptInvitation:
			if(fragment2 instanceof Fragment)
				freg =(ContactsFragment) fragment2;
			else
				freg = (ContactsFragment) getFragmentByTag(ContactsFragment.TAG);
			if(freg != null)
				freg.serverResponsAcceptCancelRequest(Constants.friend);
			break;
		case Constants.getYourCommunityApps:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONObject jsonObject = jObject.optJSONObject(Constants.YourCommunityApps);
				YourCommunityFragment freg1 = null;
				if(fragment2 instanceof Fragment)
					freg1 =(YourCommunityFragment) fragment2;
				else
					freg1 = ((YourCommunityFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
				if(freg1 != null)
					freg1.setCommunity(jsonObject);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;

		case Constants.getRequests:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);
				ArrayList<Contact> arrayList = new ArrayList<Contact>();
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jOb = jsonArray.optJSONObject(i);
					Contact invite = new Contact();
					invite.setId(jOb.optString(Constants.USER_ID));
					invite.setName(jOb.optString(Constants.USER_NAME));
					invite.setMobileNo(jOb.optString(Constants.userPhoneNo));
					invite.setImg(jOb.optString(Constants.userImage));
					arrayList.add(invite);
				}
				InboxRequestFragment freg1 = null;
				if(fragment2 instanceof Fragment)
					freg1 =(InboxRequestFragment) fragment2;
				else
					freg1 = ((InboxRequestFragment)getFragmentByTag(InboxRequestFragment.TAG));
				if(freg1 != null)
					freg1.setList(arrayList);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;

		case Constants.getFriendsAllPosts:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonObject = jObject.optJSONArray(Constants.list);
				InboxPostFragment freg1 = null;
				if(fragment2 instanceof Fragment)
					freg1 =(InboxPostFragment) fragment2;
				else
					freg1 = ((InboxPostFragment)getFragmentByTag(InboxPostFragment.TAG));
				if(freg1 != null)
					freg1.setList(jsonObject);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;

		case Constants.getShareApps:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);
				InboxSharedAppFragment freg1 = null;
				if(fragment2 instanceof Fragment)
					freg1 =(InboxSharedAppFragment) fragment2;
				else
					freg1 = ((InboxSharedAppFragment)getFragmentByTag(InboxSharedAppFragment.TAG));
				if(freg1 != null)
					freg1.setList(jsonArray);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;

		case Constants.getAllInbox:
			if(jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW){
				JSONObject jsonObject = jObject.optJSONObject(Constants.YourCommunityApps);
				YourCommunityFragment freg1 = null;
				if(fragment2 instanceof Fragment)
					freg1 =(YourCommunityFragment) fragment2;
				else
					freg1 = ((YourCommunityFragment)getFragmentByTag(FollowedChannelsFragment.TAG));
				if(freg1 != null)
					freg1.setCommunity(jsonObject);
			}else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		case Constants.getAppVisibility:
			jObject = (JSONObject) object;
			message = jObject.optString(Constants.p_message);
			HashMap<String, ApplicationBean> mapOfApp = new HashMap<String, ApplicationBean>();
			if (jObject.optInt(Constants.dataToFollow, Constants.NOT_FLOW) == Constants.FLOW) {
				// Log.i("App catagory", "2");
				JSONArray jsonArray = jObject.optJSONArray(Constants.list);
				if (fragment2 != null) {
					MyAppShareFragment appShareFragment = (MyAppShareFragment) fragment2;
					appShareFragment.serviceResponse(jsonArray);
				}
			} else {
				ToastCustomClass.showToast(this, message);
			}
			break;
		}
	}



}
