package com.noto.gappss.networkTask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.noto.gappss.MyApplication;
import com.noto.gappss.baseClasses.Constants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;


public class MyAsyncTask extends AsyncTask<String, Void, ApiResponse> implements URLsClass {
    private Context context = null;
    private String progressMsg = null;
    private ProgressDialog progressDialog;
    private CallBackListener callBackListener = null;
    private ParsingClass parsingClass = null;
    //private int parsingType = 1;
    public boolean IsProgressBarShow = true;
    private boolean isPost = false;
    private HashMap<String, Object> params;
    private Boolean isFileUploading = false;

    public CallBackListener getCallBackListener() {
        return callBackListener;
    }

    public void setCallBackListener(CallBackListener callBackListener) {
        this.callBackListener = callBackListener;
    }

    public MyAsyncTask(Context context, HashMap<String, Object> params, String progressMsg, Boolean isPost) {
        this.context = context;
        this.progressMsg = progressMsg;
        //this.parsingType = parsingType;
        parsingClass = ParsingClass.getInstance(context);
        this.isPost = isPost;
        this.params = params;
    }

    public MyAsyncTask(Context context, HashMap<String, Object> params, String progressMsg, Boolean isPost, Boolean isFileUploading) {
        this.context = context;
        this.progressMsg = progressMsg;
        //this.parsingType = parsingType;
        parsingClass = ParsingClass.getInstance(context);
        this.isPost = isPost;
        this.isFileUploading = isFileUploading;
        this.params = params;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (IsProgressBarShow) {
            try {

                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(progressMsg);
                progressDialog.setCancelable(false);
                if (!progressDialog.isShowing())
                    progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected ApiResponse doInBackground(String... arg0) {
        ApiResponse apiResponse = new ApiResponse();
        //	apiResponse = MyApplication.getApplication().getApisResponce();
        if (isFileUploading) {
            Log.i("isFileUploading", "==" + isFileUploading);
            for (String string : arg0) {
                apiResponse = fileUploadMethod(string, params, apiResponse, isPost);
            }
        } else {
            Log.i("isFileUploading else", "==" + isFileUploading);
            for (String string : arg0) {
                apiResponse = parsingClass.startUpConfigrationOfParsingPost(string, params, apiResponse, isPost);
            }
        }

        return apiResponse;
    }

    @Override
    protected void onPostExecute(ApiResponse result) {
        super.onPostExecute(result);
        if (progressDialog != null) {
            try {
                progressDialog.dismiss();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

        if (callBackListener != null)
            callBackListener.callback(result);
    }

    public interface CallBackListener {
        public void callback(Object object);
    }


    public ApiResponse fileUploadMethod(String string, HashMap<String, Object> params2, ApiResponse apiResponse, boolean isPost2) {

        MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (params != null && params.size() > 0) {
            Log.i("fileUploadMethod", params2.toString());
            Set<String> keySet = params.keySet();
            Iterator<String> iterator = keySet.iterator();
            while (iterator.hasNext()) {
                String key = iterator.next();
                try {
                    if (key.equalsIgnoreCase(Constants.p_fileToUpload)) {
                        String file = (String) params.get(key);
                        if (!file.equals("")) {
                            File file2 = new File(file);
                            multipartEntity.addPart(key, new FileBody(file2));
                        }

                        //multipartEntity.addPart(key, new ByteArrayBody((byte[]) params.get(key),"image.jpg"));
                    } else {
                        multipartEntity.addPart(key, new StringBody(String.valueOf(params.get(key))));
                    }

                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                //restClient.AddParam(key, String.valueOf(params.get(key)));
            }

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            try {
                multipartEntity.writeTo(bytes);
                String content = bytes.toString();
                Log.i("MULTPART", content);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            apiResponse = postUrlResponse(string, multipartEntity, apiResponse);
            Log.i("fileUploadMethod", "==fileUploadMethod==" + string + multipartEntity.toString());

        }
        return apiResponse;
    }


    public ApiResponse postUrlResponse(String url, MultipartEntity nameValuePairs, ApiResponse apiResponse) {
        Log.i("postUrlResponse", "==postUrlResponse");
        try {

            HttpPost post = new HttpPost(url);
            post.addHeader("Authorization", MyApplication.base64EncodedCredentials);

            post.setEntity(nameValuePairs);
            HttpClient client = new DefaultHttpClient();//httpParams
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            apiResponse = convertStreamToString(entity.getContent(), apiResponse, url);

        } catch (ConnectTimeoutException cte) {
            // TODO: handle exception
            Log.i("INTERNET EXCEPTION 2", cte.getMessage());
            apiResponse.setErrorMsg(cte.getMessage());
            return apiResponse;

        } catch (Exception e) {

            Log.i("INTERNET EXCEPTION 1", e.getMessage());
            apiResponse.setErrorMsg(e.getMessage());
            return apiResponse;
            //Log.i("INTERNET EXCEPTION 2", e.getMessage().toString());
        }
        return apiResponse;

    }

    private ApiResponse convertStreamToString(InputStream is, ApiResponse apiResponse, String url) {
        Log.i("convertStreamToString", "==convertStreamToString");
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8 * 1024);
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        Log.i("convertStreamToString", "==" + sb.toString());
        apiResponse.addResponce(url, sb.toString());
        return apiResponse;
    }


}




/*package com.jaipurnews.dis.networkTask;

import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;

import com.jaipurnews.dis.MyApplication;

public class MyAsyncTask extends AsyncTask<String, Void, ApiResponse> implements URLsClass
{	
	private Context context = null;
	private String progressMsg = null;
	private ProgressDialog progressDialog;
	private CallBackListener callBackListener = null;
	private ParsingClass parsingClass= null;
	//private int parsingType = 1;
	public boolean IsProgressBarShow = true;
	private boolean isPost = false;
	private HashMap<String, Object> params;

	public CallBackListener getCallBackListener() 
	{
		return callBackListener;
	}

	public void setCallBackListener(CallBackListener callBackListener) 
	{
		this.callBackListener = callBackListener;
	}


	public MyAsyncTask(Context context , HashMap<String, Object> params, String progressMsg, Boolean isPost)
	{
		this.context  = context;
		this.progressMsg = progressMsg;
		//this.parsingType = parsingType;
		parsingClass =  ParsingClass.getInstance(context);
		this.isPost  = isPost;
		this.params = params;
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		if(IsProgressBarShow)
		{
			try 
			{
				if(context instanceof FragmentActivity){
					progressDialog = new ProgressDialog((FragmentActivity)context);
				}else{
					progressDialog = new ProgressDialog((Activity)context);
				}
				progressDialog.setMessage(progressMsg);
				progressDialog.setCancelable(false);
				if(!progressDialog.isShowing())
					progressDialog.show();
			} 
			catch (Exception e) 
			{			
				e.printStackTrace();
			}
			catch (Throwable e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	protected ApiResponse doInBackground(String... arg0) {
		ApiResponse apiResponse = null;

		apiResponse = MyApplication.getApplication().getApisResponce();
		for(String string : arg0) {
			apiResponse = parsingClass.startUpConfigrationOfParsingPost(string, params, apiResponse, isPost);
		}
		return apiResponse;
	}

	@Override
	protected void onPostExecute(ApiResponse result) 
	{
		super.onPostExecute(result);
		if(progressDialog != null)
		{			
			try 
			{
				progressDialog.dismiss();
			}
			catch (Throwable e) 
			{			
				e.printStackTrace();
			}
		}

		if(callBackListener != null)
			callBackListener.callback(result);		
	}

	public interface CallBackListener
	{
		public void callback(Object object);
	}	
}
 */