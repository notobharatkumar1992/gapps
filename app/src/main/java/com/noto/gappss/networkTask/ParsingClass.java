package com.noto.gappss.networkTask;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

import com.noto.gappss.MyApplication;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class ParsingClass {
    /**
     *
     */
    private static final long serialVersionUID = 9898989898l;
    private static final String AUTH_TOKEN_KEY = "authtoken";
    //private static ParsingClass parsingClass=null;
    Context context;


    public static ParsingClass getInstance(Context context) {
        //if(parsingClass == null)
        ParsingClass parsingClass = new ParsingClass(context);
        return parsingClass;
    }

    private ParsingClass(Context context) {
        this.context = context;
    }

    public ApiResponse startUpConfigrationOfParsingPost(String strUrl, HashMap<String, Object> params, ApiResponse apiResponce, boolean isPost) {
        if (Is_Internet_Available_Class.internetIsAvailable(context)) {
            RestClient restClient = null;

            // Post Method HttpPost
            try {
                //System.out.println("Print inside Parsing API calling time : "+strUrl);
                restClient = new RestClient(strUrl);
                restClient.AddHeader("Authorization", MyApplication.base64EncodedCredentials);
                try {
                    //System.out.println("After Full URL Encoding is : "+strUrl);
                    // Post Method HttpPost
                    try {
                        if (params != null && params.size() > 0) {
                            Set<String> keySet = params.keySet();
                            Iterator<String> iterator = keySet.iterator();
                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                restClient.AddParam(key, String.valueOf(params.get(key)));
                            }
                        }

                        if (isPost)
                            restClient.Execute(RequestMethod.POST);
                        else
                            restClient.Execute(RequestMethod.GET);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        apiResponce.setErrorMsg(e2.getMessage());
                        return apiResponce;
                    }

                    String response = restClient.getResponse();
                    apiResponce.addResponce(strUrl, response);


					/*if(response != null && response.length() > 0){
                        JSONArray jsonArray = new JSONArray(response);
						for(int index = 0; index < jsonArray.length(); index++){
							JSONObject obj = (JSONObject) jsonArray.get(index);
							String id = (String) obj.get(Constants.category_id);
							String category = (String) obj.get("category");							
						}						
					}*/
                } catch (Exception e) {
                    e.printStackTrace();
                    apiResponce.setErrorMsg(e.getMessage());
                    return apiResponce;
                }
            } catch (Exception e) {
                e.printStackTrace();
                apiResponce.setErrorMsg(e.getMessage());
                return apiResponce;
            }

            return apiResponce;
        } else {
            //ToastCustomClass.showToast(context, "No Internet connection found.\n Please start your internet!", 4000).show();
            //alertWarningMsg(context);
            apiResponce.setErrorMsg("No Internet connection found.\n Please start your internet!");
            return apiResponce;
        }
    }
}



