
package com.noto.gappss.networkTask;

import java.util.HashMap;

import android.content.Context;

import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.preference.MySharedPreferences;
import com.noto.gappss.utilities.UtilsClass;

public class ApiManager implements URLsClass {

	private static ApiManager apiManager = null;
	public static ApiManager getInstance(){
		if(apiManager == null)
			apiManager = new ApiManager();

		return apiManager;
	}
	
	public void getCategories(BaseFragmentActivity fragment){
		if(UtilsClass.isConnectingToInternet(fragment)){	
		String urlsArr[] = {URLsClass.service_type_get_categories};
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(fragment, Constants.USER_ID, ""));
		params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
		fragment.serviceCaller(fragment, urlsArr, params, true, false, Constants.getCat, true);
		}else {
			UtilsClass.plsStartInternet(fragment);			
		}	
	}

	/*public void getCategoriesChannels(BaseFragment fragment){
		if(UtilsClass.isConnectingToInternet(fragment.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_cat_channels};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(fragment.getActivity(), Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
			fragment.serviceCaller(fragment, urlsArr, params, true, false, Constants.getSelCatChannels, true);
			//((BaseFragmentActivity)fragment.getActivity()).server			
			//fragment.getActivity().serviceCaller(fragment, urlsArr, params, true, false, Constants.getSelCatChannels, true);
		}else {
			UtilsClass.plsStartInternet(fragment.getActivity());			
		}	
	}*/

	

	public void getChannelDetail(BaseFragmentActivity context, String cid){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_getChannelDetail};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.channelId, cid);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
			context.serviceCaller(context, urlsArr, params, true, false, Constants.getChannelDetail, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}

	public void joinChannel(BaseFragmentActivity context, String cid){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_joinChannel};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.channelId, cid);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
			context.serviceCaller(context, urlsArr, params, true, false, Constants.joinChannel, true);
		}else {
			UtilsClass.plsStartInternet(context);


		}		
	}

	public void leaveChannel(BaseFragmentActivity context, String cid){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_leaveChannel};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.channelId, cid);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
			context.serviceCaller(context, urlsArr, params, true, false, Constants.leaveChannel, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}
	public void getContactList(BaseFragment context,String contactList){
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getContactList};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getStringFromDefaultFile(getActivity(), Constants.USER_ID, "");
			params.put(Constants.p_contact_list, contactList);//
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getContactList, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());			
		}		
	}
	
	public void acceptDenyInvitation(BaseFragment context,String senderId, int requestType, int requstServiceType){
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_acceptInvitation};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getStringFromDefaultFile(getActivity(), Constants.USER_ID, "");
			params.put(Constants.senderId, senderId);
			params.put(Constants.requestType, requestType);//
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, requstServiceType, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());			
		}		
	}
	
	public void sendInvitation(BaseFragmentActivity context,String receiverId,int serviceType){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_sendInvitation};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));//MySharedPreferences.getInstance().getStringFromDefaultFile(getActivity(), Constants.USER_ID, "");
			params.put(Constants.p_receiver_id, receiverId);//
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, serviceType, true);
		}else {
			UtilsClass.plsStartInternet(context);			
		}		
	}
	
	public void channelStatistics(BaseFragmentActivity context,String postId, String channelId){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_channelStatistics};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));//MySharedPreferences.getInstance().getStringFromDefaultFile(getActivity(), Constants.USER_ID, "");
			params.put(Constants.postId, postId);
			params.put(Constants.channelId, channelId);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.channelStatistics, true);
		}else {
			UtilsClass.plsStartInternet(context);	
		}		
	}
	
	public void getUserChannelDetail(BaseFragmentActivity context,String channelId, String userId){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_getUserChannelDetail};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, userId);//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			//params.put(URLsClass.p_postId, postId);
			params.put(Constants.channelId, channelId);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getUserChannelDetail, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}
	
	public void createChannel(BaseFragmentActivity context,String name,String des,String type,String mode, String string){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_createChannel};
			
			//ByteArrayOutputStream bos = new ByteArrayOutputStream();
			//string.compress(CompressFormat.JPEG, 100, bos);
           // byte[] data = bos.toByteArray();
           // reqEntity.addPart("file", new ByteArrayBody(data,"image.jpg"));
			
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			
			params.put(Constants.name, name);
			params.put(Constants.description, des);
			params.put(Constants.p_channel_type, type);
			params.put(Constants.p_channel_mode, mode);
			params.put(Constants.p_fileToUpload, string);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
			
            context.serviceCaller(context, urlsArr, params, true, true, Constants.createChannel, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}
	
	public void getChannelSettings(BaseFragmentActivity context){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_getChannelSettings};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getChannelSettings, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}
	
	public void setChannelSettings(BaseFragmentActivity context,String show_subscribe_feeds,String allow_post,String allow_search,String allow_feeds){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_setChannelSettings};
			HashMap<String, Object> params = new HashMap<String, Object>();
			
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.p_show_subscribe_feeds, show_subscribe_feeds);
			params.put(Constants.p_allow_post, allow_post);
			params.put(Constants.p_allow_search, allow_search);
			params.put(Constants.p_allow_feeds, allow_feeds);
			
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.setChannelSettings, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}
	
	public void setNotificationSettings(BaseFragmentActivity context,String notify){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_setChannelSettings};
			HashMap<String, Object> params = new HashMap<String, Object>();
			
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.p_show_subscribe_feeds, notify);
			
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.setChannelSettings, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}
	
	
	public void createPost(BaseFragmentActivity context,String channelId,String title,String des,String appPack,String appName,String appUrl,String cat,String fileUrl){
		if(UtilsClass.isConnectingToInternet(context)){	
			//userId , channelId,title,description,appPack,appName,app_url,category,fileToUpload
			String urlsArr[] = {URLsClass.service_type_createPost};
			HashMap<String, Object> params = new HashMap<String, Object>();
			
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.channelId, channelId);
			params.put(Constants.title, title);
			params.put(Constants.description, des);
			params.put(Constants.p_appPack, appPack);
			params.put(Constants.p_appName, appName);
			params.put(Constants.p_app_url, appUrl);
			params.put(Constants.p_categories, cat);
			params.put(Constants.p_fileToUpload, fileUrl);
			
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, true, Constants.createPost, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}
	
	public void deleteChannel(BaseFragmentActivity context,String channelId){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_deleteChannel};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.channelId, channelId);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.deleteChannel, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}
	
	public void getFollowedChannels(BaseFragment context){
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getFollowedChannels};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getFollowedChannels, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}		
	}
	public void getYourChannels(BaseFragment context){
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getYourChannels};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getYourChannels, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}		
	}
	
	public void getExploreChannels(BaseFragment context){
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getExploreChannels};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getExploreChannels, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}		
	}
	
	public void getPostsByChannel(BaseFragmentActivity context,String channelId){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_getPostsByChannel};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.channelId, channelId);//dummy id 3
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getPostsByChannel, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}
	
	public void postDetail(BaseFragmentActivity context,String postId){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_postDetail};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.postId, postId);//Dummy 3
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.postDetail, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}
	
	public void createComment(BaseFragmentActivity context,String postId,String comment,String parentId){
		if(UtilsClass.isConnectingToInternet(context)){	
			String urlsArr[] = {URLsClass.service_type_createComment};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.postId, postId);//dummy 3
			params.put(Constants.comment, comment);
			params.put(Constants.parentId, parentId);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.createComment, true);
		}else {
			UtilsClass.plsStartInternet(context);
		}		
	}

	public void getFavorateFeeds(BaseFragment context) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getFavorate};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getFavorate, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
	}
	
	
	public void getChannelFeeds(BaseFragment context) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getChannelsFeeds};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getChannelFeeds, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
	}
	
	public void getFriendsFeeds(BaseFragment context) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getFriendsFeeds};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getFriendsFeeds, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
	}

	public void getAllFeeds(BaseFragment context) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getAllFeeds};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getAllFeeds, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
		
	}

	public void getYourCommunityApps(BaseFragment context) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getYourCommunityApps};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getYourCommunityApps, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
		
	}
	
	public void getGappssCommunityApps(BaseFragment context, String countryCode) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getGappssCommunityApps};
			HashMap<String, Object> params = new HashMap<String, Object>();
			//params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
			params.put(Constants.countryCode, countryCode);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getGappssCommunityApps, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
		
	}
	
	public void getFriendsList(BaseFragment context) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getFriendsList};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getFriendsList, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
		
	}

	public void getDevicesDetails(BaseFragment context, String string) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getDevicesDetails};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
			params.put(Constants.friendId, string);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getDevicesDetails, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
	}
	
	public void getFriendAllChannel(BaseFragment context , String friendId){
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getFriendAllChannel};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.friendId, friendId);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getFriendAllChannel, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}		
	}
	
	public void getFriendAllApps(BaseFragment context , String friendId){
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getFriendAllApps};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.friendId, friendId);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getFriendAllApps, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}		
	}

	public void getFriendsActivites(BaseFragment context,String friendId) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getFriendsActivites};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getString(context, Constants.USER_ID, ""));
			params.put(Constants.friendId, friendId);
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getFriendsFeeds, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
	}
	
	public void sendChannelInvitation(BaseFragment context,String receiverId,String channelId){
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_sendChannelInvitation};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getStringFromDefaultFile(getActivity(), Constants.USER_ID, "");
			params.put(Constants.p_receiver_id, receiverId);//
			params.put(Constants.channelId, channelId);//
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.sendChannelInvitation, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());		
		}		
	}
	
	public void acceptChannelInvitation(BaseFragment context,String receiverId,String channelId,String requestType){
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_acceptChannelInvitation};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));//MySharedPreferences.getInstance().getStringFromDefaultFile(getActivity(), Constants.USER_ID, "");
			params.put(Constants.senderId, receiverId);//
			params.put(Constants.channelId, channelId);//
			params.put(Constants.requestType, requestType);//
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.acceptChannelInvitation, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
			
			
		}		
	}
	
	public void getAppVisibility(BaseFragment context){
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getAppVisibility};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getAppVisibility, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
			
			
		}		
	}
	
	public void getShareApps(BaseFragment context) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getShareApps};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, 132);//MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getShareApps, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
	}
	public void getFriendsAllPosts(BaseFragment context) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getFriendsAllPosts};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, 130);//MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getFriendsAllPosts, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
	}
	public void getRequests(BaseFragment context) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getRequests};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, 132);//MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getRequests, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
	}
	
	public void getAllInbox(BaseFragment context) {
		// TODO Auto-generated method stub
		if(UtilsClass.isConnectingToInternet(context.getActivity())){	
			String urlsArr[] = {URLsClass.service_type_getRequests};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.USER_ID, 130);//MySharedPreferences.getInstance().getString(context.getActivity(), Constants.USER_ID, ""));
			params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
            context.serviceCaller(context, urlsArr, params, true, false, Constants.getAllInbox, true);
		}else {
			UtilsClass.plsStartInternet(context.getActivity());
		}
	}
	
	
}
