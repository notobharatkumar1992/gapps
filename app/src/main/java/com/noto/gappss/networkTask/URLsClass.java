package com.noto.gappss.networkTask;

public interface URLsClass {
    public final String reverseGeoCodingApi = "http://maps.googleapis.com/maps/api/geocode/json";
    public final String address = "address";
    public final String sensor = "sensor";

    //public final static String baseUrl = "http://192.168.1.186/gapps/UserWebservices/";//old local
    //public final static String baseUrl = "http://192.168.1.186/tisaadmingapps/UserWebservices/";//new local
//	public final static String baseUrl = "http://notosolutions.net/gapps/UserWebservices/";
    public final static String baseUrl = "http://gappss.com/UserWebservices/";
    public final static String baseUrl_other = "http://gappss.com/";

    public final static String googleAppUrl = "https://play.google.com/store/apps/details?hl=en&id=";

    //Tearm and condition url
    public final static String tearmAndCondition = baseUrl_other + "terms-and-conditions";
    public final static String privacyPolicy = baseUrl_other + "privacy-policy";
    public final static String openSourceLicenses = baseUrl_other + "open-source-licenses";

    // Service Type
    public final static String service_type_register = "registerUser";
    public final static String service_type_login = "login";
    public final static String service_type_forget_pass = "forgotpassword";
    public final static String service_type_get_categories = baseUrl + "getCategories";
    public final static String service_type_set_categories = baseUrl + "setCategories";
    public final static String service_type_getAppVisibility = baseUrl + "getAppVisibility";
    public final static String service_type_setAppVisibility = baseUrl + "setAppVisibility";
    public final static String service_type_getFriendsAppVisibility = baseUrl + "getFriendsAppVisibility";
    public final static String service_type_getChannels = baseUrl + "getChannels";
    public final static String service_type_getChannelDetail = baseUrl + "getChannelDetail";

    public final static String service_type_ = baseUrl + "getChannelDetail";
    public final static String service_type_joinChannel = baseUrl + "joinChannel";
    public final static String service_type_leaveChannel = baseUrl + "leavelChannel";
    public final static String service_type_getContactList = baseUrl + "getContactList";
    public final static String service_type_sendInvitation = baseUrl + "sendInvitation";
    public final static String service_type_channelStatistics = baseUrl + "getChannelStatistics";
    public final static String service_type_getUserChannelDetail = baseUrl + "getUserChannelDetail";
    public final static String service_type_createChannel = baseUrl + "createChannel/"; //multiplepart entity
    public final static String service_type_getChannelSettings = baseUrl + "getChannelSettings";
    public final static String service_type_setChannelSettings = baseUrl + "setChannelSettings";
    public final static String service_type_createPost = baseUrl + "createPost/"; //multiplepart entity
    public final static String service_type_deleteChannel = baseUrl + "deleteChannel";
    public final static String service_type_getFollowedChannels = baseUrl + "getFollowedChannels";
    public final static String service_type_getYourChannels = baseUrl + "getYourChannels";
    public final static String service_type_getExploreChannels = baseUrl + "getExploreChannels";
    public final static String service_type_acceptInvitation = baseUrl + "acceptRequests";
    //public final static String service_type_cat_channels = baseUrl+"getSelectedCategoriesChannels";
    public final static String service_type_getPostsByChannel = baseUrl + "getPostsByChannel";
    public final static String service_type_postDetail = baseUrl + "postDetail";
    public final static String service_type_createComment = baseUrl + "createComment";

    public final static String service_type_getInterestedApps = baseUrl + "getInterestedApps";
    public final static String service_type_getInterestedChannels = baseUrl + "getInterestedChannels";
    public final static String service_type_getFavorate = baseUrl + "getFavorate";
    public final static String service_type_getChannelsFeeds = baseUrl + "getChannelsFeeds";
    public final static String service_type_getFriendsFeeds = baseUrl + "getFriendsFeeds";
    public final static String service_type_getAllFeeds = baseUrl + "getAllFeeds";
    public final static String service_type_getYourCommunityApps = baseUrl + "getYourCommunityApps";
    public final static String service_type_getGappssCommunityApps = baseUrl + "getGAPPSSCommunityApps";

    public final static String service_type_getFriendsList = baseUrl + "getFriendsList";
    public final static String service_type_getDevicesDetails = baseUrl + "getDevicesDetails";
    public final static String service_type_getFriendAllChannel = baseUrl + "getFriendAllChannel";
    public final static String service_type_getFriendAllApps = baseUrl + "getFriendAllApps";
    public final static String service_type_getFriendsActivites = baseUrl + "getFriendsActivites";

    public final static String service_type_sendChannelInvitation = baseUrl + "sendChannelInvitation";
    public final static String service_type_acceptChannelInvitation = baseUrl + "acceptChannelInvitation";

    public final static String service_type_getShareApps = baseUrl + "getShareApps";
    public final static String service_type_getFriendsAllPosts = baseUrl + "getFriendsAllPosts";
    public final static String service_type_getRequests = baseUrl + "getRequests";
    public final static String service_type_getAllInbox = baseUrl + "getAllInbox";
    //API KEY
    //public final static String API_KEY_VALUE = "EFH127FL";

    //Device Type


    public final String and = "&";

    public final static int POST = 1;
    public final static int GET = 2;

    public final static String status = "status";
    public final static String results = "response";
    public final static int OK = 1;
    public final static int NOTOK = 0;

    public final int fromAddressToLatLng = 101;
}
