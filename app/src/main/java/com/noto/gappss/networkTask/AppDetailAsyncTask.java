package com.noto.gappss.networkTask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.HashMap;

public class AppDetailAsyncTask extends AsyncTask<String, Void, String> {
	
	public static String applicationReviews = "applicationReviews";
	public static String applicationDetails = "applicationDetails";
	HashMap<String, Object> params;
	String appId = "";
	String webServiceType = "";
	boolean isPost = false;
	Context context;
	private ProgressDialog mProgressDialog;
	private CallBackListener callBackListener = null;
	
	public CallBackListener getCallBackListener() 
	{
		return callBackListener;
	}

	public void setCallBackListener(CallBackListener callBackListener) 
	{
		this.callBackListener = callBackListener;
	}

	public AppDetailAsyncTask(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	public AppDetailAsyncTask(Context context, String appId,String webServiceType,
			Boolean isPost) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.appId = appId;
		this.webServiceType = webServiceType;
		this.isPost = this.isPost;

	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		// Create a progressdialog
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setMessage("Please wait...\nFeaching app detail");
		mProgressDialog.setIndeterminate(false);
		mProgressDialog.setCancelable(false);
		// Show progressdialog
		mProgressDialog.show();
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		String string = getAppDetail();
		//String string = multipleAppDetail(appId);
		return string;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		mProgressDialog.dismiss();
		if(callBackListener != null)
			callBackListener.callback(result);
		
	}

	public String getAppDetail() {
        String respons = "";
		try {//https://gplaystore.p.mashape.com/applicationReviews
			HttpResponse<JsonNode> response = Unirest
					.get("https://gplaystore.p.mashape.com/"+webServiceType+"?id="+appId+"&lang=en")
					.header("X-Mashape-Key","SdhiRToaSpmshlArJIBEMIs1idBop1Oxw6IjsnBLLRqk8ZBi0h")// SdhiRToaSpmshlArJIBEMIs1idBop1Oxw6IjsnBLLRqk8ZBi0h
					.header("Accept", "application/json").asJson();

			JsonNode jsonNode = response.getBody();
			Log.i("JSON NODE", "==" + jsonNode.toString());
			respons = jsonNode.toString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			respons = "";
		}
		return respons;
	}
	
	public interface CallBackListener
	{
		public void callback(Object object);
	}	
	
	public String multipleAppDetail(String apps){
		
		try {
			HttpResponse<JsonNode> response = Unirest.post("https://gplaystore.p.mashape.com/applicationDetails")
					.header("X-Mashape-Key", "SdhiRToaSpmshlArJIBEMIs1idBop1Oxw6IjsnBLLRqk8ZBi0h")
					.header("Content-Type", "application/json")
					.header("Accept", "application/json")
					.body("{\"lang\":\"it\",\"ids\":[\""+apps+"\"]}")
					.asJson();
			JsonNode jsonNode = response.getBody();
			Log.i("JSON NODE", "==" + jsonNode.toString());
			String respons = jsonNode.toString();
			return respons;
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
