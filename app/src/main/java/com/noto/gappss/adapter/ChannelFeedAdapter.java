package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.squareup.picasso.Picasso;

public class ChannelFeedAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	private BaseFragment fragment;
	private int width;
	private JSONArray arrlist;
	private int rememberPosition;
	private HolderPost holderPost;
	private HolderLeaveJoin holderLeaveJoin;

	public ChannelFeedAdapter() {
		// TODO Auto-generated constructor stub
	}

	public ChannelFeedAdapter(BaseFragment fragment,  JSONArray arrlist) {
		this.fragment = fragment;
		try {
			//this.arrlist = arrlist.optJSONObject(0).optJSONArray(Constants.list);
			this.arrlist = arrlist;
		} catch (Exception e) {			
			e.printStackTrace();
			this.arrlist = new JSONArray();
		}
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;
	}

	public void setList(JSONArray arrSearched) {
		this.arrlist = arrSearched;
	}

	public JSONArray getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.length();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return (arrlist.optJSONObject(position).optInt(Constants.type));
	}
	/*@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}*/

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		
		int key = getItemViewType(postion);
		
			
			switch (key) {
			case Constants.feedTypePost:
				holderPost = new HolderPost();
				view = inflater.inflate(R.layout.item_channel_feed_post, null);	
				holderPost.img = (ImageView) view.findViewById(R.id.img);
				holderPost.title = (TextView) view.findViewById(R.id.title);
				holderPost.description = (TextView) view.findViewById(R.id.description);
				holderPost.post_user = (TextView) view.findViewById(R.id.post_user);
				holderPost.message = (TextView) view.findViewById(R.id.message);
				view.setTag(holderPost);
				break;

			case Constants.feedTypeJoinChannel:
				holderLeaveJoin = new HolderLeaveJoin();
				view = inflater.inflate(R.layout.item_channel_feed_leave_join, null);
				holderLeaveJoin.user = (TextView) view.findViewById(R.id.user);
				holderLeaveJoin.message = (TextView) view.findViewById(R.id.message);
				view.setTag(holderLeaveJoin);
				break;
				
			case Constants.feedTypeLeaveChannel:
				holderLeaveJoin = new HolderLeaveJoin();
				view = inflater.inflate(R.layout.item_channel_feed_leave_join, null);
				holderLeaveJoin.user = (TextView) view.findViewById(R.id.user);
				holderLeaveJoin.message = (TextView) view.findViewById(R.id.message);
				view.setTag(holderLeaveJoin);
				break;
				
			}	
			
		
		
		
		JSONObject jsonObject = arrlist.optJSONObject(postion);
		switch (key) {
		case Constants.feedTypePost:
			
			String img = jsonObject.optString(Constants.postImage);
			String user = jsonObject.optString(Constants.USER_NAME);
			String title = jsonObject.optString(Constants.postTitle);
			String description = jsonObject.optString(Constants.postDescription);
			String msg = jsonObject.optString(Constants.p_message); 
			
			holderPost.post_user.setText(Html.fromHtml("@"+user));
			holderPost.title.setText(Html.fromHtml(title));
			holderPost.description.setText(Html.fromHtml(description));
			holderPost.message.setText(msg);
			
			if(img != null && !img.isEmpty())
			Picasso.with(fragment.getActivity()).load(img).placeholder(R.drawable.noimg).into(holderPost.img);
			
			
			break;
		case Constants.feedTypeJoinChannel :
			
			String user2 = jsonObject.optString(Constants.USER_NAME);
			String message = jsonObject.optString(Constants.p_message);
		
			holderLeaveJoin.user.setText(Html.fromHtml("@"+user2));
			holderLeaveJoin.message.setText(Html.fromHtml(message));
			break;
        case Constants.feedTypeLeaveChannel:
			
			 user2 = jsonObject.optString(Constants.USER_NAME);
			 message = jsonObject.optString(Constants.p_message);
		
			holderLeaveJoin.user.setText(Html.fromHtml("@"+user2));
			holderLeaveJoin.message.setText(Html.fromHtml(message));
			break;
		}
			
		
		
		/*JSONObject jO = arrlist.optJSONObject(postion);
		String channelId = jO.optString(Constants.channelId);
		String title = jO.optString(Constants.title);
		String description = jO.optString(Constants.description);
		String image = jO.optString(Constants.image);
		String username = jO.optString(Constants.username);
		
		
		JSONArray array = jO.optJSONArray(Constants.Comment);
		

		holder.img.setTag(postion);
		holder.title.setTag(postion);
		holder.description.setTag(postion);
		holder.comment_one.setTag(postion);
		holder.comment_two.setTag(postion);
		holder.comment_rel.setTag(postion);
		holder.rel_top.setTag(postion);
*/		
		
		
		//view.setTag(R.id.text_channel, jO);
					
		return view;
	}

	static class HolderPost {
		RelativeLayout rel_top = null;
		ImageView img = null;
		TextView title = null;
		TextView description = null;
		TextView post_user = null;
		TextView message = null;
	}
	
	static class HolderLeaveJoin {
		TextView user = null;
		TextView message = null;
	}
	public void refreshList(){
		JSONArray array = new JSONArray();
		for (int i = 0; i < arrlist.length(); i++) {
			if (i != rememberPosition) {
				array.put(arrlist.optJSONObject(i));
			}
			
		}
		arrlist = array;
		notifyDataSetChanged();
	}
}
