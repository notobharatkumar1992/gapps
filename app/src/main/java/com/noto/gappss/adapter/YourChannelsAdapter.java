package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.ToastCustomClass;
import com.squareup.picasso.Picasso;

public class YourChannelsAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private BaseFragment fragment;

	private Holder holder;
	private int width;
	private JSONArray arrlist;
	private int rememberPosition;

	public YourChannelsAdapter() {
		// TODO Auto-generated constructor stub
	}

	public YourChannelsAdapter(BaseFragment fragment,  JSONArray arrlist) {
		this.fragment = fragment;
		try {
			//this.arrlist = arrlist.optJSONObject(0).optJSONArray(Constants.list);
			this.arrlist = arrlist;
		} catch (Exception e) {			
			e.printStackTrace();
			this.arrlist = new JSONArray();
		}
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;
	}

	public void setList(JSONArray arrSearched) {
		this.arrlist = arrSearched;
	}

	public JSONArray getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.length();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_your_channel, null);
			holder.img_logo = (ImageView) view.findViewById(R.id.img_logo);
			holder.text_title = (TextView) view.findViewById(R.id.text_title);
			holder.btn_sub_unsub = (TextView) view.findViewById(R.id.btn_sub_unsub);
			
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

	/*	AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, width+20);
		view.setLayoutParams(params);*/
		
		JSONObject jO = arrlist.optJSONObject(postion);
		String channelId = jO.optString(Constants.channelId);
		String channelName = jO.optString(Constants.channelName);
		String channelDescription = jO.optString(Constants.channelDescription);
		String channelType = jO.optString(Constants.channelType);
		String channelMode = jO.optString(Constants.channelMode);
		String channelImage = jO.optString(Constants.channelImage);

		holder.img_logo.setTag(postion);
		holder.text_title.setTag(postion);
		holder.btn_sub_unsub.setTag(postion);
		
		holder.text_title.setText(Html.fromHtml(channelName));
		//holder.btn_sub_unsub.setText(Html.fromHtml(channelDescription));
		
		view.setTag(R.id.text_channel, jO);
		if(channelImage != null && !channelImage.isEmpty())
			Picasso.with(fragment.getActivity()).load(channelImage).placeholder(R.drawable.noimg).into(holder.img_logo);
		else
			holder.img_logo.setImageResource(R.drawable.create_channel);
		
		holder.btn_sub_unsub.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//JSONObject jsonObject = (JSONObject) v.getTag(R.id.text_channel);
				int pos = (Integer) v.getTag();
				rememberPosition = pos;
				String channelId = arrlist.optJSONObject(pos).optString(Constants.channelId);
				ToastCustomClass.showToast(fragment.getActivity(), channelId+"");
				ApiManager.getInstance().deleteChannel((BaseFragmentActivity)fragment.getActivity(), channelId);
				
				
			}
		});
		
		return view;
	}

	static class Holder {
		public TextView btn_sub_unsub;
		ImageView img_logo = null;
		TextView text_title = null;
	}
	public void refreshList(){
		JSONArray array = new JSONArray();
		for (int i = 0; i < arrlist.length(); i++) {
			if (i != rememberPosition) {
				array.put(arrlist.optJSONObject(i));
			}
			
		}
		arrlist = array;
		notifyDataSetChanged();
	}
}
