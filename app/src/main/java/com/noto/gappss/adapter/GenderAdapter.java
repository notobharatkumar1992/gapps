package com.noto.gappss.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.noto.gappss.R;
import com.noto.gappss.beanClasses.Gender;

public class GenderAdapter extends BaseAdapter{

	ArrayList<Gender> arrayList;
	private LayoutInflater inflater;
	Context context;
	public GenderAdapter(Context context, ArrayList<Gender> objects) {
		
		// TODO Auto-generated constructor stub
		arrayList = objects;
		this.context = context;
		inflater = LayoutInflater.from(context);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
			convertView = inflater.inflate(R.layout.item_gender, null);
			
		
			TextView gender = (TextView) convertView.findViewById(R.id.gender);
			gender.setText(arrayList.get(position).getGender());
	
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
