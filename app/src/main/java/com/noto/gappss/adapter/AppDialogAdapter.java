package com.noto.gappss.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.beanClasses.ApplicationBean;

public class AppDialogAdapter extends BaseAdapter implements Filterable{
	
	private LayoutInflater inflater;
	private Holder holder;
	private ArrayList<ApplicationBean> arrSearched;
	private ArrayList<ApplicationBean> orig;
	private BaseFragmentActivity fragment;
	private int width;
	private int data = 0; // 0 for user app's and 1 for friend app's 

	public AppDialogAdapter() {
	}

	public AppDialogAdapter(BaseFragmentActivity fragment, ArrayList<ApplicationBean> arrlist) {
		this.fragment = fragment;
		this.arrSearched = arrlist;

		inflater = LayoutInflater.from(fragment);
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 20;
		//width = ((MyApplication.getApplication().getHeightPixel() / 3)/2);
		//Log.i("LaVoixAdapter", "==" + arrSearched.size());
	}
	public void setAdapterDataState(int i){
		data = i;
	}

	public void setList(ArrayList<ApplicationBean> arrSearched) {
		this.arrSearched = arrSearched;
		notifyDataSetChanged();
	}

	public ArrayList<ApplicationBean> getList() {
		return this.arrSearched;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrSearched.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_app_dialog_grid, null);
			holder.img_logo = (ImageView) view.findViewById(R.id.img_logo);
			holder.app_name = (TextView) view.findViewById(R.id.app_name);
			holder.chk = (CheckBox) view.findViewById(R.id.chk);

			view.setTag(holder);
			AbsListView.LayoutParams params = new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, width);
			view.setLayoutParams(params);
		} else {
			holder = (Holder) view.getTag();
		}
		
		
		ApplicationBean applicationBean = arrSearched.get(position);
		
		holder.app_name.setText(applicationBean.getTitle()+"");
		view.setTag(R.id.title,applicationBean);
		holder.img_logo.setTag(position);
		holder.app_name.setTag(position);
		holder.chk.setTag(position);
		holder.chk.setChecked(arrSearched.get(position).isChecked());
		//holder.app_name.setText(" ");
		
		
		holder.img_logo.setBackgroundDrawable(applicationBean.getApplogo(fragment));
		
		holder.chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {				
				arrSearched.get((Integer)buttonView.getTag()).setChecked(isChecked);
				buttonView.setButtonDrawable(R.drawable.check_box_selector);
				//holder.checkBox1.setButtonDrawable(R.drawable.check_box_selector);
			}
		});
		return view;
	}

	static class Holder {
		public CheckBox chk;
		//public RelativeLayout img_logo = null;
		ImageView img_logo = null;
		TextView app_name = null;
	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected void publishResults(CharSequence constraint,	FilterResults results) {
				arrSearched = (ArrayList<ApplicationBean>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				final FilterResults oReturn = new FilterResults();
				final ArrayList<ApplicationBean> results = new ArrayList<ApplicationBean>();
				if (orig == null)
					orig = arrSearched;
				if (constraint != null) {
					if (orig != null && orig.size() > 0) {
						for (final ApplicationBean g : orig) {
							if (g.getTitle().toLowerCase().contains(constraint.toString()))
								results.add(g);
						}
					}
					oReturn.values = results;
				}
				return oReturn;
			}
		};
	}
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}	

}
