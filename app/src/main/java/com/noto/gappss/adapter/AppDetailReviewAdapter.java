package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.adapter.ChartAppAdapter.Holder;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.utilities.ToastCustomClass;
import com.squareup.picasso.Picasso;

import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AppDetailReviewAdapter extends BaseAdapter{
	
	private LayoutInflater inflater;
	private BaseFragment fragment;

	private Holder holder;
	private int width;
	private JSONArray arrlist;
	private int rememberPosition;

	public AppDetailReviewAdapter() {
		// TODO Auto-generated constructor stub
	}

	public AppDetailReviewAdapter(BaseFragment fragment,  JSONArray arrlist) {
		this.fragment = fragment;
		try {
			//this.arrlist = arrlist.optJSONObject(0).optJSONArray(Constants.list);
			this.arrlist = arrlist;
		} catch (Exception e) {			
			e.printStackTrace();
			this.arrlist = new JSONArray();
		}
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;
	}

	public void setList(JSONArray arrSearched) {
		this.arrlist = arrSearched;
	}

	public JSONArray getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.length();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_app_review, null);
			holder.img = (ImageView) view.findViewById(R.id.img);
			holder.title = (TextView) view.findViewById(R.id.title);
			holder.description = (TextView) view.findViewById(R.id.description);
			holder.author = (TextView) view.findViewById(R.id.authors);
			holder.date = (TextView) view.findViewById(R.id.date);
			holder.ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
		
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

	/*	AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, width+20);
		view.setLayoutParams(params);*/
		
		JSONObject jO = arrlist.optJSONObject(postion);
		
		String ratingBar = jO.optString(Constants.rating);
		String title = jO.optString(Constants.title);
		String description = jO.optString(Constants.body);
		String date = jO.optString(Constants.date);
		
		JSONObject authorJsonObj = jO.optJSONObject(Constants.author);
		String image = authorJsonObj.optString(Constants.image);
		String author = authorJsonObj.optString(Constants.name);
		
		
		//JSONArray array = jO.optJSONArray(Constants.Comment);
		
		holder.img.setTag(postion);
		holder.title.setTag(postion);
		holder.description.setTag(postion);
		holder.author.setTag(postion);
		holder.date.setTag(postion);
		holder.ratingBar.setTag(postion);
		
		holder.title.setText(Html.fromHtml(title));
		holder.description.setText(Html.fromHtml(description));
		holder.author.setText(Html.fromHtml(author));
		holder.date.setText(Html.fromHtml(date));
		holder.ratingBar.setRating(Float.parseFloat(ratingBar));
		
		view.setTag(R.id.text_channel, jO);
		if(image != null && !image.isEmpty())
		{
			//holder.img.setVisibility(View.VISIBLE);		
			Picasso.with(fragment.getActivity()).load(image).placeholder(R.drawable.noimg).into(holder.img);
		}		
		


		
		return view;
	}

	static class Holder {
		RelativeLayout comment_rel = null;
		ImageView img = null;
		TextView title = null;
		TextView description = null;
		TextView author = null;
		TextView date = null;
		RatingBar ratingBar = null;
		
	
	}
	public void refreshList(JSONArray array){
		arrlist = array;
		notifyDataSetChanged();
	}


}
