package com.noto.gappss.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.adapter.AllFeedsAdapter.HolderComment;
import com.noto.gappss.adapter.AllFeedsAdapter.HolderLeaveJoin;
import com.noto.gappss.adapter.AllFeedsAdapter.HolderPost;
import com.noto.gappss.adapter.ChartAppAdapter.Holder;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.networkTask.URLsClass;
import com.squareup.picasso.Picasso;

import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class InboxRequestAdapter extends BaseAdapter{
	
	private LayoutInflater inflater;
	private BaseFragment fragment;

	private Holder holder;
	private int width;
	private  ArrayList<Contact> arrlist;
	private int rememberPosition;

	public InboxRequestAdapter() {
		// TODO Auto-generated constructor stub
	}

	public InboxRequestAdapter(BaseFragment fragment,  ArrayList<Contact> arrlist) {
		this.fragment = fragment;
		try {
			//this.arrlist = arrlist.optJSONObject(0).optJSONArray(Constants.list);
			this.arrlist = arrlist;
		} catch (Exception e) {			
			e.printStackTrace();
			this.arrlist = new ArrayList<Contact>();
		}
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;
	}

	public void setList( ArrayList<Contact> arrSearched) {
		this.arrlist = arrSearched;
		notifyDataSetChanged();
	}

	public  ArrayList<Contact> getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_contact, null);
			holder.invite_title = (TextView) view.findViewById(R.id.invite_title);
			holder.tv_status = (TextView) view.findViewById(R.id.tv_status);
			holder.tv_deny = (TextView) view.findViewById(R.id.tv_deny);
			holder.invite_no = (TextView) view.findViewById(R.id.invite_no);
			holder.img = (ImageView) view.findViewById(R.id.img);
			holder.message = (TextView) view.findViewById(R.id.message);
		
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}
        Contact contact = arrlist.get(postion);
        holder.tv_deny.setTag(postion);
        holder.tv_status.setTag(postion);
        
        holder.invite_title.setText(contact.getName());
        holder.invite_no.setText(contact.getMobileNo());
	
		if(contact.getImg() != null && !contact.getImg().isEmpty())
		{	
			Picasso.with(fragment.getActivity()).load(contact.getImg()).placeholder(R.drawable.noimg).into(holder.img);
		}

		holder.tv_status.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Integer pos = (Integer) v.getTag();
				Contact contact = arrlist.get(pos);
				ApiManager.getInstance().acceptDenyInvitation(fragment, contact.getId(), Constants.accept, Constants.acceptInvitationInbox); 
			}
		});

		holder.tv_deny.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Integer pos = (Integer) v.getTag();
				Contact contact = arrlist.get(pos);
				ApiManager.getInstance().acceptDenyInvitation(fragment, contact.getId(), Constants.deny, Constants.denyInvitationInbox);
			}
		});
		return view;
	}

	static class Holder {
		public TextView tv_deny;
		public TextView tv_status;
		ImageView img = null;
		TextView invite_title = null;
		TextView invite_no = null;
		TextView message = null;
		CheckBox invite_checkbox = null;
	
	}
	public void refreshList(){
		
		notifyDataSetChanged();
	}
}
