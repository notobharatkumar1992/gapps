package com.noto.gappss.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.beanClasses.Contact;

public class InviteAdapter extends BaseAdapter  implements Filterable{

	private LayoutInflater inflater;
	private BaseFragment fragment;
	private ArrayList<Contact> arrlist;
	private ArrayList<Contact> orig;
	private int width = 0;
	private Holder holder;

	public InviteAdapter() {
		// TODO Auto-generated constructor stub
	}

	public InviteAdapter(BaseFragment fragment,
			ArrayList<Contact> array_list) {

		this.fragment = fragment;
		this.arrlist = array_list;
		inflater = inflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 40;

	}

	public void setList(ArrayList<Contact> arrlist) {

		this.arrlist = arrlist;
	}

	public ArrayList<Contact> getList() {

		return this.arrlist;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrlist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_invite, null);
			
			holder.invite_title = (TextView) view
					.findViewById(R.id.invite_title);
			holder.invite_no = (TextView) view
					.findViewById(R.id.invite_no);
			holder.invite_checkbox = (CheckBox) view
					.findViewById(R.id.invite_checkbox);
			holder.message = (TextView) view.findViewById(R.id.message);

			view.setTag(holder);
		} else {

			holder = (Holder) view.getTag();

		}

		holder.invite_title.setTag(position);
		holder.invite_no.setTag(position);
		holder.invite_checkbox.setTag(position);
		holder.message.setTag(position);

		
		holder.invite_title.setText(arrlist.get(position).getName());
		holder.invite_no.setText(arrlist.get(position).getMobileNo());
		holder.invite_checkbox.setChecked(arrlist.get(position).isChecked());
		
		holder.invite_checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				CheckBox checkBox = (CheckBox) buttonView;
				arrlist.get((Integer)buttonView.getTag()).setChecked(isChecked);
				//ToastCustomClass.showToast(fragment.getActivity(), arrlist.get((Integer)buttonView.getTag()).isChecked()+"==");			
				}
		});
		
		

		return view;
	}

	static class Holder {
		TextView invite_title = null;
		TextView invite_no = null;
		TextView message = null;
		CheckBox invite_checkbox = null;

	}

	

	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				// TODO Auto-generated method stub
				arrlist = (ArrayList<Contact>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				final FilterResults oReturn = new FilterResults();
				final ArrayList<Contact> results = new ArrayList<Contact>();
				if (orig == null)
					orig = arrlist;
				if (constraint != null) {
					if (orig != null && orig.size() > 0) {
						for (final Contact g : orig) {
							if (g.getName().toLowerCase().contains(constraint.toString()))
								results.add(g);
						}
					}
					oReturn.values = results;
				}
				return oReturn;
			}
		};
	}
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}
	
	public void notifyList(ArrayList<Contact> arrayList){
		
		for (int i = 0; i < arrayList.size(); i++) {
			
			if (arrayList.get(i).getStatus() == 1) {
				for (int j = 0; j < arrlist.size(); j++) {
					if (arrayList.get(i).getId().equals(arrlist.get(j).getId())) {
						//Log.i("asdfdasfasdf", arrlist.get(j).toString());
						arrlist.remove(j);
					}
				}
			}
		}
		notifyDataSetChanged();
		
	}
	

}
