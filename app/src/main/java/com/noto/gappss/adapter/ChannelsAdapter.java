package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.squareup.picasso.Picasso;

public class ChannelsAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private BaseFragment fragment;

	private Holder holder;
	private int width;
	private JSONArray arrlist;

	public ChannelsAdapter() {
		// TODO Auto-generated constructor stub
	}

	public ChannelsAdapter(BaseFragment fragment,  JSONArray arrlist) {
		this.fragment = fragment;
		this.arrlist = arrlist;
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;

	}

	public void setList(JSONArray arrSearched) {
		this.arrlist = arrSearched;
	}

	public JSONArray getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		try {
			return arrlist.length();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_grid, null);
			holder.img_logo = (ImageView) view.findViewById(R.id.img_logo);
			holder.text_title = (TextView) view.findViewById(R.id.text_title);
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}
		
		holder.text_title.setVisibility(View.VISIBLE);

		AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, width+20);
		view.setLayoutParams(params);

		/*JSONObject jObj = explorer.optJSONObject(i);
		 String catId = jObj.optString(Constants.catId);
		 String catName = jObj.optString(Constants.catName);
		 JSONArray list = jObj.optJSONArray(Constants.list);
		 if(list != null && list.length() > 0){
			 for(int j=0;j<list.length();j++){
				 JSONObject jO = list.optJSONObject(j);
				 String channelId = jO.optString(Constants.channelId);
				 String channelName = jO.optString(Constants.channelName);
				 String channelDescription = jO.optString(Constants.channelDescription);
				 String channelType = jO.optString(Constants.channelType);
				 String channelMode = jO.optString(Constants.channelMode);
				 String channelImage = jO.optString(Constants.channelImage);
			 }
		 }	*/

		
		JSONObject jO = arrlist.optJSONObject(postion);
		String channelId = jO.optString(Constants.channelId);
		String channelName = jO.optString(Constants.channelName);
		String channelDescription = jO.optString(Constants.channelDescription);
		String channelType = jO.optString(Constants.channelType);
		String channelMode = jO.optString(Constants.channelMode);
		String channelImage = jO.optString(Constants.channelImage);

		holder.img_logo.setTag(postion);
		holder.text_title.setTag(postion);
		view.setTag(R.id.text_channel, jO);
		if(channelImage != null && !channelImage.isEmpty())
			Picasso.with(fragment.getActivity()).load(channelImage).placeholder(R.drawable.noimg).into(holder.img_logo);
		else
			holder.img_logo.setImageResource(R.drawable.noimg);
		holder.text_title.setText(channelName);
		return view;
	}

	static class Holder {
		ImageView img_logo = null;
		TextView text_title = null;
	}
}
