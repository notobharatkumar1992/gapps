package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.adapter.InboxSharedAppsAdapter.Holder;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.URLsClass;
import com.squareup.picasso.Picasso;

import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class InboxPostAdapter extends BaseAdapter{
		
		private LayoutInflater inflater;
		private BaseFragment fragment;

		private Holder holderPost;
		private int width;
		private JSONArray arrlist;
		private int rememberPosition;

		public InboxPostAdapter() {
			// TODO Auto-generated constructor stub
		}

		public InboxPostAdapter(BaseFragment fragment,  JSONArray arrlist) {
			this.fragment = fragment;
			try {
				//this.arrlist = arrlist.optJSONObject(0).optJSONArray(Constants.list);
				this.arrlist = arrlist;
			} catch (Exception e) {			
				e.printStackTrace();
				this.arrlist = new JSONArray();
			}
			inflater = LayoutInflater.from(fragment.getActivity());
			width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;
		}

		public void setList(JSONArray arrSearched) {
			this.arrlist = arrSearched;
			notifyDataSetChanged();
		}

		public JSONArray getList() {
			return this.arrlist;
		}

		@Override
		public int getCount() {
			Log.i("LENGTH", "=="+arrlist.length());
			return arrlist.length();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
		@Override
		public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
			Log.i("getView", "=="+position);
			holderPost = new Holder();
			if (view == null) {
				view = inflater.inflate(R.layout.item_channel_feed_post, null);	
				holderPost.img = (ImageView) view.findViewById(R.id.img);
				holderPost.title = (TextView) view.findViewById(R.id.title);
				holderPost.description = (TextView) view.findViewById(R.id.description);
				holderPost.post_user = (TextView) view.findViewById(R.id.post_user);
				holderPost.message = (TextView) view.findViewById(R.id.message);
			
				view.setTag(holderPost);
			} else {
				holderPost = (Holder) view.getTag();
			}

		
			JSONObject jsonObject = arrlist.optJSONObject(position);
			String img = jsonObject.optString(Constants.postImage);
			String user = jsonObject.optString(Constants.USER_NAME);
			String title = jsonObject.optString(Constants.postTitle);
			String description = jsonObject.optString(Constants.postDescription);
			String msg = jsonObject.optString(Constants.p_message); 
			
			holderPost.post_user.setText(Html.fromHtml("@"+user));
			holderPost.title.setText(Html.fromHtml(title));
			holderPost.description.setText(Html.fromHtml(description));
			holderPost.message.setText(msg);
			
			if(img != null && !img.isEmpty())
				Picasso.with(fragment.getActivity()).load(img).placeholder(R.drawable.noimg).into(holderPost.img);
			

			return view;
		}



		static class Holder {
			RelativeLayout rel_top = null;
			ImageView img = null;
			TextView title = null;
			TextView description = null;
			TextView post_user = null;
			TextView message = null;
		
		}
		public void refreshList(){
			JSONArray array = new JSONArray();
			for (int i = 0; i < arrlist.length(); i++) {
				if (i != rememberPosition) {
					array.put(arrlist.optJSONObject(i));
				}
				
			}
			arrlist = array;
			notifyDataSetChanged();
		}

		
	}
