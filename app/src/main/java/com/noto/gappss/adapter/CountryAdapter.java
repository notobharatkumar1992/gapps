package com.noto.gappss.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.beanClasses.Country;

public class CountryAdapter extends BaseAdapter implements Filterable{

	private LayoutInflater inflater;
	private BaseFragment fragment;
	private ArrayList<Country> arrlist;
	private ArrayList<Country> orig;
	private Holder holder;
	private int width;

	public CountryAdapter() {
		// TODO Auto-generated constructor stub
	}

	public CountryAdapter(BaseFragment fragment, ArrayList<Country> arrlist) {
		this.fragment = fragment;
		this.arrlist = arrlist;
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 40;
	}

	public void setList(ArrayList<Country> arrSearched) {
		this.arrlist = arrSearched;
	}

	public ArrayList<Country> getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_all_category, null);
			holder.img_logo = (ImageView) view.findViewById(R.id.img_logo);
			holder.text_title = (TextView) view.findViewById(R.id.text_title);
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

		holder.img_logo.setTag(postion);
		holder.text_title.setTag(postion);
		view.setTag(R.id.text_title,arrlist.get(postion).getDialCode());
		view.setTag(R.id.img_logo,arrlist.get(postion).getFlag());
		holder.text_title.setText(arrlist.get(postion).getName());
		//if(0 != arrlist.get(postion).getFlag())
		holder.img_logo.setImageResource(arrlist.get(postion).getFlag());

		return view;
	}

	static class Holder {
		ImageView img_logo = null;
		TextView text_title = null;
	}
	
	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected void publishResults(CharSequence constraint,	FilterResults results) {
				arrlist = (ArrayList<Country>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				final FilterResults oReturn = new FilterResults();
				final ArrayList<Country> results = new ArrayList<Country>();
				if (orig == null)
					orig = arrlist;
				if (constraint != null) {
					if (orig != null && orig.size() > 0) {
						for (final Country g : orig) {
							if (g.getName().toLowerCase().contains(constraint.toString()))
								results.add(g);
						}
					}
					oReturn.values = results;
				}
				return oReturn;
			}
		};
	}
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}
}
