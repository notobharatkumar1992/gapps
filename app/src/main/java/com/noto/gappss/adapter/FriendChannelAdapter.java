package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.squareup.picasso.Picasso;

public class FriendChannelAdapter extends BaseAdapter{


	private LayoutInflater inflater;
	private BaseFragment fragment;

	private Holder holder;
	private int width;
	private JSONArray arrlist;
	private int rememberPosition;

	public FriendChannelAdapter() {
		// TODO Auto-generated constructor stub
	}

	public FriendChannelAdapter(BaseFragment fragment,  JSONArray arrlist) {
		this.fragment = fragment;
		try {
			this.arrlist = arrlist;//arrlist.optJSONObject(0).optJSONArray(Constants.list);
		} catch (Exception e) {			
			e.printStackTrace();
			this.arrlist = new JSONArray();
		}
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;

	}

	public void setList(JSONArray arrSearched) {
		this.arrlist = arrSearched;
	}

	public JSONArray getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.length();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_friend_channel, null);
			holder.img_logo = (ImageView) view.findViewById(R.id.img_logo);
			holder.text_title = (TextView) view.findViewById(R.id.text_title);
			holder.description = (TextView) view.findViewById(R.id.description);

			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

		/*	AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, width+20);
		view.setLayoutParams(params);*/

		JSONObject jO = arrlist.optJSONObject(postion);
		String channelId = jO.optString(Constants.channelId);
		String channelName = jO.optString(Constants.channelName);
		String channelDescription = jO.optString(Constants.channelDescription);
		String channelType = jO.optString(Constants.channelType);
		String channelMode = jO.optString(Constants.channelMode);
		String channelImage = jO.optString(Constants.channelImage);

		holder.img_logo.setTag(postion);
		holder.text_title.setTag(postion);
		holder.description.setTag(postion);
		
		holder.text_title.setText(Html.fromHtml(channelName));
		holder.description.setText(Html.fromHtml(channelDescription));

		view.setTag(R.id.text_channel, jO);
		if(channelImage != null && !channelImage.isEmpty())
			Picasso.with(fragment.getActivity()).load(channelImage).placeholder(R.drawable.noimg).into(holder.img_logo);
		
		return view;
	}

	static class Holder {
		public TextView btn_sub_unsub;
		ImageView img_logo = null;
		TextView text_title = null;
		TextView description = null;
	}
	
	public void refreshList(){
		JSONArray array = new JSONArray();
		for (int i = 0; i < arrlist.length(); i++) {
			if (i != rememberPosition) {
				array.put(arrlist.optJSONObject(i));
			}
			
		}
		arrlist = array;
		notifyDataSetChanged();
	}
}
