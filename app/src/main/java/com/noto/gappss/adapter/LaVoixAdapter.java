package com.noto.gappss.adapter;

import java.util.ArrayList;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.beanClasses.LaVoix;



	public class LaVoixAdapter extends BaseAdapter {
		private LayoutInflater inflator;
		private Holder holder;
		private ArrayList<LaVoix> arrSearched;
		private BaseFragment fragment;
		private int width;

		public LaVoixAdapter(BaseFragment fragment, ArrayList<LaVoix> arrSearched) {
			this.fragment = fragment;
			this.arrSearched = arrSearched;
			inflator = LayoutInflater.from(fragment.getActivity());
			width = (MyApplication.getApplication().getWidthPixel() / 3) - 40;
			//Log.i("LaVoixAdapter", "==" + arrSearched.size());
		}

		public void setList(ArrayList<LaVoix> arrSearched) {
			this.arrSearched = arrSearched;
		}

		public ArrayList<LaVoix> getList() {
			return this.arrSearched;
		}

		public void addRoute(LaVoix route) {
			this.arrSearched.add(route);
		}

		public void removeRoute(int pos) {
			this.arrSearched.remove(pos);
		}

		public void removeAnimal(int pos) {
			this.arrSearched.remove(pos);
		}

		@Override
		public int getCount() {
			return arrSearched.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			int j = 0;
			//Log.i("getView", "==" + j++);
			if (convertView == null) {
				holder = new Holder();
				/*convertView = (View) inflator.inflate(R.layout.item_frag_lavoix, null);
				holder.la_route = (RelativeLayout) convertView.findViewById(R.id.la_route);
				holder.la_title = (TextView) convertView.findViewById(R.id.la_title);
				holder.la_sub_title = (TextView) convertView.findViewById(R.id.la_sub_title);
				holder.la_img = (ImageView) convertView.findViewById(R.id.la_img);*/
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}


			//holder.la_route.setTag(position);
			holder.la_title.setTag(position);
			holder.la_sub_title.setTag(position);
			holder.la_img.setTag(position);
           /* if (arrSearched.get(position).getImgUrl().length() > 0) {
            	Picasso.with(fragment.getActivity()).load(arrSearched.get(position).getImgUrl()).placeholder(fragment.getActivity().getResources().getDrawable(R.drawable.no_image)).into(holder.la_img);//fragment.getActivity().getResources().getDrawable(R.drawable.icon_calender_select)
			}*/
			
	       // Picasso.with(fragment.getActivity()).load(arrSearched.get(position).getImgUrl()).placeholder(fragment.getActivity().getResources().getDrawable(R.drawable.no_image)).into(holder.la_img);//fragment.getActivity().getResources().getDrawable(R.drawable.icon_calender_select)
			holder.la_title.setText(arrSearched.get(position).getTitle());
			holder.la_sub_title.setText(arrSearched.get(position).getDescription());		
			
			return convertView;
		}

		static class Holder {
			public RelativeLayout la_route = null;
			ImageView la_img = null;
			TextView la_title = null;
			TextView la_sub_title = null;
			

		}

		public void setAddress(int pos, LaVoix route) {
			arrSearched.set(pos, route);
			notifyDataSetChanged();
		}

		public void startActivityToGetAddress(Class className, View v) {
			/*Integer pos = (Integer) v.getTag();
			Bundle bundle = new Bundle();
			bundle.putInt(com.dimadimaraja.noto.baseClasses.Constants.position, pos);
			bundle.putSerializable(com.dimadimaraja.noto.baseClasses.Constants.position.object, arrSearched.get(pos));

			Intent intent = new Intent(fragment.getActivity(), className);
			intent.putExtra(com.dimadimaraja.noto.baseClasses.Constants.bundleArg, bundle);

			fragment.startActivityForResult(intent, Constants.LOCATION);*/
		}
	}
