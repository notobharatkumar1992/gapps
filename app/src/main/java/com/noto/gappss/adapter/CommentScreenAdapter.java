package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.Html;
import android.util.EventLogTags.Description;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.utilities.ToastCustomClass;
import com.squareup.picasso.Picasso;

public class CommentScreenAdapter extends BaseAdapter{


	private LayoutInflater inflater;
	private BaseFragment fragment;

	private Holder holder;
	private int width;
	private JSONArray arrlist;
	private int rememberPosition;

	public CommentScreenAdapter() {
		// TODO Auto-generated constructor stub
	}

	public CommentScreenAdapter(BaseFragment fragment,  JSONArray arrlist) {
		this.fragment = fragment;
		try {
			//this.arrlist = arrlist.optJSONObject(0).optJSONArray(Constants.list);
			this.arrlist = arrlist;
		} catch (Exception e) {			
			e.printStackTrace();
			this.arrlist = new JSONArray();
		}
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;
	}

	public void setList(JSONArray arrSearched) {
		this.arrlist = arrSearched;
	}

	public JSONArray getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.length();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}


	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_comment, null);
			
			holder.description = (TextView) view.findViewById(R.id.description);
			holder.username = (TextView) view.findViewById(R.id.username);
			
			holder.username_second = (TextView) view.findViewById(R.id.username_second);
			holder.comment_second = (TextView) view.findViewById(R.id.comment_second);
			holder.username_third = (TextView) view.findViewById(R.id.username_third);
			holder.comment_third = (TextView) view.findViewById(R.id.comment_third);
			holder.rel_container = (RelativeLayout) view.findViewById(R.id.rel_container);
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

	/*	AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, width+20);
		view.setLayoutParams(params);*/
		
		JSONObject jO = arrlist.optJSONObject(postion);
		String channelId = jO.optString(Constants.channelId);
		String mainComment = jO.optString(Constants.main_comment);
		String username = jO.optString(Constants.username);
		
		JSONArray array = jO.optJSONArray(Constants.childComments);
		Log.i("adf", array.length()+"=="+postion);
		
		holder.description.setTag(postion);
		holder.username.setTag(postion);
	    holder.username_second.setTag(postion);
	    holder.comment_second.setTag(postion);
	    holder.username_third.setTag(postion);
	    holder.comment_third.setTag(postion);
		
		holder.username.setText(Html.fromHtml("@"+username));
		holder.description.setText(Html.fromHtml(mainComment));
		
		//view.setTag(R.id.text_channel, jO);
			
	    if(array != null && array.length() > 0) {
	    	holder.rel_container.setVisibility(View.VISIBLE);
	    	holder.username_second.setText("@"+array.optJSONObject(0).optString(Constants.username));
			holder.comment_second.setText(Html.fromHtml(array.optJSONObject(0).optString(Constants.Comment)));
			if (array.length() > 1) {
			
		    	holder.username_third.setText("@"+array.optJSONObject(1).optString(Constants.username));
				holder.comment_third.setText(Html.fromHtml(array.optJSONObject(1).optString(Constants.Comment)));
				
			}
		
		}else {
			holder.rel_container.setVisibility(View.GONE);
		}
				
	/*	holder.rel_top.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//JSONObject jsonObject = (JSONObject) v.getTag(R.id.text_channel);
				int pos = (Integer) v.getTag();
				//rememberPosition = pos;
				//String channelId = arrlist.optJSONObject(pos).optString(Constants.channelId);
				ToastCustomClass.showToast(fragment.getActivity(), pos+"");
				//ApiManager.getInstance().deleteChannel((BaseFragmentActivity)fragment.getActivity(), channelId);
							
			}
		});*/
		
		return view;
	}

	static class Holder {
		RelativeLayout rel_container = null;
		TextView username = null;
		TextView description = null;
		
		TextView username_second = null;
		TextView comment_second = null;
		TextView username_third = null;
		TextView comment_third = null;
		
	}
	public void refreshList(JSONObject jsonObject){
		try {
			
			JSONArray array = new JSONArray();
			array.put(0, jsonObject);
			for (int i = 0; i < arrlist.length(); i++) {
				array.put((i+1), arrlist.optJSONObject(i));
			}
			arrlist = array;
			notifyDataSetChanged();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("TESTTTTTTTTTTTTT", "====="+e.getMessage());
		}
		/*JSONArray array = new JSONArray();
		for (int i = 0; i < arrlist.length(); i++) {
			if (i != rememberPosition) {
				array.put(arrlist.optJSONObject(i));
			}
			
		}
		arrlist = array;
		notifyDataSetChanged();*/
	}
}
