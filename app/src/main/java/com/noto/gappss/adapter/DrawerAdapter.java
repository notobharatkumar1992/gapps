package com.noto.gappss.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.beanClasses.Drawer;

public class DrawerAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private Context fragment;
	private ArrayList<Drawer> arrlist;
	private Holder holder;
	private int width;

	public DrawerAdapter() {
		// TODO Auto-generated constructor stub
	}

	public DrawerAdapter(Context context, ArrayList<Drawer> arrlist) {

		this.fragment = context;
		this.arrlist = arrlist;
		inflater = LayoutInflater.from(context);
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 40;

	}

	public void setList(ArrayList<Drawer> arrSearched) {
		this.arrlist = arrSearched;
	}

	public ArrayList<Drawer> getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_drawer, null);

			holder.text_title = (TextView) view.findViewById(R.id.text_title);
			holder.img = (ImageView) view.findViewById(R.id.img);
			holder.root = (RelativeLayout) view.findViewById(R.id.root);

			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}
		holder.text_title.setTag(postion);
		holder.img.setTag(postion);
		
		
		holder.text_title.setText(arrlist.get(postion).getTitle());
		
		
		if (arrlist.get(postion).isChecked()) {
			holder.text_title.setTextColor(arrlist.get(postion).getTextColorSelect());
			holder.img.setImageResource(arrlist.get(postion).getIconSelect());
			holder.root.setBackgroundColor(fragment.getResources().getColor(R.color.bottom_tab_background_dark));
			
		}else {
			holder.text_title.setTextColor(arrlist.get(postion).getTextColorUnselect());
			holder.img.setImageResource(arrlist.get(postion).getIconUnselect());
			holder.root.setBackgroundColor(fragment.getResources().getColor(R.color.bottom_tab_background));
		}
		view.setTag(R.id.img,arrlist.get(postion));

		return view;
	}

	static class Holder {
		RelativeLayout root = null;
		TextView text_title = null;
		ImageView img = null;

	}

}
