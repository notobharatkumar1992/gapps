package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.adapter.YourChannelsAdapter.Holder;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.ApiManager;
import com.noto.gappss.utilities.ToastCustomClass;
import com.squareup.picasso.Picasso;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PostListAdapter extends BaseAdapter{


	private LayoutInflater inflater;
	private BaseFragment fragment;

	private Holder holder;
	private int width;
	private JSONArray arrlist;
	private int rememberPosition;

	public PostListAdapter() {
		// TODO Auto-generated constructor stub
	}

	public PostListAdapter(BaseFragment fragment,  JSONArray arrlist) {
		this.fragment = fragment;
		try {
			//this.arrlist = arrlist.optJSONObject(0).optJSONArray(Constants.list);
			this.arrlist = arrlist;
		} catch (Exception e) {			
			e.printStackTrace();
			this.arrlist = new JSONArray();
		}
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;
	}

	public void setList(JSONArray arrSearched) {
		this.arrlist = arrSearched;
	}

	public JSONArray getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		try {
			return arrlist.length();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_post_list, null);
			holder.img = (ImageView) view.findViewById(R.id.img);
			holder.title = (TextView) view.findViewById(R.id.title);
			holder.description = (TextView) view.findViewById(R.id.description);
			holder.comment_one = (TextView) view.findViewById(R.id.comment_one);
			holder.comment_two = (TextView) view.findViewById(R.id.comment_two);
			holder.comment_rel = (RelativeLayout) view.findViewById(R.id.comment_rel);
			holder.rel_top = (RelativeLayout) view.findViewById(R.id.rel_top);
			
			holder.post_user = (TextView) view.findViewById(R.id.post_user);
			holder.comment_one_user = (TextView) view.findViewById(R.id.comment_one_user);
			holder.comment_two_user = (TextView) view.findViewById(R.id.comment_two_user);

			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

	/*	AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, width+20);
		view.setLayoutParams(params);*/
		
		JSONObject jO = arrlist.optJSONObject(postion);
		String channelId = jO.optString(Constants.channelId);
		String title = jO.optString(Constants.title);
		String description = jO.optString(Constants.description);
		String image = jO.optString(Constants.image);
		String username = jO.optString(Constants.username);
		
		
		JSONArray array = jO.optJSONArray(Constants.Comment);
		

		holder.img.setTag(postion);
		holder.title.setTag(postion);
		holder.description.setTag(postion);
		holder.comment_one.setTag(postion);
		holder.comment_two.setTag(postion);
		holder.comment_rel.setTag(postion);
		holder.rel_top.setTag(postion);
		
		holder.post_user.setTag(postion);
		holder.comment_one_user.setTag(postion);
		holder.comment_two_user.setTag(postion);
		
		holder.post_user.setText(Html.fromHtml("@"+username));
		holder.title.setText(Html.fromHtml(title));
		holder.description.setText(Html.fromHtml(description));
		
		view.setTag(R.id.text_channel, jO);
		if(image != null && !image.isEmpty())
		{
			//holder.img.setVisibility(View.VISIBLE);		
			Picasso.with(fragment.getActivity()).load(image).placeholder(R.drawable.noimg).into(holder.img);
		}		
	    if(array != null && array.length() > 0) {
	    	holder.comment_rel.setVisibility(View.VISIBLE);
	    	holder.comment_one_user.setText(Html.fromHtml("@"+array.optJSONObject(0).optString(Constants.username)));
			holder.comment_one.setText(Html.fromHtml(array.optJSONObject(0).optString(Constants.comment)));
			if (array.length() > 1) {
				holder.comment_two_user.setText(Html.fromHtml("@"+array.optJSONObject(1).optString(Constants.username)));
				holder.comment_two.setText(Html.fromHtml(array.optJSONObject(1).optString(Constants.comment)));	
			}
		}else {
			holder.comment_rel.setVisibility(View.GONE);
		}
				
		/*holder.rel_top.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//JSONObject jsonObject = (JSONObject) v.getTag(R.id.text_channel);
				int pos = (Integer) v.getTag();
				//rememberPosition = pos;
				//String channelId = arrlist.optJSONObject(pos).optString(Constants.channelId);
				ToastCustomClass.showToast(fragment.getActivity(), pos+"");
				//ApiManager.getInstance().deleteChannel((BaseFragmentActivity)fragment.getActivity(), channelId);
							
			}
		});*/
		
		return view;
	}

	static class Holder {
		RelativeLayout comment_rel = null;
		RelativeLayout rel_top = null;
		ImageView img = null;
		TextView title = null;
		TextView description = null;
		TextView comment_one = null;
		TextView comment_two = null;
		
		TextView post_user = null;
		TextView comment_one_user = null;
		TextView comment_two_user = null;
	}
	public void refreshList(){
		JSONArray array = new JSONArray();
		for (int i = 0; i < arrlist.length(); i++) {
			if (i != rememberPosition) {
				array.put(arrlist.optJSONObject(i));
			}
			
		}
		arrlist = array;
		notifyDataSetChanged();
	}
}
