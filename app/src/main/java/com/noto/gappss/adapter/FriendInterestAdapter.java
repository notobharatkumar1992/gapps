package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.squareup.picasso.Picasso;

public class FriendInterestAdapter extends BaseAdapter{


	private LayoutInflater inflater;
	private BaseFragment fragment;

	private Holder holder;
	private int width;
	private JSONArray arrlist;
	private int rememberPosition;

	public FriendInterestAdapter() {
		// TODO Auto-generated constructor stub
	}

	public FriendInterestAdapter(BaseFragment fragment,  JSONArray arrlist) {
		this.fragment = fragment;
		try {
			this.arrlist = arrlist;//arrlist.optJSONObject(0).optJSONArray(Constants.list);
		} catch (Exception e) {			
			e.printStackTrace();
			this.arrlist = new JSONArray();
		}
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;

	}

	public void setList(JSONArray arrSearched) {
		this.arrlist = arrSearched;
	}

	public JSONArray getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.length();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_friend_interest, null);
			holder.channelImg = (ImageView) view.findViewById(R.id.channelImg);
			holder.postImg = (ImageView) view.findViewById(R.id.postImg);
			holder.channelName = (TextView) view.findViewById(R.id.channelName);
			holder.followers = (TextView) view.findViewById(R.id.followers);
			holder.posts = (TextView) view.findViewById(R.id.posts);
			holder.joinNow = (TextView) view.findViewById(R.id.joinNow);
			holder.date = (TextView) view.findViewById(R.id.date);
			holder.postTitle = (TextView) view.findViewById(R.id.postTitle);
			

			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

/*
		JSONObject jO = arrlist.optJSONObject(postion);
		String channelId = jO.optString(Constants.channelId);
		String channelName = jO.optString(Constants.channelName);
		String channelDescription = jO.optString(Constants.channelDescription);
		String channelType = jO.optString(Constants.channelType);
		String channelMode = jO.optString(Constants.channelMode);
		String channelImage = jO.optString(Constants.channelImage);*/

		holder.channelImg.setTag(postion);
		holder.postImg.setTag(postion);
		holder.channelName.setTag(postion);
		holder.followers.setTag(postion);
		holder.posts.setTag(postion);
		holder.joinNow.setTag(postion);
		holder.date.setTag(postion);
		holder.postTitle.setTag(postion);
		
		
/*
		view.setTag(R.id.text_channel, jO);
		if(channelImage != null && !channelImage.isEmpty())
			Picasso.with(fragment.getActivity()).load(channelImage).placeholder(R.drawable.noimg).into(holder.channelImg);*/
		
		return view;
	}

	static class Holder {
		ImageView channelImg = null;
		ImageView postImg = null;
		TextView channelName = null;
		TextView followers = null;
		TextView posts = null;
		TextView joinNow = null;
		TextView date = null;
		TextView postTitle = null;
		
	}
	

}
