package com.noto.gappss.adapter;

import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.beanClasses.AppCategory;
import com.squareup.picasso.Picasso;


	public class PopularCatAdapter extends BaseAdapter {

		private LayoutInflater inflater;
		private BaseFragment fragment;

		private Holder holder;
		private int width;
		private ArrayList<AppCategory> arrlist;

		public PopularCatAdapter() {
			// TODO Auto-generated constructor stub
		}

		public PopularCatAdapter(BaseFragment fragment,  ArrayList<AppCategory> arrlist) {
			this.fragment = fragment;
			this.arrlist = arrlist;
			inflater = LayoutInflater.from(fragment.getActivity());
			width = ((MyApplication.getApplication().getHeightPixel() / 4)/2) - 5;

		}

		public void setList(ArrayList<AppCategory> arrSearched) {
			this.arrlist = arrSearched;
		}

		public ArrayList<AppCategory> getList() {
			return this.arrlist;
		}

		@Override
		public int getCount() {
			return arrlist.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int postion, View view, ViewGroup parent) {
			holder = new Holder();
			if (view == null) {
				view = inflater.inflate(R.layout.item_grid, null);
				holder.img_logo = (ImageView) view.findViewById(R.id.img_logo);
				holder.text_title = (TextView) view.findViewById(R.id.text_title);
				view.setTag(holder);
			} else {
				holder = (Holder) view.getTag();
			}

			AbsListView.LayoutParams params = new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, width);
			view.setLayoutParams(params);
			

			
			holder.img_logo.setTag(postion);
			holder.text_title.setTag(postion);
			//view.setTag(R.id.text_channel, jO);
			/*if(arrlist.get(postion).getCatLogo() != null && !arrlist.get(postion).getCatLogo().isEmpty())
				Picasso.with(fragment.getActivity()).load(arrlist.get(postion).getCatLogo()).placeholder(R.drawable.create_channel).into(holder.img_logo);
			else
				holder.img_logo.setImageResource(R.drawable.create_channel);
			holder.text_title.setText(arrlist.get(postion).getCatName());*/
			return view;
		}

		static class Holder {
			ImageView img_logo = null;
			TextView text_title = null;
		}
	}

