package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import android.database.DataSetObserver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.fragments.ChannelGridFragment;

public class CategoryPagerAdapter extends FragmentStatePagerAdapter{
	//private ArrayList<HashMap<String, Object>> sub_list;
	//private int count = 0;
	private FragmentActivity fmAct;
	private JSONArray allOpenChannels;
	private boolean isYourChannel;

	public CategoryPagerAdapter(FragmentActivity fmAct, FragmentManager fm, JSONArray allOpenChannels, boolean isYourChannel) {			
		super(fm);
		this.fmAct = fmAct;
		this.allOpenChannels = allOpenChannels;
		this.isYourChannel = isYourChannel;
		//this.sub_list = MyApplication.getApplication().getjCategories();
		//this.sub_list = (ArrayList<HashMap<String, Object>>) sub_list;
	}



	@Override
	public Fragment getItem(int pos) {
		Fragment fragment = ChannelGridFragment.newInstance((allOpenChannels.optJSONObject(pos).optJSONArray(Constants.list)), isYourChannel);//NewAppFragment.getInstance();		
		return fragment;
	}

	@Override
	public int getCount() {
		return allOpenChannels.length();
	}

	@Override
	public CharSequence getPageTitle(int position){
		JSONObject jObj = allOpenChannels.optJSONObject(position);
		if(jObj != null){
			String catName = jObj.optString(Constants.catName);;
			if(catName != null){
				return catName;
			}
		}

		return "";
	}

	public void clearList(){
		/*if(sub_list != null)
			sub_list.clear();*/
	}

	public void setList(JSONArray sub_list){
		
		this.allOpenChannels = null;
		this.allOpenChannels = sub_list;		
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		if (observer != null) {
			super.unregisterDataSetObserver(observer);
		}
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {	
		if(observer != null)
			super.registerDataSetObserver(observer);
	}
	
	public int getItemPosition(Object object){
	     return POSITION_NONE;
	}

	/*@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		//super.destroyItem(container, position, object);
		//container.removeViewAt(position);
	}*/
}
