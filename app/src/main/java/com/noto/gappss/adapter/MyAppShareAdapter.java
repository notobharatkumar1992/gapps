package com.noto.gappss.adapter;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.ApplicationBean;
import com.noto.gappss.fragments.CommunityFragment;

public class MyAppShareAdapter extends BaseAdapter implements Filterable{
	
	private LayoutInflater inflater;
	private Holder holder;
	private ArrayList<ApplicationBean> arrSearched;
	private ArrayList<ApplicationBean> orig;
	private BaseFragment fragment;
	private int width;
	private String type;

	public MyAppShareAdapter() {
	}

	public MyAppShareAdapter(BaseFragment fragment, ArrayList<ApplicationBean> arrlist,String type) {
		this.fragment = fragment;
		this.arrSearched = arrlist;
		this.type = type;

		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 40;
		//Log.i("LaVoixAdapter", "==" + arrSearched.size());
	}
	

	public void setList(ArrayList<ApplicationBean> arrSearched) {
		this.arrSearched = arrSearched;
		notifyDataSetChanged();
	}

	public ArrayList<ApplicationBean> getList() {
		return this.arrSearched;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrSearched.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_my_app_share, null);
			holder.la_root = (RelativeLayout) view.findViewById(R.id.root);
			holder.newapp_img_logo = (ImageView) view.findViewById(R.id.newapp_img_logo);
			holder.newapp_text_title = (TextView) view.findViewById(R.id.newapp_text_title);
			holder.checkBox1 = (CheckBox) view.findViewById(R.id.checkBox1);
		
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

		holder.newapp_img_logo.setTag(position);
		holder.newapp_text_title.setTag(position);
		holder.checkBox1.setTag(position);
	    
		if (type.equals(CommunityFragment.TAG)) {
			holder.checkBox1.setVisibility(View.GONE);
		}

		ApplicationBean applicationBean = null;
		applicationBean = arrSearched.get(position);
		view.setTag(R.id.link, applicationBean.getPackageName());

		holder.newapp_text_title.setText(applicationBean.getTitle());
		holder.newapp_img_logo.setBackgroundDrawable(applicationBean.getApplogo(fragment.getActivity()));
		
		holder.checkBox1.setChecked(applicationBean.isInstalled());
		

		holder.checkBox1.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {				
				arrSearched.get((Integer)buttonView.getTag()).setInstalled(isChecked);
				buttonView.setButtonDrawable(R.drawable.check_box_selector);
				//holder.checkBox1.setButtonDrawable(R.drawable.check_box_selector);
			}
		});
		
		return view;
	}

	static class Holder {
		public RelativeLayout la_root = null;
		ImageView newapp_img_logo = null;
		TextView newapp_text_title = null;
		CheckBox checkBox1 = null;

	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected void publishResults(CharSequence constraint,	FilterResults results) {
				arrSearched = (ArrayList<ApplicationBean>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				final FilterResults oReturn = new FilterResults();
				final ArrayList<ApplicationBean> results = new ArrayList<ApplicationBean>();
				if (orig == null)
					orig = arrSearched;
				if (constraint != null) {
					if (orig != null && orig.size() > 0) {
						for (final ApplicationBean g : orig) {
							if (g.getTitle().toLowerCase().contains(constraint.toString()))
								results.add(g);
						}
					}
					oReturn.values = results;
				}
				return oReturn;
			}
		};
	}
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}	

}
