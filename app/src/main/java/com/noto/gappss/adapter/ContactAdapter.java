package com.noto.gappss.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.beanClasses.Contact;
import com.noto.gappss.customView.SectionedBaseAdapter;
import com.noto.gappss.networkTask.ApiManager;

public class ContactAdapter extends SectionedBaseAdapter {

	private BaseFragment fragment;
	private HashMap<Integer, ArrayList<Contact>> arrlist;
	private LayoutInflater inflater;
	private Holder holder;
	private ArrayList<Integer> headers;

	public ContactAdapter(BaseFragment fragment, ArrayList<Integer> headers, HashMap<Integer, ArrayList<Contact>> array_list) {
		this.fragment = fragment;
		this.headers = headers;
		this.arrlist = array_list;		

		inflater = LayoutInflater.from(fragment.getActivity());
		//width = (MyApplication.getApplication().getWidthPixel() / 3) - 40;
	}

	@Override
	public Object getItem(int section, int position) {
		// TODO Auto-generated method stub
		return arrlist.get(headers.get(section)).get(position);
	}

	@Override
	public long getItemId(int section, int position) {
		return arrlist.get(headers.get(section)).get(position).getType();
	}

	@Override
	public int getSectionCount() {
		return headers.size();
	}

	@Override
	public int getCountForSection(int section) {
		try {
			return arrlist.get(headers.get(section)).size();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public View getItemView(final int section, int position, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_contact, null);

			holder.invite_title = (TextView) view.findViewById(R.id.invite_title);
			holder.tv_status = (TextView) view.findViewById(R.id.tv_status);
			holder.tv_deny = (TextView) view.findViewById(R.id.tv_deny);
			holder.invite_no = (TextView) view.findViewById(R.id.invite_no);
			holder.invite_checkbox = (CheckBox) view.findViewById(R.id.invite_checkbox);
			holder.message = (TextView) view.findViewById(R.id.message);

			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();		
		}

		Contact contact = arrlist.get(headers.get(section)).get(position);		
		view.setTag(R.id.invite_title, contact);
		view.setTag(R.id.invite_checkbox,position);
		holder.invite_title.setTag(position);
		holder.invite_no.setTag(position);
		holder.invite_checkbox.setTag(position);
		holder.invite_checkbox.setTag(R.id.home, headers.get(section));
		holder.tv_status.setTag(position);
		holder.tv_status.setTag(R.string.username, contact.getId());
		holder.tv_deny.setTag(position);
		holder.tv_deny.setTag(R.string.username, contact.getId());
		holder.message.setTag(position);


		holder.tv_status.setOnClickListener(null);
		holder.tv_deny.setOnClickListener(null);

		switch (contact.getType()) {
		case Constants.sentRequest:
			holder.tv_status.setText(Constants.waitingResponse);
			holder.tv_status.setVisibility(View.VISIBLE);
			holder.invite_checkbox.setVisibility(View.GONE);
			holder.tv_deny.setVisibility(View.GONE);
			break;
		case Constants.receivedRequest:			
			holder.tv_status.setText(Constants.allow);
			holder.tv_status.setVisibility(View.VISIBLE);
			holder.invite_checkbox.setVisibility(View.GONE);
			holder.tv_deny.setVisibility(View.VISIBLE);

			holder.tv_status.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Integer pos = (Integer) v.getTag();
					Contact contact = arrlist.get(headers.get(section)).get(pos);
					contact.setChecked(true);
					String senderId = (String) v.getTag(R.string.username);
					ApiManager.getInstance().acceptDenyInvitation(fragment, senderId, Constants.accept, Constants.acceptInvitation); 
				}
			});

			holder.tv_deny.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Integer pos = (Integer) v.getTag();
					Contact contact = arrlist.get(headers.get(section)).get(pos);
					contact.setChecked(true);
					String senderId = (String) v.getTag(R.string.username);
					ApiManager.getInstance().acceptDenyInvitation(fragment, senderId, Constants.deny, Constants.denyInvitation);
				}
			});
			break;
		case Constants.noFriend:
			//holder.tv_status.setText(Constants.noFriendStr);
			holder.tv_status.setVisibility(View.GONE);
			holder.tv_deny.setVisibility(View.GONE);
			holder.invite_checkbox.setVisibility(View.VISIBLE);
			break;
		case Constants.friend:
			//holder.tv_status.setText(Constants.friendStr);
			holder.invite_checkbox.setVisibility(View.GONE);
			holder.tv_status.setVisibility(View.GONE);
			holder.tv_deny.setVisibility(View.GONE);
			break;
		} 
		holder.invite_title.setText(contact.getName());
		holder.invite_no.setText(contact.getMobileNo());
		holder.invite_checkbox.setChecked(contact.isChecked());

		holder.invite_checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				int pos = (Integer)buttonView.getTag();
				int section = (Integer)buttonView.getTag(R.id.home);
				arrlist.get(section).get(pos).setChecked(isChecked);
			}
		});
		return view;
	}

	@Override
	public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
		LinearLayout layout = null;
		if (convertView == null) {
			LayoutInflater inflator = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			layout = (LinearLayout) inflator.inflate(R.layout.header_item, null);
		} else {
			layout = (LinearLayout) convertView;
		}
		//((TextView) layout.findViewById(R.id.textItem)).setText("Header for section " + section);
		String userTypeStr = "";
		switch (headers.get(section)) {
		case Constants.sentRequest:
			userTypeStr = Constants.sentRequestStr;
			break;
		case 1:
			userTypeStr = Constants.receivedReqFriendStr;
			break;
		case 2:
			userTypeStr = Constants.friendStr;
			break;
		case 3:
			userTypeStr = Constants.noFriendStr;
			break;
		}
		((TextView) layout.findViewById(R.id.textItem)).setText(userTypeStr);
		return layout;
	}

	static class Holder {
		public TextView tv_deny;
		public TextView tv_status;
		TextView invite_title = null;
		TextView invite_no = null;
		TextView message = null;
		CheckBox invite_checkbox = null;
	}

	public ArrayList<Contact> getNoFriendList() {
		return arrlist.get(Constants.noFriend);
	}

	public ArrayList<Contact> getFriendList(int requestType) {
		if(!headers.contains(requestType)){
			headers.add(requestType);
			arrlist.put(requestType, new ArrayList<Contact>());
		}
		return arrlist.get(requestType);
	}
	public void chekNoFriend(int requestType, int pos){
		arrlist.get(requestType).get(pos).setChecked(true);
	}
}
