package com.noto.gappss.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.beanClasses.AppCategory;
import com.squareup.picasso.Picasso;

public class AllCategoryAdapter extends BaseAdapter {

		private LayoutInflater inflater;
		private BaseFragment fragment;
		private ArrayList<AppCategory> arrlist;
		private Holder holder;
		private int width;

		public AllCategoryAdapter() {
			// TODO Auto-generated constructor stub
		}

		public AllCategoryAdapter(BaseFragment fragment,
				ArrayList<AppCategory> arrlist) {

			this.fragment = fragment;
			this.arrlist = arrlist;
			inflater = LayoutInflater.from(fragment.getActivity());
			width = (MyApplication.getApplication().getWidthPixel() / 3) - 40;

		}

		public void setList(ArrayList<AppCategory> arrSearched) {
			this.arrlist = arrSearched;
		}

		public ArrayList<AppCategory> getList() {
			return this.arrlist;
		}

		@Override
		public int getCount() {
			return arrlist.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressLint("NewApi")
		@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
		@Override
		public View getView(int postion, View view, ViewGroup parent) {
			holder = new Holder();
			if (view == null) {
				view = inflater.inflate(R.layout.item_all_category, null);
				holder.img_logo = (ImageView) view.findViewById(R.id.img_logo);
				holder.text_title = (TextView) view.findViewById(R.id.text_title);
				
				view.setTag(holder);
			} else {
				holder = (Holder) view.getTag();
			}

			holder.img_logo.setTag(postion);
			holder.text_title.setTag(postion);
			
			holder.text_title.setText(arrlist.get(postion).getCatName());
			//holder.checkBox1.setButtonDrawable(R.drawable.check_box_selector);
			if(!arrlist.get(postion).getCatLogo().isEmpty())
				Picasso.with(fragment.getActivity()).load(arrlist.get(postion).getCatLogo()).placeholder(R.drawable.noimg).into(holder.img_logo);
			
			
			
			

			return view;
		}

		static class Holder {
			ImageView img_logo = null;
			TextView text_title = null;
		}
	}
