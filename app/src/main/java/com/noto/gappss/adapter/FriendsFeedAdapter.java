package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;

public class FriendsFeedAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	private BaseFragment fragment;

	private Holder holder;
	private int width;
	private JSONArray arrlist;
	private int rememberPosition;

	public FriendsFeedAdapter() {
		// TODO Auto-generated constructor stub
	}

	public FriendsFeedAdapter(BaseFragment fragment,  JSONArray arrlist) {
		this.fragment = fragment;
		try {
			//this.arrlist = arrlist.optJSONObject(0).optJSONArray(Constants.list);
			this.arrlist = arrlist;
		} catch (Exception e) {			
			e.printStackTrace();
			this.arrlist = new JSONArray();
		}
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;
	}

	public void setList(JSONArray arrSearched) {
		this.arrlist = arrSearched;
	}

	public JSONArray getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.length();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_friend_feeds, null);
			
			holder.user = (TextView) view.findViewById(R.id.username);
			holder.message = (TextView) view.findViewById(R.id.message);
			
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

		
		JSONObject jO = arrlist.optJSONObject(postion);
		String user = jO.optString(Constants.USER_NAME);
		String message = jO.optString(Constants.p_message);
		
		holder.user.setTag(postion);
		holder.message.setTag(postion);
		
		holder.user.setText(Html.fromHtml("@"+user));
		holder.message.setText(Html.fromHtml(message));
		
		//view.setTag(R.id.text_channel, jO);
		
		return view;
	}

	static class Holder {
		TextView user = null;
		TextView message = null;
	}
	public void refreshList(){
		JSONArray array = new JSONArray();
		for (int i = 0; i < arrlist.length(); i++) {
			if (i != rememberPosition) {
				array.put(arrlist.optJSONObject(i));
			}
			
		}
		arrlist = array;
		notifyDataSetChanged();
	}

}
