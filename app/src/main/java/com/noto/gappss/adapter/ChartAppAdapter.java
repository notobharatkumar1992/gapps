package com.noto.gappss.adapter;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.utilities.ToastCustomClass;
import com.squareup.picasso.Picasso;

public class ChartAppAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	private BaseFragment fragment;

	private Holder holder;
	private int width;
	private JSONArray arrlist;
	private int rememberPosition;

	public ChartAppAdapter() {
		// TODO Auto-generated constructor stub
	}

	public ChartAppAdapter(BaseFragment fragment,  JSONArray arrlist) {
		this.fragment = fragment;
		try {
			//this.arrlist = arrlist.optJSONObject(0).optJSONArray(Constants.list);
			this.arrlist = arrlist;
		} catch (Exception e) {			
			e.printStackTrace();
			this.arrlist = new JSONArray();
		}
		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 30;
	}

	public void setList(JSONArray arrSearched) {
		this.arrlist = arrSearched;
		notifyDataSetChanged();
	}

	public JSONArray getList() {
		return this.arrlist;
	}

	@Override
	public int getCount() {
		return arrlist.length();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_chart_app, null);
			holder.img = (ImageView) view.findViewById(R.id.img);
			holder.title = (TextView) view.findViewById(R.id.title);
			holder.description = (TextView) view.findViewById(R.id.description);
			holder.link = (TextView) view.findViewById(R.id.link);
		
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

	/*	AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, width+20);
		view.setLayoutParams(params);*/
		
		JSONObject jO = arrlist.optJSONObject(postion);
		String channelId = jO.optString(Constants.appId);
		String title = jO.optString(Constants.appName);
		String description = jO.optString(Constants.description);
		String image = jO.optString(Constants.icon);
		String appPack = jO.optString(Constants.appPackageName);
		
		
		//JSONArray array = jO.optJSONArray(Constants.Comment);
		
		holder.img.setTag(postion);
		holder.title.setTag(postion);
		holder.description.setTag(postion);
		holder.link.setTag(appPack);
		
		holder.title.setText(Html.fromHtml(title));
		holder.description.setText(Html.fromHtml(description));
		
		view.setTag(R.id.link, appPack);
		if(image != null && !image.isEmpty())
		{	
			Picasso.with(fragment.getActivity()).load(image).placeholder(R.drawable.noimg).into(holder.img);
		}/*else {
			//test
			Picasso.with(fragment.getActivity()).load("https://lh4.ggpht.com//oN-F5q6EIU5KTAX-bfesgXHTeFEnoO-mfRoYuL8Dk2Swu7kKKjODCXGsCWfgy9nwYXne=w512").placeholder(R.drawable.noimg).into(holder.img);
		}*/		
		
		holder.link.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String link = (String) v.getTag();
				//ToastCustomClass.showToast(fragment.getActivity(), link);
				
				//String url = "https://play.google.com/store/apps/details?id=com.app.ip.bmt";
				   Intent i = new Intent(Intent.ACTION_VIEW);
				   i.setData(Uri.parse(URLsClass.googleAppUrl+link));
				   fragment.getActivity().startActivity(i);
				
			}
		});

		
		return view;
	}

	static class Holder {
		RelativeLayout comment_rel = null;
		ImageView img = null;
		TextView title = null;
		TextView description = null;
		TextView link = null;
	
	}
	public void refreshList(){
		JSONArray array = new JSONArray();
		for (int i = 0; i < arrlist.length(); i++) {
			if (i != rememberPosition) {
				array.put(arrlist.optJSONObject(i));
			}
			
		}
		arrlist = array;
		notifyDataSetChanged();
	}

	
}
