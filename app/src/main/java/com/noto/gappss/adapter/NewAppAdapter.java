package com.noto.gappss.adapter;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noto.gappss.MyApplication;
import com.noto.gappss.R;
import com.noto.gappss.baseClasses.BaseFragment;
import com.noto.gappss.beanClasses.ApplicationBean;
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)

public class NewAppAdapter extends BaseAdapter implements Filterable{

	private LayoutInflater inflater;
	private Holder holder;
	private ArrayList<ApplicationBean> arrSearched;
	private ArrayList<ApplicationBean> orig;
	private BaseFragment fragment;
	private int width;
	private int data = 0; // 0 for user app's and 1 for friend app's 

	public NewAppAdapter() {
	}

	public NewAppAdapter(BaseFragment fragment, ArrayList<ApplicationBean> arrlist) {
		this.fragment = fragment;
		this.arrSearched = arrlist;

		inflater = LayoutInflater.from(fragment.getActivity());
		width = (MyApplication.getApplication().getWidthPixel() / 3) - 40;
		//Log.i("LaVoixAdapter", "==" + arrSearched.size());
	}
	public void setAdapterDataState(int i){
		data = i;
	}

	public void setList(ArrayList<ApplicationBean> arrSearched) {
		this.arrSearched = arrSearched;
		notifyDataSetChanged();
	}

	public ArrayList<ApplicationBean> getList() {
		return this.arrSearched;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrSearched.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		holder = new Holder();
		if (view == null) {
			view = inflater.inflate(R.layout.item_new_app, null);
			holder.la_root = (RelativeLayout) view.findViewById(R.id.root);
			holder.newapp_img_logo = (ImageView) view.findViewById(R.id.newapp_img_logo);
			holder.newapp_text_title = (TextView) view.findViewById(R.id.newapp_text_title);
			holder.newapp_btn_public = (TextView) view.findViewById(R.id.newapp_btn_public);

			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}

		holder.newapp_img_logo.setTag(position);
		holder.newapp_text_title.setTag(position);
		holder.newapp_btn_public.setTag(position);
		holder.newapp_btn_public.setVisibility(View.VISIBLE);

		ApplicationBean applicationBean = null;
		applicationBean = arrSearched.get(position);		

		holder.newapp_text_title.setText(applicationBean.getTitle());
		holder.newapp_img_logo.setBackground(applicationBean.getApplogo(fragment.getActivity()));
		if (data == 0) {
			if(!applicationBean.isInstalled()){
				holder.la_root.setEnabled(false);
				holder.newapp_btn_public.setEnabled(false);
				applicationBean.setStatus(3);
				arrSearched.set(position, applicationBean);
			}else{
				applicationBean.setStatus(applicationBean.getStatus());
				arrSearched.set(position, applicationBean);
				holder.la_root.setEnabled(true);
				holder.newapp_btn_public.setEnabled(true);
			}

			if (applicationBean.getStatus() == 1) {
				holder.newapp_btn_public.setBackground(fragment.getResources().getDrawable(R.drawable.btn_public));
				holder.newapp_btn_public.setText(fragment.getResources().getString(R.string.publics));
			}else if (applicationBean.getStatus() == 3){
				holder.newapp_btn_public.setBackground(fragment.getResources().getDrawable(R.drawable.btn_private));
				holder.newapp_btn_public.setText(fragment.getResources().getString(R.string.uninstalled));
			} else{
				holder.newapp_btn_public.setBackground(fragment.getResources().getDrawable(R.drawable.btn_private));
				holder.newapp_btn_public.setText(fragment.getResources().getString(R.string.privates));
			}

			holder.newapp_btn_public.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(final View v) {
					//ToastCustomClass.showToast(fragment.getActivity(), "click");
					final TextView textView = (TextView)v;
					if (arrSearched.get((Integer)v.getTag()).getStatus() == 2) {
						AlertDialog.Builder alertBuilder = new AlertDialog.Builder(fragment.getActivity());
						alertBuilder.setTitle("");
						alertBuilder.setMessage("Are you sure you want to make this app available in your community?");
						alertBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								textView.setBackground(fragment.getResources().getDrawable(R.drawable.btn_public));
								textView.setText(fragment.getResources().getString(R.string.publics));
								arrSearched.get((Integer)v.getTag()).setStatus(1);				
							}
						});
						alertBuilder.setNegativeButton("Cancel", null);

						AlertDialog alertDialog = alertBuilder.create();
						alertDialog.show();
					}else {
						textView.setBackground(fragment.getResources().getDrawable(R.drawable.btn_private));
						textView.setText(fragment.getResources().getString(R.string.privates));
						arrSearched.get((Integer)v.getTag()).setStatus(2);
					}
				}
			});


		}else {
			holder.newapp_btn_public.setVisibility(View.INVISIBLE);
		}		

		return view;
	}

	static class Holder {
		public RelativeLayout la_root = null;
		ImageView newapp_img_logo = null;
		TextView newapp_text_title = null;
		TextView newapp_btn_public = null;

	}

	@Override
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected void publishResults(CharSequence constraint,	FilterResults results) {
				arrSearched = (ArrayList<ApplicationBean>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				final FilterResults oReturn = new FilterResults();
				final ArrayList<ApplicationBean> results = new ArrayList<ApplicationBean>();
				if (orig == null)
					orig = arrSearched;
				if (constraint != null) {
					if (orig != null && orig.size() > 0) {
						for (final ApplicationBean g : orig) {
							if (g.getTitle().toLowerCase().contains(constraint.toString()))
								results.add(g);
						}
					}
					oReturn.values = results;
				}
				return oReturn;
			}
		};
	}
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}	
}
