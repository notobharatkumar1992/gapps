package com.noto.gappss;

import java.util.Arrays;
import java.util.HashMap;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.noto.gappss.baseClasses.BaseFragmentActivity;
import com.noto.gappss.baseClasses.Constants;
import com.noto.gappss.networkTask.URLsClass;
import com.noto.gappss.preference.MySharedPreferences;
import com.noto.gappss.utilities.ToastCustomClass;
import com.noto.gappss.utilities.UtilsClass;

public class LoginActivity extends BaseFragmentActivity implements OnClickListener {

	private ImageView fb_login;
	private EditText login_username;
	private EditText login_password;
	private Button button_login;
	private TextView login_forgotpass;
	private EditText edit_emailad;
	private CallbackManager callbackManager;
	AlertDialog alert;
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		// getSupportActionBar().hide();
		enableBackButton();
		setTitleOnAction(getResources().getString(R.string.login_upercase));
		setContentView(R.layout.activity_login);
		initControls(savedInstanceState);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		fb_login = (ImageView) findViewById(R.id.fb_login);
		login_username = (EditText) findViewById(R.id.login_username);
		login_password = (EditText) findViewById(R.id.login_password);
		button_login = (Button) findViewById(R.id.button_login);
		login_forgotpass =  (TextView) findViewById(R.id.login_forgotpass);

		login_username.setText(MySharedPreferences.getInstance().getString(this, Constants.PH_NUMBER, ""));
		//login_username.setEnabled(false);
		fb_login.setOnClickListener(this);
		button_login.setOnClickListener(this);
		login_forgotpass.setOnClickListener(this);

		//Log.i("Android Device model", UtilsClass.getDeviceModel(this));
		//Log.i("Android id", UtilsClass.getAndroidI(this));
		//Log.i("Android device id", UtilsClass.getAndroidDevic(this));

		//Log.i("Android Device VERSION ", UtilsClass.getDeviceSDKVersion(this));
		//Log.i("Android Device VERSION Name", UtilsClass.getDeviceSDKVersionName(this));
		String text = "<font color='#142D8A'>"+"<a href=''>"+"Forgot your password?</a></font>";
		login_forgotpass.setText(Html.fromHtml(text));
	}

	@Override
	protected void setValueOnUI() {

	}

	@Override
	public void onBackPressed() {
		startMyActivity(LoginSignupActivity.class, null);
		super.onBackPressed();		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action buttons
		switch (item.getItemId()) {
		case android.R.id.home:		
			onBackPressed();
			return true;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.fb_login:
			fbLogin();
			break;
		case R.id.button_login:
			if (login_username.getText().toString().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_ph_username_not_null));
			}else if (login_password.getText().toString().trim().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_password_not_null));
			}else if (UtilsClass.isConnectingToInternet(this)) {				
				String ph_no = login_username.getText().toString().trim();
				int length = ph_no.length();
				if(length == 9)
					ph_no = "0"+ph_no;
				length = ph_no.length();
				//ph_no = ph_no.replaceFirst("^0+(?!$)", "");
				if (length >= 10) {	
					String[] url = {URLsClass.baseUrl+URLsClass.service_type_login};
					HashMap<String, Object> params = new HashMap<String, Object>();
					params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
					params.put(Constants.p_mobile_no, login_username.getText().toString().trim());
					params.put(Constants.password, login_password.getText().toString().trim());

					if(ph_no.equals("9602487039")){
						params.put(Constants.deviceId, "358240054993209");
						//params.put(Constants.deviceId, "911356500030832");
					}else if(ph_no.equals("0123456789")){
						params.put(Constants.deviceId, "358240054993209");	
					}else if(ph_no.equals("9929572110")){
						params.put(Constants.deviceId, "911328854704965");	
					}else if(ph_no.equals("8302510544")){
						//local user
						params.put(Constants.deviceId, "C102MS15MS5");	
					}
					else{
						params.put(Constants.deviceId, UtilsClass.getAndroidDevic(this));
					}

					params.put(Constants.p_device_brand, UtilsClass.getDeviceModel(this));
					params.put(Constants.p_device_type, Constants.android);
					params.put(Constants.p_device_os, UtilsClass.getDeviceSDKVersion(this));

					params.put(Constants.type,Constants.login_type_normal);
					serviceCaller(LoginActivity.this, url, params, true, false, Constants.login, true);
				}
			}else {    
				UtilsClass.plsStartInternet(this);
			}
			break;
		case R.id.login_forgotpass:
			forgotPasswordDialog();
			break;
		case R.id.btn_cancel:
			alert.cancel();

			break;
		case R.id.btn_request:
			if (edit_emailad.getText().toString().equalsIgnoreCase("")) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_email_not_null));
			}else if (!UtilsClass.isEmailValid(edit_emailad.getText().toString().trim())) {
				ToastCustomClass.showToast(this, getResources().getString(R.string.val_email_valid));
			}else if (UtilsClass.isConnectingToInternet(this)) {
				String[] url = {URLsClass.baseUrl+URLsClass.service_type_forget_pass};
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
				params.put(Constants.EMAIL, edit_emailad.getText().toString().trim());
				serviceCaller(LoginActivity.this, url, params, true, false, Constants.forgetPass, true);
			}else {
				UtilsClass.plsStartInternet(this);
			}
			break;
		}
	}


	@Override
	protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
		if(super.callBackFromApi(object, act, requstCode)){
			JSONObject jsonObject = (JSONObject) object;
			int dataFlow = jsonObject.optInt(Constants.dataToFollow);
			String statusText = jsonObject.optString(Constants.p_message);
			if(dataFlow == 1){
				JSONObject response = jsonObject.optJSONObject(Constants.p_user);
				switch (requstCode) {
				case Constants.login:
					//{"response":{"status":true,"userId":"51","email":"vjain@mailinator.com","username":"vjain",
					//"dob":"0000-00-00","gender":"1","type":"1","statusMessage":"User login successfully."}}
					Log.i("Login", jsonObject.toString());					
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.LOGIN_TYPE, response.optString(Constants.type));
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.USER_ID, response.optString(Constants.USER_ID));
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.EMAIL, response.optString(Constants.EMAIL));
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.USER_NAME, response.optString(Constants.USER_NAME));
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.DOB, response.optString(Constants.DOB));
					MySharedPreferences.getInstance().putStringKeyValue(this, Constants.PH_NUMBER, response.optString(Constants.PH_NUMBER));
					MySharedPreferences.getInstance().putBooleanKeyValue(this, Constants.LOGIN, true);

					if (response.optInt(Constants.GENDER) == 1) {
						MySharedPreferences.getInstance().putStringKeyValue(this, Constants.GENDER, Constants.MALE);
					}else{
						MySharedPreferences.getInstance().putStringKeyValue(this, Constants.GENDER, Constants.FEMALE);
					}

					startMyActivity(AddCategoryActivity.class, null);
					finish();
					break;
				case Constants.forgetPass:
					//{"response":{"status":true,"statusMessage":"Please check your email for reset Password!"}}
					//{"response":{"status":0,"message":"This email does not exist."},"dataflow":0}

					alert.cancel();
					UtilsClass.showAlertDialog(this, statusText, "Message");
					break;
				}
			}else{
				UtilsClass.showAlertDialog(this, statusText, "Message");
			}
		}
		return true;
	}

	public void fbLogin(){
		callbackManager = CallbackManager.Factory.create();
		// Set permissions 
		LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email","public_profile"));
		LoginManager.getInstance().registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult loginResult) {

				//System.out.println("Success");
				//Log.i("ACCESS TOKEN1", "="+loginResult.getAccessToken().getToken());

				//Log.i("ACCESS TOKEN3", "="+loginResult.getAccessToken().getPermissions());
				GraphRequest request =  GraphRequest.newMeRequest(
						loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
							@Override
							public void onCompleted(JSONObject object, GraphResponse response) {
								if (response.getError() != null) {
									// handle error
									Log.d("ERROR","ERROR");
								} else {
									//Log.d("SUCCESS","");
									//Log.i("JSONOBJECT", object.toString());
									//Log.i("GRAPH RESPONSE", response.toString());
									//{"id":"1640502796227231","name":"Leander Daniel","first_name":"Leander","last_name":"Daniel","email":"leander.daniel1027@gmail.com","gender":"female"}
									if(object != null && object.optString("id") != null){
										//Log.d("object","object");
										String[] url = {URLsClass.baseUrl+URLsClass.service_type_login};
										HashMap<String, Object> params = new HashMap<String, Object>();
										//params.put(Constants.p_password, object.optString("id"));
										params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
										params.put(Constants.p_fb_id, object.optString("id"));
										params.put(Constants.p_mobile_no,  MySharedPreferences.getInstance().getString(LoginActivity.this, Constants.PH_NUMBER, ""));
										params.put(Constants.type,Constants.login_type_fb);

										params.put(Constants.deviceId, UtilsClass.getAndroidDevic(LoginActivity.this));
										params.put(Constants.p_device_brand, UtilsClass.getDeviceModel(LoginActivity.this));
										params.put(Constants.p_device_type, Constants.android);
										params.put(Constants.p_device_os, UtilsClass.getDeviceSDKVersion(LoginActivity.this));

										serviceCaller(LoginActivity.this, url, params, true, false, Constants.login, true);
									}
								}
							}
						});
				Bundle parameters = new Bundle();
				// parameters.putString("fields", "id,name,first_name,last_name,email,birthday,gender,link,location");
				parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday,location");
				request.setParameters(parameters);
				request.executeAsync();
			}

			@Override
			public void onCancel() {
				Log.d("oncancel","On cancel");
				LoginManager.getInstance().logOut();
			}

			@Override
			public void onError(FacebookException error) { 
				Log.d("onerror",error.toString());
				LoginManager.getInstance().logOut();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//Facebook
		if(callbackManager != null)
			callbackManager.onActivityResult(requestCode, resultCode, data);  
	}

	public void forgotPasswordDialog(){


		AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
		LayoutInflater inflater = getLayoutInflater();

		View view = inflater.inflate(R.layout.forget_password_dialog, null);
		builder.setView(view);

		edit_emailad = (EditText) view.findViewById(R.id.edit_emailad);
		TextView btn_cancel = (TextView) view.findViewById(R.id.btn_cancel);
		TextView btn_request = (TextView) view.findViewById(R.id.btn_request);

		btn_cancel.setOnClickListener(this);
		btn_request.setOnClickListener(this);

		alert = builder.create();
		alert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		alert.show();
		/*WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(alert.getWindow().getAttributes());
		lp.gravity = Gravity.BOTTOM;
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		alert.show();
		alert.getWindow().setAttributes(lp);
		alert.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));*/

	}
}
